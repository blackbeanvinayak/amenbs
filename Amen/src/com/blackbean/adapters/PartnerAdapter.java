package com.blackbean.adapters;

import java.util.ArrayList;

import com.blackbean.eventbuoy2.amen.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PartnerAdapter extends BaseAdapter
{
	Context context;
	ArrayList<String> object;
	
	public PartnerAdapter(Context context,ArrayList<String> object) {
		// TODO Auto-generated constructor stub
		this.context=context;
		this.object=object;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return object.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view=inflater.inflate(R.layout.parnerlist_single_view, null);
		TextView textview=(TextView)view.findViewById(R.id.singletext);
		textview.setText(object.get(arg0));
		
		
		return view;
	}
	

}
