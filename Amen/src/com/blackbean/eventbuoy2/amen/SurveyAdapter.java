package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;

import com.blackbean.eventbuoy2.amen.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SurveyAdapter extends ArrayAdapter<String>{
ArrayList<String> surveyArrayList;
Context context;
	public SurveyAdapter(Context context, int resource,ArrayList<String> surveytitleaArrayList) {
		super(context, R.layout.survey_adapter,surveytitleaArrayList);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.surveyArrayList=surveytitleaArrayList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view=inflater.inflate(R.layout.survey_adapter, parent,false);
		TextView title=(TextView)view.findViewById(R.id.txt_survey_title);
		title.setText(surveyArrayList.get(position));
		return view;
	}

}
