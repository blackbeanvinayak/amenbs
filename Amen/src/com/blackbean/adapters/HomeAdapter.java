package com.blackbean.adapters;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;


/**
 * Set Home custom Listview with image and name
 * @author rahul
 *
 */
public class HomeAdapter extends ArrayAdapter<String> {
Context context;
ArrayList<String> pagenameArrayList;
ArrayList<Integer> pageicon;
private static final String H2="h2";


	public HomeAdapter(Context context, int resource,ArrayList<String> pagename,ArrayList<Integer> pageicon) {
		super(context, resource,pagename);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.pagenameArrayList=pagename;
		this.pageicon=pageicon;
	}

	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView=	inflater.inflate(R.layout.adapter_home, parent,false);
		ImageView imageView=(ImageView)rowView.findViewById(R.id.home_img);
		TextView textView=(TextView)rowView.findViewById(R.id.home_txt);
		 /*File pictureFile = new File(Environment
					.getExternalStorageDirectory().getPath()
					+ "/shrm/menu/" + pageicon.get(position));
			if (pictureFile != null) {
				Bitmap bitmap = BitmapFactory.decodeFile(pictureFile
						.getAbsolutePath());
				if(bitmap!=null)
					imageView.setImageBitmap(bitmap);
			}*/
		imageView.setImageResource(this.pageicon.get(position));
		
		//Typeface mFont = Typeface.createFromAsset(getContext().getAssets(), "quicksand_regular.otf");
		//textView.setTypeface(mFont);
		textView.setText(pagenameArrayList.get(position));
			
		
		return rowView;
	}

}
