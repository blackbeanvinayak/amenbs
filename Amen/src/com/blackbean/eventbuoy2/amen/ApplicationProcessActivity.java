package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.utility.NetworkCheck;

public class ApplicationProcessActivity extends Activity
{
	TextView desc;
	ListView doclist;
	ImageView home;
	ArrayList<String> data,links;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.application_process);
		data=new ArrayList<String>();
		links=new ArrayList<String>();

		data.add("Download SHRM Declaration Form");
		data.add("Excellence in HR Analytics");
		data.add("Excellence in Diversity and Inclusion");
		data.add("Excellence in Social Media People Campaign");
		data.add("Excellence in Workplace Flexibility");
		data.add("Excellence in Community Impact");
		data.add("Excellence in Developing the Leaders of Tomorrow");
		data.add("Employer with Best Employee Health and Wellness Initiatives");
		data.add("Academic Institute of the Year (For Contribution in the field of HR)");
		data.add("Excellence in Human Resource - South Asia (excluding India) & Middle East");

		links.add("http://annualindia.shrmindia.org/docs/SHRM_Declaration_Form.docx");
		links.add("http://annualindia.shrmindia.org/docs/forms/Excellence%20in%20HR%20Analytics-%20Application%20Form.docx");
		links.add("http://annualindia.shrmindia.org/docs/forms/Excellence%20in%20Diversity%20&%20Inclusion-%20Application%20Form.docx");
		links.add("http://annualindia.shrmindia.org/docs/forms/Excellence%20in%20Social%20Media%20Campaign-%20Application%20Form.docx");
		links.add("http://annualindia.shrmindia.org/docs/forms/Excellence%20in%20Workplace%20Flexibility-Application%20Form.docx");
		links.add("http://annualindia.shrmindia.org/docs/forms/Excellence%20in%20Community%20Impact%20-Application%20Form.docx");
		links.add("http://annualindia.shrmindia.org/docs/forms/Excellence%20in%20Developing%20Leaders%20of%20Tomorrow-%20Application%20Form.docx");
		links.add("http://annualindia.shrmindia.org/docs/forms/Employer%20with%20best%20Health%20and%20Wellness%20Initiatives%20-Application%20Form.docx");
		links.add("http://annualindia.shrmindia.org/docs/forms/Academic%20Institute%20of%20the%20year-%20Application%20Form.docx");	
		links.add("http://annualindia.shrmindia.org/docs/forms/Application%20Form-%20HR%20Excellence%20(SA%20&%20ME).docx");



		app_process_adapter adapter=new app_process_adapter(ApplicationProcessActivity.this, data,links);
		doclist=(ListView)findViewById(R.id.applicationlist);
		desc=(TextView)findViewById(R.id.desc);
		home=(ImageView)findViewById(R.id.home);
		desc.setText(Html.fromHtml("Please download the Declaration form as well as the Application forms for the specific Award categories, from the links below. Along with your filled out application forms, please send us the scanned copy of the Declaration form, signed and stamped by the required authority, at <font color='#A5CD39'>SHRMIAwards@shrm.org </font>Also, you can attach 2-3 supporting documents only (in word document/pdf formats), for a particular Award category."));


		doclist.setAdapter(adapter);

		home.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				Intent intent=new Intent(ApplicationProcessActivity.this,HomeActivity.class);
				startActivity(intent);

			}
		});

	}



	class app_process_adapter extends BaseAdapter
	{
		Context context;
		ArrayList<String> data1=new ArrayList<String>();
		ArrayList<String> links=new ArrayList<String>();

		public app_process_adapter(Context context,ArrayList<String> data1,ArrayList<String> links) {
			// TODO Auto-generated constructor stub
			this.context=context;
			this.data1=data1;
			this.links=links;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return data1.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(final int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view=inflater.inflate(R.layout.application_adapter,null);

			TextView text=(TextView)view.findViewById(R.id.txt);
			text.setText(this.data1.get(arg0));
			ImageView img=(ImageView)view.findViewById(R.id.img);
			img.setImageResource(R.drawable.download_white);
			img.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (NetworkCheck.getConnectivityStatusString(context)) {
						try{
							//Toast.makeText(getBaseContext(), "Opening PDF... ", Toast.LENGTH_SHORT).show();
							Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse(links.get(arg0)));
							startActivity(intent);
						}catch(Exception e)
						{
							Toast.makeText(getBaseContext(), "Problem occur during download ", Toast.LENGTH_SHORT).show();
						}
					}else {
						Toast.makeText(context, "Please Check Your Internet Connection",
								Toast.LENGTH_LONG).show();
					}

				}
			});

			return view;
		}

	}

}
