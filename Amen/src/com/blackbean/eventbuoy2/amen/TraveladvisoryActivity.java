package com.blackbean.eventbuoy2.amen;

import com.blackbean.eventbuoy2.amen.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class TraveladvisoryActivity extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.traveladvisory);
		
		TextView net=(TextView) findViewById(R.id.net);
		net.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intentEmail = new Intent(Intent.ACTION_SEND);
				intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@benciaafricaadventure.com"});
				//intentEmail.putExtra(Intent.EXTRA_SUBJECT, "your subject");
				//intentEmail.putExtra(Intent.EXTRA_TEXT, "message body");
				intentEmail.setType("message/rfc822");
				startActivity(Intent.createChooser(intentEmail, "Choose an email provider :"));
			}
		});
		

		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(TraveladvisoryActivity.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();

			}
		});


	}

}
