package com.blackbean.eventbuoy2.amen;

import java.io.File;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.utility.JSONParser;
import com.blackbean.utility.NetworkCheck;
import com.blackbean.utility.SetGet;

public class SurveyActivity extends MainActivity{
	LinearLayout layout_discussion;
	ListView listView;
	SurveyAdapter surveyadapter;
	ArrayList<String> surveyArrayList,id_surveyArrayList;
	JSONParser jsonParser = new JSONParser();
	JSONObject jsonObject = null;
	public static JSONArray response_array_qusetion = null;
	Getquestioninfo getquestioninfo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_servey);

		listView=(ListView)findViewById(R.id.lv_question);

		/*if (NetworkCheck.getConnectivityStatusString(this)) {
			new Getquestioninfo(this).execute();

		}

		else {
			Toast.makeText(this, "Please Check Your Internet Connection",
					Toast.LENGTH_LONG).show();
		}*/




		surveyArrayList=new ArrayList<String>();
		id_surveyArrayList=new ArrayList<String>();

		surveyadapter=new SurveyAdapter(this, 0, surveyArrayList);
		listView.setAdapter(surveyadapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				Intent intent=new Intent(SurveyActivity.this, ServeyOptionSubmitActivity.class);
				intent.putExtra("id", id_surveyArrayList.get(arg2));
				intent.putExtra("question",surveyArrayList.get(arg2));

				startActivity(intent);
				finish();

			}
		});


		// right Image Setting code
		//int position=getIntent().getIntExtra("POSITION", 0);
		String imageBlack=getIntent().getStringExtra("ImageName");
		ImageView icon=(ImageView) findViewById(R.id.icon);
		File pictureFile = new File(Environment
				.getExternalStorageDirectory().getPath()
				+ "/shrm/menu/" + imageBlack);
		if (pictureFile != null) {
			Bitmap bitmap = BitmapFactory.decodeFile(pictureFile
					.getAbsolutePath());
			if(bitmap!=null)
				icon.setImageBitmap(bitmap);
		}
		//icon.setColorFilter(Color.argb(255, 0, 0, 0));

		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SurveyActivity.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();

			}
		});




	}
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		super.onPrepareOptionsMenu(menu);

		return true;
	}
	public class Getquestioninfo extends AsyncTask<String, String, String> {
		Dialog dialog;
		Context context;
		public Getquestioninfo(Context context) {
			// TODO Auto-generated constructor stub
			this.context=context;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new Dialog(context);
			ProgressBar bar = new ProgressBar(context);
			dialog.setContentView(bar);
			dialog.setTitle("Please Wait...");
			dialog.show();

		}



		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			//http://listoftablets.com/SHRM/admin/GetSurveyQuestions.php

			try {
				jsonObject = jsonParser
						.getJSONFromUrl("http://listoftablets.com/SHRM/admin/GetSurveyQuestions.php");
				if (jsonObject != null) {
					response_array_qusetion = jsonObject
							.getJSONArray("survey_question");
					for (int i = 0; i < response_array_qusetion.length(); i++) {
						JSONObject obj_temp = new JSONObject();
						obj_temp = response_array_qusetion.getJSONObject(i);
						id_surveyArrayList.add(obj_temp.getString("survey_question_id"));
						surveyArrayList.add(obj_temp
								.getString("survey_question_title"));
					}
				}
			} catch (Exception e) {
				System.out.println("error : " + e);

			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
			surveyadapter.notifyDataSetChanged();
		}

	}
}
