package com.blackbean.adapters;

import java.util.ArrayList;

import com.blackbean.eventbuoy2.amen.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class NoteAdapter extends BaseAdapter{

	Context context;
	ArrayList<NoteModel>rowItems;
	
	public NoteAdapter(Context context,ArrayList<NoteModel>rowItems) {
		// TODO Auto-generated constructor stub
		this.context=context;
		this.rowItems=rowItems;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return rowItems.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return rowItems.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return rowItems.indexOf(rowItems.get(arg0));
	}

	private static class ViewHolder
	{
		TextView textViewNoteInfo,textViewNoteContent;
	}
	
	@Override
	public View getView(int arg0, View convertView, ViewGroup arg2) {
		// TODO Auto-generated method stub
		
		ViewHolder holder;
		LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if (convertView==null) {
			convertView=inflater.inflate(R.layout.note_row, null);
			holder=new ViewHolder();
			
			holder.textViewNoteContent=(TextView) convertView.findViewById(R.id.textViewNoteContent);
			holder.textViewNoteInfo=(TextView) convertView.findViewById(R.id.textViewNoteInfo);
			
			convertView.setTag(holder);
		}
		else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.textViewNoteContent.setText(rowItems.get(arg0).getNote_content());
		holder.textViewNoteInfo.setText(rowItems.get(arg0).getNote_info());
		
		return convertView;
	}

}
