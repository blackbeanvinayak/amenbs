package com.blackbean.eventbuoy2.amen;



import com.blackbean.eventbuoy2.amen.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
public class SplashScreenActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				Intent intent=new Intent(SplashScreenActivity.this,HomeActivity.class);
				startActivity(intent);
				finish();
			}
		}, 2000);
	}

}
