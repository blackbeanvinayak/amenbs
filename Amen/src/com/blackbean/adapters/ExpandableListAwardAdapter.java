package com.blackbean.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.R.dimen;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;


public class ExpandableListAwardAdapter extends BaseExpandableListAdapter  {

	private Context _context;
	private List<String> _listDataHeader; // header titles
	private HashMap<String, List<String>> _listDataChild;
	//private HashMap<ArrayList<String>, List<String>> _listDataChild;
	int id;
	 ArrayList<String> info;
     ArrayList<String> agenda_id;
  /*   public ExpandableListMobileAdapter(Context context, List<String> listDataHeader,
    		 HashMap<String, List<String>> listChildData) {
    	 
     this._context = context;
     this._listDataHeader = listDataHeader;
     this._listDataChild = listChildData;
   
   
}*/
     ArrayList<HashMap<String, ArrayList<String>>> map;

     public ExpandableListAwardAdapter(Context context, List<String> listDataHeader,
    		 ArrayList<HashMap<String, ArrayList<String>>> map ){
    	 
     this._context = context;
     this._listDataHeader = listDataHeader;
   //  this._listDataChild = listChildData;
   this.map=map;
   
}
     
     
	public Object getChild(int groupPosition, int childPosititon) {
		return null;
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	public View getChildView(final int groupPosition,  final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
            
                        LayoutInflater infalInflater = (LayoutInflater) this._context
                                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        convertView = infalInflater.inflate(R.layout.list_item, null);
                
                TextView txtListChild = (TextView) convertView
                                .findViewById(R.id.lblListItem);
                
               
               txtListChild.setText(map.get(0).get(_listDataHeader.get(groupPosition)).get(childPosition));
        
        	/*  txtListChild.setText(_listDataChild.get("Mobile Advertising & Marketing").get(childPosition));
        	  txtListChild.setText(_listDataChild.get("Consumer Mobile Services").get(childPosition));
        	  txtListChild.setText(_listDataChild.get("Innovative Mobile App").get(childPosition));
           */   
        	 // String headerTitle = (String) getGroup(groupPosition);
              
       	   /*if(headerTitle.equalsIgnoreCase("Display Campaigns"))
              {
      		   txtListChild.setText(_listDataChild.get("Display Campaigns").get(childPosition));
              }
      	   if(headerTitle.equalsIgnoreCase("Email Marketing Campaign"))
             {
     		   txtListChild.setText(_listDataChild.get("Email Marketing Campaign").get(childPosition));
             }
      	   if(headerTitle.equalsIgnoreCase("Digital Integrated Campaign"))
             {
     		   txtListChild.setText(_listDataChild.get("Digital Integrated Campaign").get(childPosition));
             }   */  
      	
               convertView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					Dialog dialog=new Dialog(_context);
					dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.award_description);
					TextView title=(TextView)dialog.findViewById(R.id.title);
					TextView titledesc=(TextView)dialog.findViewById(R.id.titledescription);
					
					title.setText(map.get(0).get(_listDataHeader.get(groupPosition)).get(childPosition));
					titledesc.setText(map.get(1).get(_listDataHeader.get(groupPosition)).get(childPosition));
					dialog.show();
					
				}
			});
                      
             
                return convertView;
		
		
	}

	public int getChildrenCount(int groupPosition) {
		return this.map.get(0).get(_listDataHeader.get(groupPosition)).size();
	}

	public Object getGroup(int groupPosition) {
		return null;
	}

	public int getGroupCount() {
		return this.map.get(0).size();
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);
		//lblListHeader.setTypeface(null, Typeface.ITALIC);
		lblListHeader.setText(_listDataHeader.get(groupPosition));
	
		return convertView;
	}

	public boolean hasStableIds() {
		return false;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}


}
