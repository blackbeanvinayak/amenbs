package com.blackbean.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;

public class BMDialogAdapter extends ArrayAdapter<String>{
Context context;
ArrayList<String> name,designation;
	public BMDialogAdapter(Context context, int resource,ArrayList<String> name,ArrayList<String> designation) {
		super(context, R.layout.dialog_boardmember,name);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.name=name;
		this.designation=designation;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view=inflater.inflate(R.layout.dialog_boardmember, parent,false);
		TextView txt_name=(TextView)view.findViewById(R.id.txt_name);
		TextView txt_desi=(TextView)view.findViewById(R.id.txt_desi);
		txt_name.setText(this.name.get(position));
		txt_desi.setText(this.designation.get(position));
		return view;
	}

}
