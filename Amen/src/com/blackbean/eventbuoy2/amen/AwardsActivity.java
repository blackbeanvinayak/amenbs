package com.blackbean.eventbuoy2.amen;

import com.blackbean.eventbuoy2.amen.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class AwardsActivity extends Activity {
	ImageView award, application, eligibility, jury, winner, faq;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_awards);

		award = (ImageView) findViewById(R.id.award);
		application = (ImageView) findViewById(R.id.application);
		eligibility = (ImageView) findViewById(R.id.eligibility);
		jury = (ImageView) findViewById(R.id.jury);
		winner = (ImageView) findViewById(R.id.winner);
		faq = (ImageView) findViewById(R.id.faq);

		// right Image Setting code
		//int position=getIntent().getIntExtra("POSITION", 0);
		//String imageBlack=getIntent().getStringExtra("ImageName");
		/*ImageView icon=(ImageView) findViewById(R.id.icon);
		File pictureFile = new File(Environment
				.getExternalStorageDirectory().getPath()
				+ "/shrm/menu/" + imageBlack);
		if (pictureFile != null) {
			Bitmap bitmap = BitmapFactory.decodeFile(pictureFile
					.getAbsolutePath());
			if(bitmap!=null)
				icon.setImageBitmap(bitmap);
		}
*/		//icon.setColorFilter(Color.argb(255, 0, 0, 0));

		award.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(AwardsActivity.this,
						AwardInfoActivity.class);

				startActivity(intent);
			}
		});

		application.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(AwardsActivity.this,
						ApplicationProcessActivity.class);

				startActivity(intent);

			}
		});

		eligibility.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AwardsActivity.this,
						EligibilityActivity.class);

				startActivity(intent);

			}
		});

		jury.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/*Intent intent = new Intent(AwardsActivity.this,
						Jury_Gridview.class);
				startActivity(intent);*/
				/*if(SetGet.JURY_COUNT==0)
				{
					if (NetworkCheck.getConnectivityStatusString(AwardsActivity.this)) 
					{
						new UpdateJueryMembers().execute();
					}
					SetGet.JURY_COUNT++;
				}else {*/
					Intent intent = new Intent(AwardsActivity.this,
							Jury_Gridview.class);
					startActivity(intent);
				//}


			}
		});

		winner.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(AwardsActivity.this,
						WinnerActivity.class);
				startActivity(intent);

			}
		});

		faq.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(AwardsActivity.this,
						FAQsActivity.class);

				startActivity(intent);

			}
		});
		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AwardsActivity.this,
						HomeActivity.class);
				startActivity(intent);

			}
		});

	}

	/*private class UpdateJueryMembers extends AsyncTask<Void, Void, Void>
	{
		String val = null;
		JSONObject jsonObject = null;
		JSONParser jsonParser = new JSONParser();
		JSONParserString jsonParserString=new JSONParserString();

		ArrayList<String> jury_image_urlArrayList;
		JSONArray response_json_array_jury=null;
		ArrayList<String> imageURL;
		private List<Bitmap> imagebitmap = new ArrayList<Bitmap>();


		Dialog dialog;

		int oldCount=Integer.valueOf(SetGet.readUsingSharedPreference(AwardsActivity.this, "JURY_COUNT").trim());
		int newCount=0;
		DataBase db=new DataBase(AwardsActivity.this);

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog=new Dialog(AwardsActivity.this);
			ProgressBar bar = new ProgressBar(AwardsActivity.this);		
			dialog.setContentView(bar);
			dialog.setTitle("Downloading Please Wait ...");
			dialog.setCancelable(false);
			dialog.show();

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			// Get the value of JuryMember Update Count
			try {
				val = jsonParserString
						.getJSONFromUrl("http://shrm.eventbuoy.com/admin/get_jury_info_updatecount.php");
				System.out.println("Agenda Event Count "+val);
				newCount=Integer.valueOf(val.trim());
			}catch(Exception e)
			{
				System.out.println("Count Exception "+e);
				e.printStackTrace();
				System.out.println("Agenda Event Count "+val);
			}

			if(oldCount<newCount)
			{
				SetGet.storeUsingSharedPreference(AwardsActivity.this, "JURY_COUNT",val.trim());
				db.open();

				//delete Table Jury
				db.deleteJuryTable();

				// Delete SpeakerImage Folder
				File dir = new File(Environment.getExternalStorageDirectory()
						.getPath() + "/shrm/jury");
				if (dir.isDirectory()) {
					String[] children = dir.list();
					for (int i = 0; i < children.length; i++) {
						new File(dir, children[i]).delete();
					}
				}

				// Getting Updated Data from server
				jury_image_urlArrayList=new ArrayList<String>();
				try {
					db.CreateTableJury();
					jsonObject=jsonParser.getJSONFromUrl("http://shrm.eventbuoy.com/admin/GetJury.php");
					if(jsonObject != null)
					{
						String jury_id, jury_name, jury_info, jury_designation, jury_image, jury_type;
						response_json_array_jury=jsonObject.getJSONArray("jury_info");
						for (int i = 0; i < response_json_array_jury.length(); i++) {
							JSONObject agenda_temp = new JSONObject();
							try {
								agenda_temp = response_json_array_jury.getJSONObject(i);

								jury_id = agenda_temp.getString("jury_id");
								jury_name = agenda_temp.getString("jury_name");
								jury_info = agenda_temp.getString("info");
								jury_designation = agenda_temp.getString("jury_designation");
								jury_image = agenda_temp.getString("jury_image");
								jury_type = agenda_temp.getString("type");					
								jury_image_urlArrayList.add(jury_image);

								db.addJuryMember(jury_id,jury_name,jury_info,jury_designation,jury_image,jury_type);

							} catch (JSONException e) {
								System.out.println("Json Exception :" + e);
							}
						}
					}
					SetGet.setJuryImageURL(jury_image_urlArrayList);

					db.close();

					imageURL=SetGet.getJuryImageURL();
					File icons = new File(Environment.getExternalStorageDirectory()
							.getPath() + "/shrm/jury");
					if (!icons.exists()) {
						icons.mkdirs();
					}
					try {
						for (int i = 0; i < imageURL.size(); i++) {
							URL imageUrl = new URL(
									"http://shrm.eventbuoy.com/uploads/jury/"
											+ imageURL.get(i));

							imagebitmap.add(i,getBitmapFromURL(imageUrl.toString()));


						}

						for (int i = 0; i < imageURL.size(); i++) {

							storeImageJury(imagebitmap.get(i), imageURL.get(i));
						}
						//check = true;
					} catch (Exception e) {
						//check = false;
						e.printStackTrace();
					}



				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
					System.out.println("JSon Exception "+e.toString());
				}

			}

			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
			imagebitmap.clear();
			Intent intent = new Intent(AwardsActivity.this,
					Jury_Gridview.class);
			startActivity(intent);
		}


	}


	private void storeImageJury(Bitmap image, String name) {
		String path = null;

		path = "/shrm/jury/";

		File pictureFile = new File(Environment.getExternalStorageDirectory()
				.getPath() + path + name);
		if (pictureFile.exists())
		{

			pictureFile.delete();

		}

		if (!pictureFile.exists()) {

			try {
				pictureFile.createNewFile();
				FileOutputStream fos = new FileOutputStream(pictureFile);
				image.compress(Bitmap.CompressFormat.PNG, 90, fos);
				System.out.println(pictureFile.getName() + "  "
						+ "download complete");
				fos.close();
			} catch (FileNotFoundException e) {
				Log.d("", "File not found: " + e.getMessage());
			} catch (IOException e) {
				Log.d("", "Error accessing file: " + e.getMessage());
			}
		}

	}
*/

	/*public Bitmap getBitmapFromURL(String imageUrl) {
		try {
			URL url = new URL(imageUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}*/
}