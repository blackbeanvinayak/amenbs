package com.blackbean.eventbuoy2.amen;
/*package com.blackbean.eventbuoy2.shrm;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackbean.adapters.ExpandableListAdapter;
import com.blackbean.adapters.ExpandableListWebAdapter;


public class AwardInformationWebActivity extends Activity
{
	TextView award_category;
	ImageView image;
	ExpandableListWebAdapter listAdapter;
	ExpandableListView expandable;
	static List<String> listDataHeader;
	static HashMap<String, List<String>> listDataChild;
	static HashMap<String, List<String>> listDataChildsocial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.award_informtion);
		image=(ImageView)findViewById(R.id.image);
		
		award_category=(TextView)findViewById(R.id.category);
		award_category.setText(getIntent().getExtras().getString("name"));	
	
		if(getIntent().getExtras().getInt("image_id")==0){
			image.setImageResource(R.drawable.digital);
		}
		if(getIntent().getExtras().getInt("image_id")==1){
			image.setImageResource(R.drawable.website);
		}
		if(getIntent().getExtras().getInt("image_id")==2){
			image.setImageResource(R.drawable.mobile);
		}
		if(getIntent().getExtras().getInt("image_id")==3){
			image.setImageResource(R.drawable.special);
		}
		
		
		 expandable=(ExpandableListView)findViewById(R.id.explist);
		 listDataChild = new HashMap<String, List<String>>();
        listDataChildsocial = new HashMap<String, List<String>>();
        listDataHeader=new ArrayList<String>();
        listDataHeader.add("eCommerce");
        listDataHeader.add("News content");
        listDataHeader.add("Automotive Services/Information");
        listDataHeader.add("Travel");
        listDataHeader.add("Financial");
        listDataHeader.add("Entertainment");
        listDataHeader.add("Education");
        listDataHeader.add("Brand/Product Websites");
       

     
        List<String> mam = new ArrayList<String>();
        mam.add("Website or web-portal which provides the consumers the facility of online commerce, trade and exchange vis-à-vis the retail sale of any product or service or a range of products and services.");
        List<String> cms = new ArrayList<String>();
        cms.add("Website/web-portal/webpage/social media forum specially dedicated to the service of providing consumers with online news content and updates in a plenary, convenient and transparent manner. ");
        List<String> lma = new ArrayList<String>();
       lma.add("Website/web-portal/webpage/social  media forum developed with the objective of furnishing all automobile related services and/or information. Ecommerce websites pertaining to the automotive sector may only either apply for their entry under this category or under the e-Commerce category. ");
       
       List<String> mam1 = new ArrayList<String>();
       mam1.add("Website/web-portal/webpage/social media forum dedicated to offering travel services, arrangement facilities and information. These include online agents for purchasing tickets, hotel rooms, rental cars, vacation packages, other travel services, online travel guides, travel writings and travel tools.");
       List<String> cms1 = new ArrayList<String>();
       cms1.add("Website/web-portal/webpage/social media forum providing financial services and/or information. These include online stock trading, financial news, mortgage information, online banking, bill paying services or other consumer related financial services.");
       List<String> lma1 = new ArrayList<String>();
      lma1.add("Website/web-portal/webpage/social media forum providing entertainment service and/or information in any/all of the following formats: video, audio, gaming, social media, and others. ");
      
     
      List<String> mam2 = new ArrayList<String>();
      mam2.add("Website/web-portal/webpage/social media forum providing educational courses and/or information regarding course material, study material, online training, education related information, and others. ");
      List<String> cms2 = new ArrayList<String>();
      cms2.add("Website/web-portal/webpage providing information regarding a brand / product/service and/or promoting a brand / product/service. These could include features, review, and information of any type of consumer brand / product / service. ");
     
    
       
       
       
       
       List<String> sociallistsocial = new ArrayList<String>();
       sociallistsocial.add("social");
 
        listDataChild.put(listDataHeader.get(0), mam); // Header, Child
        listDataChild.put(listDataHeader.get(1), cms);
        listDataChild.put(listDataHeader.get(2), lma);
        listDataChild.put(listDataHeader.get(3), mam1); // Header, Child
        listDataChild.put(listDataHeader.get(4), cms1);
        listDataChild.put(listDataHeader.get(5), lma1);
        listDataChild.put(listDataHeader.get(6), mam2); 
        listDataChild.put(listDataHeader.get(7), cms2);
       
     
  
        listAdapter = new ExpandableListWebAdapter(AwardInformationWebActivity.this,
                        listDataHeader, listDataChild);

        // setting list adapter
        expandable.setAdapter(listAdapter);

        ImageView menu=(ImageView)findViewById(R.id.logo);
		menu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(AwardInformationWebActivity.this, HomeActivity.class);
				startActivity(intent);
				
			}
		});	
	}

}
*/