package com.blackbean.eventbuoy2.amen;
import com.blackbean.eventbuoy2.amen.R;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AboutUsActivity extends Activity
{
	TextView text,link;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about_us);
		//dynamic changes in design
		
		LinearLayout layout=(LinearLayout)findViewById(R.id.layout);
		layout.setBackgroundColor(Color.WHITE);
		text=(TextView)findViewById(R.id.text);
		text.setTextColor(Color.BLACK);// set text color
		
		
		link=(TextView)findViewById(R.id.link);
		link.setTextColor(Color.BLUE);//set link text color
		link.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String url = "http://www.google.com/";
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(url));
				startActivity(i);
			}
		});
		ImageView menu=(ImageView)findViewById(R.id.logo);
		menu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(AboutUsActivity.this, HomeActivity.class);
				startActivity(intent);
				
			}
		});
	}

}
