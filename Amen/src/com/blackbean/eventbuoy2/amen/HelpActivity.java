package com.blackbean.eventbuoy2.amen;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.blackbean.eventbuoy2.amen.R;
import com.google.analytics.tracking.android.EasyTracker;

public class HelpActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);
	
		final EditText edit_subject=(EditText)findViewById(R.id.edit_subject);
		final EditText edit_querry=(EditText)findViewById(R.id.edit_querry);
		
		Button send_querry=(Button)findViewById(R.id.btn_send_querry);
	
		send_querry.setOnClickListener(new OnClickListener() {
		
			
			public void onClick(View v) {
				
				try
				{
					
					if(edit_subject.getText().toString().equalsIgnoreCase("")||edit_querry.getText().toString().equalsIgnoreCase(""))
					{
						Toast.makeText(getApplicationContext(), "Please enter all the fields", Toast.LENGTH_LONG).show();
					}
					else
					{
				Intent emailIntent=new Intent(Intent.ACTION_SEND);
				emailIntent.setData(Uri.parse("mailto:"));
				String [] to={"neeraj.deginal@shrm.org"};
				emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
				emailIntent.putExtra(Intent.EXTRA_SUBJECT, edit_subject.getText().toString());
				emailIntent.putExtra(Intent.EXTRA_TEXT, edit_querry.getText().toString());
				emailIntent.setType("message/rfc822");
				startActivity(Intent.createChooser(emailIntent, "Email"));
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
					Toast.makeText(getApplicationContext(), "Error ocurred", Toast.LENGTH_LONG).show();
				}
				
			}
		});
		
		ImageView menu=(ImageView)findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(HelpActivity.this, HomeActivity.class);
				startActivity(intent);
				
			}
		});
	
	}

	
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		//------------- Flurry Analytics-----------
	//	FlurryAgent.onStartSession(this, "S8WXXMCCGWVYJ6M7XG8Z");
		//-----------------------------------------
		EasyTracker.getInstance(this).activityStart(this);
	}



	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}
	
}
