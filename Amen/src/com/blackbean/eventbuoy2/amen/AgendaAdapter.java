package com.blackbean.eventbuoy2.amen;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;
import com.blackbean.utility.DataBase;
/**
 * AGENDA LISTVIEW CONTAINS EVENT NAME,EVENT ADDRESS,EVENT TIME
 *IT IS ONE ROW OF LISTVIEW 
 */
public class AgendaAdapter extends ArrayAdapter<String> {
	Context context;
	Date agendaDateTime = null ;
	Date currentDateTime = null; 

	ArrayList<String> agenda_name;
	ArrayList<String> agenda_location;
	ArrayList<String>agenda_time;
	ArrayList<String> agenda_id;
	ArrayList<String> check_agenda;
	String date;
	DataBase dataBase;
	final static int RQS_1 = 1;

	public AgendaAdapter(Context context,ArrayList<String> event_id,ArrayList<String> event_name,
			ArrayList<String> event_address,ArrayList<String> event_time,String date) {
		super(context, android.R.layout.simple_list_item_1,event_name);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.agenda_id=event_id;



		this.agenda_name=event_name;
		this.agenda_location=event_address;
		this.agenda_time=event_time;
		this.date = date;
		check_agenda=new ArrayList<String>();


	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView=	inflater.inflate(R.layout.agenda_adapter, parent,false);


		final ImageView addremoveimage=(ImageView)rowView.findViewById(R.id.AddRemoveButton);
		ImageView showArrow=(ImageView)rowView.findViewById(R.id.arrow);
		TextView eventName=(TextView)rowView.findViewById(R.id.EventName);
		TextView eventAddress=(TextView)rowView.findViewById(R.id.EventAddress);
		TextView eventtime=(TextView)rowView.findViewById(R.id.EventTime);

		//if (HomeActivity.whichEvent == StaticData.Event_Nairobi) {
			addremoveimage.setVisibility(View.INVISIBLE);
		//}

		dataBase=new DataBase(context);
		dataBase.open();

		/*if(dataBase.hasSpeakerForThisAgenda(Integer.parseInt(agenda_id.get(position))))
		{
			showArrow.setVisibility(View.VISIBLE);
			addremoveimage.setVisibility(View.VISIBLE);

		}
		else {
			showArrow.setVisibility(View.INVISIBLE);
			addremoveimage.setVisibility(View.INVISIBLE);

		}*/


		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy KK:mm");
		String strDate = sdf.format(c.getTime());




		String tempTime[]=agenda_time.get(position).split("-");
		String KKmm = tempTime[0].trim();


		String tempDate[] = date.split(" ");
		System.out.println("Umesh "+tempDate[1]);

		String ddMMyyyy = null;

		if (tempDate[2].equals("Feb")) {
			ddMMyyyy=tempDate[1].trim()+"/02/2015";	
		}else if (tempDate[2].equals("April")) 
		{
			ddMMyyyy=tempDate[1].trim()+"/04/2015";
		}else if (tempDate[2].equals("May"))
		{
			ddMMyyyy=tempDate[1].trim()+"/05/2015";
		}

		System.out.println("A Date "+ddMMyyyy+" "+ KKmm);
		System.out.println("C Date "+strDate);


		try {
			agendaDateTime = sdf.parse(ddMMyyyy+" "+ KKmm);
			currentDateTime =sdf.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("STR Date "+agendaDateTime);


		if(dataBase.CheckMyScheduleByAgendaId(Integer.parseInt(agenda_id.get(position))))
		{
			addremoveimage.setImageResource(R.drawable.tick);

			//addremoveimage.getResources().toString()
			//Vmassage="Do you want remove it from your schedule";
			//vbtnName="Remove";
		}
		else
		{
			addremoveimage.setImageResource(R.drawable.add);


			//	Vmassage="Do you want Add it in your schedule";
			//	vbtnName="Add";
		}

		dataBase.close();


		eventName.setText(agenda_name.get(position));
		eventAddress.setText(agenda_location.get(position));
		eventtime.setText(agenda_time.get(position));

		addremoveimage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				dataBase=new DataBase(context);
				dataBase.open();
				if(dataBase.CheckMyScheduleByAgendaId(Integer.parseInt(agenda_id.get(position))))
				{
					AlertDialog.Builder builderRemove =new AlertDialog.Builder(context);
					//Vmassage="Do you want Add it in your schedule";
					builderRemove.setMessage("Do you want Remove it from your schedule");

					builderRemove.setTitle("My Schedule Confirmation");

					//builder.setMessage(Vmassage);
					builderRemove.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							addremoveimage.setImageResource(R.drawable.add);



							//ArrayList<Integer> multitrack_agenda_id=new ArrayList<Integer>();
							dataBase=new DataBase(context);
							dataBase.open();

							//multitrack_agenda_id=dataBase.getMultiTrackIdFromAgenda(agenda_id.get(position));
							//agendid=dataBase.get

							if(dataBase.deleteMySchedule(agenda_id.get(position)))
							{
								//agenda_id.remove(Integer.parseInt(agenda_id.get(position)));
								Toast.makeText(context,"Deleted", Toast.LENGTH_SHORT).show();
							}
							//dataBase.addmyschedule(Integer.parseInt(agenda_id.get(position)));
							dataBase.close();
						}

					});
					builderRemove.setNegativeButton("NO", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
						}
					});

					//builderRemove.create();
					builderRemove.show();


				}
				else 
				{
					/*AlertDialog.Builder builderAdd=new AlertDialog.Builder(context);
					builderAdd.setMessage("Do you want add  it in your schedule");

					builderAdd.setTitle("My Schedule Confirmation");

					//builder.setMessage(Vmassage);
					builderAdd.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub

							ArrayList<Integer> multitrack_agenda_id=new ArrayList<Integer>();

								addremoveimage.setImageResource(R.drawable.tick);
								dataBase=new DataBase(context);
								dataBase.open();

							//multitrack_agenda_id=dataBase.getMultiTrackIdFromAgenda(agenda_id.get(position));	
							//	System.out.println("Track Id"+multitrack_agenda_id);

								dataBase.addmyschedule(Integer.parseInt(agenda_id.get(position)));
							//dataBase.addmyschedule(multitrack_agenda_id);
							//dataBase.close();

						//	Intent intent=new Intent(context,AgendaActivity.class);
						//     context.startActivity(intent);


						}
					});
					builderAdd.setNegativeButton("NO", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
						}
					});
					builderAdd.show();*///builderAdd.create();


					final Dialog dialog = new Dialog(context);
					dialog.setTitle("Schedule Confirmation");
					dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					dialog.setContentView(R.layout.schedule);
					Button btn_ok,btn_cancel;
					btn_ok=(Button)dialog.findViewById(R.id.btn_ok);
					btn_cancel=(Button)dialog.findViewById(R.id.btn_cancel);

					final Spinner spinner = (Spinner) dialog
							.findViewById(R.id.sp_time);
					//String str[] = { "Alarm Time", "15 min before", "20 min before", "30 min before", "40 min before" };
					ArrayList<String> arrayList=new ArrayList<String>();
					arrayList.add("No Alarm");
					arrayList.add("1 min before");
					arrayList.add("10 min before");
					arrayList.add("15 min before");
					arrayList.add("20 min before");
					arrayList.add("25 min before");
					arrayList.add("30 min before");

					//spinner.setAdapter(new ArrayAdapter<String>(context,
					//		android.R.layout.simple_list_item_1, arrayList));
					spinner.setAdapter(new ScheduleAdapter(context, arrayList));

					if (agendaDateTime.compareTo(currentDateTime) < 0) {
						spinner.setEnabled(false);
					}

					btn_ok.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							//String Valarmtime=spinner.getSelectedItem().toString();
							String Valarmtime=spinner.getAdapter().getItem(spinner.getSelectedItemPosition()).toString();
							String Vtime[]=agenda_time.get(position).split(" ");
							String  vvtime[]=Vtime[0].split(":");
							int vhourse=Integer.parseInt(vvtime[0]);
							int vminut=Integer.parseInt(vvtime[1]);
							//int hourOfDay=Integer.parseInt(Vtime[0].trim());

							String Vdate[]=AgendaActivity.date.getText().toString().split(" ");
							//String v1date[]=Vdate[1].split("/");
							int date=Integer.parseInt(Vdate[1].toString());
							int month=/*Integer.parseInt(Vdate[2])-1*/0;
							if (Vdate[2].equals("Feb")) {
								month = 02;
							}else if (Vdate[2].equals("April")) {
								month = 04;
							}
							Calendar c=Calendar.getInstance();

							if(!Valarmtime.equalsIgnoreCase("No Alarm")){
								String arr[]=Valarmtime.split(" ");
								int beforetime=Integer.parseInt(arr[0]);
								//	c.set(2014,month, date, hourOfDay, 00,00);
								c.set(2015,month, date, vhourse, vminut,00);
								c.add(Calendar.MINUTE, -beforetime);
								setAlarm(c,agenda_name.get(position));

								ArrayList<Integer> multitrack_agenda_id = new ArrayList<Integer>();

								addremoveimage
								.setImageResource(R.drawable.tick);
								dataBase = new DataBase(context);
								dataBase.open();
								dataBase.addmyschedule(Integer
										.parseInt(agenda_id.get(position)));

							}
							else{

								ArrayList<Integer> multitrack_agenda_id = new ArrayList<Integer>();

								addremoveimage
								.setImageResource(R.drawable.tick);
								dataBase = new DataBase(context);
								dataBase.open();
								dataBase.addmyschedule(Integer
										.parseInt(agenda_id.get(position)));

							}

							dialog.dismiss();
						}
					});
					btn_cancel.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dialog.dismiss();	
						}
					});
					dialog.show();
				}
			}
		});

		return rowView;
	}

	private void setAlarm(Calendar targetCal,String agenda){

		Toast.makeText(context, "ALARM SET " + targetCal.getTime(),Toast.LENGTH_LONG).show();

		Intent intent = new Intent(context, AlarmReceiver.class);
		intent.putExtra("agenda", agenda);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context, RQS_1, intent, 0);
		AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		alarmManager.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(), pendingIntent);	  
	}



	class ScheduleAdapter extends BaseAdapter
	{
		Context context;
		ArrayList<String> list;
		public ScheduleAdapter(Context context,ArrayList<String> list) {
			// TODO Auto-generated constructor stub
			this.context=context;
			this.list=list;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return list.size();
		}

		@Override
		public String getItem(int arg0) {
			// TODO Auto-generated method stub
			return list.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view=inflater.inflate(R.layout.single_text_adapter,null);

			TextView txt=(TextView)view.findViewById(R.id.text1);
			txt.setText(list.get(arg0));
			txt.setTextColor(Color.BLACK);

			return view;
		}

	}



}
