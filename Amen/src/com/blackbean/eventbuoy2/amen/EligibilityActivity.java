package com.blackbean.eventbuoy2.amen;

import com.blackbean.eventbuoy2.amen.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class EligibilityActivity extends Activity
{
	TextView description;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.eligibility_criteria);
		
		description=(TextView)findViewById(R.id.eligibility_desc);
		
		description.setText("\nParticipation is open to the following organizations:\n\nParticipation is open to both SHRM as well as non-SHRM membersIndustry: Firms operating in any of the industry segments within the categories of Services, Manufacturing and Agriculture.Origin:\n\nIndian organizations - Multinational corporations, family owned business houses which have domestic and/or international operationsForeign owned Multinational corporations, their captives and MNC third-party players with operations in India that cater to both domestic and export markets.In case of organizations applying for the International Award category (Excellence in Human Resource - South Asia & Middle East), they would need to have operations in a country excluding India.\n\nLegal status:\n\nPrivate sectorPublic sector enterprise\n\nSound financial performance in the past 3 years The Initiatives / Projects shared in the Application form should be completely executed or should be an ongoing initiative/project.The Initiatives / Projects shared in the Application form should have been implemented/ initiated within the period, 1st April 2011 - 31st March 2014.Sponsors of SHRM Awards 2014 are not allowed to participate.The Awards Management may modify the eligibility criteria from time to time with retrospective effect.The Awards Management holds the right to disqualify any application which does not meet the eligibility criteria without assigning any reason whatsoever.To enable equal representation of large and small to medium sized companies, and an objective comparison two categories have been created. However, separate assessment of the applications category wise will only be carried out if there are sufficient applications in that category, else all the applications will be reviewed together in an award category.");
		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(EligibilityActivity.this,
						HomeActivity.class);
				startActivity(intent);
				finish();

			}
		});
		
	}

}
