package com.blackbean.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.utility.DataBase;
import com.blackbean.utility.ImageLoader;

public class AgendaSpeakerAdapter extends BaseAdapter {
	Context context;
	//ArrayList<String> speaker_id;
	DataBase dataBase;
	private static final String Speaker_NAME="speaker_name";
	private static final String Speaker_Designation="speaker_designation";
	private static final String Speaker_Image="speaker_image";
	ArrayList<String> speaker_name ;
	ArrayList<String> speaker_designation;
	ArrayList<String> speaker_image;
	public AgendaSpeakerAdapter(Context context, int resource,ArrayList<String> speaker_name,ArrayList<String>speaker_designation,
			ArrayList<String>speaker_image) {
		//super(context, android.R.layout.simple_list_item_1,speakerid);
		// TODO Auto-generated constructor stub
		this.context=context;   
		// this.speaker_id=new ArrayList<String>();
		//this.speaker_id= speakerid;
		//init();
		
		this.speaker_name=speaker_name;
		this.speaker_designation=speaker_designation;
		this.speaker_image=speaker_image;

	}


	/*@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub



			LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view=inflater.inflate(R.layout.agenda_speaker_adapter, parent,false);



			String speaker_name,speaker_designation;
			dataBase=new DataBase(context);
			 dataBase.open();

			 speaker_name=dataBase.getSpeakerInfoBySpeaker_id(Integer.parseInt(speaker_id.get(position)), Speaker_NAME);
			 speaker_designation=dataBase.getSpeakerInfoBySpeaker_id(Integer.parseInt(speaker_id.get(position)), Speaker_Designation);

			 dataBase.close();
			TextView name=(TextView)view.findViewById(R.id.speaker_nametxt);
			TextView designation=(TextView)view.findViewById(R.id.speaker_designtxt);
			ImageView image=(ImageView)view.findViewById(R.id.speaker_image);
			name.setText(speaker_name.get(position));
			designation.setText(speaker_designation.get(position));
			return view;
		}*/

	/*public void init()
	{
		speaker_name=new ArrayList<String>();
		speaker_designation=new ArrayList<String>();
		speaker_image=new ArrayList<String>();
		dataBase=new DataBase(context);
		dataBase.open();
		
		String event_category;
		if (HomeActivity.whichEvent == StaticData.Event_Nairobi) {
			event_category = "Nairobi";
		}else {
			event_category = "Delhi";
		}
		
		for (int i = 0; i < speaker_id.size(); i++) {


			speaker_name.add(dataBase.getSpeakerInfoBySpeaker_id(Integer.parseInt(speaker_id.get(i)), Speaker_NAME,event_category));
			speaker_designation.add(dataBase.getSpeakerInfoBySpeaker_id(Integer.parseInt(speaker_id.get(i)), Speaker_Designation,event_category));
			speaker_image.add(dataBase.getSpeakerInfoBySpeaker_id(Integer.parseInt(speaker_id.get(i)), Speaker_Image,event_category));

		}
		dataBase.close();
	}*/


	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return this.speaker_name.size();
	}


	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater;
		View view=convertView;
	//	if (convertView==null)
		//{
			inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view=inflater.inflate(R.layout.agenda_speaker_adapter, parent,false);
		//}

		TextView name=(TextView)view.findViewById(R.id.speaker_nametxt);
		ImageView image=(ImageView)view.findViewById(R.id.speaker_image);

		/*File pictureFile = new File(Environment
				.getExternalStorageDirectory().getPath()
				+ "/sankalp/speakers/" +speaker_image.get(position));

		if (pictureFile != null) {
			Bitmap bitmap = BitmapFactory.decodeFile(pictureFile.getAbsolutePath());
			image.setImageBitmap(bitmap);
		}	*/
		
		
		String url="http://amen.eventbuoy.com/admin/uploads/"
				+ this.speaker_image.get(position).replaceAll(" ", "%20");
		name.setText(speaker_name.get(position));
		System.out.println("http://amen.eventbuoy.com/admin/uploads/"
				+ speaker_image.get(position).replaceAll(" ", "%20"));
		ImageLoader imageLoader = new ImageLoader(context);
		imageLoader.DisplayImage(url, image);
		
		//+"\n"+speaker_designation.get(position));
		//	designation.setText(speaker_designation.get(position));
		//	name.setTextColor(Color.BLACK);
		return view;
	}


}
