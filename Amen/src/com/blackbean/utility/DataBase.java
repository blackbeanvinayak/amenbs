package com.blackbean.utility;

import java.util.ArrayList;
import java.util.HashMap;

import com.blackbean.adapters.NoteModel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Create Database,table insert,update,delete and retrieve data from database
 * 
 * @author rahul
 * 
 */
public class DataBase {
	private static final String DB = "amen.db";
	private static final int Version = 1;
	private static final String Track_Table = "Track";
	private static final String Speaker_Table = "Speaker";
	private static final String Agenda_Speaker_Table = "Agenda_Speaker";
	private static final String Sponsor_Table = "sponsors";
	private static final String Agenda_Table = "Agenda";
	private static final String Attendees_Table = "Attendees";
	private static final String Track_Attendee_Table = "Track_Attendee";
	private static final String MySchedule_Table = "MySchedule";
	private static final String Note_Table = "Note";
	private static final String Note_Table1="Note1";
	private static final String Jury_Table = "jury";
	private static final String Menu_Table ="Menu";
	private static final String Program_Table ="programs";
	private static final String Page_Table = "page";
	private static final String Text_Color_Table = "text_color";
	private static final String Icon_Table = "icons";

	// Agenda variables
	private static final String Agenda_ID = "agenda_id";
	private static final String Agenda_NAME = "agenda_name";
	private static final String Agenda_LOCATION = "agenda_location";
	private static final String Agenda_DATE = "agenda_date";
	private static final String Agenda_DAY = "agenda_day";
	private static final String Agenda_TIME = "agenda_time";
	private static final String Agenda_DESCRIPTION = "agenda_description";
	private static final String Agenda_Order = "agenda_order";
	private static final String Agenda_Track_ID = "Track_track_id";
	private static final String Create_Table_Agenda = "create table Agenda (agenda_id integer,agenda_name TEXT,agenda_location Text,agenda_day Text,agenda_date Text,agenda_time Text,agenda_description Text,agenda_order integer,Track_track_id integer,event_category Text)";
	private static final String Drop_Agenda = "drop table if exists Agenda";

	// Track Table Variables
	private static final String Track_ID = "track_id";
	private static final String Track_NAME = "track_name";
	private static final String Track_LOCATION = "track_location";
	private static final String Track_DATE = "track_date";
	private static final String Track_Time = "track_time";
	private static final String Track_DESCRIPTION = "track_description";
	private static final String Create_Table_Track = "create table Track (track_id integer,track_name TEXT,track_location Text,track_date Text,track_time Text,track_description Text)";
	private static final String Drop_Track = "drop table if exists Track";

	// Speaker Table Variables

	private static final String Speaker_ID = "speaker_id";
	private static final String Speaker_NAME = "speaker_name";
	private static final String Speaker_Designation = "speaker_designation";
	private static final String Speaker_Desription = "speaker_description";
	private static final String Speaker_Image = "speaker_image";
	private static final String Speaker_FB = "speaker_fb";
	private static final String Speaker_TW = "speaker_twiter";
	private static final String Speaker_GM = "speaker_google";
	private static final String Speaker_LK = "speaker_linkedin";
	private static final String Speaker_Order = "speaker_order";
	private static final String Speaker_Track_ID = "Track_track_id";
	// private static final String
	// Create_Table_Speaker="create table Speaker (speaker_id integer,speaker_name TEXT,speaker_designation Text,speaker_description Text,speaker_image Text,speaker_fb Text,speaker_twiter Text,speaker_google Text,speaker_linkedin Text,speaker_order,Track_track_id)";
	private static final String Drop_Speaker = "drop table if exists Speaker";

	private static final String Speaker_Note = "speaker_note";
	private static final String Create_Table_Speaker = "create table Speaker (speaker_id integer,speaker_name TEXT,speaker_designation Text,speaker_description Text,speaker_image Text,speaker_fb Text,speaker_twiter Text,speaker_google Text,speaker_linkedin Text,speaker_order,Track_track_id,speaker_note Text,event_category Text)";

	// Attendee Variables

	private static final String Attendee_ID = "attendee_id";
	private static final String Attendee_Name = "attendee_name";
	private static final String Attendee_Email = "attendee_email";
	private static final String Attendee_Mobile = "attendee_mobile";
	private static final String Attendee_Fees = "attendee_fees";
	private static final String Attendee_Payment_Details = "attendee_patment_details";
	private static final String Create_Table_Attendee = "create table Attendee(attendee_id integer,attendee_name Text,attendee_email Text,attendee_mobile integer,attendee_fees double,attendee_patment_details Text)";
	private static final String Drop_Attendee = "drop table if exists Attendee";

	// Agenda_Speakers
	private static final String Agenda_Speaker_ID = "agenda_speaker_id";
	private static final String Agenda_agenda_ID = "Agenda_agenada_id";
	private static final String Speaker_speaker_ID = "Speaker_speaker_id";
	private static final String Create_Table_Agenda_Speaker = "create table Agenda_Speaker(agenda_speaker_id integer,Agenda_agenada_id integer,Speaker_speaker_id integer)";
	private static final String Drop_Agenda_Speaker = "drop table if exists Agenda_Speaker";

	// Track Attendee variables
	private static final String Track_Attendee_Id = "track_attendee_id";
	private static final String Track_Track_ID = "Track_track_id";
	private static final String Attendies_Attendee_Id = "Attendies_Attendee_Id";
	private static final String Attendies_Attendee_email = "Attendies_attendee_email";
	private static final String Attendies_Attendee_mobile = "Attendies_attendee_mobile";
	private static final String Create_Table_Track_Attendee = "create table Track_attendee (Agenda_agenada_id integer,Speaker_speaker_id integer,agenda_speaker_id integer)";
	private static final String Drop_Track_Attendee = "drop table if exists Track_attendee ";

	// Sponsers variables
	private static final String Sponsers_Id = "sponsers_id";
	private static final String Sponsers_Name = "sponsers_name";
	private static final String Sponsers_Type = "sponsers_type";
	private static final String Sponsers_Image = "sponsers_image";
	private static final String Sponsers_Description = "sponsers_description";
	private static final String Sponsers_Url = "sponsers_url";
	private static final String Sponsers_Order = "sponsers_order";
	private static final String Create_Table_Sponsors = "create table sponsors(sponsers_id integer,sponsers_name Text,sponsers_type Text,sponsers_image Text,sponsor_description Text ,sponsers_url Text,sponsers_order integer,event_category Text)";
	private static final String Drop_Sponsors = "drop table if exists sponsors";

	// MySchedule Variables
	private static final String MySchedule_Id = "myschedule_id";
	private static final String Create_Table_MySchedule = "create table MySchedule(myschedule_id integer primary key autoincrement,agenda_id integer)";
	private static final String Drop_Myschedule = "drop table if exists MySchedule";

	// Note Table Variable
	private static final String Note_Id = "note_id";
	private static final String Note_Text = "note_text";
	private static final String Create_Table_Note = "create table Note(note_id integer,agenda_id integer,note_text text)";
	private static final String Drop_Note = "drop table if exists Note";

	// Note1 Table Variable
	private static final String Note_Id1 = "note_id1";
	private static final String Note_Info1 = "note_info1";
	private static final String Note_Content1 = "note_content1";
	private static final String Note_Place1 = "note_place1";
	private static final String Note_Place_Id1 = "note_place_id1";
	private static final String Create_Table_Note1 = "create table Note1(note_id1 integer PRIMARY KEY AUTOINCREMENT,note_info1 text,note_content1 text,note_place1 text,note_place_id1 integer)";
	private static final String Drop_Note1 = "drop table if exists Note1";

	//Menu Table Varialbes
	private static final String MenuId="menu_id";
	private static final String MenuName="menu_name";
	private static final String MenuImage="menu_image";
	private static final String MenuImageBlack="menu_image_black";
	private static final String Create_Table_Menu="create table Menu(menu_id integer PRIMARY KEY AUTOINCREMENT,menu_name text,menu_image text,menu_image_black text)";
	private static final String Drop_Menu="drop table if exists Menu";

	// Page Design Table
	private static final String Page_Id = "page_id";
	private static final String Page_Name = "name";
	private static final String Create_Table_Page = "create table page(page_id integer,name text)";
	private static final String Drop_Page = "drop table if exists page";

	// Text Color Table
	private static final String Color_Id = "color_id";
	private static final String H1 = "h1";
	private static final String H2 = "h2";
	private static final String H3 = "h3";
	private static final String H4 = "h4";
	private static final String H5 = "h5";
	private static final String H6 = "h6";
	private static final String BackGround = "bg";
	private static final String Create_Table_Text_Color = "create table text_color(color_id integer primary key autoincrement,page_id integer,h1 text,h2 text,h3 text,h4 text,h5 text,h6 text,bg text)";
	private static final String Drop_Text_Color = "drop table if exists text_color";

	// Icons Color Table
	/*private static final String Icon_Id = "icon_id";
	private static final String Logo = "logo";
	private static final String Title = "title";
	private static final String Search = "search";
	private static final String Add = "add";
	private static final String Remove = "remove";
	private static final String Expandable = "expandable";
	private static final String Next = "next";
	private static final String Previous = "previous";
	private static final String Create_Table_Icons = "create table icons(icon_id integer,page_id integer,logo blob,title blob,search blob,add blob,remove blob,next blob,previous blob)";
	private static final String Drop_Icons = "drop table if exists icons";*/
	private static final String ProgramId="program_id";
	private static final String ProgramName="program_name";
	private static final String ProgramDate="program_date";
	private static final String ProgramVenue="program_venue";
	private static final String ProgramInfo="program_info";
	private static final String ProgramImage="program_image";
	private static final String ProgramLink="program_link";
	private static final String Create_Table_Program="create table programs(program_id integer,program_name Text,program_date Text,program_info Text,program_venue Text,program_link Text,program_image Text,event_category Text)";
	private static final String Drop_Program="drop table if exists programs";




	private static final String Jury_ID = "jury_id";
	private static final String Jury_Name = "jury_name";
	private static final String Jury_designation = "jury_designation";
	private static final String Jury_info = "jury_info";
	private static final String Jury_Image = "jury_image";
	private static final String Jury_Type = "jury_type";

	private static final String Create_Table_Jury = "create table jury (jury_id integer,jury_name TEXT,jury_designation Text,jury_info Text,jury_image Text,jury_type Text,event_category Text)";
	private static final String Drop_Jury = "drop table if exists jury";

	/*
	 * private static final String TYPE="type"; private static final String
	 * LOGO="logo"; private static final String URL="sponsor_url"; private
	 * static final String SPonsor_Order="sponsor_order"; private static final
	 * String Create_Table_Sponsors=
	 * "create table sponsors(id integer,name Text,type Text,logo Text,sponsor_url Text ,sponsor_order integer)"
	 * ; private static final String
	 * Drop_Sponsors="drop table if exists sponsors";
	 */

	private Context context;
	private SQLiteDatabase db;
	private DataBaseHelper DBHelper;

	private static String AgendaValue = "";

	public DataBase(Context context) {
		this.context = context;
		DBHelper = new DataBaseHelper(this.context);
	}

	private static class DataBaseHelper extends SQLiteOpenHelper {

		public DataBaseHelper(Context context) {
			super(context, DB, null, Version);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			db.execSQL(Create_Table_Agenda);
			db.execSQL(Create_Table_Agenda_Speaker);
			db.execSQL(Create_Table_Speaker);
			db.execSQL(Create_Table_Attendee);
			db.execSQL(Create_Table_Track);
			db.execSQL(Create_Table_Sponsors);
			db.execSQL(Create_Table_Track_Attendee);
			db.execSQL(Create_Table_MySchedule);
			db.execSQL(Create_Table_Note);
			db.execSQL(Create_Table_Note1);			
			db.execSQL(Create_Table_Page);
			db.execSQL(Create_Table_Text_Color);
			db.execSQL(Create_Table_Jury);
			db.execSQL(Create_Table_Menu);
			db.execSQL(Create_Table_Program);

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			db.execSQL(Drop_Agenda);
			db.execSQL(Drop_Agenda_Speaker);
			db.execSQL(Drop_Speaker);
			db.execSQL(Drop_Attendee);
			db.execSQL(Drop_Track);
			db.execSQL(Drop_Sponsors);
			db.execSQL(Drop_Track_Attendee);
			db.execSQL(Drop_Myschedule);
			db.execSQL(Drop_Note);
			db.execSQL(Drop_Note1);
			db.execSQL(Drop_Page);
			db.execSQL(Drop_Text_Color);
			db.execSQL(Drop_Jury);
			db.execSQL(Drop_Menu);
			db.execSQL(Drop_Program);
			onCreate(db);
		}
	}

	/**
	 * Open Database
	 * 
	 * @return
	 */
	public DataBase open() {
		db = DBHelper.getWritableDatabase();
		return this;
	}

	/**
	 * Close Database
	 */
	public void close() {
		DBHelper.close();
	}

	public void deleteAlltables()
	{
		db.execSQL(Drop_Agenda);
		db.execSQL(Drop_Agenda_Speaker);
		db.execSQL(Drop_Speaker);
		db.execSQL(Drop_Attendee);
		db.execSQL(Drop_Track);
		db.execSQL(Drop_Sponsors);
		db.execSQL(Drop_Track_Attendee);
		db.execSQL(Drop_Myschedule);
		db.execSQL(Drop_Note);
		db.execSQL(Drop_Note1);
		db.execSQL(Drop_Menu);
		db.execSQL(Drop_Page);
		db.execSQL(Drop_Text_Color);
		db.execSQL(Drop_Jury);
		db.execSQL(Drop_Program);


		//  db.execSQL(Drop_agenda_doc_list);
		// db.execSQL(Drop_agenda_Speaker_doc_list);
		// db.execSQL(Drop_CBM);
		// db.execSQL(Drop_Table_CCB);


		DBHelper.onCreate(db);
	}

	public void deleteAgendaTable() {
		db.execSQL(Drop_Agenda);
	}
	public void deleteSpeakerTable() {
		db.execSQL(Drop_Speaker);
	}
	public void deleteAgenda_SpeakerTable() {
		db.execSQL(Drop_Agenda_Speaker);
	}
	public void deletePartnerTable() {
		db.execSQL(Drop_Sponsors);
	}
	public void deleteJuryTable() {
		db.execSQL(Drop_Jury);
	}
	public void deleteProgramsTable() {
		db.execSQL(Drop_Program);
	}

	/*
	 * Create Table Sponsers
	 */
	public void CreateTablePartner() {
		db.execSQL(Create_Table_Sponsors);
	}
	public void CreateTableJury() {
		db.execSQL(Create_Table_Jury);
	}

	// create table Menu
	public void CreateTableMenu() {
		db.execSQL(Create_Table_Menu);
	}
	public void create_Table_Programs() {
		db.execSQL(Create_Table_Program);
	}

	/*
	 * Create Table Agenda, Speaker and Agenda_Speaker
	 */
	public void CreateTables_Agenda_SPeaker_AgendaSpeaker() {
		db.execSQL(Create_Table_Agenda);
		db.execSQL(Create_Table_Agenda_Speaker);
		db.execSQL(Create_Table_Speaker);
	}

	/**
	 * Add Agenda Info into database
	 * 
	 * @param agendalist
	 * @return
	 */
	public long addAgenda(String agenda_id, String agenda_name,
			String agenda_location, String date, String day, String time,
			String description, String order, String agenda_track_id,String event_category) {

		ContentValues values = new ContentValues();

		values.put(Agenda_ID, agenda_id);
		values.put(Agenda_NAME, agenda_name);
		values.put(Agenda_LOCATION, agenda_location);
		values.put(Agenda_DATE, date);
		values.put(Agenda_DAY, day);
		values.put(Agenda_TIME, time);
		values.put(Agenda_DESCRIPTION, description);
		values.put(Agenda_Order, order);
		values.put(Agenda_Track_ID, agenda_track_id);
		values.put("event_category", event_category);

		return db.insert(Agenda_Table, null, values);
	}

	public long addSponsors(ArrayList<HashMap<String, String>> sponsorlist) {
		ArrayList<HashMap<String, String>> Vsponsorlist = new ArrayList<HashMap<String, String>>();
		Vsponsorlist = sponsorlist;
		ContentValues values = new ContentValues();

		values.put(Sponsers_Id, Vsponsorlist.get(0).get(Sponsers_Id));
		values.put(Sponsers_Name, Vsponsorlist.get(1).get(Sponsers_Name));
		values.put(Sponsers_Type, Vsponsorlist.get(2).get(Sponsers_Type));
		values.put(Sponsers_Image, Vsponsorlist.get(3).get(Sponsers_Image));
		values.put(Sponsers_Order, Vsponsorlist.get(4).get(Sponsers_Order));
		values.put(Sponsers_Url, Vsponsorlist.get(5).get(Sponsers_Url));
		values.put("event_category", Vsponsorlist.get(6).get("event_category"));
		values.put("sponsor_description", Vsponsorlist.get(7).get("sponsor_description"));

		return db.insert(Sponsor_Table, null, values);
	}



	public long addPrograms(String programid,String programname,String programdate,String programlink,String programvenue,String programinfo,String programimage,String programeventcategory) {
		//ArrayList<HashMap<String, String>> Vsponsorlist = new ArrayList<HashMap<String, String>>();
		//Vsponsorlist = programslist;

		//program_id integer,program_name Text,program_date Text,program_info Text,program_venue Text,program_info Text,program_image Text
		ContentValues values = new ContentValues();

		values.put(ProgramId, programid);
		values.put(ProgramName, programname);
		values.put(ProgramDate, programdate);
		values.put(ProgramInfo, programinfo);
		values.put(ProgramLink, programlink);
		values.put(ProgramVenue, programvenue);
		values.put(ProgramImage, programimage);
		values.put("event_category", programeventcategory);

		return db.insert(Program_Table, null, values);
	}

	/**
	 * Add agenda id into myschedule.
	 * 
	 * @param agenda_id
	 * @return
	 */
	public long addmyscheduleMultitrack(ArrayList<Integer> multitrackagenda_id) {
		ArrayList<Integer> multitrackArrayList = new ArrayList<Integer>();
		multitrackArrayList = multitrackagenda_id;
		for (int i = 0; i < multitrackArrayList.size(); i++) {
			ContentValues values = new ContentValues();
			values.put(Agenda_ID, multitrackArrayList.get(i));
			db.insert(MySchedule_Table, null, values);
		}
		return 1;
	}

	public long addmyschedule(int agenda_id) {
		// ArrayList<Integer> multitrackArrayList=new ArrayList<Integer>();
		// multitrackArrayList=multitrackagenda_id;
		// for(int i=0;i<multitrackArrayList.size();i++)
		// {
		ContentValues values = new ContentValues();
		values.put(Agenda_ID, agenda_id);

		// }
		return db.insert(MySchedule_Table, null, values);
	}



	/**
	 * function for adding note for selected Agenda or speaker
	 * 
	 * .... Umesh
	 */
	public long addNote1(String note_content,String note_place,int note_place_id,String note_info) {

		ContentValues values = new ContentValues();
		values.put(Note_Content1, note_content);
		values.put(Note_Info1, note_info);
		values.put(Note_Place1, note_place);
		values.put(Note_Place_Id1, note_place_id);

		return db.insert(Note_Table1, null, values);
	}



	public long UpdateNote1(int note_id,String note_content,String note_place,int note_place_id,String note_info) {

		ContentValues values = new ContentValues();
		values.put(Note_Content1, note_content);
		values.put(Note_Info1, note_info);
		values.put(Note_Place1, note_place);
		values.put(Note_Place_Id1, note_place_id);
		long a=db.update(Note_Table1, values, Note_Id1 + "=" + note_id, null);
		return a;

	}

	public long UpdateNote1byPlaceAndPlaceId(String note_content,String note_place,int note_place_id) {

		ContentValues values = new ContentValues();
		values.put(Note_Content1, note_content);

		return db.update(Note_Table1, values, Note_Place1 + "=" + note_place + " AND "+Note_Place_Id1 + "="+note_place_id, null);
	}

	/**
	 * Get All notes from Note_Table
	 * 
	 * @param none
	 * @return cursor
	 */

	public Cursor getAllNotes() {

		Cursor c = db.query(Note_Table1, null, null, null, null, null, null);

		return c;
	}


	/**
	 * Get notes from Note_Table by place and place_id
	 * 
	 * @param place 
	 * @param place_id
	 * @return cursor
	 */
	public ArrayList<NoteModel> getNoteByPlaceId(String note_place,int note_place_id) {
		ArrayList<NoteModel>rowItems=new ArrayList<NoteModel>();
		String whereClause = Note_Place1+"= ? AND "+Note_Place_Id1+ "= ?";
		String[] whereArgs = new String[] {
				note_place,""+note_place_id };
		Cursor cursor=db.query(Note_Table1, null, whereClause, whereArgs, null, null, null);

		//int count=cursor.getCount();

		if(cursor!=null)
		{
			if(cursor.moveToFirst())
			{
				do{
					rowItems.add(new NoteModel(String.valueOf(cursor.getInt(0)),
							cursor.getString(1), cursor.getString(2), 
							String.valueOf(cursor.getInt(4)), cursor.getString(3)));
					/*System.out.println("id  "+cursor.getInt(0));
					System.out.println("content  "+cursor.getString(1));
					System.out.println("info  "+cursor.getString(2));
					System.out.println("place  "+cursor.getString(3));
					System.out.println("place id  "+cursor.getString(4));*/
				}while (cursor.moveToNext());
			}
		}

		return rowItems;
	}

	/**
	 * Get Note By place 
	 */
	public ArrayList<NoteModel> getNoteByPlace(String note_place) {
		ArrayList<NoteModel>rowItems=new ArrayList<NoteModel>();
		String whereClause = Note_Place1+"= ? ";
		String[] whereArgs = new String[] {
				note_place };
		Cursor cursor=db.query(Note_Table1, null, whereClause, whereArgs, null, null, null);

		if(cursor!=null)
		{
			if(cursor.moveToFirst())
			{
				do{
					rowItems.add(new NoteModel(String.valueOf(cursor.getInt(0)),
							cursor.getString(1), cursor.getString(2), 
							String.valueOf(cursor.getInt(4)), cursor.getString(3)));
					/*System.out.println("id  "+cursor.getInt(0));
					System.out.println("content  "+cursor.getString(1));
					System.out.println("info  "+cursor.getString(2));
					System.out.println("place  "+cursor.getString(3));
					System.out.println("place id  "+cursor.getString(4));*/
				}while (cursor.moveToNext());
			}
		}

		return rowItems;
	}

	/**
	 * Delete Note by note place id
	 */
	public boolean deleteNoteByPlaceId(String placeId) {
		// ArrayList<Integer> arrayList=new ArrayList<Integer>();
		// arrayList=agenda_id;
		// for(int i=0;i<arrayList.size();i++)
		// {
		db.delete(Note_Table1, Note_Place_Id1 + "=" + placeId, null);
		// }

		return true;
	}
	/**
	 * Delete Note by noteId
	 * @param noteId
	 * @return
	 */
	public boolean deleteNoteByNoteId(String noteId) {
		// ArrayList<Integer> arrayList=new ArrayList<Integer>();
		// arrayList=agenda_id;
		// for(int i=0;i<arrayList.size();i++)
		// {
		db.delete(Note_Table1, Note_Id1 + "=" + noteId, null);
		// }

		return true;
	}

	/**
	 * Add note for your selected agenda
	 * 
	 * @param agenda_id
	 * @param note
	 * @return
	 */
	public long addnote(int agenda_id, String note) {
		ContentValues values = new ContentValues();
		values.put(Agenda_ID, agenda_id);
		values.put(Note_Text, note);
		long id;
		Cursor c = db.query(Note_Table, new String[] { Note_Text }, Agenda_ID
				+ "=" + agenda_id, null, null, null, null);
		if (c.getCount() > 0) {
			// upda
			id = db.update(Note_Table, values, Agenda_ID + "=" + agenda_id,
					null);

		} else {

			id = db.insert(Note_Table, null, values);

		}

		return id;
	}

	/**
	 *  Function for adding MenuList
	 *  
	 * @param menu_name
	 * @param menu_image
	 * @return
	 */
	public long addMenuList(String menu_name,String menu_image,String menu_imageBlack) {
		ContentValues contentValues=new ContentValues();

		contentValues.put(MenuName, menu_name);
		contentValues.put(MenuImage, menu_image);
		contentValues.put(MenuImageBlack, menu_imageBlack);

		return db.insert(Menu_Table, null, contentValues);
	}

	//delete MenuList
	public void deleteMenuList() {
		db.execSQL(Drop_Menu);
	}

	public Cursor getAllMenuList() {

		Cursor c = db.query(Menu_Table, null, null, null, null, null, null);
		System.out.println("Cursor " + c.toString());
		return c;
	}


	/**
	 * Add list of speaker id which is connected to a track.
	 * 
	 * @param agenda_speaker_id
	 *            =Agenda Speaker Id
	 * @param agenda_id
	 *            = Agenda Id
	 * @param speaker_id
	 *            = Speaker Id
	 * @return
	 */
	public long addagendaspeaker(int agenda_speaker_id, int agenda_id,
			int speaker_id) {
		ContentValues values = new ContentValues();
		values.put(Agenda_Speaker_ID, agenda_speaker_id);
		values.put(Agenda_agenda_ID, agenda_id);
		values.put(Speaker_speaker_ID, speaker_id);
		return db.insert(Agenda_Speaker_Table, null, values);
	}

	/**
	 * Return agenda id arraylist as integer from myschedule table
	 * 
	 * @return
	 */
	public ArrayList<Integer> getMySchedule() {
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		Cursor c = db.query(MySchedule_Table, null, null, null, null, null,
				null);
		if(c != null)
		{
			while (c.moveToNext()) {
				arrayList.add(c.getInt(c.getColumnIndex(Agenda_ID)));
				//System.out.println("My ScheduleAgendaId "+ c.getInt(c.getColumnIndex(Agenda_ID)));
			}
		}
		c.close();
		return arrayList;
	}

	/**
	 * Count Agenda
	 * 
	 * @return
	 */
	public Integer getAgendaCount() {
		Cursor c = db.query(Agenda_Table, null, null, null, null, null, null);
		int count = 0;
		count = c.getCount();
		c.close();
		return count;
	}

	public Integer getSpeakerCount() {
		Cursor c = db.query(Speaker_Table, null, null, null, null, null, null);
		int count = 0;
		count = c.getCount();
		c.close();
		return count;
	}

	public ArrayList<String> getDifferentDate(String event_category) {
		ArrayList<String> arrayList = new ArrayList<String>();
		String sql = "select DISTINCT agenda_date from Agenda Where event_category='"+event_category+"'";
		Cursor c = db.rawQuery(sql, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(Agenda_DATE)));
		}
		c.close();
		return arrayList;
	}

	/**
	 * This method return note my agenda id.
	 * 
	 * @param agenda_id
	 * @return
	 */
	public String getNoteByAgendaId(int agenda_id) {
		String vnote = null;
		Cursor c = db.query(Note_Table, null, Agenda_ID + "=" + agenda_id,
				null, null, null, null);
		if (c.moveToNext()) {
			vnote = c.getString(c.getColumnIndex(Note_Text));
		}
		c.close();
		return vnote;
	}

	public String getNoteBySpeakerId(int speaker_id) {
		String vnote = null;
		Cursor c = db.query(Speaker_Table, null, Speaker_ID + "=" + speaker_id,
				null, null, null, null);
		if (c.moveToNext()) {
			vnote = c.getString(c.getColumnIndex(Speaker_Note));
		}
		c.close();
		return vnote;
	}

	/**
	 * This method check note is available in database or not
	 * 
	 * @param agenda_id
	 * @return
	 */
	public Boolean isNoteByAgendaIdavailable(int agenda_id) {
		boolean vnote = false;
		Cursor c = db.query(Note_Table, new String[] { Note_Text }, Agenda_ID
				+ "=" + agenda_id, null, null, null, null);
		if (c.getCount() > 0) {
			vnote = true;
		}

		c.close();
		return vnote;
	}

	/**
	 * This method will return a string of agenda info my agenda id.
	 * 
	 * @param agenda_id
	 * @param field
	 * @return
	 */
	public String getAgendaDataByAgendaId(int agenda_id, String field) {
		String data = null;
		Cursor c = db.query(Agenda_Table, null, Agenda_ID + "=" + agenda_id,
				null, null, null, null);
		if (c.moveToNext()) {
			data = c.getString(c.getColumnIndex(field));
		}
		c.close();
		return data;
	}

	/**
	 * This method will return arraylist of agenda field.
	 * 
	 * @param agenda_id
	 * @param field
	 * @return
	 */
	public ArrayList<String> getAgendaAllDataOffieldByField(String field) {

		AgendaValue = field;
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Agenda_Table, new String[] { AgendaValue }, null,
				null, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(AgendaValue)));
		}
		c.close();
		return arrayList;
	}

	public ArrayList<String> getAgendaAllDataOffieldByFieldAndDate(String date,
			String field ,String event_category) {

		AgendaValue = field;
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Agenda_Table, new String[] { AgendaValue },
				Agenda_DATE + "='" + date + "' AND event_category='"+event_category+"'", null, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(AgendaValue)));
		}
		c.close();
		return arrayList;
	}

	/**
	 * This method will check agenda is add in my schedule or not.
	 * 
	 * @param agenda_id
	 * @return
	 */
	public Boolean CheckMyScheduleByAgendaId(int agenda_id) {
		boolean check = false;
		Cursor c = db.query(MySchedule_Table, null,
				Agenda_ID + "=" + agenda_id, null, null, null, null);
		System.out.println(c.getCount());
		if (c.moveToNext()) {
			check = true;
		}
		c.close();
		return check;
	}

	public boolean deleteMyScheduleMultitrack(ArrayList<Integer> agenda_id) {
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		arrayList = agenda_id;
		for (int i = 0; i < arrayList.size(); i++) {
			db.delete(MySchedule_Table, Agenda_ID + "=" + arrayList.get(i),
					null);
		}

		return true;
	}

	public boolean deleteMySchedule(String agenda_id) {
		// ArrayList<Integer> arrayList=new ArrayList<Integer>();
		// arrayList=agenda_id;
		// for(int i=0;i<arrayList.size();i++)
		// {
		db.delete(MySchedule_Table, Agenda_ID + "=" + agenda_id, null);
		// }

		return true;
	}

	public ArrayList<Integer> getMyScheduleSortByTime() {
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		Cursor c = db
				.rawQuery(
						"SELECT * FROM agenda order by agenda_date,agenda_day,agenda_time",
						null);
		while (c.moveToNext()) {
			arrayList.add(c.getInt(c.getColumnIndex(Agenda_ID)));
		}
		c.close();
		return arrayList;
	}

	public Integer getSponsorCount() {
		Cursor c = db.query(Sponsor_Table, null, null, null, null, null, null);
		int count = 0;
		count = c.getCount();
		c.close();
		return count;
	}

	public ArrayList<String> getSponsorAllDataOffieldByField(String field) {

		AgendaValue = field;
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Sponsor_Table, new String[] { AgendaValue }, null,
				null, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(AgendaValue)));
		}
		c.close();
		return arrayList;
	}


	public ArrayList<String> getSponsorAllDataOffieldByFieldAndCategory(String field, String category,String eventCategory) {

		AgendaValue = field;
		ArrayList<String> arrayList = new ArrayList<String>();

		Cursor c = db.query(Sponsor_Table, new String[] { AgendaValue }, "event_category='"+eventCategory+"'",
				null, null, null, null);
		//Cursor c = db.rawQuery("SELECT "+AgendaValue+"  FROM "+Sponsor_Table+" WHERE sponsers_type=?", new String[]{category});
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(AgendaValue)));
		}
		c.close();
		return arrayList;
	}

	/**
	 * Get Speaker id by Agenda id
	 * 
	 * @param agenda_id
	 * @return
	 */
	public ArrayList<String> getSpeakerIdByAgendaId(int agenda_id) {
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Agenda_Speaker_Table, null, Agenda_agenda_ID + "="
				+ agenda_id, null, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(Speaker_speaker_ID)));
		}
		c.close();
		return arrayList;
	}






	public ArrayList<String> getProgramByeventcategry(String field,String event_category) {
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Program_Table, null, "event_category='"
				+ event_category+"'", null, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(field)));
		}
		c.close();
		return arrayList;
	}



	public ArrayList<String> getAgendaIdBySpeakerId(int speaker_id) {
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Agenda_Speaker_Table, null, Speaker_speaker_ID + "="
				+ speaker_id, null, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(Agenda_agenda_ID)));
		}
		c.close();
		return arrayList;
	}










	public Cursor getAllSpeakerFromSpeakerTable(String event_category) {

		Cursor c = db.query(Speaker_Table, null, " event_category='"+event_category+"'", null, null, null, null);
		return c;
	}

	/**
	 * you can add note for speaker
	 * 
	 * @param speaker_id
	 * @param note
	 * @return
	 */

	public long addSpeakerNote(int speaker_id, String note) {
		ContentValues values = new ContentValues();
		values.put(Speaker_ID, speaker_id);
		values.put(Speaker_Note, note);
		long id;
		Cursor c = db.query(Speaker_Table, new String[] { Speaker_Note },
				Speaker_ID + "=" + speaker_id, null, null, null, null);
		if (c.getCount() > 0) {
			// upda
			id = db.update(Speaker_Table, values,
					Speaker_ID + "=" + speaker_id, null);

		} else {

			id = db.insert(Note_Table, null, values);

		}

		return id;
	}

	public boolean hasSpeakerForThisAgenda(int agenda_id) {

		Cursor c = db.query(Agenda_Speaker_Table, null, Agenda_agenda_ID + "="
				+ agenda_id, null, null, null, null);
		if (c.getCount() > 0) {
			c.close();
			return true;
		} else {
			c.close();
			return false;
		}

	}

	public String getSpeakerInfoBySpeaker_id(int speaker_id, String field,String event_category) {
		String data = null;
		Cursor c = db.query(Speaker_Table, null, Speaker_ID + "=" + speaker_id + " AND event_category ='"+event_category+"'",
				null, null, null, null);
		if (c.moveToNext()) {
			data = c.getString(c.getColumnIndex(field));
		}
		c.close();
		return data;
	}

	public long addSpeaker(String speaker_id, String speaker_name,
			String speaker_designation, String speaker_description,
			String speaker_image_url,String speakerFB,String speakerTW,String speakerGP,String event_category) {
		ContentValues values = new ContentValues();

		values.put(Speaker_ID, speaker_id);
		values.put(Speaker_NAME, speaker_name);
		values.put(Speaker_Designation, speaker_designation);
		values.put(Speaker_Desription, speaker_description);
		values.put(Speaker_Image, speaker_image_url);
		values.put(Speaker_FB, speakerFB);
		values.put(Speaker_GM, speakerGP);
		values.put(Speaker_TW, speakerTW);
		values.put(Speaker_LK, "http://www.linkedin.com/");
		values.put("event_category", event_category);

		return db.insert(Speaker_Table, null, values);
	}

	public ArrayList<String> getSpeakerByfield(String field) {
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Speaker_Table, null, null, null, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(field)));
		}
		c.close();
		return arrayList;
	}

	public ArrayList<String> getSpeakerByfieldANDeventcategory(String field,String event_category) {
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Speaker_Table, null, "event_category='"+event_category+"'", null, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(field)));
		}
		c.close();
		return arrayList;
	}

	public ArrayList<String> getJuryByfieldAndType(String field,String type,String event_category) {
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Jury_Table, null, "jury_type=? AND event_category=?", new String[]{type,event_category}, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(field)));
		}
		c.close();
		return arrayList;
	}

	public ArrayList<String> getJuryByfield(String field,String event_category) {
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Jury_Table, null,"event_category=?", new String[]{event_category}, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(field)));
		}
		c.close();
		return arrayList;
	}

	public String getSpeakerDataByAgendaId(int Speaker_id, String field) {
		String data = null;
		Cursor c = db.query(Speaker_Table, null, Speaker_ID + "=" + Speaker_id,
				null, null, null, null);
		if (c.moveToNext()) {
			data = c.getString(c.getColumnIndex(field));
		}
		c.close();
		return data;
	}

	public ArrayList<String> getProgramByfieldANDeventcategory(String field,String event_category) {
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Program_Table, null, "event_category='"+event_category+"'", null, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(field)));
		}
		c.close();
		return arrayList;
	}

	public ArrayList<String> getprogramsByfieldAndEventCategory(String field,String event_category) {
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Program_Table, null, "event_category='"+event_category+"'", null, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(field)));
		}
		c.close();
		return arrayList;
	}

	public String getSpeakeSocialUrl(int Speaker_id, String field) {
		String data = null;
		Cursor c = db.query(Speaker_Table, null, Speaker_ID + "=" + Speaker_id,
				null, null, null, null);
		if (c.moveToNext()) {
			data = c.getString(c.getColumnIndex(field));
		}
		c.close();
		return data;
	}

	public ArrayList<Integer> getMultiTrackIdFromAgenda(String agenda_id) {
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		int track_id = 0;
		Cursor c = db.query(Agenda_Table, null, Agenda_ID + "=" + agenda_id,
				null, null, null, null);
		if (c.moveToNext()) {
			track_id = c.getInt(c.getColumnIndex(Agenda_Track_ID));
		}
		c.close();

		Cursor c1 = db.query(Agenda_Table, null, Agenda_Track_ID + "="
				+ track_id, null, null, null, null);

		while (c1.moveToNext()) {
			arrayList.add(c1.getInt(c1.getColumnIndex(Agenda_ID)));
		}
		c1.close();
		return arrayList;
	}

	public ArrayList<String> getSessionBySpeakerId(String speaker_id) {
		ArrayList<String> agendaid_arrayList = new ArrayList<String>();
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Agenda_Speaker_Table, null, Speaker_speaker_ID
				+ "=" + speaker_id, null, null, null, null);
		while (c.moveToNext()) {
			agendaid_arrayList.add(c.getString(c
					.getColumnIndex(Agenda_agenda_ID)));
		}
		c.close();
		for (int i = 0; i < agendaid_arrayList.size(); i++) {
			/*
			 * Cursor c1=db.query(Agenda_Table, null,
			 * Agenda_ID+"="+agendaid_arrayList.get(i), null, null, null, null);
			 * if(c1.moveToFirst()) {
			 * arrayList.add(c1.getString(c.getColumnIndex(Agenda_NAME))); }
			 * 
			 * 
			 * c1.close();
			 */

			arrayList.add(this.getAgendaDataByAgendaId(
					Integer.parseInt(agendaid_arrayList.get(i)), Agenda_NAME));
		}
		return arrayList;
	}

	public ArrayList<String> getSessionIdBySpeakerId(String speaker_id) {
		ArrayList<String> agendaid_arrayList = new ArrayList<String>();
		// ArrayList<String> arrayList=new ArrayList<String>();
		Cursor c = db.query(Agenda_Speaker_Table, null, Speaker_speaker_ID
				+ "=" + speaker_id, null, null, null, null);
		while (c.moveToNext()) {
			agendaid_arrayList.add(c.getString(c
					.getColumnIndex(Agenda_agenda_ID)));
		}
		c.close();

		return agendaid_arrayList;
	}

	// ///////////////**********Design Part**********////////////////////
	/**
	 * Add Page name and id into database
	 * 
	 * @param id
	 * @param name
	 * @return
	 */
	public long addPagename(int id, String name) {
		ContentValues values = new ContentValues();
		values.put(Page_Id, id);
		values.put(Page_Name, name);
		return db.insert(Page_Table, null, values);
	}

	/**
	 * Add Each page text and background color. Where h1=App Text
	 * Color,h2=Title(Header) Color,h3=Day/Date Color,h4=Content Header
	 * Color,h5=Content Text Color,h6=Time Text Color,BG=BackGround Color
	 * 
	 * @param page_id
	 * @param h1
	 *            =App Text Color
	 * @param h2
	 * @param h3
	 * @param h4
	 * @param h5
	 * @param h6
	 * @param bg
	 * @return
	 */
	public long addPageColor(int page_id, String h1, String h2, String h3,
			String h4, String h5, String h6, String bg) {
		ContentValues values = new ContentValues();
		values.put(Page_Id, page_id);
		values.put(H1, h1);
		values.put(H2, h2);
		values.put(H3, h3);
		values.put(H4, h4);
		values.put(H5, h5);
		values.put(H6, h6);
		values.put(BackGround, bg);
		return db.insert(Text_Color_Table, null, values);
	}

	public ArrayList<String> getPageName() {
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Page_Table, null, null, null, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(Page_Name)));
		}
		c.close();
		return arrayList;
	}

	public ArrayList<String> getPageId() {
		ArrayList<String> arrayList = new ArrayList<String>();
		Cursor c = db.query(Page_Table, null, null, null, null, null, null);
		while (c.moveToNext()) {
			arrayList.add(c.getString(c.getColumnIndex(Page_Id)));
		}
		c.close();
		return arrayList;
	}

	public String getColorByIdandField(int id, String field) {
		String color = null;
		Cursor c = db.query(Text_Color_Table, null, Page_Id + "=" + id, null,
				null, null, null);
		if (c.moveToNext()) {
			color = c.getString(c.getColumnIndex(field));
		}
		c.close();
		return color;
	}

	public long addJuryMember(String jury_id2, String jury_name2,
			String jury_info2, String jury_designation2, String jury_image2,
			String jury_type2,String event_category) {
		// TODO Auto-generated method stub

		ContentValues values=new ContentValues();
		values.put(Jury_ID, jury_id2);
		values.put(Jury_Name, jury_name2);
		values.put(Jury_info, jury_info2);
		values.put(Jury_Image, jury_image2);
		values.put(Jury_designation, jury_designation2);
		values.put(Jury_Type, jury_type2);
		values.put("event_category", event_category);
		//values.put(Jury_info, speaker_id);

		return db.insert(Jury_Table, null, values);

	}

}
