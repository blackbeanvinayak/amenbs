package com.blackbean.eventbuoy2.amen;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.blackbean.adapters.HomeAdapter;
import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;
import com.blackbean.utility.NetworkCheck;
import com.google.analytics.tracking.android.EasyTracker;

public class HomeActivity extends Activity {
	ListView listView;
	TextView headertext;
	ArrayList<String> pageArrayList;
	ArrayList<Integer> pageicons;
	//ArrayList<String> imageblack;
	Context context;
	public static int whichEvent;
	int pos;
	public static Activity homeActivity;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		context=this;
		homeActivity=this;
		String android_id = Secure.getString(context.getContentResolver(),
				Secure.ANDROID_ID); 
		System.out.println("Device id "+android_id);
		/*if (getIntent().getExtras() != null) {
			pushmessage();
		}*/
		whichEvent=getIntent().getIntExtra("WHICH_EVENT", 1);
		//generateSHAKEY();

		//Text Code For Sizes
		//int a=StaticData.getScreenSizeNumber(context);
		/*DataBase dataBase=new DataBase(context);
		dataBase.open();*/
		/*for (int i = 0; i < 4; i++) {
			dataBase.addMenuList("Menu Name"+i, "Menu Image"+i);
		}*/

		//Cursor c=dataBase.getAllMenuList();
		//Toast.makeText(context, ""+c.getCount(), Toast.LENGTH_LONG).show();


		pageArrayList = new ArrayList<String>();
		pageicons = new ArrayList<Integer>();
		//	imageblack = new ArrayList<String>();

		/*if(c!=null)
		{
			if (c.moveToFirst()) {
				do {
					pageArrayList.add(c.getString(1));
					pageicons.add(c.getString(2));
					imageblack.add(c.getString(3));
				} while (c.moveToNext());
			}
		}
		c.close();
		dataBase.close();*/
		headertext=(TextView) findViewById(R.id.header_txt);
		headertext.setTextSize(18.0f);
		if (whichEvent == StaticData.Event_Nairobi) {
			headertext.setText("Amen BS");
			//Enhancing Hospital Profitability\nThrough Quality And Accreditation

			pageArrayList.add("Agenda");
			pageArrayList.add("Speakers");
			//pageArrayList.add("Awards");
			//pageArrayList.add("Ticketing");
			pageArrayList.add("About");
			pageArrayList.add("Sponsors");
			pageArrayList.add("Exhibitors");
			//pageArrayList.add("Social");
			pageArrayList.add("Venue");
			pageArrayList.add("Contact");
			//pageArrayList.add("Travel Advisory");
			//pageArrayList.add("Attendee List");

			pageicons.add(R.drawable.agenda);
			pageicons.add(R.drawable.speaker);
			//pageicons.add(R.drawable.awaard);
			//pageicons.add(R.drawable.ticket);
			pageicons.add(R.drawable.abt);
			pageicons.add(R.drawable.partner);
			pageicons.add(R.drawable.exhibitor);
			//pageicons.add(R.drawable.social_san);
			pageicons.add(R.drawable.loc);
			pageicons.add(R.drawable.contact);
			//pageicons.add(R.drawable.travel);
			//pageicons.add(R.drawable.circle);
		}
		else if (whichEvent == StaticData.Event_Delhi) {
			headertext.setText("Medical Devices, Equipment and Technology");

			
			pageArrayList.add("Agenda");
			pageArrayList.add("Speakers");
			//pageArrayList.add("Programs");
			//pageArrayList.add("Awards");
			pageArrayList.add("About");
			pageArrayList.add("Sponsors");
			//pageArrayList.add("Social");
			//pageArrayList.add("Sankalp Connect");
			//pageArrayList.add("Exhibitors");
			//pageArrayList.add("My Schedule");
			pageArrayList.add("Venue");
			//pageArrayList.add("Ticketing");
			pageArrayList.add("Contact");
		//	pageArrayList.add("Coupon Code");
		//	pageArrayList.add("Accommodation");

			//pageArrayList.add("Ticketing");
			
			pageicons.add(R.drawable.agenda);
			pageicons.add(R.drawable.speaker);
			pageicons.add(R.drawable.abt);
			//pageicons.add(R.drawable.programs);// Programs
			//pageicons.add(R.drawable.awaard);
			pageicons.add(R.drawable.partner);
			//pageicons.add(R.drawable.social_san);
			//pageicons.add(R.drawable.connect); // Sankalp Connect
			//pageicons.add(R.drawable.exhibitor);
			//pageicons.add(R.drawable.myschedule);// My Schedule
			pageicons.add(R.drawable.loc);
			//pageicons.add(R.drawable.ticket);
			pageicons.add(R.drawable.contact);
			//pageicons.add(R.drawable.coupon);
			//pageicons.add(R.drawable.accommodation);

		}


		listView = (ListView) findViewById(R.id.lv_home);
		headertext = (TextView) findViewById(R.id.header_txt);
		// Typeface mFont = Typeface.createFromAsset(getAssets(),
		// "quicksand_regular.otf");
		// headertext.setTypeface(mFont);

		listView.setAdapter(new HomeAdapter(this, R.layout.adapter_home,
				pageArrayList, pageicons));
		listView.setDividerHeight(1);
		// new GetServerData(this).execute();

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				if (pageArrayList.get(position).equals("Agenda")) {

					pos=position;
					Intent intent = new Intent(HomeActivity.this,
							AgendaLocation.class);
					intent.putExtra("header", pageArrayList.get(pos));
					intent.putExtra("WHICH_EVENT", whichEvent);
					//	intent.putExtra("ImageName", imageblack.get(position));
					startActivity(intent);

				}
				if (pageArrayList.get(position).equals("My Schedule")) {

					/** GetServerData data=new GetServerData(HomeActivity.this);
					 * data.execute();*/

					Intent intent = new Intent(HomeActivity.this,
							MyScheduleActivity.class);
					intent.putExtra("header",pageArrayList.get(position));
					//intent.putExtra("ImageName", imageblack.get(position));
					//intent.putExtra("POSITION", position);

					startActivity(intent);

				}
				if (pageArrayList.get(position).equals("Speakers")) {


					pos=position;
					Intent intent = new Intent(HomeActivity.this,
							Speaker_Gridview.class);
					intent.putExtra("header", pageArrayList.get(pos));
					intent.putExtra("WHICH_EVENT", whichEvent);
					//	intent.putExtra("ImageName", imageblack.get(position));
					startActivity(intent);
				}

				/*if (pageArrayList.get(position).equals("REGISTER NOW")) {

					Intent intent = new Intent(HomeActivity.this,
							RegisterActivity.class);
					intent.putExtra("header", pageArrayList.get(position));
					//intent.putExtra("POSITION", position);
					intent.putExtra("ImageName", imageblack.get(position));
					startActivity(intent);

				}
				 */
				if (pageArrayList.get(position).equals("Sponsors")) {
					pos=position;
					Intent intent = new Intent(HomeActivity.this,
							SponsorActivity.class);
					intent.putExtra("header", pageArrayList.get(pos));
					//intent.putExtra("ImageName", imageblack.get(position));

					intent.putExtra("WHICH_EVENT", whichEvent);
					startActivity(intent);
				}
				if (pageArrayList.get(position).equals("Awards")) {
					Intent intent = new Intent(HomeActivity.this,
							NewAwardsActivity.class);
					intent.putExtra("header", pageArrayList.get(position));
					intent.putExtra("WHICH_EVENT", whichEvent);
					//intent.putExtra("ImageName", imageblack.get(position));
					startActivity(intent);
				}
				if (pageArrayList.get(position).equals("Ticketing")) 
				{
					Intent intent = new Intent(HomeActivity.this,
							TicketingActivity.class);
					intent.putExtra("header", pageArrayList.get(position));
					if(whichEvent == StaticData.Event_Nairobi)
					{
						intent.putExtra("url", "http://www.eventbrite.com/e/sankalp-africa-summit-2015-nairobi-kenya-tickets-13381531521?aff=SFWeb");
					}
					else if(whichEvent == StaticData.Event_Delhi){
						intent.putExtra("url", "http://www.caret.co.in/e/event/global-sankalp-summit-2015/");
					}
					//intent.putExtra("POSITION", position);
					//intent.putExtra("ImageName", imageblack.get(position));
					startActivity(intent);

				}

				if (pageArrayList.get(position).equals("Travel Advisory")) 
				{
					Intent intent = new Intent(HomeActivity.this,
							TraveladvisoryActivity.class);
					intent.putExtra("header", pageArrayList.get(position));
					//intent.putExtra("POSITION", position);
					//intent.putExtra("ImageName", imageblack.get(position));
					startActivity(intent);

				}
				/*if (pageArrayList.get(position).equals("SURVEY")) {
					if (NetworkCheck.getConnectivityStatusString(HomeActivity.this)) {
						Intent intent = new Intent(HomeActivity.this,
								SurveyActivity.class);
						//intent.putExtra("POSITION", position);
						intent.putExtra("ImageName", imageblack.get(position));
						startActivity(intent);
					}
					else {
						Toast.makeText(context, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
					}


				}*/
				if (pageArrayList.get(position).equals("About")) {
					Intent intent = new Intent(HomeActivity.this,
							Connect_Social.class);
					/*intent.putExtra("header", pageArrayList.get(position));
					intent.putExtra("WHICH_EVENT",whichEvent);*/
					intent.putExtra("social","About");
					if (whichEvent == StaticData.Event_Nairobi) {
						intent.putExtra("url", "file:///android_asset/about1.html");	
					}else {
						intent.putExtra("url", "file:///android_asset/about2.html");
					}
					
					startActivity(intent);
				}
				if (pageArrayList.get(position).equals("Venue")) {

					if (NetworkCheck.getConnectivityStatusString(HomeActivity.this)) {
						Intent intent = new Intent(HomeActivity.this,
								VenueActivity.class);
						intent.putExtra("header", pageArrayList.get(position));
					/*	if (whichEvent == StaticData.Event_Nairobi) {
							intent.putExtra("lat", "28.618535");
							intent.putExtra("long", "77.244736");
						}else if (whichEvent == StaticData.Event_Delhi) {
							intent.putExtra("lat", "28.618535");
							intent.putExtra("long", "77.244736");
						}*/
						//	intent.putExtra("ImageName", imageblack.get(position));
						startActivity(intent);
					}else {
						Toast.makeText(context, "Please check your internet connection.", Toast.LENGTH_SHORT).show();
					}

				}

				if (pageArrayList.get(position).equals("Social")) {
					Intent intent = new Intent(context, SocialMenuActivity.class);
					intent.putExtra("header", pageArrayList.get(position));
					if(whichEvent == StaticData.Event_Nairobi)
					{
						intent.putExtra("urlFb", "https://m.facebook.com/SankalpForum");
						intent.putExtra("urlTw", "https://twitter.com/search?q=%23SFAfrica&src=hash");
						intent.putExtra("urlLink", "https://www.linkedin.com/groups?gid=6611976&mostPopular=&trk=tyah&trkInfo=idx%3A1-2-2%2CtarId%3A1422639599150%2Ctas%3Asankalp+&trk=tyah&trkInfo=idx%3A1-2-2%2CtarId%3A1422639599150%2Ctas%3Asankalp+");
					}else if (whichEvent == StaticData.Event_Delhi) {
						intent.putExtra("urlFb", "https://m.facebook.com/SankalpForum");
						intent.putExtra("urlTw", "https://twitter.com/search?q=%23SFGlobal&src=hash");
						intent.putExtra("urlLink","https://www.linkedin.com/groups?gid=1811096&mostPopular=&trk=tyah&trkInfo=idx%3A3-1-3%2CtarId%3A1422639572073%2Ctas%3Asankalp+forum");
					}
					startActivity(intent);
				}

				if (pageArrayList.get(position).equals("Attendee List")) {
					Intent intent = new Intent(context, AttendeeList.class);
					intent.putExtra("social", pageArrayList.get(position));
					intent.putExtra("Url", "https://docs.google.com/a/myblackbean.com/spreadsheets/d/1xWg6afIl1SxHOJpIu4Ma3695E5Y7HP05j88W_TDI7lA/edit#gid=1525865416");
					startActivity(intent);
				}

				/*if (pageArrayList.get(position).equals("NOTE")) {
					Intent intent = new Intent(HomeActivity.this,
							NoteActivity.class);
					intent.putExtra("header", pageArrayList.get(position));
					//	intent.putExtra("POSITION", position);
					intent.putExtra("ImageName", imageblack.get(position));
					startActivity(intent);
					//finish();
				}
				 */
				if (pageArrayList.get(position).equals("Sankalp Connect")) {
					Intent intent = new Intent(HomeActivity.this,
							SankalpConnect.class);
					intent.putExtra("header", pageArrayList.get(position));

					startActivity(intent);
				}

				if (pageArrayList.get(position).equals("Programs")) {
					Intent intent = new Intent(HomeActivity.this,
							Program_Gridview.class);
					intent.putExtra("WHICH_EVENT", whichEvent);
					intent.putExtra("header", pageArrayList.get(position));
					startActivity(intent);
				}
				if (pageArrayList.get(position).equals("Contact")) {
					Intent intent = new Intent(context, ContactActivity.class);
					/*intent.putExtra("social", "Contact");
					intent.putExtra("url", "file:///android_asset/contact.html");*/
					startActivity(intent);

				}

				if (pageArrayList.get(position).equals("Coupon Code")) {
					Intent intent=new Intent(context, Connect_Social.class);
					intent.putExtra("social","Coupon Code");
					intent.putExtra("url", "file:///android_asset/coupon.html");
					startActivity(intent);
				}
				if (pageArrayList.get(position).equals("Accommodation")) {
					Intent intent=new Intent(context, AccomodationActivity.class);
					/*intent.putExtra("social","Accommodation");
					intent.putExtra("url", "file:///android_asset/accommodation.html");*/
					startActivity(intent);
				}
				if (pageArrayList.get(position).equals("Exhibitors")) {
					Intent intent=new Intent(context, Connect_Social.class);
					intent.putExtra("social","Exhibitor");
					intent.putExtra("url", "http://amen.eventbuoy.com/exhibitors.html");
					startActivity(intent);
				}
			}
		});
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

	public void pushmessage() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(getIntent().getStringExtra("alert"));
		builder.setMessage(getIntent().getStringExtra("message"));
		builder.setPositiveButton("Cancel",
				new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});

		builder.show();
	}

	public void generateSHAKEY() {
		PackageInfo info;
		try {

			info = getPackageManager().getPackageInfo(
					"com.blackbean.eventbuoy2.sankalp", PackageManager.GET_SIGNATURES);

			for (Signature signature : info.signatures) {
				MessageDigest md;
				md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				String something = new String(Base64.encode(md.digest(), 0));
				// String something = new
				// String(Base64.encodeBytes(md.digest()));
				Log.e("Hash key", something);
			}

		} catch (NameNotFoundException e1) {
			Log.e("name not found", e1.toString());
		} catch (NoSuchAlgorithmException e) {
			Log.e("no such an algorithm", e.toString());
		} catch (Exception e) {
			Log.e("exception", e.toString());
		}
	}

	/*private class UpdateAgenda extends AsyncTask<Void, Void, Void>
	{

		String val = null;
		JSONObject jsonObject = null;
		JSONParser jsonParser = new JSONParser();
		JSONParserString jsonParserString=new JSONParserString();

		JSONArray response_json_array_Agenda = null;
		JSONArray response_json_array_Speaker = null;
		JSONArray response_json_array_agenda_speaker = null;

		ArrayList<String> speaker_image_urlArrayList;

		int oldCount=Integer.valueOf(SetGet.readUsingSharedPreference(context, "EVENT_COUNT").trim());
		int newCount=0;
		DataBase db=new DataBase(context);

		//Image Download variables
		private List<Bitmap> imagebitmap = new ArrayList<Bitmap>();// To Get ImageBitamap
		// data from
		// Server
		// private List<Bitmap> sponsersimagebitmap = new ArrayList<Bitmap>();

		private ArrayList<String> imageURL;// Image URL
		//String folder_name;// Folder Name to to store images in SD card
		//int code;// code represents 0) Sponsers images 1)Speaker images 2)jury
		//boolean check;
		//ProgressDialog progressDialog;
		Dialog dialog;


		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			//check = false;
			dialog = new Dialog(context);
			ProgressBar bar = new ProgressBar(context);		
			dialog.setContentView(bar);
			dialog.setTitle("Downloading Please Wait ...");
			dialog.setCancelable(false);
			dialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			// Get the value of Event Details Update Count
			try {
				val = jsonParserString
						.getJSONFromUrl("http://shrm.eventbuoy.com/admin/get_event_details_updatecount.php");
				System.out.println("Agenda Event Count "+val);
				newCount=Integer.valueOf(val.trim());
			}catch(Exception e)
			{
				System.out.println("Count Exception "+e);
				e.printStackTrace();
				System.out.println("Agenda Event Count "+val);
			}


			if (oldCount<newCount) {
				System.out.println("In true");
				SetGet.storeUsingSharedPreference(context, "EVENT_COUNT", val.trim());
				db.open();

				//delete Tables Agenda,Speaker and AgendaSpeaker
				db.deleteAgendaTable();
				db.deleteSpeakerTable();
				db.deleteAgenda_SpeakerTable();

				// Delete SpeakerImage Folder
				File dir = new File(Environment.getExternalStorageDirectory()
						.getPath() + "/shrm/speakers");
				if (dir.isDirectory()) {
					String[] children = dir.list();
					for (int i = 0; i < children.length; i++) {
						new File(dir, children[i]).delete();
					}
				}

				// Getting Updated Data from server

				try {
					//db.CreateTables_Agenda_SPeaker_AgendaSpeaker();
					db.CreateTables_Agenda_SPeaker_AgendaSpeaker();
					jsonObject = jsonParser
							.getJSONFromUrl("http://shrm.eventbuoy.com/admin/get_eventdetails_speakers_agendaspeaker.php");
					if (jsonObject != null) {
						System.out.println("Agenda :" + jsonObject);
						response_json_array_Agenda = jsonObject
								.getJSONArray("event_details");
						response_json_array_Speaker = jsonObject
								.getJSONArray("speakers");
						response_json_array_agenda_speaker=jsonObject
								.getJSONArray("agenda_speaker");


						speaker_image_urlArrayList = new ArrayList<String>();


						// * Get Agenda From Server Side by loop
						ArrayList<Integer>agendaID=new ArrayList<Integer>();
						ArrayList<String>agendaName=new ArrayList<String>();
						String agenda_id = null, agenda_name, agenda_location, agenda_day, agenda_date, agenda_time, agenda_description, agenda_order, agenda_track_id;
						for (int i = 0; i < response_json_array_Agenda.length(); i++) {
							JSONObject agenda_temp = new JSONObject();
							try {
								agenda_temp = response_json_array_Agenda
										.getJSONObject(i);

								agenda_id = agenda_temp.getString("event_id");
								agendaID.add(Integer.valueOf(agenda_id));
								agenda_name = agenda_temp.getString("event_name");
								agendaName.add(agenda_name);
								agenda_location = agenda_temp
										.getString("event_location");
								agenda_day = agenda_temp.getString("event_day");
								agenda_date = agenda_temp.getString("event_date");
								agenda_time = agenda_temp.getString("event_time");
								agenda_description = agenda_temp
										.getString("event_description");
								// agenda_order = agenda_temp.getString("event_order");
								// agenda_track_id = agenda_temp
								// .getString("agenda_track_id");
								db.addAgenda(agenda_id, agenda_name,
										agenda_location, agenda_date, agenda_day,
										agenda_time, agenda_description, "", "");

							} catch (JSONException e) {
								System.out.println("Json Exception :" + e);
							}
						}

						//My Schedule Update
						ArrayList<Integer>agendaId_FromMyScehedule=db.getMySchedule();
						boolean flag=false;
						for (int i = 0; i < agendaId_FromMyScehedule.size(); i++) {
							for (int j = 0; j < agendaID.size(); j++) {
								if (agendaID.get(j)==agendaId_FromMyScehedule.get(i)) {
									flag=false;
									break;
								}
								else {
									flag=true;
								}

							}
							if (flag) {
								db.deleteMySchedule(String.valueOf(agendaId_FromMyScehedule.get(i)));	
							}

						}

						//Note Table Update

						ArrayList<NoteModel>agendaId_FromNote=db.getNoteByPlace("AGENDA");
						boolean noteFlag=false;
						for (int i = 0; i < agendaId_FromNote.size(); i++) {
							for (int j = 0; j < agendaID.size(); j++) {
								if (agendaID.get(j)==Integer.valueOf(agendaId_FromNote.get(i).getNote_palceId())) {
									noteFlag=false;
									db.UpdateNote1byPlaceAndPlaceId(agendaName.get(j), "AGENDA", agendaID.get(j));
									break;
								}
								else {
									noteFlag=true;
								}
							}

							if (noteFlag) {
								db.deleteNoteByPlaceId(String.valueOf(agendaId_FromNote.get(i)));	
							}


						}


						String speaker_id, speaker_name, speaker_designation, speaker_description, speaker_image, speaker_fb, speaker_twitter, speaker_google, speaker_linkedin, speaker_order, speaker_track_id;

						// Get Speakers From Server side by loop

						for (int i = 0; i < response_json_array_Speaker.length(); i++) {
							JSONObject speaker_temp = new JSONObject();
							try {
								speaker_temp = response_json_array_Speaker
										.getJSONObject(i);

								speaker_id = speaker_temp.getString("speaker_id");
								speaker_name = speaker_temp.getString("speaker_name");
								speaker_designation = speaker_temp
										.getString("speaker_detail1");
								speaker_description = speaker_temp
										.getString("speaker_info");
								speaker_image = speaker_temp
										.getString("speaker_image_url");
								speaker_fb=speaker_temp
										.getString("speaker_facebook");
								speaker_google=speaker_temp
										.getString("speaker_googleplus");
								speaker_twitter=speaker_temp
										.getString("speaker_twitter");

								System.out.println("speaker image" + speaker_image);
								speaker_image_urlArrayList.add(speaker_image);

								db.addSpeaker(speaker_id, speaker_name,
										speaker_designation, speaker_description,
										speaker_image,speaker_fb,speaker_twitter,speaker_google);

							} catch (JSONException e) {
								System.out.println("Json Exception :" + e);
							}
						}
						SetGet.setSpeakerImageURL(null);
						SetGet.setSpeakerImageURL(speaker_image_urlArrayList);

						// Get Agenda_Speakers From Server side by loop

						int agenda_speaker_id, agenda_agenada_id, speaker_speaker_id;

						for (int i = 0; i < response_json_array_agenda_speaker.length(); i++) {
							JSONObject agenda_temp = new JSONObject();
							try {
								agenda_temp = response_json_array_agenda_speaker
										.getJSONObject(i);

								agenda_speaker_id = agenda_temp
									.getInt("agenda_speaker_id");
								agenda_speaker_id=i;
								agenda_agenada_id = agenda_temp.getInt("agenda_id");
								speaker_speaker_id = agenda_temp.getInt("speaker_id");

								db.addagendaspeaker(agenda_speaker_id,
										agenda_agenada_id, speaker_speaker_id);

							} catch (JSONException e) {
								System.out.println("Json Exception :" + e);
							}
						}					

						db.close();
						if (oldCount<newCount) {
							DownloadImages obj=new DownloadImages(context, SetGet.getSpeakerImageURL(), 
									1, null);
							obj.execute();
						}
						imageURL=SetGet.getSpeakerImageURL();
						File icons = new File(Environment.getExternalStorageDirectory()
								.getPath() + "/shrm/speakers");
						if (!icons.exists()) {
							icons.mkdirs();
						}
						try {
							for (int i = 0; i < imageURL.size(); i++) {
								URL imageUrl = new URL(
										"http://shrm.eventbuoy.com/uploads/"
												+ imageURL.get(i));

								//imagebitmap.add(i, BitmapFactory.decodeStream(imageUrl
								//		.openConnection().getInputStream()));
								imagebitmap.add(i,getBitmapFromURL(imageUrl.toString()));


							}

	 * File icons = new
	 * File(Environment.getExternalStorageDirectory() .getPath()
	 * +File.pathSeparator+folder_name); if (!icons.exists()) {
	 * icons.mkdirs(); }


							for (int i = 0; i < imageURL.size(); i++) {

								storeImageSpeaker(imagebitmap.get(i), imageURL.get(i));
							}
							//check = true;
						} catch (Exception e) {
							//check = false;
							e.printStackTrace();
						}

					}

				}catch(Exception e)
				{
					e.printStackTrace();
				}

			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
			imagebitmap.clear();
			Intent intent = new Intent(HomeActivity.this,
					AgendaActivity.class);
			intent.putExtra("header", pageArrayList.get(pos));
			//intent.putExtra("POSITION", pos);
			intent.putExtra("ImageName", imageblack.get(pos));
			startActivity(intent);
		}

	}*/
	/*
	private class UpdateSpeaker extends AsyncTask<Void, Void, Void>
	{
		DataBase db=new DataBase(context);
		String val = null;
		JSONObject jsonObject = null;
		JSONParser jsonParser = new JSONParser();
		JSONParserString jsonParserString=new JSONParserString();

		Dialog dialog;

		int oldCount=Integer.valueOf(SetGet.readUsingSharedPreference(context, "SPEAKER_COUNT").trim());
		int newCount=0;


		JSONArray response_json_array_Agenda = null;
		JSONArray response_json_array_Speaker = null;
		JSONArray response_json_array_agenda_speaker = null;
		private ArrayList<String> imageURL;
		private List<Bitmap> imagebitmap = new ArrayList<Bitmap>();

		ArrayList<String> speaker_image_urlArrayList;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new Dialog(context);
			ProgressBar bar = new ProgressBar(context);		
			dialog.setContentView(bar);
			dialog.setTitle("Downloading Please Wait ...");
			dialog.setCancelable(false);
			dialog.show();

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			// Get the value of Speaker Update Count
			try {
				val = jsonParserString
						.getJSONFromUrl("http://shrm.eventbuoy.com/admin/get_speakers_update_count.php");
				System.out.println("Speaker Count "+val);
				newCount=Integer.valueOf(val.trim());
			}catch(Exception e)
			{
				System.out.println("Count Exception "+e);
				e.printStackTrace();
				System.out.println("Speaker Count "+val);
			}


			if (oldCount<newCount) {

				SetGet.storeUsingSharedPreference(context, "SPEAKER_COUNT", val.trim());
				db.open();

				//delete Tables Agenda,Speaker and AgendaSpeaker
				db.deleteAgendaTable();
				db.deleteSpeakerTable();
				db.deleteAgenda_SpeakerTable();

				// Delete SpeakerImage Folder
				File dir = new File(Environment.getExternalStorageDirectory()
						.getPath() + "/shrm/speakers");
				if (dir.isDirectory()) {
					String[] children = dir.list();
					for (int i = 0; i < children.length; i++) {
						new File(dir, children[i]).delete();
					}
				}

				//Getting Updated data of Speaker from Server by url
				try {
					db.CreateTables_Agenda_SPeaker_AgendaSpeaker();
					jsonObject = jsonParser
							.getJSONFromUrl("http://shrm.eventbuoy.com/admin/get_eventdetails_speakers_agendaspeaker.php");
					if (jsonObject != null) {
						System.out.println("Agenda :" + jsonObject);
						response_json_array_Agenda = jsonObject
								.getJSONArray("event_details");
						response_json_array_Speaker = jsonObject
								.getJSONArray("speakers");
						response_json_array_agenda_speaker=jsonObject
								.getJSONArray("agenda_speaker");


						speaker_image_urlArrayList = new ArrayList<String>();


						// * Get Agenda From Server Side by loop
						ArrayList<Integer>agendaID=new ArrayList<Integer>();
						ArrayList<String>agendaName=new ArrayList<String>();
						String agenda_id = null, agenda_name, agenda_location, agenda_day, agenda_date, agenda_time, agenda_description, agenda_order, agenda_track_id;
						for (int i = 0; i < response_json_array_Agenda.length(); i++) {
							JSONObject agenda_temp = new JSONObject();
							try {
								agenda_temp = response_json_array_Agenda
										.getJSONObject(i);

								agenda_id = agenda_temp.getString("event_id");
								agendaID.add(Integer.valueOf(agenda_id));
								agenda_name = agenda_temp.getString("event_name");
								agendaName.add(agenda_name);
								agenda_location = agenda_temp
										.getString("event_location");
								agenda_day = agenda_temp.getString("event_day");
								agenda_date = agenda_temp.getString("event_date");
								agenda_time = agenda_temp.getString("event_time");
								agenda_description = agenda_temp
										.getString("event_description");
								// agenda_order = agenda_temp.getString("event_order");
								// agenda_track_id = agenda_temp
								// .getString("agenda_track_id");
								db.addAgenda(agenda_id, agenda_name,
										agenda_location, agenda_date, agenda_day,
										agenda_time, agenda_description, "", "");

							} catch (JSONException e) {
								System.out.println("Json Exception :" + e);
							}
						}




						String speaker_id, speaker_name, speaker_designation, speaker_description, speaker_image, speaker_fb, speaker_twitter, speaker_google, speaker_linkedin, speaker_order, speaker_track_id;
						ArrayList<String>speakerName=new ArrayList<String>();
						ArrayList<Integer>speakerId=new ArrayList<Integer>();
						// Get Speakers From Server side by loop

						for (int i = 0; i < response_json_array_Speaker.length(); i++) {
							JSONObject speaker_temp = new JSONObject();
							try {
								speaker_temp = response_json_array_Speaker
										.getJSONObject(i);

								speaker_id = speaker_temp.getString("speaker_id");
								speakerId.add(Integer.valueOf(speaker_id));
								speaker_name = speaker_temp.getString("speaker_name");
								speakerName.add(speaker_name);
								speaker_designation = speaker_temp
										.getString("speaker_detail1");
								speaker_description = speaker_temp
										.getString("speaker_info");
								speaker_image = speaker_temp
										.getString("speaker_image_url");
								speaker_fb=speaker_temp
										.getString("speaker_facebook");
								speaker_google=speaker_temp
										.getString("speaker_googleplus");
								speaker_twitter=speaker_temp
										.getString("speaker_twitter");
								System.out.println("speaker image" + speaker_image);
								speaker_image_urlArrayList.add(speaker_image);

								db.addSpeaker(speaker_id, speaker_name,
										speaker_designation, speaker_description,
										speaker_image,speaker_fb,speaker_twitter,speaker_google);

							} catch (JSONException e) {
								System.out.println("Json Exception :" + e);
							}
						}
						SetGet.setSpeakerImageURL(null);
						SetGet.setSpeakerImageURL(speaker_image_urlArrayList);



						//Note Table Update

						ArrayList<NoteModel>speakerId_FromNote=db.getNoteByPlace("SPEAKER");
						boolean noteFlag=false;
						for (int i = 0; i < speakerId_FromNote.size(); i++) {
							for (int j = 0; j < speakerId.size(); j++) {
								if (speakerId.get(j)==Integer.valueOf(speakerId_FromNote.get(i).getNote_palceId())) {
									noteFlag=false;
									db.UpdateNote1byPlaceAndPlaceId(speakerName.get(j), "SPEAKER", speakerId.get(j));
									break;
								}
								else {
									noteFlag=true;
								}
							}

							if (noteFlag) {
								db.deleteNoteByPlaceId(String.valueOf(speakerId_FromNote.get(i)));	
							}


						}

						// Get Agenda_Speakers From Server side by loop

						int agenda_speaker_id, agenda_agenada_id, speaker_speaker_id;

						for (int i = 0; i < response_json_array_agenda_speaker.length(); i++) {
							JSONObject agenda_temp = new JSONObject();
							try {
								agenda_temp = response_json_array_agenda_speaker
										.getJSONObject(i);

								agenda_speaker_id = agenda_temp
									.getInt("agenda_speaker_id");
								agenda_speaker_id=i;
								agenda_agenada_id = agenda_temp.getInt("agenda_id");
								speaker_speaker_id = agenda_temp.getInt("speaker_id");

								db.addagendaspeaker(agenda_speaker_id,
										agenda_agenada_id, speaker_speaker_id);

							} catch (JSONException e) {
								System.out.println("Json Exception :" + e);
							}
						}					

						db.close();
						if (oldCount<newCount) {
							DownloadImages obj=new DownloadImages(context, SetGet.getSpeakerImageURL(), 
									1, null);
							obj.execute();
						}
						imageURL=SetGet.getSpeakerImageURL();
						File icons = new File(Environment.getExternalStorageDirectory()
								.getPath() + "/shrm/speakers");
						if (!icons.exists()) {
							icons.mkdirs();
						}
						try {
							for (int i = 0; i < imageURL.size(); i++) {
								URL imageUrl = new URL(
										"http://shrm.eventbuoy.com/uploads/"
												+ imageURL.get(i));

								//imagebitmap.add(i, BitmapFactory.decodeStream(imageUrl
								//		.openConnection().getInputStream()));
								imagebitmap.add(i,getBitmapFromURL(imageUrl.toString()));


							}

	 * File icons = new
	 * File(Environment.getExternalStorageDirectory() .getPath()
	 * +File.pathSeparator+folder_name); if (!icons.exists()) {
	 * icons.mkdirs(); }


							for (int i = 0; i < imageURL.size(); i++) {

								storeImageSpeaker(imagebitmap.get(i), imageURL.get(i));
							}
							//check = true;
						} catch (Exception e) {
							//check = false;
							e.printStackTrace();
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
				}


			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
			imagebitmap.clear();
			Intent intent = new Intent(HomeActivity.this,
					Speaker_Gridview.class);
			intent.putExtra("header", pageArrayList.get(pos));
			//intent.putExtra("POSITION", pos);
			intent.putExtra("ImageName", imageblack.get(pos));
			startActivity(intent);
		}
	}


	private class UpdatePartners extends AsyncTask<Void, Void, Void>
	{
		private static final String Sponsers_Id = "sponsers_id";
		private static final String Sponsers_Name = "sponsers_name";
		private static final String Sponsers_Type = "sponsers_type";
		private static final String Sponsers_Image = "sponsers_image";
		private static final String Sponsers_Description = "sponsers_description";
		private static final String Sponsers_Url = "sponsers_url";
		private static final String Sponsers_Order = "sponsers_order";


		Dialog dialog;
		String val = null;
		JSONObject jsonObject = null;
		JSONParser jsonParser = new JSONParser();
		JSONParserString jsonParserString=new JSONParserString();

		JSONArray response_json_array_Sponsor = null;
		ArrayList<String> sponsor_image_urlaArrayList;
		ArrayList<String> imageURL;
		private List<Bitmap> imagebitmap = new ArrayList<Bitmap>();


		int oldCount=Integer.valueOf(SetGet.readUsingSharedPreference(context, "PARTNER_COUNT").trim());
		int newCount=0;
		DataBase db=new DataBase(context);

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new Dialog(context);
			ProgressBar bar = new ProgressBar(context);		
			dialog.setContentView(bar);
			dialog.setTitle("Downloading Please Wait ...");
			dialog.setCancelable(false);
			dialog.show();

		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			// Get the value of Partners Update Count
			try {
				val=jsonParserString.getJSONFromUrl("http://shrm.eventbuoy.com/admin/get_sponsors_updatecount.php");
				newCount=Integer.valueOf(val.trim());
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}

			if (oldCount<newCount) {
				SetGet.storeUsingSharedPreference(context, "PARTNER_COUNT", val.trim());
				db.open();

				//delete Partner table
				db.deletePartnerTable();

				//deleting images for Partners
				File dir = new File(Environment.getExternalStorageDirectory()
						.getPath() + "/shrm/sponsers");
				if (dir.isDirectory()) {
					String[] children = dir.list();
					for (int i = 0; i < children.length; i++) {
						new File(dir, children[i]).delete();
					}
				}

				try {
					// Getting Updated Data from server
					db.CreateTablePartner();

					jsonObject=jsonParser.getJSONFromUrl("http://shrm.eventbuoy.com/admin/getSponsors.php");
					if(jsonObject != null)
					{
						response_json_array_Sponsor=jsonObject.getJSONArray("sponsors_detais");
						String sponsor_id, sponsor_name, sponsor_type, sponsor_image, sponsor_description, sponsor_url, sponsor_order;
						sponsor_image_urlaArrayList=new ArrayList<String>();

						for (int i = 0; i < response_json_array_Sponsor.length(); i++) {
							JSONObject sponsor_temp = new JSONObject();
							try {
								sponsor_temp = response_json_array_Sponsor
										.getJSONObject(i);

								sponsor_id = sponsor_temp.getString("sponsor_id");
								sponsor_name = sponsor_temp.getString("sponsor_name");

								SetGet.sponsorNameaArrayList.add(sponsor_name);

								sponsor_type = sponsor_temp.getString("sponsor_type");

								SetGet.sponsorTypeArrayList.add(sponsor_type);

								sponsor_image = sponsor_temp.getString("sponsor_logo");
								// sponsor_image_urlaArrayList.add(sponsor_image);
								sponsor_url = sponsor_temp.getString("sponsor_url");
								sponsor_order = sponsor_temp.getString("sponsor_order");

								sponsor_image_urlaArrayList.add(sponsor_image);

								HashMap<String, String> hashMap_sponsor_id = new HashMap<String, String>();
								hashMap_sponsor_id.put(Sponsers_Id, sponsor_id);

								HashMap<String, String> hashMap_sponsor_name = new HashMap<String, String>();
								hashMap_sponsor_name.put(Sponsers_Name, sponsor_name);

								HashMap<String, String> hashMap_sponsor_type = new HashMap<String, String>();
								hashMap_sponsor_type.put(Sponsers_Type, sponsor_type);

								HashMap<String, String> hashMap_sponsor_image = new HashMap<String, String>();
								hashMap_sponsor_image
								.put(Sponsers_Image, sponsor_image);

								HashMap<String, String> hashMap_sponsor_Order = new HashMap<String, String>();
								hashMap_sponsor_Order
								.put(Sponsers_Order, sponsor_order);

								HashMap<String, String> hashMap_sponsor_Url = new HashMap<String, String>();
								hashMap_sponsor_Url.put(Sponsers_Url, sponsor_url);

								ArrayList<HashMap<String, String>> Sponsors_arrayList = new ArrayList<HashMap<String, String>>();

								Sponsors_arrayList.add(hashMap_sponsor_id);
								Sponsors_arrayList.add(hashMap_sponsor_name);
								Sponsors_arrayList.add(hashMap_sponsor_type);
								Sponsors_arrayList.add(hashMap_sponsor_image);
								Sponsors_arrayList.add(hashMap_sponsor_Order);
								Sponsors_arrayList.add(hashMap_sponsor_Url);

								db.addSponsors(Sponsors_arrayList);

							} catch (JSONException e) {
								System.out.println("Json Exception :" + e);
							}


						}

					}

					db.close();

					imageURL=SetGet.getSponsorImageURL();
					File icons = new File(Environment.getExternalStorageDirectory()
							.getPath() + "/shrm/sponsers");
					if (!icons.exists()) {
						icons.mkdirs();
					}
					try {
						for (int i = 0; i < imageURL.size(); i++) {
							URL imageUrl = new URL(
									"http://shrm.eventbuoy.com/uploads/"
											+ imageURL.get(i));

							//imagebitmap.add(i, BitmapFactory.decodeStream(imageUrl
							//		.openConnection().getInputStream()));
							imagebitmap.add(i,getBitmapFromURL(imageUrl.toString()));
						}

						for (int i = 0; i < imageURL.size(); i++) {

							storeImagePartner(imagebitmap.get(i), imageURL.get(i));
						}
						//check = true;
					} catch (Exception e) {
						//check = false;
						e.printStackTrace();
					}

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

			}


			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();
			imagebitmap.clear();
			Intent intent = new Intent(HomeActivity.this,
					SponsorActivity.class);
			intent.putExtra("header", pageArrayList.get(pos));
			//	intent.putExtra("POSITION", pos);
			intent.putExtra("ImageName", imageblack.get(pos));
			startActivity(intent);
		}

	}




	private void storeImageSpeaker(Bitmap image, String name) {
		String path = null;

		path = "/shrm/speakers/";

		File pictureFile = new File(Environment.getExternalStorageDirectory()
				.getPath() + path + name);
		if (pictureFile.exists())
		{

			pictureFile.delete();

		}

		if (!pictureFile.exists()) {

			try {
				pictureFile.createNewFile();
				FileOutputStream fos = new FileOutputStream(pictureFile);
				image.compress(Bitmap.CompressFormat.PNG, 90, fos);
				System.out.println(pictureFile.getName() + "  "
						+ "download complete");
				fos.close();
			} catch (FileNotFoundException e) {
				Log.d("", "File not found: " + e.getMessage());
			} catch (IOException e) {
				Log.d("", "Error accessing file: " + e.getMessage());
			}
		}

	}
	private void storeImagePartner(Bitmap image, String name) {
		String path = null;

		path = "/shrm/sponsers/";

		File pictureFile = new File(Environment.getExternalStorageDirectory()
				.getPath() + path + name);
		if (pictureFile.exists())
		{

			pictureFile.delete();

		}

		if (!pictureFile.exists()) {

			try {
				pictureFile.createNewFile();
				FileOutputStream fos = new FileOutputStream(pictureFile);
				image.compress(Bitmap.CompressFormat.PNG, 90, fos);
				System.out.println(pictureFile.getName() + "  "
						+ "download complete");
				fos.close();
			} catch (FileNotFoundException e) {
				Log.d("", "File not found: " + e.getMessage());
			} catch (IOException e) {
				Log.d("", "Error accessing file: " + e.getMessage());
			}
		}

	}*/


	/*public Bitmap getBitmapFromURL(String imageUrl) {
		try {
			URL url = new URL(imageUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}*/
}
