package com.blackbean.eventbuoy2.amen;

import com.blackbean.eventbuoy2.amen.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.Toast;

public class TicketingActivity extends Activity{

	private WebView webviewTicketing;
	String url="";


	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ticketing_activity);

		//Home button click
		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(TicketingActivity.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();
			}
		});

		url=getIntent().getStringExtra("url");
		//Toast.makeText(TicketingActivity.this, ""+url, Toast.LENGTH_SHORT).show();
		webviewTicketing=(WebView) findViewById(R.id.webViewTicketing);
		//webview.setWebViewClient(new MyBrowser());
		
		webviewTicketing.setWebViewClient(new MyWebViewClient());
		webviewTicketing.getSettings().setLoadsImagesAutomatically(true);
		webviewTicketing.getSettings().setJavaScriptEnabled(true);
		webviewTicketing.loadUrl(url);

	}

	class MyWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {

			if (url.contains(getResources().getString(R.string.twitter_callback))) {
				Uri uri = Uri.parse(url);

				/* Sending results back */
				String verifier = uri.getQueryParameter(getString(R.string.twitter_oauth_verifier));
				Intent resultIntent = new Intent();
				resultIntent.putExtra(getString(R.string.twitter_oauth_verifier), verifier);
				setResult(RESULT_OK, resultIntent);

				/* closing webview */
				finish();
				return true;
			}
			return false;
		}
	}

}
