package com.blackbean.eventbuoy2.amen;

import java.io.File;
import java.util.ArrayList;



import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;


import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.utility.DataBase;
import com.blackbean.utility.SetGet;
import com.google.analytics.tracking.android.EasyTracker;

public class MyScheduleActivity extends Activity {
	ListView listView;
	DataBase dataBase;
	ArrayList<String> agenda_id,agenda_name,agenda_location,agenda_time;
	ImageView logoImageView;
	MyScheduleAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_myschedule);
		listView=(ListView)findViewById(R.id.lv_myschedule);

		logoImageView=(ImageView)findViewById(R.id.home);

		// right Image Setting code
		//int position=getIntent().getIntExtra("POSITION", 0);
		/*String imageBlack=getIntent().getStringExtra("ImageName");
		ImageView icon=(ImageView) findViewById(R.id.icon);
		File pictureFile = new File(Environment
				.getExternalStorageDirectory().getPath()
				+ "/shrm/menu/" + imageBlack);
		if (pictureFile != null) {
			Bitmap bitmap = BitmapFactory.decodeFile(pictureFile
					.getAbsolutePath());
			if(bitmap!=null)
				icon.setImageBitmap(bitmap);
		}*/
	//	icon.setColorFilter(Color.argb(255, 0, 0, 0));


		logoImageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(MyScheduleActivity.this,MainScreen.class);
				HomeActivity.homeActivity.finish();
				startActivity(intent);
				finish();
			}
		});



		dataBase=new DataBase(this);
		dataBase.open();
		agenda_name=new ArrayList<String>();
		agenda_location=new ArrayList<String>();
		agenda_time=new ArrayList<String>();
		agenda_id=new ArrayList<String>();

		for(int i=0;i<dataBase.getMySchedule().size();i++)
		{ 
			agenda_id.add(dataBase.getMySchedule().get(i).toString());
			agenda_name.add(dataBase.getAgendaDataByAgendaId(dataBase.getMySchedule().get(i),"agenda_name"));
			agenda_location.add(dataBase.getAgendaDataByAgendaId(dataBase.getMySchedule().get(i),"agenda_location"));
			agenda_time.add(dataBase.getAgendaDataByAgendaId(dataBase.getMySchedule().get(i),"agenda_time"));

		}
		if(dataBase.getMySchedule().size()>0)
		{
			adapter=new MyScheduleAdapter(this,0,agenda_name,agenda_location,agenda_time);
			listView.setAdapter(adapter);
		}
		dataBase.close();
		
		//listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

		listView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					final int position, long id) {
				// TODO Auto-generated method stub

				AlertDialog.Builder builder=new AlertDialog.Builder(MyScheduleActivity.this);

				builder.setMessage("Do you want to delete this event from your schedule?");
				builder.setTitle(" Remove from schedule");
				builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int which) {

						dataBase=new DataBase(MyScheduleActivity.this);
						dataBase.open();
						//	dataBase.deleteMySchedule(Integer.parseInt((agenda_id.get(position))));
						ArrayList<Integer> multitrack_agenda_id=new ArrayList<Integer>();

						//multitrack_agenda_id=dataBase.getMultiTrackIdFromAgenda(agenda_id.get(position));	


						System.out.println("Multiple Track :"+multitrack_agenda_id);

						if(dataBase.deleteMySchedule(agenda_id.get(position)))
						{
							//agenda_id.remove(Integer.parseInt(agenda_id.get(position)));
							Toast.makeText(MyScheduleActivity.this,"Deleted", Toast.LENGTH_LONG).show();
						}

						dataBase.close();
						finish();
						Intent intent=new Intent(MyScheduleActivity.this,MyScheduleActivity.class);
						startActivity(intent);
						
						//adapter.notifyDataSetChanged();


					}
				});

				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				builder.show();
				return true;
			}
		});
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub

				Intent intent=new Intent(MyScheduleActivity.this, AgendaDetailsActivity.class);
				intent.putExtra("agenda_id",agenda_id.get(position));
				startActivity(intent);

			}
		});
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

}
