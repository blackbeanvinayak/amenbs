package com.blackbean.eventbuoy2.amen;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.ProgressBar;

import com.blackbean.setget.StaticData;
import com.blackbean.utility.SetGet;

public class DownloadImages extends AsyncTask<Void, Void, Void> {

	private Context callingContext = null;// TO get Contecxt
	private List<Bitmap> imagebitmap = new ArrayList<Bitmap>();// To Get
	// ImageBitamap
	// data from
	// Server
	// private List<Bitmap> sponsersimagebitmap = new ArrayList<Bitmap>();

	private ArrayList<String> imageURL;// Image URL
	String folder_name;// Folder Name to to store images in SD card
	int code;// code represents 0) Sponsers images 1)Speaker images 2)jury
	boolean check;
	//ProgressDialog progressDialog;
	//Dialog dialog;


	/**
	 * 
	 * @param callingContext
	 *            context for given activity
	 * @param imageURL
	 *            list of images from url
	 *            "http://listoftablets.com/IACC/uploads/"
	 * @param code
	 *            code 0) To download images for Sponsers (just pass value "0")
	 *            1) To download images for Speakers (just pass value "1")
	 * @param folder_name
	 *            Name of folder to store images
	 */
	public DownloadImages(Context callingContext, ArrayList<String> imageURL,
			int code, String folder_name) {
		this.callingContext = callingContext;
		// this.icon = icon;
		this.imageURL = imageURL;
		this.folder_name = folder_name;
		this.code = code;
	}

	@Override
	protected void onPreExecute() {

		check = false;
		//dialog = new Dialog(this.callingContext);
		//ProgressBar bar = new ProgressBar(callingContext);		
		//dialog.setContentView(bar);
		//dialog.setTitle("Downloading Please Wait ...");
		//dialog.setCancelable(false);
		//dialog.show();

	}

	@SuppressWarnings("deprecation")
	protected Void doInBackground(Void... params) {

		// int length = image.size();
		// SetGet.bitmapimageArrayListfitness = new ArrayList<Bitmap>();
		File icons = new File(Environment.getExternalStorageDirectory()
				.getPath() + "/sankalp/speakers");
		if (!icons.exists()) {
			icons.mkdirs();
		}
		File icons1 = new File(Environment.getExternalStorageDirectory()
				.getPath() + "/sankalp/sponsers");
		if (!icons1.exists()) {
			icons1.mkdirs();
		}
		File icons2 = new File(Environment.getExternalStorageDirectory()
				.getPath() + "/sankalp/jury");
		if (!icons2.exists()) {
			icons2.mkdirs();
		}



		File icons3=new File(Environment.getExternalStorageDirectory()
				.getPath() + "/sankalp/menu");
		if (!icons3.exists()) {
			icons3.mkdirs();
		}


		switch (code) {
		case 0://jury images download

			try {
				for (int i = 0; i < imageURL.size(); i++) {
					
					///http://sankalp.eventbuoy.com/admin/admin/GetData.php
					URL imageUrl = new URL(
							"http://sankalp.eventbuoy.com/admin/uploads/jury/"
									+ imageURL.get(i).replaceAll(" ", "%20"));


					//imagebitmap.add(i, BitmapFactory.decodeStream(imageUrl
					//		.openConnection().getInputStream()));
					imagebitmap.add(i,getBitmapFromURL(imageUrl.toString()));

				}

				/*
				 * File icons2= new
				 * File(Environment.getExternalStorageDirectory() .getPath()
				 * +File.pathSeparator+ folder_name); if (!icons.exists()) {
				 * icons2.mkdirs(); }
				 */

				for (int i = 0; i < imageURL.size(); i++) {
					if(imagebitmap.get(i)!=null)
						storeImage(imagebitmap.get(i), imageURL.get(i));
				}
				check = true;
			} catch (Exception e) {
				check = false;
				e.printStackTrace();
			}

			break;
		case 1://speakers images

			try {
				for (int i = 0; i < imageURL.size(); i++) {
					URL imageUrl = new URL(
							"http://sankalp.eventbuoy.com/admin/uploads/"
									+ imageURL.get(i).replaceAll(" ", "%20"));

					//imagebitmap.add(i, BitmapFactory.decodeStream(imageUrl
					//		.openConnection().getInputStream()));
					imagebitmap.add(i,getBitmapFromURL(imageUrl.toString()));
				}
				/*
				 * File icons = new
				 * File(Environment.getExternalStorageDirectory() .getPath()
				 * +File.pathSeparator+folder_name); if (!icons.exists()) {
				 * icons.mkdirs(); }
				 */

				for (int i = 0; i < imageURL.size(); i++) {
					if(imagebitmap.get(i)!=null)
						storeImage(imagebitmap.get(i), imageURL.get(i));
				}
				check = true;
			} catch (Exception e) {
				check = false;
				e.printStackTrace();
			}

			break;

		case 2:
			//sponsers images download

			try {
				for (int i = 0; i < imageURL.size(); i++) {
					URL imageUrl = new URL(
							"http://sankalp.eventbuoy.com/admin/uploads/"
									+ imageURL.get(i).replaceAll(" ", "%20"));

					//imagebitmap.add(i, BitmapFactory.decodeStream(imageUrl
					//		.openConnection().getInputStream()));
					imagebitmap.add(i,getBitmapFromURL(imageUrl.toString()));

				}
				/*
				 * File icons = new
				 * File(Environment.getExternalStorageDirectory() .getPath()
				 * +File.pathSeparator+folder_name); if (!icons.exists()) {
				 * icons.mkdirs(); }
				 */

				for (int i = 0; i < imageURL.size(); i++) {
					if(imagebitmap.get(i)!=null)
						storeImage(imagebitmap.get(i), imageURL.get(i));
				}
				check = true;
			} catch (Exception e) {
				check = false;
				e.printStackTrace();
			}

			break;
		case 3:
			//menu images download

			try {
				for (int i = 0; i < imageURL.size(); i++) {
					String path="http://sankalp.eventbuoy.com/admin/uploads/menu"
							+StaticData.getScreenSizeNumber(callingContext)+"/"
							+ imageURL.get(i).replaceAll(" ", "%20");
					System.out.println("Menu Image Path " +path);
					URL imageUrl = new URL(path);

					//imagebitmap.add(i, BitmapFactory.decodeStream(imageUrl
					//		.openConnection().getInputStream()));
					imagebitmap.add(i,getBitmapFromURL(imageUrl.toString()));

				}
				/*
				 * File icons = new
				 * File(Environment.getExternalStorageDirectory() .getPath()
				 * +File.pathSeparator+folder_name); if (!icons.exists()) {
				 * icons.mkdirs(); }
				 */

				for (int i = 0; i < imageURL.size(); i++) {
					if(imagebitmap.get(i)!=null)
						storeImage(imagebitmap.get(i), imageURL.get(i));
				}
				check = true;
			} catch (Exception e) {
				check = false;
				e.printStackTrace();
			}

			break;
		default:
			break;
		}

		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		//dialog.dismiss();
		imagebitmap.clear();
		// progressDialog.dismiss();
		/*
		 * if (code=1) {
		 * 
		 * // Intent intent = new Intent(callingContext, HomeActivity.class); //
		 * callingContext.startActivity(intent); // ((Activity)
		 * callingContext).finish();
		 * 
		 * }
		 */
		switch (code) {
		case 1:
			DownloadImages images2 = new DownloadImages(callingContext,
					SetGet.getJuryImageURL(), 0, null);
			images2.execute();
			break;

		case 0:
			DownloadImages images1 = new DownloadImages(callingContext,
					SetGet.getSponsorImageURL(), 2, null);
			images1.execute();

			break;
		case 2:
			DownloadImages imagesMenu=new DownloadImages(callingContext, 
					SetGet.getMenuImageURL(), 3, null);
			imagesMenu.execute();

		case 3:
			Intent intent = new Intent(callingContext, MainScreen.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			callingContext.startActivity(intent);
			((Activity) callingContext).finish();

		default:
			break;
		}

	}

	private void storeImage(Bitmap image, String name) {
		String path = null;

		if (code == 0) {
			path = "/sankalp/jury/";

		} else if (code == 1) {
			path = "/sankalp/speakers/";
		} else if (code == 2) {
			path = "/sankalp/sponsers/";

		}else if (code == 3) {
			path = "/sankalp/menu/";
		}
		File pictureFile = new File(Environment.getExternalStorageDirectory()
				.getPath() + path + name);
		if (pictureFile.exists())
		{
			pictureFile.delete();
		}

		if (!pictureFile.exists()) {

			try {
				pictureFile.createNewFile();
				FileOutputStream fos = new FileOutputStream(pictureFile);
				image.compress(Bitmap.CompressFormat.PNG, 90, fos);
				System.out.println(pictureFile.getName() + "  "
						+ "download complete");
				fos.close();
			} catch (FileNotFoundException e) {
				Log.d("", "File not found: " + e.getMessage());
			} catch (IOException e) {
				Log.d("", "Error accessing file: " + e.getMessage());
			}
		}

	}


	public Bitmap getBitmapFromURL(String imageUrl) {
		try {
			URL url = new URL(imageUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			Bitmap myBitmap = BitmapFactory.decodeStream(input);
			return myBitmap;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

}
