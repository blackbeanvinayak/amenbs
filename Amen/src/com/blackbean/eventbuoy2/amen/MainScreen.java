package com.blackbean.eventbuoy2.amen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blackbean.setget.StaticData;

public class MainScreen extends Activity
{
	LinearLayout secondEvent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sankalp_main);
		secondEvent=(LinearLayout)findViewById(R.id.secondevent);
		
		TextView loc = (TextView) findViewById(R.id.loc);
		loc.setTextSize(10.0f);
		TextView eventName = (TextView) findViewById(R.id.eventName);
		eventName.setTextSize(12.0f);
		
		/*		if (getIntent().getExtras() != null) {
			pushmessage();
		}
		firstEvent=(LinearLayout)findViewById(R.id.firstevent);


		firstEvent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainScreen.this, HomeActivity.class);
				intent.putExtra("WHICH_EVENT", StaticData.Event_Delhi);
				startActivity(intent);
				//finish();
			}
		});*/

		secondEvent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainScreen.this, HomeActivity.class);
				intent.putExtra("WHICH_EVENT", StaticData.Event_Nairobi);
				startActivity(intent);
				//finish();
			}
		});

		/*ImageView imageView1 = (ImageView) findViewById(R.id.imageView1);
		imageView1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainScreen.this, HomeActivity.class);
				//intent.putExtra("WHICH_EVENT", StaticData.Event_Nairobi);
				startActivity(intent);
			}
		});*/

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		//System.exit(0);
		this.finish();
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	/*@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		FlurryAgent.onStartSession(this, "T5C8FKFK4YZQF34S8V2J");

	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		FlurryAgent.onEndSession(this);
	}*/
	/*@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub

		Intent intent = new Intent(MainScreen.this, HomeActivity.class);

		if(arg0.getId()==R.id.firstevent)
		{
			intent.putExtra("WHICH_EVENT", StaticData.Event_Nairobi);
		}
		else if (arg0.getId()==R.id.secondevent) 
		{
			intent.putExtra("WHICH_EVENT", StaticData.Event_Delhi);
		}
		startActivity(intent);
		finish();
	}*/
	/*public void pushmessage() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle(getIntent().getStringExtra("alert"));
		builder.setMessage(getIntent().getStringExtra("message"));
		builder.setPositiveButton("Cancel",
				new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		});

		builder.show();
	}*/


}
