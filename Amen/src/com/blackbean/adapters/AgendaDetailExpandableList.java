package com.blackbean.adapters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.blackbean.eventbuoy2.amen.HomeActivity;
import com.blackbean.eventbuoy2.amen.SpeakerDetailsActivity;
import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;
import com.blackbean.utility.DataBase;

public class AgendaDetailExpandableList extends BaseExpandableListAdapter {

	private Context _context;
	private int id;
	private List<String> _listDataHeader; // header titles
	// child data in format of header title, child title
	private HashMap<String, List<String>> _listDataChild;
	DataBase dataBase;
	ArrayList<String> speaker_speaker_id;
	String agenda_id;

	public AgendaDetailExpandableList(Context context,List<String> listDataHeader,HashMap<String, List<String>> listChildData, int id) {
		this._context = context;
		this._listDataHeader = listDataHeader;
		this._listDataChild = listChildData;
		this.id = id;
		/*	dataBase=new DataBase(_context);
		dataBase.open();
		speaker_speaker_id=new ArrayList<String>();
		this.speaker_speaker_id=dataBase.getSpeakerIdByAgendaId(this.id);
		dataBase.close();*/
	}

	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);

	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	public View getChildView(int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);

		/* Information option */
		if (headerTitle.equalsIgnoreCase("INFORMATION")) {
			LayoutInflater inflater = (LayoutInflater) _context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.info, parent, false);
			TextView info = (TextView) convertView.findViewById(R.id.info);

			info.setTextColor(Color.RED);

			info.setText(_listDataChild.get("INFORMATION").get(0));

		}
		/* Speaker Expandable Option */

		else if (headerTitle.equalsIgnoreCase("SPEAKERS"))
		{
			LayoutInflater inflater = (LayoutInflater) _context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.speaker_lv, parent, false);
			ListView listView = (ListView) convertView.findViewById(R.id.listview);
			speaker_speaker_id=new ArrayList<String>();

			speaker_speaker_id.add(_listDataChild.get("SPEAKERS").get(childPosition));

			ArrayList<String>speaker_name=new ArrayList<String>();
			ArrayList<String>speaker_designation=new ArrayList<String>();
			ArrayList<String>speaker_image=new ArrayList<String>();
			dataBase=new DataBase(_context);
			dataBase.open();

			String event_category;
			if (HomeActivity.whichEvent == StaticData.Event_Nairobi) {
				event_category = "Nairobi";
			}else {
				event_category = "Delhi";
			}

			for (int i = 0; i < speaker_speaker_id.size(); i++) {

				if (dataBase.getSpeakerInfoBySpeaker_id(Integer.parseInt(speaker_speaker_id.get(i)), "speaker_name",event_category) != null) {
					
					speaker_name.add(dataBase.getSpeakerInfoBySpeaker_id(Integer.parseInt(speaker_speaker_id.get(i)), "speaker_name",event_category));
					speaker_designation.add(dataBase.getSpeakerInfoBySpeaker_id(Integer.parseInt(speaker_speaker_id.get(i)), "speaker_designation",event_category));
					speaker_image.add(dataBase.getSpeakerInfoBySpeaker_id(Integer.parseInt(speaker_speaker_id.get(i)), "speaker_image",event_category));
				}


			}
			dataBase.close();

			AgendaSpeakerAdapter agendaSpeakerAdapter=new AgendaSpeakerAdapter(_context,android.R.layout.simple_expandable_list_item_1,speaker_name,speaker_designation,speaker_image);
			//speakerlist.setAdapter(agendaspeaker);

			/*AgendaSpeakerAdapter agendaSpeakerAdapter;
			agendaSpeakerAdapter=new AgendaSpeakerAdapter(_context, R.layout.agenda_speaker_adapter,
					speaker_speaker_id);*/
			listView.setAdapter(agendaSpeakerAdapter);
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub

					//	Intent intent = new Intent(_context,
					//	SpeakerDetailsActivity.class);
					//	Toast.makeText(_context, ""+_listDataChild.get("SPEAKERS").get(childPosition), Toast.LENGTH_LONG).show();
					dataBase=new DataBase(_context);
					dataBase.open();
					ArrayList<String> arrayList=new ArrayList<String>();
					for (int i = 0; i < dataBase.getSpeakerCount(); i++) {
						//arrayList.add(dataBase.getSpeakerDataByAgendaId(dataBase.getSpeaker().get(i), "speaker_id"));
					}

					int speaker_id=Integer.parseInt(_listDataChild.get("SPEAKERS").get(childPosition));

					int vpossition=Arrays.binarySearch(arrayList.toArray(), ""+speaker_id);

					Intent intent = new Intent(_context,SpeakerDetailsActivity.class);
					intent.putExtra("_id", vpossition);

					_context.startActivity(intent);
					dataBase.close();
					//	intent.putExtra("_id",Integer.parseInt(speaker_speaker_id.get(position)));
					//	_context.startActivity(intent);
				}
			});
		}

		/* Notes Expandable Option */

		else if (headerTitle.equalsIgnoreCase("NOTES")) {
			LayoutInflater inflater = (LayoutInflater) _context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.notes, parent, false);
			final EditText note = (EditText) convertView
					.findViewById(R.id.note_edittxt);

			DataBase database = new DataBase(_context);
			database.open();
			if (database.isNoteByAgendaIdavailable(id)) {
				String nt = database.getNoteByAgendaId(id);
				note.setText(nt);
			}
			database.close();

			Button btn = (Button) convertView.findViewById(R.id.sub_btn);
			btn.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					String note_t = note.getText().toString().trim();

					DataBase db = new DataBase(_context);
					db.open();
					db.addnote(id, note_t);
					db.close();

					Toast.makeText(_context, "Note Added",
							Toast.LENGTH_LONG).show();

				}
			});

		}

		return convertView;
	}

	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.size();
	}

	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);
		// lblListHeader.setTypeface(null, Typeface.ITALIC);
		lblListHeader.setText(headerTitle);

		return convertView;
	}

	public boolean hasStableIds() {
		return false;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
