package com.blackbean.setget;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;


public  class StaticData {

	public static final String SPONSER_ACTIVTY_BG_COLOR = "#FFFFFF";
	public static String pagebgcolor="#FFFFFF";
	public static String headerColor="#0000FF";
	public static CharSequence headerText="Product Conclave";
	public static String dividercolor="#000000";
	public static String listtextcolor="#000000";

	public static int Event_Nairobi=1;
	public static int Event_Delhi=2;
	
	/**
	 * Agenda Activity Design Variables 
	 * @return
	 */
	public static String agenda_pagebgcolor="#FFFFFF";
	public static String agenda_headerColor="#000000";
	public static CharSequence agenda_headerText="Product Conclave";
	public static String agedna_dividercolor="#000000";
	public static String agend_listtextcolor="#000000";
	public static String SPONSER_ACTIVTY_HEDER_TXT_COLOR="#000000";
	public static String SPONSER_ACTIVTY_TXT_COLOR="#000000";

	//Event Activity configuration Variables
	public static String Event_Text_Name_="EVENT_NAME";
	public static String Event_Text_Address="EVENT_ADDRESS";
	public static String Event_Text_Time="EVENT_TIME";
	public static String Event_Text_Name_Color="#000000";
	public static String Event_Text_Address_Color="#000000";
	public static String Event_Text_Time_Color="#000000";
	public static String Event_Listview_BG_Color="#FFFFFF";
	public static String Event_Listview_Divider_Color="#000000";
	public static String Event_Layout_Color="#FFFFFF";

	//Event Activity configuration Variables
	public static String Speaker_Name_="EVENT_NAME";

	public static String Speaker_Text_Name_Color="#000000";
	public static String Speaker_Text_Designation_Color="#000000";
	public static String Expandable_Text_Color="#000000";
	public static String Expandable_Header_Text_Color="";


	public static ArrayList<String> event_id_adptor=new ArrayList<String>();
	public static ArrayList<String> event_name_adptor=new ArrayList<String>();
	public static ArrayList<String> event_address_adptor=new ArrayList<String>();
	public static ArrayList<String> event_time_adptor=new ArrayList<String>();


	public static ArrayList<String> getHomeName()
	{
		ArrayList<String> arrayList=new ArrayList<String>();
		arrayList.add("AGENDA");
		arrayList.add("My SCHEDULE");
		arrayList.add("SPEAKERS");

		arrayList.add("SPONSORS");
		arrayList.add("ABOUT US");
		arrayList.add("ATTENDEE NETWORKING");
		arrayList.add("FLOOR PLANS");

		return arrayList;
	}
	public static ArrayList<Integer> getHomeImage()
	{
		ArrayList<Integer> arrayList=new ArrayList<Integer>();
		/*arrayList.add(R.drawable.agenda);
	arrayList.add(R.drawable.agenda);
	arrayList.add(R.drawable.agenda);
	arrayList.add(R.drawable.agenda);

	arrayList.add(R.drawable.agenda);
	arrayList.add(R.drawable.agenda);
	arrayList.add(R.drawable.agenda);*/
		return arrayList;
	}

	public static ArrayList<String> getEventName()
	{
		ArrayList<String> arrayList=new ArrayList<String>();
		arrayList.add("Register And Netowrking");
		arrayList.add("Register And Netowrking");
		arrayList.add("Register And Netowrking");
		arrayList.add("Register And Netowrking");
		arrayList.add("Register And Netowrking");
		arrayList.add("Register And Netowrking");

		return arrayList;
	}

	public static ArrayList<String> getEventAddress()
	{
		ArrayList<String> arrayList=new ArrayList<String>();
		arrayList.add("Sabha-1 JW Marriott, Pune");
		arrayList.add("Sabha-1 JW Marriott, Pune");
		arrayList.add("Sabha-1 JW Marriott, Pune");
		arrayList.add("Sabha-1 JW Marriott, Pune");
		arrayList.add("Sabha-1 JW Marriott, Pune");
		arrayList.add("Sabha-1 JW Marriott, Pune");


		return arrayList;
	}

	public static ArrayList<String> getEventTime()
	{
		ArrayList<String> arrayList=new ArrayList<String>();
		arrayList.add("8:30-9:30");
		arrayList.add("8:30-9:30");
		arrayList.add("8:30-9:30");
		arrayList.add("8:30-9:30");
		arrayList.add("8:30-9:30");
		arrayList.add("8:30-9:30");


		return arrayList;
	}
	public static ArrayList<String> getEventDate()
	{
		ArrayList<String> arrayList=new ArrayList<String>();
		arrayList.add("Wed 06/14");
		arrayList.add("Wed 07/14");
		arrayList.add("Wed 08/14");
		arrayList.add("Wed 09/14");
		arrayList.add("Wed 10/14");
		arrayList.add("Wed 11/14");


		return arrayList;
	}
	public static ArrayList<Integer> getAboutImage()
	{
		ArrayList<Integer> arrayList=new ArrayList<Integer>();
		/*  arrayList.add(R.drawable.agenda);
      arrayList.add(R.drawable.agenda);
      arrayList.add(R.drawable.agenda);*/
		return arrayList;
	}
	public static ArrayList<String> getaboutText()
	{
		ArrayList<String> arrayList=new ArrayList<String>();
		arrayList.add("About Us");
		arrayList.add("Black Bean");
		arrayList.add("Share With");
		return arrayList;
	}

	// Calculate Device type ldpi hdpi mdpi xhdpi 

	@SuppressLint("InlinedApi")
	public static int getScreenSizeNumber(Context context) {
		int sizeNo=0;
		int screenSize = context.getResources().getConfiguration().screenLayout &
		        Configuration.SCREENLAYOUT_SIZE_MASK;
		if(screenSize== Configuration.SCREENLAYOUT_SIZE_SMALL)
		{
			sizeNo=Configuration.SCREENLAYOUT_SIZE_SMALL;
		}
		else if (screenSize== Configuration.SCREENLAYOUT_SIZE_NORMAL) {
			sizeNo=Configuration.SCREENLAYOUT_SIZE_NORMAL;
		}
		else if (screenSize== Configuration.SCREENLAYOUT_SIZE_LARGE) {
			sizeNo=Configuration.SCREENLAYOUT_SIZE_LARGE;
		}
		else if (screenSize== Configuration.SCREENLAYOUT_SIZE_XLARGE) {
			sizeNo=Configuration.SCREENLAYOUT_SIZE_XLARGE;
		}
		return sizeNo;
	}


}
