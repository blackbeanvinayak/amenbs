package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blackbean.adapters.ExpandableListAdapter;
import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;
import com.blackbean.utility.DataBase;
import com.blackbean.utility.ImageLoader;
import com.google.analytics.tracking.android.EasyTracker;

public class JuryDetailsActivity extends FragmentActivity {
	SpeakerInfoAdapter speakerInfoAdapter;
	ViewPager mViewPager;
	int _id;
	static Context context;
	static String Vfb = null;
	static String Vtw;
	static String Vgoogle;
	static String Vlinked;

	private static final String Agenda_NAME="agenda_name";
	static ExpandableListAdapter listAdapter;
	static ExpandableListView expListView;
	static List<String> listDataHeader;
	static HashMap<String, List<String>> listDataChild;
	static HashMap<String, List<String>> listDataChildsocial;
	ImageView previous;
	ImageView next;
	static boolean Vcheckspeaker_id = false;
	static int pagerCount, currentpage = 0;
	int last;
	public static ArrayList<String> speaker_id;
	public static ArrayList<String> namearray;
	public static ArrayList<String> designation;
	public static ArrayList<String> description;
	public static ArrayList<String> speakerimage;

	public static ArrayList<String> sessionsArrayList;
	private static final String Speaker_Image="speaker_image";
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_speaker_details);
		RelativeLayout layout = (RelativeLayout) findViewById(R.id.ralativelayout);
		layout.setBackgroundColor(Color.WHITE);
		TextView headertext=(TextView)findViewById(R.id.header_txt);
		headertext.setText("Jury Information");

		DataBase	database = new DataBase(this);
		database.open();
		speaker_id = new ArrayList<String>();

		namearray = new ArrayList<String>();
		designation = new ArrayList<String>();
		description = new ArrayList<String>();
		speakerimage=new ArrayList<String>();

		String event_category;
		if (HomeActivity.whichEvent == StaticData.Event_Nairobi) {
			event_category="Nairobi";
		}else {
			event_category="Delhi";
		}

		speaker_id.addAll(database.getJuryByfield("jury_id",event_category));
		namearray.addAll(database.getJuryByfield("jury_name",event_category));
		designation.addAll(database.getJuryByfield("jury_designation",event_category));
		description.addAll(database.getJuryByfield("jury_info",event_category));
		speakerimage.addAll(database.getJuryByfield("jury_image",event_category));
		//jurytype.addAll(db.getJuryByfield("jury_type"));

		database.close();

		/*id.add("0");
		id.add("1");
		id.add("2");
		id.add("3");
		id.add("4");
		id.add("5");
		id.add("6");
		id.add("7");

		namearray.add("Professor Jagdish Sheth");
		namearray.add("Prof.Dr.M.J.Xavier");
		namearray.add("Vijay Michihito Batra");
		namearray.add("Kulwinder Singh");
		namearray.add("Shyam Sekar S.");
		namearray.add("Mr.Rob Peck");
		namearray.add("Rajiv Dingra");
		namearray.add("Alet Viegas");

		designation.add("Professor of Marketing");
		designation.add("Former Director IIM (R)");
		designation.add("Motivational Speaker");
		designation.add("Director-Global Marketing Communication Synechron Technologies");
		designation.add("S.Chief Mentor & Strategies,Startup Xperts");
		designation.add("Director Client Services,03M Directional Marketing Pvt Ltd");
		designation.add("Founder & CEO WATConsult");
		designation.add("Founder Black Bean Engagement");

		description.add("Professor JagdishSheth is the Charles H.Kellstadt Professor of Marketing at Emory University Goizueta Business School.\n\n He is known nationally and internationally for his scholarly contributions in consumer behavior, relationship marketing, competitive strategy, and geopolitical analysis. When he joined Emory’s faculty in 1991, Professor Sheth had nearly 30 years of combined experience in marketing from the University of Southern California, the University of Illinois, Columbia University, and Massachusetts Institute of Technology.\n\nThroughout his career, Professor Sheth has offered more than a thousand presentations in at least twenty countries. He has also provided consulting for numerous companies in the United States, Europe and Asia. His client list includes AT&T, BellSouth, Cox Communications, Delta, Ernst & Young, Ford, GE, Lucent Technologies, Motorola, Nortel, Pillsbury, Sprint, Square D, 3M, Whirlpool, and others. Currently, Professor Sheth sits on the Board of Directors of several public companies including Norstan, Cryo Cell International, and Wipro Limited. \n\nProfessor Sheth’s accolades include “Outstanding Marketing Educator,” an award presented by the Academy of Marketing Science, the “Outstanding Educator” award twice-presented by Sales and Marketing Executives International, and the P.D. Converse Award for his outstanding contributions to theory in marketing, presented by the American Marketing Association. Professor Sheth is the recipient of the two highest awards given by the American Marketing Association: the Richard D. Irwin Distinguished Marketing Educator Award and the Charles Coolidge Parlin Award. \n\nIn 1996, Professor Sheth was selected as the Distinguished Fellow of the Academy of Marketing Science. The following year, he was awarded the Distinguished Fellow award from the International Engineering Consortium.Professor Sheth is currently a Fellow of the American Psychological Association (known as APA).\n\nProfessor Sheth has authored or coauthored hundreds of articles and books. In 2000, he and Andrew Sobel published the best seller, Clients for Life. In 2001, Value Space, which he coauthored with Banwari Mittal, was published. Professor Sheth’s most popular book, The Rule of Three, was coauthored with Dr. RajendraSisodia and published in 2002. He has since written notable publications: Tectonic Shift, Firms of Endearment and 4 As of Marketing."); 
		description.add("Prof. MJ Xavier obtained his Doctorate in Management (1984) from the Indian Institute of Management, Calcutta. He is basically an engineer with an M.Tech. (1979) in Chemical Plant Engineering from Regional Engineering College, Warangal and B.Tech.(1976) in Chemical Engineering from Coimbatore Institute of Technology. \n\nProf. M. J. Xavier was the Founding Director of Indian Institute of Management Ranchi (IIM Ranchi). He has more than 25 years of professional experience in teaching, research, and consultancy. He has served as a Research Executive in Mode Research Pvt. Ltd., Calcutta (1982-84) and as Manager in-charge of Management Development and Services with SPIC Ltd., Chennai (1985-91). He has taught at XLRI, Jamshedpur (1984-84), IIM Bangalore (1991-96), IFMR Chennai (1998 – 2006) and Great lakes Institute of Management, Chennai (2008-2010). He is currently the Executive Director at VIT Business School (Vellore/Chennai) \n\nHis areas of interest include Marketing Research, Data Mining, e-Governance and Spirituality. He has conducted a number of training programmes for executives and has also been a consultant to several companies in India and abroad. He has authored three books and published more than 100 articles in Journals and Magazines in India and Abroad. His book `Marketing in the New Millennium’ won the DMA-Escorts Award for the best Management Book of the Year 1999. \n\nHe has served as Visiting Faculty to a number of business schools in India and abroad including The University of Buckingham, U.K. (1993), The Texas Christian University, U.S.A. (1999), California Polytechnic State University, USA (2003-05) and The American University of Armenia, Armenia (2006). ");
		description.add("Vijay MichihitoBatra is driven by a deep-seated interest in creating awareness of the need to remain positively focused in life through good and positive reportage. This idea gave birth to Think Media Inc. – a creative showcasing of innovations in the business process and bringing Corporate News Updates and giving coverage to what is inspiring and motivating. Through his upcoming (project underway) personalized show Rendezvous with Vijay Batra , he would like to explore the positive and inspirational thoughts and the success principles underlying the achievements which drive India’s Wealth Makers. People who know him closely identify him as to possessing a single-minded pursuit to deepen the understanding and impact of sowing seeds of Positive Thoughts in People’s Minds to make Life Better. It is a mission typified by his Training Organization, Think Inc. taking it further to a new platform and steadily expanding its reach is Think Media Inc. It is not a new venture alone, but also the fruition of Vijay Batra long standing dream of bringing to people’s lives a news portal developed for inspirational news what we call “Positive News”. \n\nIt is a channel geared to tell the untold, lesser known - scavenging for only the real, extracting treasure from unmined and unexpected sources. \n\nThink Media is resolved to undo the negative impacts of fear-mongering media, sensationalizing and depressing reporting and worrying trends created by pessimistic socio - economic forecasts. His optimistic venture is completely rooted in reality as Vijay Batra encourages courageous worrying instead. It is a skill that he practices with great commitment himself. He learnt this through his experience and observation of Japanese business practices. Vijay is an alumni of PHP Institute, a Think Tank affiliated to Panasonic, founded by Matsushita Konosuke, who is considered to be the ultimate authority in management and leadership in Japan. Already blessed with a unique lineage, and sharpened by his training in USA and Japan, Vijay embodies the Samurai spirit and comprehensively translates its essence, adapting it with equal intensity for the climate of the Indian soil. \n\nAs the Editor-in-Chief of Think Media Inc., Vijay Batra is committed to establish Think Media Inc. as a vehicle devoted to building opinion through openness. During his education in University of Pittsburgh he felt this deep-rooted desire to share one’s privileges by creating a common good and wellness through common sense - a wealth that would enable individuals to lead fulfilled and empowered lives. \n\nElectrifying, intensely involved, down to earth, amazingly humorous, extremely driven and passionately connected, yet a man characterized by his benevolence and honesty and remarkable simplicity. ");
		description.add("");
		description.add("");
		description.add("");
		description.add("");
		description.add("I currently lead the team at Black Bean Engagement, and work with brands, businesses and agencies. We deliver innovative and successful marketing campaigns for customers like DC Design, TastyKhana.com, HDFC Life, NASSCOM, ColumnIT, Vardenchi and more. \n\nOver the last seven years I have been dabbling in roles that require me to work as an Internet marketing strategist, social media analyst, mobile marketing, application development & eLearning for businesses that need integrated marketing solutions.\n\nParticularly interested in businesses that want to target an online audience.\n\nSpecialties: creative strategy, digital marketing, social media, online branding and eLearning");

		speakerimage.add(R.drawable.jagadish_seth);
		speakerimage.add(R.drawable.mj_xavier);
		speakerimage.add(R.drawable.vijay);
		speakerimage.add(R.drawable.kulwinder_singh);
		speakerimage.add(R.drawable.shyam_sekar_s);
		speakerimage.add(R.drawable.rob);
		speakerimage.add(R.drawable.rajivdingra);
		speakerimage.add(R.drawable.alet);*/




		_id=getIntent().getExtras().getInt("_id");
		context = JuryDetailsActivity.this;
		speakerInfoAdapter = new SpeakerInfoAdapter(
				getSupportFragmentManager(), context);
		mViewPager = (ViewPager) findViewById(R.id.pager);
		pagerCount=speakerInfoAdapter.getCount();
		mViewPager.setAdapter(speakerInfoAdapter);
		pagerCount = getCount();
		previous = (ImageView) findViewById(R.id.previous);
		next = (ImageView) findViewById(R.id.next);

		mViewPager.setCurrentItem(speaker_id.indexOf(_id+""));//****************

		//Left arrow invible of first 
		if (mViewPager.getCurrentItem() == 0) {
			previous.setVisibility(View.INVISIBLE);
		}
		//right arrow invible of last 
		if (mViewPager.getCurrentItem() == (pagerCount - 1)) {
			next.setVisibility(View.INVISIBLE);
		}

		/*if(_id==0)
		{
			previous.setVisibility(View.INVISIBLE);
		}
		if(_id==(pagerCount-1))
		{
			next.setVisibility(View.INVISIBLE);
		}*/
		ImageView menu=(ImageView)findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(JuryDetailsActivity.this, MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();

			}
		});
		previous.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);



			}
		});

		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
			}
		});

		last = mViewPager.getCurrentItem();

		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub

				if (arg0 == 0) {
					previous.setVisibility(View.INVISIBLE);
					next.setVisibility(View.VISIBLE);

				}
				if (arg0 > 0) {
					previous.setVisibility(View.VISIBLE);
				}



				if ((pagerCount - 1) == arg0) {
					previous.setVisibility(View.VISIBLE);
					next.setVisibility(View.INVISIBLE);
				}


				if (arg0 < pagerCount - 1) {
					next.setVisibility(View.VISIBLE);
				}

				currentpage = arg0;


			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	public static class SpeakerInfoAdapter extends FragmentStatePagerAdapter {

		Context context;

		public SpeakerInfoAdapter(FragmentManager fm, Context context) {
			super(fm);
			this.context = context;
			// TODO Auto-generated constructor stub
		}

		@Override
		public Fragment getItem(int i) {
			// TODO Auto-generated method stub
			Fragment fragment = new DemoObjectFragment(context);
			Bundle args = new Bundle();

			// args.putInt(DemoObjectFragment.ARG_OBJECT, i + 1); // Our object
			// is just an integer :-P

			args.putString("speaker_id", "speaker" + i);

			args.putInt("id", i);
			args.putInt("spkrid", Integer.parseInt(speaker_id.get(i)));

			fragment.setArguments(args);
			return fragment;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub

			return namearray.size();

		}

	}

	public int getCount() {
		return speakerInfoAdapter.getCount();
	}

	public static class DemoObjectFragment extends Fragment {
		public static int id;
		String name;
		String detail1;
		String detail2;
		String image_url;
		int spkr_id;

		ImageLoader imageLoader;
		Context context;

		ImageView spkr_image_speaker_info;
		public static String info;
		public static String sessions;
		public static String social;

		public static final String ARG_OBJECT = "object";
		Bitmap bmp;


		public DemoObjectFragment(Context context) {
			// TODO Auto-generated constructor stub
			this.context=context;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.adapter_jury_details,
					container, false);
			Bundle get_info = getArguments();
			id=get_info.getInt("id");
			spkr_id=get_info.getInt("spkrid");
			//TextView spkr_name = (TextView) rootView
			//                .findViewById(R.id.txt_speaker_info);
			TextView spkr_name = (TextView) rootView
					.findViewById(R.id.txt_speaker_name);
			TextView spkr_designation = (TextView) rootView
					.findViewById(R.id.txt_speaker_designation);
			TextView spkr_description = (TextView) rootView
					.findViewById(R.id.txt_speaker_description);
			spkr_image_speaker_info=(ImageView)rootView.findViewById(R.id.img_spkr);

			spkr_name.setText(namearray.get(id));
			// spkr_image_speaker_info.setImageResource(speakerimage.get(id));
			spkr_designation.setText(designation.get(id));
			spkr_description.setText(description.get(id));

			imageLoader = new ImageLoader(context);
			/*File pictureFile = new File(Environment
					.getExternalStorageDirectory().getPath()
					+ "/sankalp/jury/" + speakerimage.get(id));
			if (pictureFile != null) {
				Bitmap bitmap = BitmapFactory.decodeFile(pictureFile
						.getAbsolutePath());
				spkr_image_speaker_info.setImageBitmap(bitmap);
			}*/

			imageLoader.DisplayImage("http://sankalp.eventbuoy.com/admin/uploads/jury/"
					+ speakerimage.get(id).replaceAll(" ", "%20"), spkr_image_speaker_info);


			return rootView;
		}

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}


}