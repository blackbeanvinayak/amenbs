package com.blackbean.eventbuoy2.amen;



import java.util.ArrayList;

import com.blackbean.eventbuoy2.amen.R;






import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

public class MyScheduleAdapter extends ArrayAdapter<String> {
	Context context;
	ArrayList<String> agenda_name,agenda_location,agenda_time;
	public MyScheduleAdapter(Context context, int resource,ArrayList<String> agenda_name,ArrayList<String> agenda_location,ArrayList<String> agenda_time) {
		super(context,R.layout.myschedule_adapter,agenda_name);
		this.context=context;
		this.agenda_name=agenda_name;
		this.agenda_location=agenda_location;
		this.agenda_time=agenda_time;
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view=inflater.inflate(R.layout.myschedule_adapter,parent,false);
		TextView name=(TextView)view.findViewById(R.id.agenda_name);
		TextView location=(TextView)view.findViewById(R.id.agenda_location);
		TextView time=(TextView)view.findViewById(R.id.agenda_time);

		name.setText(agenda_name.get(position));
		location.setText(agenda_location.get(position));
		time.setText(agenda_time.get(position).trim());
		return view;
	}


}
