package com.blackbean.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.blackbean.eventbuoy2.amen.DownloadImages;
import com.blackbean.eventbuoy2.amen.HomeActivity;
import com.blackbean.eventbuoy2.amen.MainScreen;
import com.google.android.gms.internal.nu;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ProgressBar;

public class GetServerData extends AsyncTask<String, String, String> {

	public static boolean flag = false;

	Context context;
	Dialog dialog;
	JSONParser jsonParser = new JSONParser();
	JSONParserString jsonParserString = new JSONParserString();
	JSONObject jsonObject = null;
	String val;
	public static JSONArray response_json_array_Agenda = null;
	public static JSONArray response_json_array_Speaker = null;
	public static JSONArray response_json_array_Sponsor = null;
	public static JSONArray response_json_array_agenda_speaker = null;
	public static JSONArray response_json_array_jury = null;
	public static JSONArray response_json_array_menu_list = null;
	public static JSONArray response_json_array_programs = null;

	DataBase dataBase;
	ArrayList<String> speaker_image_urlArrayList;
	ArrayList<String> sponsor_image_urlaArrayList;
	ArrayList<String> jury_image_urlaArrayList;
	ArrayList<String> menu_image_urlArrayList;
	// ArrayList<String> menu_imageBlack;

	private static final String Agenda_ID = "agenda_id";
	private static final String Agenda_NAME = "agenda_name";
	private static final String Agenda_LOCATION = "agenda_location";
	private static final String Agenda_DATE = "agenda_date";
	private static final String Agenda_DAY = "agenda_day";
	private static final String Agenda_TIME = "agenda_time";
	private static final String Agenda_DESCRIPTION = "agenda_description";
	private static final String Agenda_Order = "agenda_order";
	private static final String Agenda_Track_ID = "Track_track_id";

	private static final String Sponsers_Id = "sponsers_id";
	private static final String Sponsers_Name = "sponsers_name";
	private static final String Sponsers_Type = "sponsers_type";
	private static final String Sponsers_Image = "sponsers_image";
	private static final String Sponsers_Description = "sponsers_description";
	private static final String Sponsers_Url = "sponsers_url";
	private static final String Sponsers_Order = "sponsers_order";

	private static final String Jury_ID = "jury_id";
	private static final String Jury_Name = "jury_name";
	private static final String Jury_designation = "jury_designation";
	private static final String Jury_info = "jury_info";
	private static final String Jury_Image = "jury_image";
	private static final String Jury_Type = "jury_type";
	private List<Bitmap> imagebitmap;

	static ArrayList<String> speaker_imagesArrayList;

	public static ArrayList<String> sponsor_namelArrayList,
	sponsor_typeArrayList;


	ArrayList<Integer> agendaID =new ArrayList<Integer>();

	public GetServerData(Context context) {
		this.context = context;
		sponsor_namelArrayList = new ArrayList<String>();
		speaker_image_urlArrayList = new ArrayList<String>();
		menu_image_urlArrayList = new ArrayList<String>();
		// menu_imageBlack=new ArrayList<String>();
		jury_image_urlaArrayList = new ArrayList<String>();
		List<Bitmap> imagebitmap = new ArrayList<Bitmap>();


	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		dialog = new Dialog(this.context);
		ProgressBar bar = new ProgressBar(context);
		dialog.setContentView(bar);
		dialog.setTitle("Downloading Please Wait ...");
		dialog.setCancelable(false);
		//dialog.show();
	}

	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub

		// http://sankalp.eventbuoy.com/admin/admin/GetData.php

		/*
		 * try { jsonObject=jsonParser.getJSONFromUrl(
		 * "http://sankalp.eventbuoy.com/menu_list_item_names.php"); if
		 * (jsonObject!=null) { System.out.println("Menu List :" + jsonObject);
		 * response_json_array_menu_list
		 * =jsonObject.getJSONArray("menu_list_item_names");
		 * System.out.println("Menu List Response:" +
		 * response_json_array_menu_list); dataBase = new DataBase(context);
		 * dataBase.open(); //Get menu list from jsonObject String
		 * menuName,menuImage,menuImageBlack; for (int i = 0; i <
		 * response_json_array_menu_list.length(); i++) { JSONObject
		 * menu_temp=new JSONObject(); try {
		 * menu_temp=response_json_array_menu_list.getJSONObject(i);
		 * menuName=menu_temp.getString("menu_list_item_name");
		 * menuImage=menu_temp.getString("menu_list_image_name");
		 * menuImageBlack=menu_temp.getString("menu_list_image_name_black");
		 * 
		 * menu_image_urlArrayList.add(menuImage);
		 * menu_image_urlArrayList.add(menuImageBlack);
		 * 
		 * 
		 * System.out.println("Menu Image "+menuImage);
		 * System.out.println("Menu Black "+menuImageBlack);
		 * //menu_imageBlack.add(menuImageBlack);
		 * 
		 * long a=dataBase.addMenuList(menuName, menuImage,menuImageBlack);
		 * System.out.println("Row Created + "+a); } catch (JSONException e) {
		 * // TODO: handle exception
		 * System.out.println("JsonException Menulist "+e.toString()); } }
		 * SetGet.setMenuImageURL(menu_image_urlArrayList); } } catch (Exception
		 * e) { // TODO: handle exception
		 * System.out.println("Exception Error for Menulist "+e.toString()); }
		 */



		try {
			jsonObject = jsonParser
					.getJSONFromUrl("http://amen.eventbuoy.com/admin/admin/GetData.php");
			if (jsonObject != null) {
				System.out.println("Agenda :" + jsonObject);
				response_json_array_Agenda = jsonObject
						.getJSONArray("event_details");
				response_json_array_Speaker = jsonObject
						.getJSONArray("speakers_details");
				response_json_array_Sponsor = jsonObject
						.getJSONArray("sponsor_info");
				response_json_array_agenda_speaker = jsonObject
						.getJSONArray("agenda_speaker");
				response_json_array_jury = jsonObject.getJSONArray("jury_info");
				response_json_array_programs = jsonObject
						.getJSONArray("programs");

				/*System.out.println("Agenda :" + response_json_array_Agenda);
				System.out.println("Speaker :" + response_json_array_Speaker);
				System.out.println("Sponsor :" + response_json_array_Sponsor);

				System.out.println("agenda_speaker :"
						+ response_json_array_agenda_speaker);
				System.out.println("Jury :" + response_json_array_jury);*/

				speaker_image_urlArrayList = new ArrayList<String>();
				sponsor_image_urlaArrayList = new ArrayList<String>();

				String agenda_id = null, agenda_name, agenda_location, agenda_day, agenda_date, agenda_event_category, agenda_time, agenda_description, agenda_order, agenda_track_id;

				dataBase = new DataBase(context);
				dataBase.open();

			/*	if (flag) {
					dataBase.deleteAlltables();
					dataBase.close();
					dataBase = new DataBase(context);
					//dataBase=new DataBase(context);
					//dataBase.createAllTables();
				}

				flag=true;*/

				// * Get Agenda From Server Side by loop

				for (int i = 0; i < response_json_array_Agenda.length(); i++) {
					JSONObject agenda_temp = new JSONObject();
					try {
						agenda_temp = response_json_array_Agenda
								.getJSONObject(i);

						agenda_id = agenda_temp.getString("agenda_id");
						agendaID.add(Integer.valueOf(agenda_id));
						agenda_name = agenda_temp.getString("agenda_name");
						agenda_location = agenda_temp
								.getString("agenda_location");
						agenda_day = agenda_temp.getString("agenda_day");
						agenda_date = agenda_temp.getString("agenda_date");
						agenda_time = agenda_temp.getString("agenda_time");
						agenda_description = agenda_temp
								.getString("agenda_description");

						agenda_event_category = agenda_temp
								.getString("event_category");

						// agenda_order = agenda_temp.getString("event_order");
						// agenda_track_id = agenda_temp
						// .getString("agenda_track_id");
						dataBase.addAgenda(agenda_id, agenda_name,
								agenda_location, agenda_date, agenda_day,
								agenda_time, agenda_description, "", "",
								agenda_event_category);

					} catch (JSONException e) {
						System.out.println("Json Exception :" + e);
					}
				}
				
					DataBase db = new DataBase(context);
					db.open();
					ArrayList<Integer>agendaId_FromMyScehedule=SetGet.agendaId_MySchedule;
					if(agendaId_FromMyScehedule!=null)
					{
						for (int i = 0; i < agendaId_FromMyScehedule.size(); i++) {

							if(agendaID.contains(agendaId_FromMyScehedule.get(i)))
							{
								//db.deleteMySchedule(String.valueOf(agendaId_FromMyScehedule.get(i)));
								db.addmyschedule(agendaId_FromMyScehedule.get(i));
							}
						}
					}
					db.close();

				String speaker_id, speaker_name, speaker_designation, speaker_description, speaker_image, speaker_fb, speaker_twitter, speaker_google, speaker_linkedin, speaker_order, speaker_event_category;

				// Get Speakers From Server side by loop

				for (int i = 0; i < response_json_array_Speaker.length(); i++) {
					JSONObject speaker_temp = new JSONObject();
					try {
						speaker_temp = response_json_array_Speaker
								.getJSONObject(i);

						speaker_id = speaker_temp.getString("speaker_id");
						speaker_name = speaker_temp.getString("speaker_name");
						speaker_designation = speaker_temp
								.getString("speaker_designation");
						speaker_description = speaker_temp
								.getString("speaker_info");
						speaker_image = speaker_temp
								.getString("speaker_image_url");
						speaker_fb = speaker_temp.getString("speaker_facebook");
						speaker_google = speaker_temp
								.getString("speaker_googleplus");
						speaker_twitter = speaker_temp
								.getString("speaker_twitter");
						speaker_event_category = speaker_temp
								.getString("event_category");

						//System.out.println("speaker image" + speaker_image);
						//speaker_image_urlArrayList.add(speaker_image);

						dataBase.addSpeaker(speaker_id, speaker_name,
								speaker_designation, speaker_description,
								speaker_image, speaker_fb, speaker_twitter,
								speaker_google, speaker_event_category);

					} catch (JSONException e) {
						System.out.println("Json Exception :" + e);
					}
				}
				SetGet.setSpeakerImageURL(speaker_image_urlArrayList);

				String sponsor_id, sponsor_name, sponsor_type, sponsor_image, sponsor_description, sponsor_url, sponsor_order, event_category;

				for (int i = 0; i < response_json_array_Sponsor.length(); i++) {
					JSONObject sponsor_temp = new JSONObject();
					try {
						sponsor_temp = response_json_array_Sponsor
								.getJSONObject(i);

						sponsor_id = sponsor_temp.getString("sponsor_id");
						sponsor_name = sponsor_temp.getString("sponsor_name");

						SetGet.sponsorNameaArrayList.add(sponsor_name);

						sponsor_type = sponsor_temp.getString("sponsor_type");

						SetGet.sponsorTypeArrayList.add(sponsor_type);

						sponsor_image = sponsor_temp.getString("sponsor_logo");
						// sponsor_image_urlaArrayList.add(sponsor_image);
						sponsor_url = sponsor_temp.getString("sponsor_url");
						sponsor_order = sponsor_temp.getString("sponsor_order");
						event_category = sponsor_temp
								.getString("event_category");
						sponsor_description = sponsor_temp.getString("sponsor_description");

						sponsor_image_urlaArrayList.add(sponsor_image);

						HashMap<String, String> hashMap_sponsor_id = new HashMap<String, String>();
						hashMap_sponsor_id.put(Sponsers_Id, sponsor_id);

						HashMap<String, String> hashMap_sponsor_name = new HashMap<String, String>();
						hashMap_sponsor_name.put(Sponsers_Name, sponsor_name);

						HashMap<String, String> hashMap_sponsor_type = new HashMap<String, String>();
						hashMap_sponsor_type.put(Sponsers_Type, sponsor_type);

						HashMap<String, String> hashMap_sponsor_image = new HashMap<String, String>();
						hashMap_sponsor_image
						.put(Sponsers_Image, sponsor_image);

						HashMap<String, String> hashMap_sponsor_Order = new HashMap<String, String>();
						hashMap_sponsor_Order
						.put(Sponsers_Order, sponsor_order);

						HashMap<String, String> hashMap_sponsor_Url = new HashMap<String, String>();
						hashMap_sponsor_Url.put(Sponsers_Url, sponsor_url);

						HashMap<String, String> hashMap_event_category = new HashMap<String, String>();
						hashMap_event_category.put("event_category",
								event_category);

						HashMap<String, String> hashMap_sponsor_description = new HashMap<String, String>();
						hashMap_sponsor_description.put("sponsor_description",
								sponsor_description);


						ArrayList<HashMap<String, String>> Sponsors_arrayList = new ArrayList<HashMap<String, String>>();



						Sponsors_arrayList.add(hashMap_sponsor_id);
						Sponsors_arrayList.add(hashMap_sponsor_name);
						Sponsors_arrayList.add(hashMap_sponsor_type);
						Sponsors_arrayList.add(hashMap_sponsor_image);
						Sponsors_arrayList.add(hashMap_sponsor_Order);
						Sponsors_arrayList.add(hashMap_sponsor_Url);
						Sponsors_arrayList.add(hashMap_event_category);
						Sponsors_arrayList.add(hashMap_sponsor_description);

						dataBase.addSponsors(Sponsors_arrayList);

					} catch (JSONException e) {
						System.out.println("Json Exception :" + e);
					}
				}
				SetGet.setSponsorImageURL(sponsor_image_urlaArrayList);

				int agenda_speaker_id, agenda_agenada_id, speaker_speaker_id;

				for (int i = 0; i < response_json_array_agenda_speaker.length(); i++) {
					JSONObject agenda_temp = new JSONObject();
					try {
						agenda_temp = response_json_array_agenda_speaker
								.getJSONObject(i);

						agenda_speaker_id = agenda_temp
								.getInt("agenda_speaker_id");
						agenda_agenada_id = agenda_temp.getInt("agenda_id");
						speaker_speaker_id = agenda_temp.getInt("speaker_id");

						long a=dataBase.addagendaspeaker(agenda_speaker_id,
								agenda_agenada_id, speaker_speaker_id);
						//System.out.println("Nikhil "+a);

					} catch (JSONException e) {
						System.out.println("Json Exception :" + e);
					}
				}

				String jury_id, jury_name, jury_info, jury_designation, jury_image, jury_type,jury_event_category;

				for (int i = 0; i < response_json_array_jury.length(); i++) {
					JSONObject agenda_temp = new JSONObject();
					try {
						agenda_temp = response_json_array_jury.getJSONObject(i);

						jury_id = agenda_temp.getString("jury_id");
						jury_name = agenda_temp.getString("jury_name");
						jury_info = agenda_temp.getString("jury_info");
						jury_designation = agenda_temp
								.getString("jury_designation");
						jury_image = agenda_temp.getString("jury_image");
						jury_type = agenda_temp.getString("jury_type");
						jury_event_category= agenda_temp.getString("event_category");
						jury_image_urlaArrayList.add(jury_image);

						dataBase.addJuryMember(jury_id, jury_name, jury_info,
								jury_designation, jury_image, jury_type,jury_event_category);

					} catch (JSONException e) {
						System.out.println("Json Exception :" + e);
					}
				}

				SetGet.setJuryImageURL(jury_image_urlaArrayList);

				String program_id, program_name, program_venue, program_info, program_date, program_image, program_link, program_event_category;

				JSONObject progrmjson = new JSONObject();
				for (int i = 0; i < response_json_array_programs.length(); i++) {
					progrmjson = response_json_array_programs.getJSONObject(i);
					program_id = progrmjson.getString("program_id");
					program_name = progrmjson.getString("program_name");
					program_venue = progrmjson.getString("program_venue");
					program_date = progrmjson.getString("program_date");
					program_image = progrmjson.getString("program_image");
					program_link = progrmjson.getString("program_link");
					program_event_category = progrmjson
							.getString("event_category");
					program_info = progrmjson.getString("program_description");
					// String programid,String programname,String
					// programdate,String programlink,String programvenue,String
					// programinfo,String programimage,String
					// programeventcategory
					dataBase.addPrograms(program_id, program_name,
							program_date, program_link, program_venue,
							program_info, program_image, program_event_category);
					/*dataBase.addPrograms(program_id, program_name,
							program_date, program_link, program_venue,
							program_info, program_image, program_event_category);*/

				}

				dataBase.close();


				/*// Get the value of Event Details Update Count 
				try { 
					val =jsonParserString .getJSONFromUrl("http://sankalp.eventbuoy.com/admin/admin/get_event_details_updatecount.php"); 
					System.out.println("Agenda Event Count "+val);
				}catch(Exception e) 
				{ 
					System.out.println(e); 
				}

				SetGet.storeUsingSharedPreference(context, "EVENTCOUNT",
						val);

				// Get the value of Event Details Update Count 
				try { 
					val =jsonParserString .getJSONFromUrl("http://sankalp.eventbuoy.com/admin/admin/get_event_details_updatecount.php"
							); System.out.println("Agenda Event Count "+val);
				}catch(Exception e) { System.out.println(e); }

				SetGet.storeUsingSharedPreference(context, "EVENT_COUNT",
						val);

				// Get the value of Speaker Update Count 
				try {
					val =jsonParserString .getJSONFromUrl("http://sankalp.eventbuoy.com/admin/admin/get_speakers_update_count.php");
					System.out.println("Speaker Count "+val);
				}catch(Exception e) { System.out.println(e); }
				SetGet.storeUsingSharedPreference(context, "SPEAKER_COUNT",val);

				// Get the value of Partners Update Count 
				try { 
					val =jsonParserString .getJSONFromUrl(
							"http://sankalp.eventbuoy.com/admin/admin/get_sponsors_updatecount.php"); 
					System.out.println("Agenda Event Count "+val);
				}catch(Exception e) { System.out.println(e); }
				SetGet.storeUsingSharedPreference(context, "PARTNER_COUNT",
						val);

				// Get the value of Jury Member Update Count 
				try { 
					val =jsonParserString .getJSONFromUrl(
							"http://sankalp.eventbuoy.com/admin/admin/get_jury_info_updatecount.php"
							); 
					System.out.println("Agenda Event Count "+val);
				}catch(Exception e) { System.out.println(e); }
				SetGet.storeUsingSharedPreference(context, "JURY_COUNT",
						val);

				// Get the value for MenuList Update Count 
				try { 
					val = jsonParserString .getJSONFromUrl(
							"http://sankalp.eventbuoy.com/admin/admin/get_program_update_count.php"
							); System.out.println("Program Count "+val); 
				} catch	(Exception e)
				{ // TODO: handle exception
					System.out.println(e);
				}

				SetGet.storeUsingSharedPreference(context, "PROGRAM_COUNT",
						val);
*/
				JSONParser jsonParser1 = new JSONParser();
				JSONObject jsonObject1 = new JSONObject();
				JSONArray jArray = null;
				
				try {
					jsonObject1 = jsonParser1.getJSONFromUrl("http://amen.eventbuoy.com/admin/admin/get_update_counts.php");
					if (jsonObject1 != null) {
						
						jArray = jsonObject1.getJSONArray("update_count");
						
						//System.out.println("jArray "+jsonObject);
						
						for (int i = 0; i < jArray.length(); i++) {
							JSONObject temp = new JSONObject();
							//System.out.println("OBJECT "+temp.toString());
							try {
								temp=jArray.getJSONObject(i);
								
								SetGet.storeUsingSharedPreference(context, "EVENT_COUNT",temp.getString("event_info_update_count"));
								SetGet.storeUsingSharedPreference(context, "SPEAKER_COUNT",temp.getString("speakers_update_count"));
								SetGet.storeUsingSharedPreference(context, "PARTNER_COUNT",temp.getString("sponsors_update_count"));
								SetGet.storeUsingSharedPreference(context, "JURY_COUNT",temp.getString("jury_info_update_count"));
								SetGet.storeUsingSharedPreference(context, "PROGRAM_COUNT",temp.getString("program_update_count"));
								
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}
						}
						
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}

				
				
				
			} else {
				System.out.println("sorry");
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			e.printStackTrace();
		}

		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		//dialog.dismiss();

		// DownloadImages images=new DownloadImages(context,null);
		// images.execute();
		/*
		 * DownloadImages images = new DownloadImages(context,
		 * SetGet.getSpeakerImageURL(), 1, null); images.execute();
		 */

		ArrayList<String> sp_img_URL = SetGet.getSpeakerImageURL();

		//ImageLoader imageLoader = new ImageLoader(context);

		/*for (int i = 0; i < sp_img_URL.size(); i++) {
			imageLoader.DisplayImage(
					"http://sankalp.eventbuoy.com/admin/uploads/"
							+ sp_img_URL.get(i).replaceAll(" ", "%20"), null);
		}

		ArrayList<String> partner_img_URL = SetGet.getSponsorImageURL();

		for (int i = 0; i < partner_img_URL.size(); i++) {
			imageLoader.DisplayImage(
					"http://sankalp.eventbuoy.com/admin/uploads/"
							+ partner_img_URL.get(i).replaceAll(" ", "%20"),
					null);
		}

		ArrayList<String> jury_img_URL = SetGet.getSponsorImageURL();

		for (int i = 0; i < jury_img_URL.size(); i++) {
			imageLoader.DisplayImage(
					"http://sankalp.eventbuoy.com/admin/uploads/jury/"
							+ jury_img_URL.get(i).replaceAll(" ", "%20"), null);
		}
		 */
		Intent intent = new Intent(context, MainScreen.class);
		//intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		context.startActivity(intent);
		((Activity) context).finish();

		/*
		 * DownloadImages imagesMenu=new DownloadImages(context,
		 * SetGet.getMenuImageURL(), 3, null); imagesMenu.execute();
		 */

		/*
		 * DownloadImages images1 = new DownloadImages(context,
		 * SetGet.getSponsorImageURL(), 0, null); images1.execute();
		 * DownloadImages images2 = new DownloadImages(context,
		 * SetGet.getJuryImageURL(), 2, null); images2.execute();
		 */

		/*
		 * Intent intent = new Intent(context, HomeActivity.class);
		 * context.startActivity(intent); ((Activity) context).finish();
		 */

	}

}
