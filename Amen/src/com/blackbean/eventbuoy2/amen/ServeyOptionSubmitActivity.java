package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.utility.JSONParser;
import com.blackbean.utility.NetworkCheck;


public class ServeyOptionSubmitActivity extends Activity {
	// RadioGroup linear;
	Button submit;
	RadioGroup group;
	TextView question;
	
	// int id=-1;
	public static ArrayList<String> survey_option_id, survey_option_title;
	ArrayList<Integer> buttonID;
	int selected=-1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.servey_submit);
		final int _id = Integer.parseInt(getIntent().getExtras().getString("id"));

		group = (RadioGroup) findViewById(R.id.list_option);
		question=(TextView)findViewById(R.id.question);
		group.setOrientation(RadioGroup.VERTICAL);
		question.setText(getIntent().getExtras().getString("question"));

		group.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup arg0, int arg1) {
				// TODO Auto-generated method stub
				selected=arg1;
			
			//	Toast.makeText(ServeyOptionSubmitActivity.this, ""+arg1, Toast.LENGTH_SHORT).show();

			}
		});
		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ServeyOptionSubmitActivity.this,
						HomeActivity.class);
				startActivity(intent);
				finish();

			}
		});

		// linear = (LinearLayout) findViewById(R.id.list_option);
		submit = (Button) findViewById(R.id.submit);
		submit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				//if(group.isSelected())
				{
					
					//Toast.makeText(ServeyOptionSubmitActivity.this, ""+), Toast.LENGTH_SHORT).show();
					//buttonID.indexOf(group.getCheckedRadioButtonId());
					
					
					
				}
				
				
				if(selected==-1)
				{
					Toast.makeText(ServeyOptionSubmitActivity.this, "Please Select option", Toast.LENGTH_SHORT).show();
					
				}
				else 
				{
					Intent intent=new Intent(ServeyOptionSubmitActivity.this,ServeyResultActivity.class );
					intent.putExtra("_id",_id);
					intent.putExtra("ans",selected);
					intent.putExtra("question",question.getText().toString());
					startActivity(intent);
					finish();
				}
				
				
			
		

			}
		});
		if (NetworkCheck.getConnectivityStatusString(ServeyOptionSubmitActivity.this)) {
			GetOptions option = new GetOptions(this, _id);
			option.execute();	
		}
		else {
			Toast.makeText(ServeyOptionSubmitActivity.this, "Internet is needed", Toast.LENGTH_LONG).show();
		}
		

	}

	class GetOptions extends AsyncTask<Void, Void, Void> {
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		Context context;
		JSONArray response = new JSONArray();
		int id;
		Dialog dialog;

		public GetOptions(Context context, int id) {
			// TODO Auto-generated constructor stub
			this.context = context;
			this.id = id;
			survey_option_title = new ArrayList<String>();
			survey_option_id = new ArrayList<String>();
			buttonID=new ArrayList<Integer>();

		}
		
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new Dialog(context);
			ProgressBar bar = new ProgressBar(context);
			dialog.setContentView(bar);
			dialog.setTitle("Please Wait...");
			dialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			try {

				jsonObject = jsonParser
						.getJSONFromUrl("http://listoftablets.com/SHRM/admin/GetSurveyOptionsByQuestion_ID.php?survey_question_id="
								+ id);

				// String
				// story_id,story_type,story_person_name,story_person_location,story_person_image,story_description,story_video;

				if (jsonObject != null)

				{
					response = jsonObject.getJSONArray("survey_option");
					for (int i = 0; i < response.length(); i++) {
						JSONObject agenda_temp = new JSONObject();
						try {
							agenda_temp = response.getJSONObject(i);

							survey_option_id.add(agenda_temp
									.getString("survey_option_id"));
							survey_option_title.add(agenda_temp
									.getString("survey_option_title"));

						} catch (JSONException e) {
							System.out.println("Json Exception :" + e);
						}
					}
				} else {
					System.out.println("sorry story");
				}

			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//LayoutParams param=new LayoutParams(LayoutParams.FILL_PARENT, 5);
			//View view1 = new View(ServeyOptionSubmitActivity.this);
			//view1.setLayoutParams(param);
			dialog.dismiss();
			for (int i = 0; i < survey_option_id.size(); i++) {
				RadioButton rbtn = new RadioButton(context);
				rbtn.setHeight(80);
				rbtn.setTextColor(Color.WHITE);
				rbtn.setTextSize(18);
				//rbtn.setTag(""+i);
				buttonID.add(rbtn.getId());
				rbtn.setText(survey_option_title.get(i));
				//group.addView(view1);
				rbtn.setId(Integer.parseInt(survey_option_id.get(i)));
				group.addView(rbtn);

			}
			// linear.addView(group);
		}

	}

}
