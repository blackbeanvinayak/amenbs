package com.blackbean.eventbuoy2.amen;
/*package com.blackbean.eventbuoy2.shrm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackbean.adapters.ExpandableListAdapter;

public class AwardInformationActivity extends Activity {
	TextView award_category;
	ImageView image;
	ExpandableListAdapter listAdapter;
	ExpandableListView expandable;
	static List<String> listDataHeader;
	static HashMap<String, List<String>> listDataChild;
	static HashMap<String, List<String>> listDataChildsocial;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.award_informtion);
		image = (ImageView) findViewById(R.id.image);

		award_category = (TextView) findViewById(R.id.category);
		award_category.setText(getIntent().getExtras().getString("name"));

		if (getIntent().getExtras().getInt("image_id") == 0) {
			image.setImageResource(R.drawable.digital);
		}
		if (getIntent().getExtras().getInt("image_id") == 1) {
			image.setImageResource(R.drawable.website);
		}
		if (getIntent().getExtras().getInt("image_id") == 2) {
			image.setImageResource(R.drawable.mobile);
		}
		if (getIntent().getExtras().getInt("image_id") == 3) {
			image.setImageResource(R.drawable.special);
		}

		expandable = (ExpandableListView) findViewById(R.id.explist);
		listDataChild = new HashMap<String, List<String>>();
		listDataChildsocial = new HashMap<String, List<String>>();
		listDataHeader = new ArrayList<String>();
		listDataHeader.add("Mobile Advertising & Marketing");
		listDataHeader.add("Consumer Mobile Services");

		listDataHeader.add("Innovative Mobile App");

		// Adding child data
		List<String> mam = new ArrayList<String>();
		mam.add("Original, creative and acclaimed mobile marketing campaign executed via the medium of text or multimedia messaging, in-app advertising, branding, barcodes, QRcodes or any other mobile platform based property with/without cohesion with social media that helped achieve success and recognition.");
		List<String> cms = new ArrayList<String>();
		cms.add("Remarkable service which provides facilities pertaining to entertainment needs of the users including graphics, music,video, social media, games, m-commerce, messaging, mailing, data-storing, GPS/location tracking, etc. through cross-platform forums, mobile- based browsers, applications and/or portals.");
		List<String> lma = new ArrayList<String>();
		lma.add("Preventative and innovative mobile App designed with the idea of serving the consumer or enterprise users in order to meticulously cater to a need that hasn’t been previously addressed by any other mobile application or portal");

		List<String> sociallistsocial = new ArrayList<String>();
		sociallistsocial.add("social");

		listDataChild.put(listDataHeader.get(0), mam); // Header, Child
		listDataChild.put(listDataHeader.get(1), cms);
		listDataChild.put(listDataHeader.get(2), lma);

		listAdapter = new ExpandableListAdapter(AwardInformationActivity.this,
				listDataHeader, listDataChild);

		// setting list adapter
		expandable.setAdapter(listAdapter);

		ImageView menu = (ImageView) findViewById(R.id.logo);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AwardInformationActivity.this,
						HomeActivity.class);
				startActivity(intent);

			}
		});
	}

}
*/