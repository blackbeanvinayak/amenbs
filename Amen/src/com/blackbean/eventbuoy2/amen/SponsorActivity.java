package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;
import com.blackbean.utility.DataBase;
import com.blackbean.utility.ImageLoader;

/**
 * Display the question answer description which has selected .
 * 
 * @author blackbeanUmesh
 * 
 */
public class SponsorActivity extends FragmentActivity {

	Context context;
	ViewPager mViewPager;
	DescriptionAdapter descriptionAdapter;
	ImageView logoImageView;
	int index = 0;

	static int sponsor_count;
	static ArrayList<String> sponsor_id, sponsor_name, sponsor_type,sponser_desc,
	sponsor_url;
	static ArrayList<String> sponsers_image;
	int pagerCount, currentpage = 0;
	ImageView previous, next;
	String sponser_category,eventCategory;
	TextView PartnerHeader;

	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_sponsor);
		logoImageView = (ImageView) findViewById(R.id.home);

		PartnerHeader=(TextView) findViewById(R.id.PartnerHeader);

		//eventCategory=getIntent().getStringExtra("eventCategory");
		if(HomeActivity.whichEvent==StaticData.Event_Nairobi)
		{
			eventCategory="HPI";
		}else {
			eventCategory="MDET";
		}

		sponser_category=getIntent().getStringExtra("sponser_category");


		/*if(eventCategory.equals("Nairobi"))
		{
			switch (position) {
			case 0:

				sponser_category="Associate Partners";
				break;
			case 1:

				sponser_category="Content Partners";
				break;

			case 2:
				sponser_category="Founding Partners";

				break;

			case 3:
				sponser_category="Lanyrd Partner";
				break;

			case 4:
				sponser_category="Outreach Partners";
				break;

			case 5:
				sponser_category="Strategic Partner";
				break;
			case 6:
				sponser_category="Innovation Track Partner";
				break;
			case 7:
				sponser_category="Online Media Partners";
				break;
			case 8:
				sponser_category="Enterprise Support Partners";
				break;
			case 9:
				sponser_category="Inclusive Business Track Partner";
				break;
			case 10: sponser_category="Mobile App Partner";
			break;		
			}
		}else if (eventCategory.equals("Delhi")){
			switch (position) {
			case 0:

				sponser_category="Industry and Expo Partner";
				break;
			case 1:

				sponser_category="Strategic Partner";
				break;

			case 2:
				sponser_category="Core Partner - Healthcare";

				break;

			case 3:
				sponser_category="Core Partner - Innovation";
				break;

			case 4:
				sponser_category="Support Partner - Financial Inclusion";
				break;

			case 5:
				sponser_category="Knowledge Partner";
				break;
			case 6:
				sponser_category="Sankalp Awards Grand Prize Partner";
				break;
			case 7:
				sponser_category="Sankalp Awards Partner";
				break;
			case 8:
				sponser_category="Sankalp Peoples Choice Awards Partner";
				break;
			case 9:
				sponser_category="Local Ecosystem Partner";
				break;
			case 10: sponser_category="Mobile App Partner";

			break;	

			case 11: sponser_category = "Online Media Partner";
			break;
			}
		}
		 */
		PartnerHeader.setText("Sponsors");

		logoImageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SponsorActivity.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();
			}
		});

		sponsor_id = new ArrayList<String>();
		sponsor_name = new ArrayList<String>();
		sponsor_type = new ArrayList<String>();
		sponsers_image = new ArrayList<String>();
		sponsor_url = new ArrayList<String>();
		sponser_desc = new ArrayList<String>();

		DataBase db = new DataBase(this);
		db.open();

		sponsor_id.addAll(db
				.getSponsorAllDataOffieldByFieldAndCategory("sponsers_id",sponser_category,eventCategory));
		sponsor_name.addAll(db
				.getSponsorAllDataOffieldByFieldAndCategory("sponsers_name",sponser_category,eventCategory));
		sponsor_type.addAll(db
				.getSponsorAllDataOffieldByFieldAndCategory("sponsers_type",sponser_category,eventCategory));
		sponsers_image.addAll(db
				.getSponsorAllDataOffieldByFieldAndCategory("sponsers_image",sponser_category,eventCategory));
		sponsor_url.addAll(db
				.getSponsorAllDataOffieldByFieldAndCategory("sponsers_url",sponser_category,eventCategory));

		sponser_desc.addAll(db
				.getSponsorAllDataOffieldByFieldAndCategory("sponsor_description",sponser_category,eventCategory));

		db.close();



		View view = this.getWindow().getDecorView();

		/**
		 * CONFIGURATION VARIABLES FOR GRAPHICS etc (TextColor,BackgroundColor)
		 */
		view.setBackgroundColor(Color
				.parseColor(StaticData.SPONSER_ACTIVTY_BG_COLOR));
		TextView sponsers_header = (TextView) findViewById(R.id.sponsers_header);
		// sponsers_header.setTextColor(Color.parseColor(StaticData.SPONSER_ACTIVTY_HEDER_TXT_COLOR));

		context = this;
		previous = (ImageView) findViewById(R.id.previous);
		next = (ImageView) findViewById(R.id.next);
		previous.setVisibility(View.INVISIBLE);
		descriptionAdapter = new DescriptionAdapter(
				getSupportFragmentManager(), context);
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(descriptionAdapter);

		pagerCount = descriptionAdapter.getCount();
		if (pagerCount == 1 || sponsor_count ==0) {
			next.setVisibility(View.INVISIBLE);
			previous.setVisibility(View.INVISIBLE);
		}

		previous.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				if (currentpage != 0) {

					if (currentpage == pagerCount - 2) {
						next.setVisibility(View.VISIBLE);
					}
					if (currentpage == 1) {
						v.setVisibility(View.INVISIBLE);
					}

					mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);
					//currentpage--;

				} else {
					v.setVisibility(View.INVISIBLE);
					next.setVisibility(View.VISIBLE);
				}

			}
		});
		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (currentpage != pagerCount) {
					if (currentpage == 0) {
						previous.setVisibility(View.VISIBLE);
					}
					if (currentpage == pagerCount - 2) {
						v.setVisibility(View.INVISIBLE);
					}

					mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
					//currentpage++;

				} else {
					v.setVisibility(View.INVISIBLE);
					previous.setVisibility(View.VISIBLE);
				}

			}
		});

		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub

				if (arg0 == 0) {
					previous.setVisibility(View.INVISIBLE);
					next.setVisibility(View.VISIBLE);

				}
				if (arg0 > 0) {
					previous.setVisibility(View.VISIBLE);
				}

				if ((pagerCount - 1) == arg0) {
					previous.setVisibility(View.VISIBLE);
					next.setVisibility(View.INVISIBLE);
				}

				if (arg0 < pagerCount - 1) {
					next.setVisibility(View.VISIBLE);
				}

				currentpage = arg0;

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});
	}

	public static class DescriptionAdapter extends FragmentPagerAdapter {
		Context context;

		public DescriptionAdapter(FragmentManager fm, Context context) {
			super(fm);
			this.context = context;
			// TODO Auto-generated constructor stub
		}

		@Override
		public Fragment getItem(int i) {
			// TODO Auto-generated method stub

			Fragment fragment = new SimpleFragment(context);
			Bundle args = new Bundle();

			args.putInt("position", i);

			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub

			return sponsor_id.size();
		}

	}

	public static class SimpleFragment extends Fragment {

		ImageLoader imageLoader;
		Context context;

		public SimpleFragment(Context context) {
			// TODO Auto-generated constructor stub
			this.context=context;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub sponsers_type
			final View rootView = inflater.inflate(R.layout.adapter_sponsor,
					container, false);
			TextView sponsers_name = (TextView) rootView
			.findViewById(R.id.sponsers_name);
			TextView sponsers_type = (TextView) rootView
					.findViewById(R.id.sponsers_type);
			sponsers_type.setVisibility(View.GONE);

			
			
			Bundle get_info = getArguments();
			final int position = get_info.getInt("position");
			
			sponsers_name.setText(sponsor_name.get(position));
			ImageView sponsersimage = (ImageView) rootView
					.findViewById(R.id.sponsers_image);

			imageLoader = new ImageLoader(context);

			// sponsers_image.setImageResource(SponsorActivity.sponsers_image.get(position));

			/*sponsersimage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (!sponsor_url.get(position).equalsIgnoreCase("Not")) {
						try {
							Intent intent = new Intent(Intent.ACTION_VIEW, Uri
									.parse(sponsor_url.get(position)));

							startActivity(intent);
						} catch (Exception e) {

						}
					}

				}
			});
			*/
			imageLoader.DisplayImage("http://amen.eventbuoy.com/admin/uploads/"
					+ sponsers_image.get(position).replaceAll(" ", "%20"), sponsersimage);

			System.out.println("Partner Link "+"http://amen.eventbuoy.com/admin/uploads/"
					+ sponsers_image.get(position).replaceAll(" ", "%20"));

			/*sponsersimage.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(context, SponserDescriptionActivity.class);
					
					intent.putExtra("NAME", sponsor_name.get(position));
					intent.putExtra("DESC", sponser_desc.get(position));
					startActivity(intent);
				}
			});*/

			return rootView;
		}

	}

}
