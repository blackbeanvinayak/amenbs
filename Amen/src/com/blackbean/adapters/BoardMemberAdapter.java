package com.blackbean.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;

public class BoardMemberAdapter extends ArrayAdapter<String>{
Context context;
ArrayList<Integer> image;
ArrayList<String> arrayList;
	public BoardMemberAdapter(Context context, int resource,ArrayList<String> memnberlist) {
		super(context, R.layout.boardmember_adapter,memnberlist);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.arrayList=memnberlist;
		image=new ArrayList<Integer>();
	
		
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view=inflater.inflate(R.layout.boardmember_adapter, parent,false);
		TextView textView=(TextView)view.findViewById(R.id.member_name_txt);
		ImageView image=(ImageView)view.findViewById(R.id.member_image);
		image.setImageResource(R.drawable.members);
		textView.setText(this.arrayList.get(position));
		return view;
	}

	
}
