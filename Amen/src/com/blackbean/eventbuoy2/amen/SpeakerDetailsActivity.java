package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blackbean.adapters.ExpandableListAdapter;
import com.blackbean.adapters.NoteAdapter1;
import com.blackbean.adapters.NoteModel;
import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;
import com.blackbean.utility.DataBase;
import com.blackbean.utility.ImageLoader;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.google.analytics.tracking.android.EasyTracker;

public class SpeakerDetailsActivity extends FragmentActivity {
	SpeakerInfoAdapter speakerInfoAdapter;
	static ViewPager mViewPager;
	int _id;
	static Context context;
	static String Vfb = null;
	static String Vtw;
	static String Vgoogle;
	static String Vlinked;

	private static final String Agenda_NAME = "agenda_name";
	static ExpandableListAdapter listAdapter;
	static ExpandableListView expListView;
	static List<String> listDataHeader;
	static HashMap<String, List<String>> listDataChild;
	static HashMap<String, List<String>> listDataChildsocial;
	ImageView previous;
	ImageView next;
	static boolean Vcheckspeaker_id = false;
	static int pagerCount, currentpage = 0;
	int last;
	public static ArrayList<String> speaker_id;
	public static ArrayList<String> namearray;
	public static ArrayList<String> designation;
	public static ArrayList<String> description;
	public static ArrayList<String> speakerimage;

	public static ArrayList<String> sessionsArrayList;
	private static final String Speaker_Image = "speaker_image";

	public ImageLoader imageLoader;
	String event_category;
	int whichevent;
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.activity_speaker_details);
		RelativeLayout layout = (RelativeLayout) findViewById(R.id.ralativelayout);
		layout.setBackgroundColor(Color.WHITE);

		//DataBase database = new DataBase(this);
		//database.open();
		speaker_id = new ArrayList<String>();

		namearray = new ArrayList<String>();
		designation = new ArrayList<String>();
		description = new ArrayList<String>();
		speakerimage = new ArrayList<String>();
		_id = getIntent().getExtras().getInt("_id");

		whichevent=getIntent().getExtras().getInt("WHICH_EVENT");

		event_category="kochi";
		/*if(whichevent==StaticData.Event_Nairobi)
		{
			event_category="HPI";
		}else {
			event_category="MDET";
		}*/

		DataBase db = new DataBase(this);
		db.open();
		/*speaker_id.addAll(db.getSpeakerByfield("speaker_id"));
		namearray.addAll(db.getSpeakerByfield("speaker_name"));
		designation.addAll(db.getSpeakerByfield("speaker_designation"));
		description.addAll(db.getSpeakerByfield("speaker_description"));
		speakerimage.addAll(db.getSpeakerByfield("speaker_image"));
		 */
		speaker_id.addAll(db.getSpeakerByfieldANDeventcategory("speaker_id",event_category));
		namearray.addAll(db.getSpeakerByfieldANDeventcategory("speaker_name",event_category));
		designation.addAll(db
				.getSpeakerByfieldANDeventcategory("speaker_designation",event_category));
		description.addAll(db
				.getSpeakerByfieldANDeventcategory("speaker_description",event_category));
		speakerimage.addAll(db.getSpeakerByfieldANDeventcategory("speaker_image",event_category));
		db.close();

		/*
		 * id.add("0"); id.add("1"); id.add("2"); id.add("3"); id.add("4");
		 * id.add("5"); id.add("6"); id.add("7"); id.add("8"); id.add("9");
		 * id.add("10");
		 * 
		 * namearray.add("Mr. G. Vishwanathan");
		 * namearray.add("Professor Jagdish Sheth");
		 * 
		 * namearray.add("Vijay Michihito Batra");
		 * namearray.add("Prof.Dr.M.J.Xavier"); namearray.add("Rajiv Dingra");
		 * //name.add("Kulwinder Singh");
		 * 
		 * namearray.add("Mr.Rob Peck"); namearray.add("Shyam Sekar S.");
		 * namearray.add("Alet Viegas"); namearray.add("Ahmad Aftab Naqvi");
		 * namearray.add("Sorav Jain"); namearray.add("Sunny Nagpal");
		 * 
		 * 
		 * designation.add("Chancellor VIT University");
		 * designation.add("Professor of Marketing Emory University");
		 * 
		 * designation.add(
		 * "Motivational Speaker, Auther and founder Think Media Inc.");
		 * //designation
		 * .add("Director-Global Marketing Communication Synechron Technologies"
		 * ); designation.add(
		 * "Former Director IIM (R),Executive Director VIT University");
		 * designation.add("Founder & CEO WATConsult");
		 * 
		 * designation.add(
		 * "Director Client Services,03M Directional Marketing Pvt Ltd");
		 * designation.add("S.Chief Mentor & Strategies,Startup Xperts");
		 * designation.add("Founder Black Bean Engagement");
		 * designation.add("CEO GOZOOP");
		 * designation.add("Thinker in Chief Echovme");
		 * designation.add("Managing Director & Co-Founder Hitpool India");
		 * 
		 * description.add(""); description.add(
		 * "Professor JagdishSheth is the Charles H.Kellstadt Professor of Marketing at Emory University Goizueta Business School.\n\n He is known nationally and internationally for his scholarly contributions in consumer behavior, relationship marketing, competitive strategy, and geopolitical analysis. When he joined Emory’s faculty in 1991, Professor Sheth had nearly 30 years of combined experience in marketing from the University of Southern California, the University of Illinois, Columbia University, and Massachusetts Institute of Technology.\n\nThroughout his career, Professor Sheth has offered more than a thousand presentations in at least twenty countries. He has also provided consulting for numerous companies in the United States, Europe and Asia. His client list includes AT&T, BellSouth, Cox Communications, Delta, Ernst & Young, Ford, GE, Lucent Technologies, Motorola, Nortel, Pillsbury, Sprint, Square D, 3M, Whirlpool, and others. Currently, Professor Sheth sits on the Board of Directors of several public companies including Norstan, Cryo Cell International, and Wipro Limited. \n\nProfessor Sheth’s accolades include “Outstanding Marketing Educator,” an award presented by the Academy of Marketing Science, the “Outstanding Educator” award twice-presented by Sales and Marketing Executives International, and the P.D. Converse Award for his outstanding contributions to theory in marketing, presented by the American Marketing Association. Professor Sheth is the recipient of the two highest awards given by the American Marketing Association: the Richard D. Irwin Distinguished Marketing Educator Award and the Charles Coolidge Parlin Award. \n\nIn 1996, Professor Sheth was selected as the Distinguished Fellow of the Academy of Marketing Science. The following year, he was awarded the Distinguished Fellow award from the International Engineering Consortium.Professor Sheth is currently a Fellow of the American Psychological Association (known as APA).\n\nProfessor Sheth has authored or coauthored hundreds of articles and books. In 2000, he and Andrew Sobel published the best seller, Clients for Life. In 2001, Value Space, which he coauthored with Banwari Mittal, was published. Professor Sheth’s most popular book, The Rule of Three, was coauthored with Dr. RajendraSisodia and published in 2002. He has since written notable publications: Tectonic Shift, Firms of Endearment and 4 As of Marketing."
		 * );
		 * 
		 * description.add(
		 * "Vijay MichihitoBatra is driven by a deep-seated interest in creating awareness of the need to remain positively focused in life through good and positive reportage. This idea gave birth to Think Media Inc. – a creative showcasing of innovations in the business process and bringing Corporate News Updates and giving coverage to what is inspiring and motivating. Through his upcoming (project underway) personalized show Rendezvous with Vijay Batra , he would like to explore the positive and inspirational thoughts and the success principles underlying the achievements which drive India’s Wealth Makers. People who know him closely identify him as to possessing a single-minded pursuit to deepen the understanding and impact of sowing seeds of Positive Thoughts in People’s Minds to make Life Better. It is a mission typified by his Training Organization, Think Inc. taking it further to a new platform and steadily expanding its reach is Think Media Inc. It is not a new venture alone, but also the fruition of Vijay Batra long standing dream of bringing to people’s lives a news portal developed for inspirational news what we call “Positive News”. \n\nIt is a channel geared to tell the untold, lesser known - scavenging for only the real, extracting treasure from unmined and unexpected sources. \n\nThink Media is resolved to undo the negative impacts of fear-mongering media, sensationalizing and depressing reporting and worrying trends created by pessimistic socio - economic forecasts. His optimistic venture is completely rooted in reality as Vijay Batra encourages courageous worrying instead. It is a skill that he practices with great commitment himself. He learnt this through his experience and observation of Japanese business practices. Vijay is an alumni of PHP Institute, a Think Tank affiliated to Panasonic, founded by Matsushita Konosuke, who is considered to be the ultimate authority in management and leadership in Japan. Already blessed with a unique lineage, and sharpened by his training in USA and Japan, Vijay embodies the Samurai spirit and comprehensively translates its essence, adapting it with equal intensity for the climate of the Indian soil. \n\nAs the Editor-in-Chief of Think Media Inc., Vijay Batra is committed to establish Think Media Inc. as a vehicle devoted to building opinion through openness. During his education in University of Pittsburgh he felt this deep-rooted desire to share one’s privileges by creating a common good and wellness through common sense - a wealth that would enable individuals to lead fulfilled and empowered lives. \n\nElectrifying, intensely involved, down to earth, amazingly humorous, extremely driven and passionately connected, yet a man characterized by his benevolence and honesty and remarkable simplicity. "
		 * ); description.add(
		 * "Prof. MJ Xavier obtained his Doctorate in Management (1984) from the Indian Institute of Management, Calcutta. He is basically an engineer with an M.Tech. (1979) in Chemical Plant Engineering from Regional Engineering College, Warangal and B.Tech.(1976) in Chemical Engineering from Coimbatore Institute of Technology. \n\nProf. M. J. Xavier was the Founding Director of Indian Institute of Management Ranchi (IIM Ranchi). He has more than 25 years of professional experience in teaching, research, and consultancy. He has served as a Research Executive in Mode Research Pvt. Ltd., Calcutta (1982-84) and as Manager in-charge of Management Development and Services with SPIC Ltd., Chennai (1985-91). He has taught at XLRI, Jamshedpur (1984-84), IIM Bangalore (1991-96), IFMR Chennai (1998 – 2006) and Great lakes Institute of Management, Chennai (2008-2010). He is currently the Executive Director at VIT Business School (Vellore/Chennai) \n\nHis areas of interest include Marketing Research, Data Mining, e-Governance and Spirituality. He has conducted a number of training programmes for executives and has also been a consultant to several companies in India and abroad. He has authored three books and published more than 100 articles in Journals and Magazines in India and Abroad. His book `Marketing in the New Millennium’ won the DMA-Escorts Award for the best Management Book of the Year 1999. \n\nHe has served as Visiting Faculty to a number of business schools in India and abroad including The University of Buckingham, U.K. (1993), The Texas Christian University, U.S.A. (1999), California Polytechnic State University, USA (2003-05) and The American University of Armenia, Armenia (2006). "
		 * ); //description.add(""); description.add(""); description.add("");
		 * description.add(""); description.add(
		 * "I currently lead the team at Black Bean Engagement, and work with brands, businesses and agencies. We deliver innovative and successful marketing campaigns for customers like DC Design, TastyKhana.com, HDFC Life, NASSCOM, ColumnIT, Vardenchi and more. \n\nOver the last seven years I have been dabbling in roles that require me to work as an Internet marketing strategist, social media analyst, mobile marketing, application development & eLearning for businesses that need integrated marketing solutions.\n\nParticularly interested in businesses that want to target an online audience.\n\nSpecialties: creative strategy, digital marketing, social media, online branding and eLearning"
		 * ); description.add(""); description.add(""); description.add("");
		 * 
		 * speakerimage.add(R.drawable.g_vishwanathan);
		 * speakerimage.add(R.drawable.jagadish_seth);
		 * 
		 * speakerimage.add(R.drawable.vijay);
		 * speakerimage.add(R.drawable.mj_xavier);
		 * //speakerimage.add(R.drawable.kulwinder_singh);
		 * speakerimage.add(R.drawable.rajivdingra);
		 * 
		 * speakerimage.add(R.drawable.rob);
		 * speakerimage.add(R.drawable.shyam_sekar_s);
		 * speakerimage.add(R.drawable.alet);
		 * speakerimage.add(R.drawable.ahemed);
		 * speakerimage.add(R.drawable.sorav_ain);
		 * speakerimage.add(R.drawable.sunny_nagpal);
		 */


		context = SpeakerDetailsActivity.this;
		speakerInfoAdapter = new SpeakerInfoAdapter(
				getSupportFragmentManager(), context);
		mViewPager = (ViewPager) findViewById(R.id.pager);
		pagerCount = speakerInfoAdapter.getCount();
		mViewPager.setAdapter(speakerInfoAdapter);
		pagerCount = getCount();
		previous = (ImageView) findViewById(R.id.previous);
		next = (ImageView) findViewById(R.id.next);
		if(speaker_id.contains(_id+""))
		{
			mViewPager.setCurrentItem(speaker_id.indexOf(_id+""));// ****************
		}
		if (mViewPager.getCurrentItem() == 0) {
			previous.setVisibility(View.INVISIBLE);
		}
		if (mViewPager.getCurrentItem() == (pagerCount - 1)) {
			next.setVisibility(View.INVISIBLE);
		}
		ImageView menu = (ImageView) findViewById(R.id.home);
		//menu.setColorFilter(Color.argb(255, 0, 0, 0));
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SpeakerDetailsActivity.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();
			}
		});
		final ImageView shareSHRM=(ImageView) findViewById(R.id.shareSHRM);
		/*shareSHRM.setOnClickListener(new OnClickListener() {

			@SuppressLint("InlinedApi")
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				LayoutInflater layoutInflater  = (LayoutInflater)getBaseContext()
						.getSystemService(LAYOUT_INFLATER_SERVICE); 
				View popupView = layoutInflater.inflate(R.layout.popup_speaker, null); 
				final PopupWindow popupWindow=new PopupWindow(popupView,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

				ImageView img_facebook=(ImageView) popupView.findViewById(R.id.img_facebook);
				ImageView img_twitter=(ImageView) popupView.findViewById(R.id.img_twitter);
				ImageView img_mail=(ImageView) popupView.findViewById(R.id.img_mail);

				img_facebook.setOnClickListener(new OnClickListener() {

					@SuppressWarnings("deprecation")
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String url;
						DataBase db=new DataBase(context);
						db.open();

						url = db.getSpeakeSocialUrl(mViewPager.getCurrentItem(), "speaker_fb");

						System.out.println("SPurl "+url);
						System.out.println("Spid "+mViewPager.getCurrentItem());
						db.close();

						Intent intent=new Intent(context, SocialActivity.class);
						intent.putExtra("URL", url);
						startActivity(intent);
						//finish();
						popupWindow.dismiss();	
					}
				});

				img_twitter.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String url;
						DataBase db=new DataBase(context);
						db.open();

						url = db.getSpeakeSocialUrl(_id, "speaker_twiter");
						System.out.println("SPurl "+url);
						System.out.println("Spid "+_id);
						db.close();

						Intent intent=new Intent(context, SocialActivity.class);
						intent.putExtra("URL", url);
						startActivity(intent);
						//finish();

						popupWindow.dismiss();	

					}
				});

				img_mail.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String url;
						DataBase db=new DataBase(context);
						db.open();

						url = db.getSpeakeSocialUrl(_id, "speaker_google");
						db.close();


						Intent intent=new Intent(context, SocialActivity.class);
						intent.putExtra("URL", url);
						startActivity(intent);
						//finish();

						popupWindow.dismiss();	

					}
				});
				popupWindow.setBackgroundDrawable(new BitmapDrawable());
				popupWindow.setOutsideTouchable(true);
				popupWindow.showAsDropDown(shareSHRM, 50, -20);
			}
		});*/


		previous.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);

			}
		});

		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);

			}
		});

		last = mViewPager.getCurrentItem();

		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub

				if (arg0 == 0) {
					previous.setVisibility(View.INVISIBLE);
					next.setVisibility(View.VISIBLE);

				}
				if (arg0 > 0) {
					previous.setVisibility(View.VISIBLE);
				}

				if ((pagerCount - 1) == arg0) {
					previous.setVisibility(View.VISIBLE);
					next.setVisibility(View.INVISIBLE);
				}

				if (arg0 < pagerCount - 1) {
					next.setVisibility(View.VISIBLE);
				}

				currentpage = arg0;

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	public static class SpeakerInfoAdapter extends FragmentStatePagerAdapter {

		Context context;

		public SpeakerInfoAdapter(FragmentManager fm, Context context) {
			super(fm);
			this.context = context;
			// TODO Auto-generated constructor stub
		}

		@Override
		public Fragment getItem(int i) {
			// TODO Auto-generated method stub
			Fragment fragment = new DemoObjectFragment();
			Bundle args = new Bundle();

			// args.putInt(DemoObjectFragment.ARG_OBJECT, i + 1); // Our object
			// is just an integer :-P

			args.putString("speaker_id", "speaker" + i);

			args.putInt("id", i);
			args.putInt("spkrid", Integer.parseInt(speaker_id.get(i)));

			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub

			return namearray.size();

		}

	}

	public int getCount() {
		return speakerInfoAdapter.getCount();
	}

	public static class DemoObjectFragment extends Fragment {
		public int id;
		int spkr_id;
		String name;
		String detail1;
		String detail2;
		String image_url;

		ImageView spkr_image_speaker_info;
		public static String info;
		public static String sessions;
		public static String social;

		ArrayList<NoteModel>NotesArrayList;
		ListView listView;
		NoteAdapter1 adapterNote;
		int note_id=-1;


		public static final String ARG_OBJECT = "object";
		Bitmap bmp;

		ImageView imgFacebook,imgTwitter,imgMessage;
		ImageLoader imageLoader;

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.adapter_speaker_details,
					container, false);
			Bundle get_info = getArguments();
			NotesArrayList=new ArrayList<NoteModel>();
			imageLoader = new  ImageLoader(context);

			id = get_info.getInt("id");
			spkr_id=get_info.getInt("spkrid");
			final TextView spkr_description = (TextView) rootView
					.findViewById(R.id.txt_speaker_info);
			final TextView spkr_name = (TextView) rootView
					.findViewById(R.id.txt_speaker_name);
			final TextView spkr_designation = (TextView) rootView
					.findViewById(R.id.txt_speaker_designation);

			listView=(ListView) rootView
					.findViewById(R.id.list_note);

			imgFacebook=(ImageView) rootView.findViewById(R.id.imgFacebook);
			imgTwitter=(ImageView) rootView.findViewById(R.id.imgTwitter);
			imgMessage=(ImageView) rootView.findViewById(R.id.imgMessage);

			DataBase database=new DataBase(context);
			database.open();
			//database.getNoteByPlaceId("SPEAKER", spkr_id);
			//add info in ListView
			NotesArrayList=database.getNoteByPlaceId("SPEAKER", spkr_id);
			for (int i = 0; i < NotesArrayList.size(); i++) {
				System.out.println("Id   "+NotesArrayList.get(i).getNote_id());
				System.out.println("Content   "+NotesArrayList.get(i).getNote_content());
				System.out.println("Info   "+NotesArrayList.get(i).getNote_info());
				System.out.println("Place   "+NotesArrayList.get(i).getNote_place());
				System.out.println("PlaceId   "+NotesArrayList.get(i).getNote_palceId());
			}
			adapterNote=new NoteAdapter1(context, NotesArrayList);
			listView.setAdapter(adapterNote);

			final Button btn_info = (Button) rootView
					.findViewById(R.id.btn_info);
			final Button btn_session = (Button) rootView
					.findViewById(R.id.btn_session); 
			final Button btn_notes = (Button) rootView
					.findViewById(R.id.btn_notes);
			Button btn_save = (Button) rootView
					.findViewById(R.id.save_btn);
			Button btn_Add = (Button) rootView
					.findViewById(R.id.add_btn);

			// RelativeLayout layout_info,layout_speaker,layout_notes;
			final RelativeLayout layout_info = (RelativeLayout) rootView
					.findViewById(R.id.layout_info);
			final RelativeLayout layout_speaker = (RelativeLayout) rootView
					.findViewById(R.id.layout_speaker);
			final RelativeLayout layout_notes = (RelativeLayout) rootView
					.findViewById(R.id.layout_notes);
			final RelativeLayout layout_notesList = (RelativeLayout) rootView
					.findViewById(R.id.layout_notesList);

			/*DataBase database = new DataBase(getActivity());
			database.open();*/
			ArrayList<String> sessions = new ArrayList<String>();
			ArrayList<String> sessionid=new ArrayList<String>();
			sessions.addAll(database.getSessionBySpeakerId(spkr_id + ""));
			sessionid.addAll(database.getSessionIdBySpeakerId(spkr_id+""));

			final EditText edittext = (EditText) rootView
					.findViewById(R.id.edit_note);

			edittext.setText(database.getNoteBySpeakerId(spkr_id));

			database.close();

			ListView agendalist = (ListView) rootView
					.findViewById(R.id.agenda_list);
			//agendalist.setAdapter(new ArrayAdapter<String>(context,
			//		android.R.layout.simple_expandable_list_item_1, sessions));
			//ShowAgenda ag=new ShowAgenda(context, sessions);


			spkr_image_speaker_info = (ImageView) rootView
					.findViewById(R.id.img_spkr);

			spkr_name.setText(namearray.get(id));
			// spkr_image_speaker_info.setImageResource(speakerimage.get(id));
			spkr_designation.setText(designation.get(id));
			spkr_description.setText(description.get(id));

			/*	File pictureFile = new File(Environment
					.getExternalStorageDirectory().getPath()
					+ "/sankalp/speakers/" + speakerimage.get(id));
			if (pictureFile != null) {
				Bitmap bitmap = BitmapFactory.decodeFile(pictureFile
						.getAbsolutePath());
				spkr_image_speaker_info.setImageBitmap(bitmap);
			}*/

			imageLoader.DisplayImage("http://amen.eventbuoy.com/admin/uploads/"
					+ speakerimage.get(id).replaceAll(" ", "%20"), spkr_image_speaker_info);

			agendalist.setAdapter(new ShowAgenda(getActivity(), sessions,sessionid));


			/*agendalist.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) 
				{
					// TODO Auto-generated method stub
					
				}
			});*/


			btn_info.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					arg0.setBackgroundResource(R.drawable.color_cover_left);
					btn_notes
					.setBackgroundResource(android.R.color.transparent);
					btn_session
					.setBackgroundResource(android.R.color.transparent);
					layout_notes.setVisibility(View.INVISIBLE);
					layout_notesList.setVisibility(View.INVISIBLE);
					layout_speaker.setVisibility(View.INVISIBLE);
					layout_info.setVisibility(View.VISIBLE);

				}
			});

			btn_notes.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					arg0.setBackgroundResource(R.drawable.color_cover_right);
					btn_info.setBackgroundResource(android.R.color.transparent);
					btn_session
					.setBackgroundResource(android.R.color.transparent);
					layout_notes.setVisibility(View.INVISIBLE);
					layout_notesList.setVisibility(View.VISIBLE);
					layout_speaker.setVisibility(View.INVISIBLE);
					layout_info.setVisibility(View.INVISIBLE);

				}
			});
			btn_session.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					arg0.setBackgroundResource(R.drawable.color_cover_right);
					btn_notes
					.setBackgroundResource(android.R.color.transparent);
					btn_info.setBackgroundResource(android.R.color.transparent);
					layout_notes.setVisibility(View.INVISIBLE);
					layout_notesList.setVisibility(View.INVISIBLE);
					layout_speaker.setVisibility(View.VISIBLE);
					layout_info.setVisibility(View.INVISIBLE);

				}
			});



			btn_save.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub 

					DataBase database=new DataBase(context);
					database.open();

					if(note_id!=-1)
					{
						database.UpdateNote1(note_id, edittext.getText().toString().trim(), "SPEAKER",
								Integer.valueOf(speaker_id.get(mViewPager.getCurrentItem())), 
								spkr_name.getText().toString());
						Toast.makeText(context, "Note Updated ", Toast.LENGTH_LONG).show();
						//Toast.makeText(context, "a " +a, Toast.LENGTH_LONG).show();
					}else {
						database.addNote1(edittext.getText().toString().trim(), "SPEAKER",
								Integer.valueOf(speaker_id.get(mViewPager.getCurrentItem())), 
								spkr_name.getText().toString());

						Toast.makeText(context, "Note Saved ", Toast.LENGTH_LONG).show();

						/*Toast.makeText(context, "a " +b, Toast.LENGTH_LONG).show();
						System.out.println("......"+edittext.getText().toString().trim());
						System.out.println("......"+Integer.valueOf(speaker_id.get(mViewPager.getCurrentItem())));
						System.out.println("......"+spkr_name.getText().toString());*/


					}
					database.close();


					NotesArrayList.clear();
					database.open();
					NotesArrayList=database.getNoteByPlaceId("SPEAKER", Integer.valueOf(speaker_id.get(mViewPager.getCurrentItem())));
					adapterNote=new NoteAdapter1(context, NotesArrayList);
					listView.setAdapter(adapterNote);
					
					database.close();
					edittext.setText("");
					layout_notes.setVisibility(View.INVISIBLE);
					layout_info.setVisibility(View.INVISIBLE);
					layout_speaker.setVisibility(View.INVISIBLE);
					layout_notesList.setVisibility(View.VISIBLE);

				}
			});

			btn_Add.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					DataBase database=new DataBase(context);
					database.open();

					/*database.addNote1(edittext.getText().toString().trim(), "SPEAKER",
							Integer.valueOf(speaker_id.get(mViewPager.getCurrentItem())), 
							spkr_name.getText().toString());*/
					note_id=-1;
					/*NotesArrayList.clear();
					NotesArrayList=database.getNoteByPlaceId("SPEAKER", Integer.valueOf(speaker_id.get(mViewPager.getCurrentItem())));
					adapterNote=new NoteAdapter1(context, NotesArrayList);
					listView.setAdapter(adapterNote);*/
					/*	for (int i = 0; i < NotesArrayList.size(); i++) {

						System.out.println("Id   "+NotesArrayList.get(i).getNote_id());
						System.out.println("Content   "+NotesArrayList.get(i).getNote_content());
						System.out.println("Info   "+NotesArrayList.get(i).getNote_info());
						System.out.println("Place   "+NotesArrayList.get(i).getNote_place());
						System.out.println("PlaceId   "+NotesArrayList.get(i).getNote_palceId());
					}	*/		

					database.close();
					layout_notes.setVisibility(View.VISIBLE);
					layout_info.setVisibility(View.INVISIBLE);
					layout_speaker.setVisibility(View.INVISIBLE);
					layout_notesList.setVisibility(View.INVISIBLE);
				}
			});
			listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					edittext.setText(NotesArrayList.get(arg2).getNote_content());
					note_id=Integer.parseInt(NotesArrayList.get(arg2).getNote_id());

					layout_notes.setVisibility(View.VISIBLE);
					layout_info.setVisibility(View.INVISIBLE);
					layout_speaker.setVisibility(View.INVISIBLE);
					layout_notesList.setVisibility(View.INVISIBLE);

				}
			});
			// TextView spkr_description = (TextView) rootView
			// .findViewById(R.id.txt_speaker_description);


			//Mail Sendimg Code
			imgMessage.setOnClickListener(new OnClickListener() {

				@SuppressLint("InlinedApi")
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					String to=null;
					Intent email = new Intent(Intent.ACTION_SEND);
					email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
					//email.putExtra(Intent.EXTRA_CC, new String[]{ to});
					//email.putExtra(Intent.EXTRA_BCC, new String[]{to});
					email.putExtra(Intent.EXTRA_SUBJECT , spkr_name.getText().toString());
					email.putExtra(Intent.EXTRA_TEXT , spkr_designation.getText().toString()
							+"\n"+"https://play.google.com/store/apps/details?id=com.blackbean.eventbuoy2.amen");

					//Convert drawable to Uri
					/*Resources resources = context.getResources();
					int resId=R.drawable.icon_share;*/

					/*email .putExtra(Intent.EXTRA_STREAM, Uri.parse(Environment
							.getExternalStorageDirectory().getPath()
							+ "/sankalp/speakers/" + speakerimage.get(id)));*/
					//need this to prompts email client only
					email.setType("message/rfc822");

					startActivity(Intent.createChooser(email, "Choose an Email client :"));
				}
			});


			//Facebook sharing code

			imgFacebook.setOnClickListener(new OnClickListener() {

				@SuppressWarnings("deprecation")
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					System.err.println("cache path "+context.getCacheDir()+spkr_name.getText().toString());
					Bundle parameters = new Bundle();
					parameters.putString("description",spkr_designation.getText().toString()+"\n"+spkr_description.getText().toString());
					//parameters.putString("message", "https://play.google.com/store/apps/details?id=com.blackbean.eventbuoy2.sankalp");
					parameters.putString("picture","http://amen.eventbuoy.com/admin/uploads/"
							+ speakerimage.get(id).replaceAll(" ", "%20"));
					parameters.putString("link","https://play.google.com/store/apps/details?id=com.blackbean.eventbuoy2.amen");
					parameters.putString("name", spkr_name.getText().toString());
					parameters.putString("caption", "Amen");

					Facebook mFacebook=new Facebook("1376444199346062");

					/*System.out.println("desc  "+parameters.get("description"));
					System.out.println("link  "+parameters.get("link"));
					System.out.println("name  "+parameters.get("name"));
					System.out.println("capt  "+parameters.get("caption"));*/


					mFacebook.dialog((Activity)context, "feed", parameters, new DialogListener() {

						@Override
						public void onFacebookError(FacebookError e) {
							// TODO Auto-generated method stub
							Toast.makeText(context, "Error While Sharing ", Toast.LENGTH_SHORT).show();
						}

						@Override
						public void onError(DialogError e) {
							// TODO Auto-generated method stub
							Toast.makeText(context, "Error While Sharing ", Toast.LENGTH_SHORT).show();
						}

						@Override
						public void onComplete(Bundle values) {
							// TODO Auto-generated method stub
							String string1 = null;
							for (String string : values.keySet()) {
								//System.out.println("val "+string);
								string1=string;
							}
							if(string1!=null)
								Toast.makeText(context, "Shared Sucessfully", Toast.LENGTH_SHORT).show();
							else {
								Toast.makeText(context, "Sharing Cancel", Toast.LENGTH_SHORT).show();
							}
						}

						@Override
						public void onCancel() {
							// TODO Auto-generated method stub
							Toast.makeText(context, "Sharing Cancel", Toast.LENGTH_SHORT).show();
						}
					});
				}
			});
			
			imgTwitter.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent=new Intent(context, TwitterShare.class);
					String shareText=spkr_name.getText().toString()+"\n"+
							spkr_designation.getText().toString();
					if(shareText.length()>62)
					{
						shareText=shareText.substring(0, 61)+"\n" +
								"https://play.google.com/store/apps/details?id=com.blackbean.eventbuoy2.amen";
					}
					
					intent.putExtra("ShareText", shareText);

					spkr_image_speaker_info.buildDrawingCache();
					Bitmap bmap = spkr_image_speaker_info.getDrawingCache();
					/*intent.putExtra("ImagePath", Environment.getExternalStorageDirectory().getPath()
							+ "/sankalp/speakers/" + speakerimage.get(id));*/
					intent.putExtra("ImagePath", bmap);

					startActivity(intent);
				}
			});



			return rootView;
		}


		class ShowAgenda extends BaseAdapter
		{
			Context context;
			ArrayList<String> agenda;
			ArrayList<String> agendaID;
			public ShowAgenda(Context context,ArrayList<String> agenda,ArrayList<String> agendaID) {
				// TODO Auto-generated constructor stub
				this.context=context;
				this.agenda=agenda;
				this.agendaID=agendaID;
			}

			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return agenda.size();
			}

			@Override
			public Object getItem(int arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public View getView(final int position, View arg1, ViewGroup arg2) {
				// TODO Auto-generated method stub
				LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View view=inflater.inflate(R.layout.single_text_adapter, null);

				TextView text=(TextView)view.findViewById(R.id.text1);
				text.setText(agenda.get(position));

				text.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View argp) {
						// TODO Auto-generated method stub

						Intent intent=new Intent(context, AgendaDetailsActivity.class);
						intent.putExtra("agenda_id",agendaID.get(position));
						startActivity(intent);
					}
				});

				return view;
			}

		}

	}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

}