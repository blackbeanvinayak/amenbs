package com.blackbean.eventbuoy2.amen;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.utility.GPSTracker;
import com.blackbean.utility.JSONParser;

public class VenueActivity extends Activity{
	String urlString="http://maps.google.com/?q=";
	String finalUrl = "";
	WebView webview;
	JSONObject jsonObject;
	JSONParser jsonParser;
	public static JSONArray json_array = null;
	String venu_lat;
	String venue_long;
	String venue_location;
	TextView header_txt;

	Button btnGetDirection;
	GPSTracker gps;
	
	Button btnKochi,btnBangalore,btnRaipur;

	@SuppressLint({ "SetJavaScriptEnabled", "NewApi" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_venue);
		//getActionBar().hide();
		webview=(WebView) findViewById(R.id.webViewMapFull);
		header_txt=(TextView) findViewById(R.id.header_txt);

		btnGetDirection=(Button) findViewById(R.id.btnGetDirection);
		btnBangalore = (Button) findViewById(R.id.btnBangalore);
		btnKochi =(Button) findViewById(R.id.btnKochi);
		btnRaipur = (Button) findViewById(R.id.btnRaipur);
		
		/*venu_lat=getIntent().getStringExtra("lat");
		venue_long=getIntent().getStringExtra("long");*/
		
		venu_lat = "9.990414";
		venue_long = "76.285189";

		finalUrl=urlString+venu_lat+","+venue_long;

		webview.setWebViewClient(new MyBrowser());
		webview.getSettings().setLoadsImagesAutomatically(true);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.loadUrl(finalUrl);

		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(VenueActivity.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();
			}
		});

		btnKochi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				venu_lat = "9.990414";
				venue_long = "76.285189";

				finalUrl=urlString+venu_lat+","+venue_long;

				webview.setWebViewClient(new MyBrowser());
				webview.getSettings().setLoadsImagesAutomatically(true);
				webview.getSettings().setJavaScriptEnabled(true);
				webview.loadUrl(finalUrl);
				
				btnKochi.setBackgroundResource(R.drawable.color_cover_left);
				btnBangalore.setBackgroundColor(Color.TRANSPARENT);
				btnRaipur.setBackgroundColor(Color.TRANSPARENT);
			}
		});
		
		btnBangalore.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				venu_lat = "12.971599";
				venue_long = "77.594563";

				finalUrl=urlString+venu_lat+","+venue_long;

				webview.setWebViewClient(new MyBrowser());
				webview.getSettings().setLoadsImagesAutomatically(true);
				webview.getSettings().setJavaScriptEnabled(true);
				webview.loadUrl(finalUrl);
				
				btnBangalore.setBackgroundResource(R.drawable.color_cover_meddle);
				btnKochi.setBackgroundColor(Color.TRANSPARENT);
				btnRaipur.setBackgroundColor(Color.TRANSPARENT);
				
			}
		});
		
		btnRaipur.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				venu_lat = "21.251384";
				venue_long = "81.629641";

				finalUrl=urlString+venu_lat+","+venue_long;

				webview.setWebViewClient(new MyBrowser());
				webview.getSettings().setLoadsImagesAutomatically(true);
				webview.getSettings().setJavaScriptEnabled(true);
				webview.loadUrl(finalUrl);
				
				btnRaipur.setBackgroundResource(R.drawable.color_cover_right);
				btnBangalore.setBackgroundColor(Color.TRANSPARENT);
				btnKochi.setBackgroundColor(Color.TRANSPARENT);
			}
		});
		
		btnGetDirection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				gps=new GPSTracker(VenueActivity.this);

				if(gps.canGetLocation()){

					double latitude = gps.getLatitude();
					double longitude = gps.getLongitude();
					String currentlatlong=latitude+","+longitude;
					finalUrl="https://www.google.com/maps?saddr="+currentlatlong +"&daddr="+venu_lat+","+venue_long; 
					/*System.out.println(urlString);
					web.loadUrl(urlString);*/
					Intent intent=new Intent(VenueActivity.this, GetDirection.class);
					intent.putExtra("URL",finalUrl);
					System.out.println("URL"+finalUrl);
					startActivity(intent);
				}
			}
		});
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		finish();
	}
	public class MyBrowser extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		//FlurryAgent.onStartSession(this, "4D85YD7VCFW97NC3S7BK");

	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//FlurryAgent.onEndSession(this);
	}

}