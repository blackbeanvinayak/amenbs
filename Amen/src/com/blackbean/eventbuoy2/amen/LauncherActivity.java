package com.blackbean.eventbuoy2.amen;



import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.utility.DataBase;
import com.blackbean.utility.GetServerData;
import com.blackbean.utility.JSONParser;
import com.blackbean.utility.NetworkCheck;
import com.blackbean.utility.SetGet;
import com.gamethrive.GameThrive;
import com.gamethrive.NotificationOpenedHandler;
import com.google.analytics.tracking.android.EasyTracker;

public class LauncherActivity extends Activity {
	ImageView logo_enter, logo_bb;
	TextView textView;
	Context context;
	Dialog dialog;
	public int COUNT=0;
	private static GameThrive gameThrive;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_launcher);
		context=this;

		if (gameThrive == null)
		{
			gameThrive = new GameThrive(this, "1075925989435", "d8121a44-cbd7-11e4-a8bd-3304d4196344",new ExampleNotificationOpenedHandler());
		}
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		boolean check;
		check = preferences.getBoolean("issankalpInstalled", false);



		/*SetGet.EVENT_COUNT=0;
		SetGet.SPEAKER_COUNT=0;
		SetGet.PARTNER_COUNT=0;
		SetGet.JURY_COUNT=0;
		SetGet.MENULIST_COUNT=0;*/

		if (check == false)
		{

			if (NetworkCheck
					.getConnectivityStatusString(LauncherActivity.this)) {

				SharedPreferences.Editor editor = preferences.edit();
				editor.putBoolean("issankalpInstalled", true);
				editor.commit();

				GetServerData get=new GetServerData(LauncherActivity.this);
				get.execute();
			}
			else {

				Toast.makeText(LauncherActivity.this, "Internet Connection Required !", Toast.LENGTH_SHORT).show();
			}

		}
		else {
			if (NetworkCheck.getConnectivityStatusString(LauncherActivity.this)) {

				new GetUpdateCount().execute();
			}
			else {
				new Handler().postDelayed(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						Intent intent = new Intent(LauncherActivity.this,
								MainScreen.class);
						startActivity(intent);
						finish();
					}
				}, 1000);
				/*Intent intent = new Intent(LauncherActivity.this,
						MainScreen.class);
				startActivity(intent);
				finish();*/
			}
		}

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		gameThrive.onPaused();
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		gameThrive.onResumed();
	}
	private class ExampleNotificationOpenedHandler implements NotificationOpenedHandler {
		/**
		 * Callback to implement in your app to handle when a notification is opened from the Android status bar or
		 * a new one comes in while the app is running.
		 * This method is located in this activity as an example, you may have any class you wish implement NotificationOpenedHandler and define this method.
		 *
		 * @param message        The message string the user seen/should see in the Android status bar.
		 * @param additionalData The additionalData key value pair section you entered in on gamethrive.com.
		 * @param isActive       Was the app in the foreground when the notification was received.
		 */
		@Override
		public void notificationOpened(String message, JSONObject additionalData, boolean isActive) {
			String messageTitle;
			AlertDialog.Builder builder = null;
			if (additionalData != null) {
				if (additionalData.has("discount"))
					messageTitle = "Discount!";
				else if (additionalData.has("bonusCredits"))
					messageTitle = "Bonus Credits!";
				else
					messageTitle = "Other Extra Data";
				builder = new AlertDialog.Builder(LauncherActivity.this)
				.setTitle(messageTitle)
				.setMessage(message + "\n\n" + additionalData.toString());
			}
			else if (isActive) // If a push notification is received when the app is being used it does not display in the notification bar so display in the app.
				builder = new AlertDialog.Builder(LauncherActivity.this)
			.setTitle("Message")
			.setMessage(message);
			// Add your game logic around this so the user is not interrupted during gameplay.
			if (builder != null)
				builder.setCancelable(true)
				.setPositiveButton("OK",null)
				.create().show();
		}
	}

	private class GetUpdateCount extends AsyncTask<Void, Void, Void>
	{

		int oldCountMenuList=Integer.valueOf(SetGet.readUsingSharedPreference(context, "PROGRAM_COUNT").trim());
		int newCountMenuList=0;
		int oldCountEvent=Integer.valueOf(SetGet.readUsingSharedPreference(context, "EVENT_COUNT").trim());
		int newCountEvent=0;
		int oldCountSpeaker=Integer.valueOf(SetGet.readUsingSharedPreference(context, "SPEAKER_COUNT").trim());
		int newCountSpeaker=0;
		int oldCountPartner=Integer.valueOf(SetGet.readUsingSharedPreference(context, "PARTNER_COUNT").trim());
		int newCountPartner=0;
		int oldCountJury=Integer.valueOf(SetGet.readUsingSharedPreference(LauncherActivity.this, "JURY_COUNT").trim());
		int newCountJury=0;




		String val = null;
		/*JSONParserString jsonParserString=new JSONParserString();*/
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = new JSONObject();
		JSONArray jArray = null;
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				jsonObject = jsonParser.getJSONFromUrl("http://amen.eventbuoy.com/admin/admin/get_update_counts.php");
				if (jsonObject != null) {

					jArray = jsonObject.getJSONArray("update_count");

					//System.out.println("jArray "+jsonObject);

					for (int i = 0; i < jArray.length(); i++) {
						JSONObject temp = new JSONObject();
						//System.out.println("OBJECT "+temp.toString());
						try {
							temp=jArray.getJSONObject(i);
							newCountMenuList=Integer.valueOf(temp.getString("program_update_count"));
							newCountEvent=Integer.valueOf(temp.getString("event_info_update_count"));
							newCountJury=Integer.valueOf(temp.getString("jury_info_update_count"));
							newCountPartner=Integer.valueOf(temp.getString("sponsors_update_count"));
							newCountSpeaker=Integer.valueOf(temp.getString("speakers_update_count"));

						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
					}	
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}


			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (oldCountEvent < newCountEvent || oldCountJury < newCountJury
					|| oldCountMenuList < newCountMenuList
					|| oldCountPartner < newCountPartner
					|| oldCountSpeaker < newCountSpeaker) 
			{
				DataBase db = new DataBase(LauncherActivity.this);
				db.open();
				SetGet.agendaId_MySchedule=db.getMySchedule();
				db.deleteAlltables();
				db.close();
				new GetServerData(context).execute();
			}else {

				Intent intent = new Intent(LauncherActivity.this,
						MainScreen.class);
				startActivity(intent);
				finish();
			}

		}
	}

}
