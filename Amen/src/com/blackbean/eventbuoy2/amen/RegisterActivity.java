package com.blackbean.eventbuoy2.amen;



import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.blackbean.adapters.RegisterExpandableAdapter;
import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.utility.SetGet;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ImageView;


public class RegisterActivity extends Activity
{
	ExpandableListView expand;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		ImageView menu=(ImageView)findViewById(R.id.home);

		// right Image Setting code
		//int position=getIntent().getIntExtra("POSITION", 0);
		String imageBlack=getIntent().getStringExtra("ImageName");
		ImageView icon=(ImageView) findViewById(R.id.icon);
		File pictureFile = new File(Environment
				.getExternalStorageDirectory().getPath()
				+ "/shrm/menu/" + imageBlack);
		if (pictureFile != null) {
			Bitmap bitmap = BitmapFactory.decodeFile(pictureFile
					.getAbsolutePath());
			if(bitmap!=null)
				icon.setImageBitmap(bitmap);
		}
		//icon.setColorFilter(Color.argb(255, 0, 0, 0));

		expand=(ExpandableListView)findViewById(R.id.expand);

		ArrayList<String> header=new ArrayList<String>();
		header.add("INDIAN");
		header.add("INTERNATIONAL");

		ArrayList<String> indian=new ArrayList<String>();
		ArrayList<String> international=new ArrayList<String>();
		ArrayList<String> indianPrice=new ArrayList<String>();
		ArrayList<String> internationalPrice=new ArrayList<String>();
		indian.add("SHRM MEMBER");
		indian.add("NON-MEMBER");
		indian.add("BECOME A MEMBER #");
		indian.add("GROUP REGISTRATION");
		indian.add("SHRM STUDENT MEMBER");
		indian.add("BECOME A STUDENT MEMBER NOW*");
		international.add("NON MEMBER PRICING");
		international.add("GROUP PRICING");
		international.add("MEMBER PRICING");

		indianPrice.add("20,000");
		indianPrice.add("22,500");
		indianPrice.add("23,500");
		indianPrice.add("21,000");
		indianPrice.add("11,000");
		indianPrice.add("12,000");

		internationalPrice.add("450");
		internationalPrice.add("425");
		internationalPrice.add("400");

		List<ArrayList<String>> datachild=new ArrayList<ArrayList<String>>();
		datachild.add(indian);
		datachild.add(international);

		List<ArrayList<String>> datachildprice=new ArrayList<ArrayList<String>>();
		datachildprice.add(indianPrice);
		datachildprice.add(internationalPrice);



		//HashMap<ArrayList<String>,List<ArrayList<String>>> childs=new HashMap<ArrayList<String>, List<ArrayList<String>>>();
		//childs.put(header,datachild);
		ArrayList<HashMap<String, List<String>>> map=new ArrayList<HashMap<String,List<String>>>();

		HashMap<String,List<String>> ind=new HashMap<String, List<String>>();
		HashMap<String,List<String>> price=new HashMap<String, List<String>>();
		ind.put(header.get(0), indian);
		ind.put(header.get(1), international);
		price.put(header.get(0), indianPrice);
		price.put(header.get(1), internationalPrice);

		map.add(ind);
		map.add(price);

		//map.get(0).get("dfds").size();

		expand.setAdapter(new RegisterExpandableAdapter(this, map,header));


		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(RegisterActivity.this, HomeActivity.class);
				startActivity(intent);

			}
		});
	} 

}
