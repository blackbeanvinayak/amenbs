package com.blackbean.eventbuoy2.amen;

import com.blackbean.eventbuoy2.amen.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class FaqsDescription extends Activity
{
	TextView questiontext,answerstext;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.faq_description);
		
		String question=getIntent().getExtras().getString("question");
		String answer=getIntent().getExtras().getString("answer");
		
		questiontext=(TextView)findViewById(R.id.questionText);
		answerstext=(TextView)findViewById(R.id.descriptionText);
		
		questiontext.setText(question);
		answerstext.setText(answer);
		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(FaqsDescription.this,
						HomeActivity.class);
				startActivity(intent);

			}
		});
		
	}

}
