package com.blackbean.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;

public class RegisterExpandableAdapter extends BaseExpandableListAdapter
{
	Context context;
	ArrayList<String> groupheader;
	ArrayList<HashMap<String, List<String>>> map;
	public RegisterExpandableAdapter(Context context,ArrayList<HashMap<String, List<String>>> map,ArrayList<String> header ) {
		// TODO Auto-generated constructor stub
		this.context=context;
		this.groupheader=header;
		this.map=map;
	}

	@Override
	public Object getChild(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getChildId(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getChildView(final int groupPosition,  final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater infalInflater = (LayoutInflater) this.context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = infalInflater.inflate(R.layout.register_childexpand, null);
		TextView text=(TextView)convertView.findViewById(R.id.text);
		TextView price=(TextView)convertView.findViewById(R.id.price);
		text.setText(map.get(0).get(groupheader.get(groupPosition)).get(childPosition));
		price.setText(map.get(1).get(groupheader.get(groupPosition)).get(childPosition));
		convertView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				if(groupPosition==0)
				{
					String url = "https://www.cvent.com/events/shrm-india-annual-conference-exposition-2014/registration-dc3e7b29d4874a8ca5decb9a4efcdc50.aspx";
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(url));
					context.startActivity(i);
					
				}if(groupPosition==1)
				{
					String url = "https://www.cvent.com/events/shrm-india-annual-conference-exposition-2014-international/registration-658ca20037274ab6add45042043d0ed5.aspx";
					Intent i = new Intent(Intent.ACTION_VIEW);
					i.setData(Uri.parse(url));
					context.startActivity(i);
				}
				
			}
		});
		
		return convertView;
	}

	@Override
	public int getChildrenCount(int args) {
		// TODO Auto-generated method stub
		return map.get(0).get(groupheader.get(args)).size();
	}

	@Override
	public Object getGroup(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return groupheader.size();
	}

	@Override
	public long getGroupId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent)  {
		// TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.register_groupexpand, null);
		}
		TextView text=(TextView)convertView.findViewById(R.id.group_name);
		text.setText(groupheader.get(groupPosition));
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

}
