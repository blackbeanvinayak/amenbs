package com.blackbean.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;


public class ExpandableListAdapter extends BaseExpandableListAdapter  {

	private Context _context;
	private List<String> _listDataHeader; // header titles
	private HashMap<String, List<String>> _listDataChild;
	int id;
	 ArrayList<String> info;
     ArrayList<String> agenda_id;
     public ExpandableListAdapter(Context context, List<String> listDataHeader,
             HashMap<String, List<String>> listChildData) {
    	 
     this._context = context;
     this._listDataHeader = listDataHeader;
     this._listDataChild = listChildData;
   
   
}

	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	public View getChildView(int groupPosition,  final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
            
                        LayoutInflater infalInflater = (LayoutInflater) this._context
                                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        convertView = infalInflater.inflate(R.layout.list_item, null);
                
                TextView txtListChild = (TextView) convertView
                                .findViewById(R.id.lblListItem);
                
               txtListChild.setTextColor(Color.BLACK);
        
        	  String headerTitle = (String) getGroup(groupPosition);
              
        	   if(headerTitle.equalsIgnoreCase("Mobile Advertising & Marketing"))
                {
        		   txtListChild.setText(_listDataChild.get("Mobile Advertising & Marketing").get(childPosition));
                }
        	   if(headerTitle.equalsIgnoreCase("Consumer Mobile Services"))
               {
       		   txtListChild.setText(_listDataChild.get("Consumer Mobile Services").get(childPosition));
               }
        	   if(headerTitle.equalsIgnoreCase("Innovative Mobile App"))
               {
       		   txtListChild.setText(_listDataChild.get("Innovative Mobile App").get(childPosition));
               }
        	   
                      
             
                return convertView;
		
		
	}

	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.size();
	}

	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);
		//lblListHeader.setTypeface(null, Typeface.ITALIC);
		lblListHeader.setText(headerTitle);
	
		return convertView;
	}

	public boolean hasStableIds() {
		return false;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}


}
