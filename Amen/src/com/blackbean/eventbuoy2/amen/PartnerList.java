package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;

import com.blackbean.adapters.PartnerAdapter;
import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
/***
 * to show Partner list Category
 * 
 * @author blackbeanumesh
 *
 */
public class PartnerList extends Activity
{
	ListView listview;
	ArrayList<String> listItems;
	int whichEvent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.partnerlist);

		ImageView menu = (ImageView) findViewById(R.id.logo);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(PartnerList.this, MainScreen.class));
				HomeActivity.homeActivity.finish();
				finish();
			}
		});

		whichEvent=getIntent().getIntExtra("WHICH_EVENT", 1);

		listview=(ListView)findViewById(R.id.partnerlist);

		listItems=new ArrayList<String>();
		if(whichEvent == StaticData.Event_Nairobi)
		{
			listItems.add("Associate Partner");
			listItems.add("Content Partner");
			listItems.add("Founding Partner");
			listItems.add("Lanyrd Partner");
			listItems.add("Outreach Partner");
			listItems.add("Strategic Partner");
			listItems.add("Innovation Track Partner");
			listItems.add("Online Media Partners");
			listItems.add("Enterprise Support Partners");
			listItems.add("Inclusive Business Track Partners");
			listItems.add("Mobile App Partner");
		}else if (whichEvent == StaticData.Event_Delhi) {

			listItems.add("Industry and Expo Partner");
			listItems.add("Strategic Partner");
			listItems.add("Core Partner - Healthcare");
			listItems.add("Core Partner - Innovation");
			listItems.add("Support Partner - Financial Inclusion");
			listItems.add("Knowledge Partner");
			listItems.add("Sankalp Awards Grand Prize Partner");
			listItems.add("Sankalp Awards Partner");
			listItems.add("Sankalp Peoples Choice Awards Partner");
			listItems.add("Local Ecosystem Partner");
			listItems.add("Volunteer Partner");
			listItems.add("Support Partner");
			listItems.add("Investment Partner");
			listItems.add("Outreach Partner");
			listItems.add("Online Media Partner");
			listItems.add("Mobile App Partner");

		}


		listview.setAdapter(new PartnerAdapter(this, listItems));

		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				Intent intent=new Intent(PartnerList.this, SponsorActivity.class);
				intent.putExtra("POSITION", arg2);
				intent.putExtra("sponser_category", listItems.get(arg2));
				if(whichEvent == StaticData.Event_Nairobi)
				{
					intent.putExtra("eventCategory", "Nairobi");
				}
				else if(whichEvent == StaticData.Event_Delhi){
					intent.putExtra("eventCategory", "Delhi");
				}

				startActivity(intent);

			}
		});


	}



}
