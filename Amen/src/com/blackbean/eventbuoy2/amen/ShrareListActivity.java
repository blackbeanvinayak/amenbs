package com.blackbean.eventbuoy2.amen;

import java.io.File;
import java.util.ArrayList;

import com.blackbean.adapters.HomeAdapter;
import com.blackbean.adapters.HomeAdapter1;
import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.utility.SetGet;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class ShrareListActivity extends Activity {
	ListView list;
	ArrayList<Integer> icons;
	ArrayList<String> pageArrayList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share_list);
		icons = new ArrayList<Integer>();
		pageArrayList = new ArrayList<String>();
		pageArrayList.add("FACEBOOK");
		pageArrayList.add("TWITTER");
		pageArrayList.add("LINKEDIN");

		list = (ListView) findViewById(R.id.lv_home);

		icons.add(R.drawable.facebook);
		icons.add(R.drawable.twitter);
		icons.add(R.drawable.linkedin);

		list.setAdapter(new HomeAdapter1(this, R.layout.adapter_home,
				pageArrayList, icons));

		// right Image Setting code
		//int position=getIntent().getIntExtra("POSITION", 0);
		String imageBlack=getIntent().getStringExtra("ImageName");
		ImageView icon=(ImageView) findViewById(R.id.icon);
		File pictureFile = new File(Environment
				.getExternalStorageDirectory().getPath()
				+ "/shrm/menu/" + imageBlack);
		if (pictureFile != null) {
			Bitmap bitmap = BitmapFactory.decodeFile(pictureFile
					.getAbsolutePath());
			if(bitmap!=null)
				icon.setImageBitmap(bitmap);
		}
		//icon.setColorFilter(Color.argb(255, 0, 0, 0));

		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ShrareListActivity.this,
						HomeActivity.class);
				startActivity(intent);
				finish();

			}
		});

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				if (arg2 == 0) {
					String url = "https://www.facebook.com/SHRMIndia";
					Intent i = new Intent(ShrareListActivity.this,
							Connect_Social.class);
					i.putExtra("url", url);
					i.putExtra("social", "FACEBOOK");
					startActivity(i);

				}
				if (arg2 == 1) 
				{

					//String url = "https://www.linkedin.com/groups?gid=6671242&trk=group_mgt_name";
					Intent i = new Intent(ShrareListActivity.this,
							Twitter.class);

					startActivity(i);
				}
				if (arg2 == 2) 
				{
					// https://www.linkedin.com/groups/SHRM-India-Annual-Conference-2014-6671242/about?report%2Esuccess=6CA_0_ERR4BL0R6ui96ZYEsEEvetbKYvL3_f6JKxJv-emvCLZtQvkCKETocJW7aGvEB5TpAEJmAqSLCXvzsokTOUk7g2ZNtXKA35sAwhjWEJnozQPtCLE88xJ7AgZNYQvVQv2k_RT_CU1agnQt6vEpQEEZaJnoHXZ0_v2zbeHLxUzR0fZlpXJj6NunaULhznWFDLjgoEJmAYmrPnnEwLj8QxsvEgnvH5lywheZA
					String url = "https://www.linkedin.com/groups?gid=6671242&trk=group_mgt_name";
					Intent i = new Intent(ShrareListActivity.this,
							Connect_Social.class);
					i.putExtra("url", url);
					i.putExtra("social", "LINKEDIN");
					startActivity(i);

				} 

			}
		});

	}

}
