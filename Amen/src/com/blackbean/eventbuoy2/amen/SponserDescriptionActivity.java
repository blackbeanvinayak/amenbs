package com.blackbean.eventbuoy2.amen;

import com.blackbean.eventbuoy2.amen.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class SponserDescriptionActivity extends Activity{

	String name,desc;
	TextView txtName,txtDesc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		setContentView(R.layout.sponser_desc_activity);
		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SponserDescriptionActivity.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();
			}
		});

		name=getIntent().getStringExtra("NAME");
		desc=getIntent().getStringExtra("DESC");

		txtDesc = (TextView) findViewById(R.id.textViewSPDescription);
		txtName = (TextView) findViewById(R.id.textViewSPName);

		txtDesc.setText(desc);
		txtName.setText(name);

	}
}
