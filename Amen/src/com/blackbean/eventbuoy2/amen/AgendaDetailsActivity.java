package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.blackbean.adapters.AgendaSpeakerAdapter;
import com.blackbean.adapters.NoteAdapter1;
import com.blackbean.adapters.NoteModel;
import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;
import com.blackbean.utility.DataBase;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.google.analytics.tracking.android.EasyTracker;

/**
 * Show details of selected agenda item.
 * 
 * @author rahul
 * 
 */
public class AgendaDetailsActivity extends Activity {
	int agenda_id;
	TextView txt_agenda_name, txt_agenda_descrption, txt_agenda_date,
	txt_agenda_location;
	RelativeLayout layout_info, layout_speaker, layout_notes,layout_notesList;
	Button btn_speaker, btn_info, btn_notes,btn_AddNotes;
	ListView speakerlist;
	EditText edit_note;

	// Note Variables
	ListView listView;
	ArrayList<NoteModel>NotesArrayList;
	NoteAdapter1 adapterNote;
	int note_id=-1;

	ImageView logoiImageView;
	ArrayList<String> speaker_id;
	private static final String Agenda_NAME = "agenda_name";
	private static final String Agenda_LOCATION = "agenda_location";
	private static final String Agenda_DATE = "agenda_date";
	private static final String Agenda_DAY = "agenda_day";
	private static final String Agenda_TIME = "agenda_time";
	private static final String Agenda_DESCRIPTION = "agenda_description";

	//Sharing icons
	ImageView imgMessage,imgFacebook,imgTwitter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_agenda_details);
		// agenda_id=Integer.parseInt(getIntent().getExtras().getInt("agenda_id");

		// agenda_id=Integer.parseInt(getIntent().getStringExtra("agenda_id"));
		String agenda_name = getIntent().getStringExtra("agenda_name");
		String agenda_description = getIntent().getStringExtra(
				"agenda_description");
		agenda_id = Integer.parseInt(getIntent().getStringExtra("agenda_id"));
		speaker_id=new ArrayList<String>();

		txt_agenda_name = (TextView) findViewById(R.id.name);
		txt_agenda_location = (TextView) findViewById(R.id.location);
		txt_agenda_date = (TextView) findViewById(R.id.date);
		txt_agenda_descrption = (TextView) findViewById(R.id.agenda_info_txt);
		edit_note = (EditText) findViewById(R.id.edit_note);
		listView=(ListView) findViewById(R.id.list_note);
		NotesArrayList=new ArrayList<NoteModel>();

		speakerlist = (ListView) findViewById(R.id.speaker_list);

		imgMessage=(ImageView) findViewById(R.id.imgMessage);
		imgFacebook=(ImageView) findViewById(R.id.imgFacebook);
		imgTwitter=(ImageView) findViewById(R.id.imgTwitter);
		/*logoiImageView=(ImageView) findViewById(R.id.home);
		logoiImageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(AgendaDetailsActivity.this, MainScreen.class));
				finish();
			}
		});*/



		DataBase dataBase = new DataBase(this);
		dataBase.open();
		txt_agenda_name.setText(dataBase.getAgendaDataByAgendaId(agenda_id,
				Agenda_NAME));
		txt_agenda_location.setText(dataBase.getAgendaDataByAgendaId(agenda_id,
				Agenda_LOCATION));
		txt_agenda_date.setText(dataBase.getAgendaDataByAgendaId(agenda_id,
				Agenda_TIME));
		txt_agenda_descrption.setText(dataBase.getAgendaDataByAgendaId(
				agenda_id, Agenda_DESCRIPTION));

		speaker_id.addAll(dataBase.getSpeakerIdByAgendaId(agenda_id));


		//Getting Notes by place and placeId
		NotesArrayList=dataBase.getNoteByPlaceId("AGENDA", agenda_id);
		adapterNote=new NoteAdapter1(AgendaDetailsActivity.this, NotesArrayList);
		listView.setAdapter(adapterNote);

		dataBase.close();

		ArrayList<String>speaker_name=new ArrayList<String>();
		ArrayList<String>speaker_designation=new ArrayList<String>();
		ArrayList<String>speaker_image=new ArrayList<String>();
		dataBase=new DataBase(AgendaDetailsActivity.this);
		dataBase.open();

		String event_category="";
		/*if (HomeActivity.whichEvent == StaticData.Event_Nairobi) {
			event_category = "HPI";
		}else {
			event_category = "MDET";
		}*/
		String eventCat = getIntent().getStringExtra("EVENT");

		if (eventCat.equals("kochi")) {
			event_category="kochi";
		}else if(eventCat.equals("bangalore")){
			event_category = "bangalore";
		}else if (eventCat.equals("raipur")) {
			event_category = "raipur";
		}

		for (int i = 0; i < speaker_id.size(); i++) {
			if (dataBase.getSpeakerInfoBySpeaker_id(Integer.parseInt(speaker_id.get(i)), "speaker_name",event_category) != null) {
				speaker_name.add(dataBase.getSpeakerInfoBySpeaker_id(Integer.parseInt(speaker_id.get(i)), "speaker_name",event_category));
				speaker_designation.add(dataBase.getSpeakerInfoBySpeaker_id(Integer.parseInt(speaker_id.get(i)), "speaker_designation",event_category));
				speaker_image.add(dataBase.getSpeakerInfoBySpeaker_id(Integer.parseInt(speaker_id.get(i)), "speaker_image",event_category));
			}
		}

		dataBase.close();
		AgendaSpeakerAdapter agendaspeaker=new AgendaSpeakerAdapter(this,android.R.layout.simple_expandable_list_item_1,speaker_name,speaker_designation,speaker_image);
		speakerlist.setAdapter(agendaspeaker);

		speakerlist.setOnItemClickListener(new OnItemClickListener() {

			@SuppressLint("InlinedApi")
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(AgendaDetailsActivity.this,
						SpeakerDetailsActivity.class);
				//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				intent.putExtra("_id", Integer.parseInt(speaker_id.get(arg2)));
				//System.out.println("Umesh "+Integer.parseInt(speaker_id.get(arg2)));
				startActivity(intent);

			}
		});

		// Typeface mFont = Typeface.createFromAsset(getAssets(),
		// "nexa_light.otf");

		// ImageView image=(ImageView)findViewById(R.id.event_image);

		// txt_agenda_name=(TextView)findViewById(R.id.name);
		// txt_agenda_descrption=(TextView)findViewById(R.id.description);
		layout_info = (RelativeLayout) findViewById(R.id.layout_info);
		layout_speaker = (RelativeLayout) findViewById(R.id.layout_speaker);
		layout_notes = (RelativeLayout) findViewById(R.id.layout_notes);
		layout_notesList=(RelativeLayout) findViewById(R.id.layout_notesList);

		btn_info = (Button) findViewById(R.id.btn_info);
		btn_speaker = (Button) findViewById(R.id.btn_speaker);
		btn_notes = (Button) findViewById(R.id.btn_notes);
		btn_AddNotes=(Button) findViewById(R.id.add_btn);



		//Mail Sendimg Code
		imgMessage.setOnClickListener(new OnClickListener() {

			@SuppressLint("InlinedApi")
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String to=null;
				Intent email = new Intent(Intent.ACTION_SEND);
				email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
				//email.putExtra(Intent.EXTRA_CC, new String[]{ to});
				//email.putExtra(Intent.EXTRA_BCC, new String[]{to});
				email.putExtra(Intent.EXTRA_SUBJECT , txt_agenda_name.getText().toString());


				//Convert drawable to Uri
				Resources resources = AgendaDetailsActivity.this.getResources();
				int resId=R.drawable.icon_share;

				email .putExtra(Intent.EXTRA_STREAM, Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + resources.getResourcePackageName(resId) + '/' + resources.getResourceTypeName(resId) + '/' + resources.getResourceEntryName(resId)));

				email.putExtra(Intent.EXTRA_TEXT , txt_agenda_name.getText().toString()+ "\n"+"https://play.google.com/store/apps/details?id=com.blackbean.eventbuoy2.amen");

				//need this to prompts email client only
				email.setType("message/rfc822");

				startActivity(Intent.createChooser(email, "Choose an Email client :"));
			}
		});

		//facebook sharing code
		imgFacebook.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Bundle parameters = new Bundle();
				parameters.putString("description",txt_agenda_descrption.getText().toString());
				parameters.putString("picture","http://amen.eventbuoy.com/admin/uploads/amen_logo.jpg");
				parameters.putString("link","https://play.google.com/store/apps/details?id=com.blackbean.eventbuoy2.amen");
				parameters.putString("name",txt_agenda_name.getText().toString());
				parameters.putString("caption","Amen BS" +"\n");

				Facebook mFacebook=new Facebook("1376444199346062");
				mFacebook.dialog(AgendaDetailsActivity.this, "feed", parameters, new DialogListener() {

					@Override
					public void onFacebookError(FacebookError e) {
						// TODO Auto-generated method stub
						Toast.makeText(AgendaDetailsActivity.this, "Error While Sharing ", Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onError(DialogError e) {
						// TODO Auto-generated method stub
						Toast.makeText(AgendaDetailsActivity.this, "Error While Sharing ", Toast.LENGTH_SHORT).show();
					}

					@Override
					public void onComplete(Bundle values) {
						// TODO Auto-generated method stub
						String string1 = null;
						for (String string : values.keySet()) {
							//System.out.println("val "+string);
							string1=string;
						}
						if(string1!=null)
							Toast.makeText(AgendaDetailsActivity.this, "Shared Sucessfully", Toast.LENGTH_SHORT).show();
						else {
							Toast.makeText(AgendaDetailsActivity.this, "Sharing Cancel", Toast.LENGTH_SHORT).show();
						}
					}

					@Override
					public void onCancel() {
						// TODO Auto-generated method stub
						Toast.makeText(AgendaDetailsActivity.this, "Sharing Cancel", Toast.LENGTH_SHORT).show();
					}
				});

			}
		});

		// sharing Twitter code 

		imgTwitter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(AgendaDetailsActivity.this, TwitterShare.class);

				String shareText="\n"+"https://play.google.com/store/apps/details?id=com.blackbean.eventbuoy2.amen";
				intent.putExtra("ShareText", shareText);
				startActivity(intent);
			}
		});

		Button btn = (Button) findViewById(R.id.save_btn);
		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/*String note_t = edit_note.getText().toString().trim();

				DataBase db = new DataBase(AgendaDetailsActivity.this);
				db.open();
				db.addnote(agenda_id, note_t);
				db.close();*/
				DataBase db=new DataBase(AgendaDetailsActivity.this);
				db.open();

				if(note_id!=-1)
				{
					db.UpdateNote1(note_id, edit_note.getText().toString().trim(), "AGENDA",
							(agenda_id),txt_agenda_name.getText().toString());
				}else {
					db.addNote1(edit_note.getText().toString().trim(), "AGENDA",
							(agenda_id),txt_agenda_name.getText().toString());
				}


				NotesArrayList.clear();

				//Toast.makeText(AgendaDetailsActivity.this, ""+db.getAllNotes().getCount(), Toast.LENGTH_LONG).show();

				NotesArrayList=db.getNoteByPlaceId("AGENDA",agenda_id);

				for (int i = 0; i < NotesArrayList.size(); i++) {

					System.out.println("Id   "+NotesArrayList.get(i).getNote_id());
					System.out.println("Content   "+NotesArrayList.get(i).getNote_content());
					System.out.println("Info   "+NotesArrayList.get(i).getNote_info());
					System.out.println("Place   "+NotesArrayList.get(i).getNote_place());
					System.out.println("PlaceId   "+NotesArrayList.get(i).getNote_palceId());
				}			


				adapterNote=new NoteAdapter1(AgendaDetailsActivity.this, NotesArrayList);
				listView.setAdapter(adapterNote);

				db.close();
				edit_note.setText("");
				layout_notes.setVisibility(View.INVISIBLE);
				layout_info.setVisibility(View.INVISIBLE);
				layout_speaker.setVisibility(View.INVISIBLE);
				layout_notesList.setVisibility(View.VISIBLE);


				Toast.makeText(AgendaDetailsActivity.this, "Note Added",
						Toast.LENGTH_LONG).show();

			}
		});

		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AgendaDetailsActivity.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();
			}
		});

		btn_info.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				arg0.setBackgroundResource(R.drawable.color_cover_left);
				btn_notes.setBackgroundResource(android.R.color.transparent);
				btn_speaker.setBackgroundResource(android.R.color.transparent);
				layout_notes.setVisibility(View.INVISIBLE);
				layout_notesList.setVisibility(View.INVISIBLE);
				layout_speaker.setVisibility(View.INVISIBLE);
				layout_info.setVisibility(View.VISIBLE);

			}
		});
		btn_notes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				arg0.setBackgroundResource(R.drawable.color_cover_right);
				btn_info.setBackgroundResource(android.R.color.transparent);
				btn_speaker.setBackgroundResource(android.R.color.transparent);
				layout_notesList.setVisibility(View.VISIBLE);
				layout_notes.setVisibility(View.INVISIBLE);

				layout_speaker.setVisibility(View.INVISIBLE);
				layout_info.setVisibility(View.INVISIBLE);

			}
		});

		btn_speaker.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				arg0.setBackgroundResource(R.drawable.color_cover_right);
				btn_notes.setBackgroundResource(android.R.color.transparent);
				btn_info.setBackgroundResource(android.R.color.transparent);
				layout_notes.setVisibility(View.INVISIBLE);
				layout_notesList.setVisibility(View.INVISIBLE);
				layout_speaker.setVisibility(View.VISIBLE);
				layout_info.setVisibility(View.INVISIBLE);

			}
		});

		btn_AddNotes.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				note_id=-1;

				layout_notes.setVisibility(View.VISIBLE);
				layout_info.setVisibility(View.INVISIBLE);
				layout_speaker.setVisibility(View.INVISIBLE);
				layout_notesList.setVisibility(View.INVISIBLE);
			}
		});

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				edit_note.setText(NotesArrayList.get(arg2).getNote_content());
				note_id=Integer.parseInt(NotesArrayList.get(arg2).getNote_id());

				layout_notes.setVisibility(View.VISIBLE);
				layout_info.setVisibility(View.INVISIBLE);
				layout_speaker.setVisibility(View.INVISIBLE);
				layout_notesList.setVisibility(View.INVISIBLE);

			}
		});



	}
	/*class speakerAdapter extends BaseAdapter
	{
		ArrayList<String> speaker;
		Context context;
		public speakerAdapter(Context context,ArrayList<String> speaker) {
			// TODO Auto-generated constructor stub
			this.speaker=speaker;
			this.context=context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return speaker.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view=inflater.inflate(R.layout.agenda_speaker_adapter, null);

			return view;
		}

	}*/



	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

}
