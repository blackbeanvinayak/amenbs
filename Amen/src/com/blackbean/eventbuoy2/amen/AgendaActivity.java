package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.blackbean.utility.DataBase;
import com.google.analytics.tracking.android.EasyTracker;

@SuppressLint("DefaultLocale")
public class AgendaActivity extends FragmentActivity {
	// static ListView listview;
	static TextView date;
	static EditText SeachDate;
	static ViewPager vp;
	LinearLayout linearLayout;
	TextView header_txt;
	DescriptionAdapter descriptionAdapter;
	ImageView next, previous;
	static int textlength = 0;
	Context context;
	int checknextprevious = 0;
	static DataBase dataBase;

	ArrayList<String> array_speaker_name = new ArrayList<String>();
	ArrayList<String> array_speaker_id = new ArrayList<String>();
	static ArrayList<String> listview_agenda_id;

	static String Vdate;
	static ArrayList<String> dateArrayList;
	static int pagerCount, currentpage = 0;
	static AgendaAdapter agendaAdapter;
	static ArrayList<String> agenda_id = new ArrayList<String>();

	static ArrayList<String> agenda_name = new ArrayList<String>();
	static ArrayList<String> agenda_location = new ArrayList<String>();
	static ArrayList<String> agenda_time = new ArrayList<String>();
	static ArrayList<String> agenda_speaker=new ArrayList<String>();
	static ArrayList<Save> save;
	ArrayList<String> day_event_name1;
	ArrayList<String> day_event_location1;
	ArrayList<String> day_event_time1;
	static ArrayList<String> day_event_id1;
	private static final String Agenda_ID = "agenda_id";
	private static final String Agenda_NAME = "agenda_name";
	private static final String Agenda_LOCATION = "agenda_location";
	private static final String Agenda_TIME = "agenda_time";

	int edtCount=0;
	int searchBy=1;
	public static final int SEARCH_Time=1;
	public static final int SEARCH_Session=2;
	public static final int SEARCH_Speaker=3;
	ImageView imageSearch;
	static String event_category;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ajenda);
		save = new ArrayList<Save>();
		linearLayout = (LinearLayout) findViewById(R.id.agenda_layout);
		imageSearch=(ImageView) findViewById(R.id.imageSearch);

		dataBase = new DataBase(this);
		dataBase.open();

		SeachDate = (EditText) findViewById(R.id.SearchEdittext);
		context = this;

		date = (TextView) findViewById(R.id.date);

		/*if(HomeActivity.whichEvent==StaticData.Event_Nairobi)
		{
			event_category="HPI";
		}else {
			event_category="MDET";
		}*/
		String eventCat = getIntent().getStringExtra("EVENT");

		if (eventCat.equals("kochi")) {
			event_category="kochi";
		}else if(eventCat.equals("bangalore")){
			event_category = "bangalore";
		}else if (eventCat.equals("raipur")) {
			event_category = "raipur";
		}

		dateArrayList = new ArrayList<String>();
		dateArrayList = dataBase.getDifferentDate(event_category);

		/***
		 * Menu click event
		 */
		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AgendaActivity.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();
			}
		});


		descriptionAdapter = new DescriptionAdapter(
				getSupportFragmentManager(), context);
		vp = (ViewPager) findViewById(R.id.pager);
		vp.setAdapter(descriptionAdapter);
		pagerCount = descriptionAdapter.getCount();
		next = (ImageView) findViewById(R.id.next_imageview);

		next.setVisibility(View.VISIBLE);

		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				vp.setCurrentItem(vp.getCurrentItem() + 1);

			}
		});
		previous = (ImageView) findViewById(R.id.previous_imageview);
		previous.setVisibility(View.INVISIBLE);

		previous.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				vp.setCurrentItem(vp.getCurrentItem() - 1);
			}
		});


		if (dateArrayList.size() == 1) {
			next.setVisibility(View.INVISIBLE);
		}



		// Only first time display dialog on search Edittext click

		SeachDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(edtCount<=0)
				{
					edtCount++;
					showSearchDialog(context);

				}
			}
		});


		imageSearch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				searchBy=showSearchDialog(context);

			}
		});

		array_speaker_name = new ArrayList<String>();
		array_speaker_id = new ArrayList<String>();
		dataBase=new DataBase(context);
		dataBase.open();
		Cursor c=dataBase.getAllSpeakerFromSpeakerTable(event_category);
		if (c!=null) {
			if (c.moveToFirst()) {
				do {
					array_speaker_name.add(c.getString(1));
					array_speaker_id.add(String.valueOf(c.getInt(0)));
				} while (c.moveToNext());
			}
		}
		System.err.println("Array Speaker and id\n");
		for (int j = 0; j < array_speaker_id.size(); j++) {
			System.out.println("name "+array_speaker_name.get(j));
			System.out.println("id "+array_speaker_id.get(j));
		}

		dataBase.close();

		SeachDate.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub
				System.out.println("COUNT "+count);
				//if (count>0) {


				String text = SeachDate.getText().toString().toLowerCase();

				day_event_name1 = new ArrayList<String>();
				day_event_location1 = new ArrayList<String>();
				day_event_time1 = new ArrayList<String>();
				day_event_id1 = new ArrayList<String>();
				day_event_id1.clear();

				if (searchBy == AgendaActivity.SEARCH_Time) {

					for (int i = 0; i < save.get(vp.getCurrentItem()).sagenda_time
							.size(); i++) {

						if (save.get(vp.getCurrentItem()).sagenda_time.get(i)
								.startsWith(text)) {
							day_event_time1.add(save.get(vp.getCurrentItem()).sagenda_time
									.get(i));
							day_event_name1.add(save.get(vp.getCurrentItem()).sagenda_name
									.get(i));
							day_event_location1.add(save.get(vp.getCurrentItem()).sagenda_location
									.get(i));
							day_event_id1.add(save.get(vp.getCurrentItem()).sagenda_id
									.get(i));
						}

					}
				}else if (searchBy == AgendaActivity.SEARCH_Session) {

					for (int i = 0; i < save.get(vp.getCurrentItem()).sagenda_name
							.size(); i++) {


						if (save.get(vp.getCurrentItem()).sagenda_name.get(i).toLowerCase()
								.contains(text)) {
							day_event_time1.add(save.get(vp.getCurrentItem()).sagenda_time
									.get(i));
							day_event_name1.add(save.get(vp.getCurrentItem()).sagenda_name
									.get(i));
							day_event_location1.add(save.get(vp.getCurrentItem()).sagenda_location
									.get(i));
							day_event_id1.add(save.get(vp.getCurrentItem()).sagenda_id
									.get(i));
						}

					}
				}else if (searchBy == AgendaActivity.SEARCH_Speaker) {

					ArrayList<String> agenda_id_from_speaker=new ArrayList<String>();
					ArrayList<String> speakerID=new ArrayList<String>();

					for (int i = 0; i < array_speaker_name.size(); i++) {
						if(array_speaker_name.get(i).toLowerCase().contains(text))
						{
							System.out.println("Speaker id "+ array_speaker_id.get(i));
							speakerID.add(array_speaker_id.get(i));
							dataBase=new DataBase(AgendaActivity.this);
							dataBase.open();
							agenda_id_from_speaker.addAll(
									dataBase.getAgendaIdBySpeakerId(Integer.valueOf(array_speaker_id.get(i))));
						}
					}
					for (int i = 0; i < save.get(vp.getCurrentItem()).sagenda_id
							.size(); i++) {
						for (int j = 0; j < agenda_id_from_speaker.size(); j++) {


							if(save.get(vp.getCurrentItem()).sagenda_id.get(i).equals(agenda_id_from_speaker.get(j)))
							{
								day_event_time1.add(save.get(vp.getCurrentItem()).sagenda_time
										.get(i));
								day_event_name1.add(save.get(vp.getCurrentItem()).sagenda_name
										.get(i));
								day_event_location1.add(save.get(vp.getCurrentItem()).sagenda_location
										.get(i));
								day_event_id1.add(save.get(vp.getCurrentItem()).sagenda_id
										.get(i));
							}
						}
					}
				}

				AgendaAdapter ar = new AgendaAdapter(AgendaActivity.this,
						day_event_id1, day_event_name1, day_event_location1,
						day_event_time1,dateArrayList.get(vp.getCurrentItem()));

				save.get(vp.getCurrentItem()).slistview.setAdapter(ar);
				//}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		vp.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub

				if (arg0 == 0) {
					previous.setVisibility(View.INVISIBLE);
					next.setVisibility(View.VISIBLE);

				}
				if (arg0 > 0) {
					previous.setVisibility(View.VISIBLE);
				}

				if ((pagerCount - 1) == arg0) {
					previous.setVisibility(View.VISIBLE);
					next.setVisibility(View.INVISIBLE);
				}

				if (arg0 < pagerCount - 1) {
					next.setVisibility(View.VISIBLE);
				}

				currentpage = arg0;

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub
				date.setText(dateArrayList.get(vp.getCurrentItem()));
				// date.setText("1/1/1");
				SeachDate.setText("");
				save.get(vp.getCurrentItem()).slistview
				.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						DataBase dataBase=new DataBase(AgendaActivity.this);
						dataBase.open();
						//if(dataBase.hasSpeakerForThisAgenda(Integer.parseInt(day_event_id1.get(position))))
						{ 
							Intent intent = new Intent(AgendaActivity.this,
									AgendaDetailsActivity.class);
							intent.putExtra("agenda_id",day_event_id1.get(position).toString());
							intent.putExtra("EVENT", event_category);
							startActivity(intent);
						}
						dataBase.close();
					}
				});
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	public static class DescriptionAdapter extends FragmentStatePagerAdapter {
		Context context;

		public DescriptionAdapter(FragmentManager fm, Context context) {
			super(fm);
			this.context = context;
			// TODO Auto-generated constructor stub
		}

		@Override
		public Fragment getItem(int i) {
			// TODO Auto-generated method stub
			Fragment fragment = new SimpleFragment();
			Bundle args = new Bundle();
			// args.putString("question", staticData.getQuestion().get(arg0));
			args.putInt("id", i);
			args.putString("date", dateArrayList.get(i));
			// Toast.makeText(this.context, ""+(i), Toast.LENGTH_LONG).show();
			fragment.setArguments(args);
			//this.notifyDataSetChanged();
			return fragment;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return dateArrayList.size();

		}

	}

	public static class SimpleFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			// TODO Auto-generated method stub

			final Bundle get_info = getArguments();
			/*
			 * retrieve data from database by date and pass the info of event to
			 * the adapter
			 */

			Vdate = get_info.getString("date");

			int event_number = get_info.getInt("id");

			View rootView = inflater.inflate(R.layout.agenda_listview,
					container, false);
			Save temp = new Save();

			temp.slistview = (ListView) rootView
					.findViewById(R.id.agenda_listview);

			dataBase = new DataBase(getActivity());
			dataBase.open();
			agenda_id = dataBase.getAgendaAllDataOffieldByFieldAndDate(Vdate,
					Agenda_ID,event_category);
			agenda_name = dataBase.getAgendaAllDataOffieldByFieldAndDate(Vdate,
					Agenda_NAME,event_category);
			agenda_location = dataBase.getAgendaAllDataOffieldByFieldAndDate(
					Vdate, Agenda_LOCATION,event_category);
			agenda_time = dataBase.getAgendaAllDataOffieldByFieldAndDate(Vdate,
					Agenda_TIME,event_category);


			temp.sagenda_id = agenda_id;
			temp.sagenda_name = agenda_name;
			temp.sagenda_location = agenda_location;
			temp.sagenda_time = agenda_time;
			temp.sagenda_speaker=agenda_speaker;

			agendaAdapter = new AgendaAdapter(getActivity(), agenda_id,
					agenda_name, agenda_location, agenda_time,Vdate);
			if (event_number >= save.size()) {
				save.add(temp);
				save.get(event_number).slistview.setAdapter(agendaAdapter);
			} else {
				save.get(event_number).slistview = temp.slistview;
			}

			dataBase.close();


			save.get(vp.getCurrentItem()).slistview
			.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent,
						View view, int position, long id) {
					// TODO Auto-generated method stub
					dataBase = new DataBase(getActivity());
					dataBase.open();
					listview_agenda_id = new ArrayList<String>();

					listview_agenda_id = dataBase
							.getAgendaAllDataOffieldByFieldAndDate(date
									.getText().toString(), Agenda_ID,event_category);

					Intent intent = new Intent(getActivity(),
							AgendaDetailsActivity.class);
					/*intent.putExtra("agenda_id", day_event_id1
							.get(position).toString());*/
					//intent.putExtra("agenda_id", save.get(vp.getCurrentItem()).sagenda_id.get(position).toString());

					getActivity().startActivity(intent);

					dataBase.close();
				}
			});

			return rootView;
		}
	}

	public static class Save {

		ListView slistview;
		ArrayList<String> sagenda_id;
		ArrayList<String> sagenda_name;
		ArrayList<String> sagenda_location;
		ArrayList<String> sagenda_time;
		ArrayList<String> sagenda_speaker;

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}



	public int showSearchDialog(Context context) {

		//final int val=0;
		final Dialog dialog=new Dialog(context);
		dialog.setContentView(R.layout.search_dialog);
		LinearLayout linearTime,linearSession,linearSpeaker;

		linearTime=(LinearLayout) dialog.findViewById(R.id.linearTime);
		linearSession=(LinearLayout) dialog.findViewById(R.id.linearSession);
		linearSpeaker=(LinearLayout) dialog.findViewById(R.id.linearSpeaker);

		dialog.show();

		linearTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				searchBy=AgendaActivity.SEARCH_Time;
				dialog.dismiss();
				imageSearch.setImageResource(R.drawable.time_icon_white);
				SeachDate.setHint("Search By Time");
				SeachDate.setText("");

			}
		});

		linearSession.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				searchBy=AgendaActivity.SEARCH_Session;
				dialog.dismiss();
				imageSearch.setImageResource(R.drawable.session_icon_white);
				SeachDate.setHint("Search By Session");
				SeachDate.setText("");
				//Toast.makeText(AgendaActivity.this, ""+searchBy, Toast.LENGTH_LONG).show();
			}
		});
		linearSpeaker.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				searchBy=AgendaActivity.SEARCH_Speaker;
				dialog.dismiss();
				imageSearch.setImageResource(R.drawable.speaker_icon_white);
				SeachDate.setHint("Search By Speaker");
				SeachDate.setText("");
				//Toast.makeText(AgendaActivity.this, ""+searchBy, Toast.LENGTH_LONG).show();
			}
		});


		return searchBy;
	}

}