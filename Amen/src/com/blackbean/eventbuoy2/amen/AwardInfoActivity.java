package com.blackbean.eventbuoy2.amen;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackbean.adapters.ExpandableListAdapter;
import com.blackbean.adapters.ExpandableListAwardAdapter;
import com.blackbean.eventbuoy2.amen.R;


public class AwardInfoActivity extends Activity
{
	TextView award_category;
	ImageView image;
	ExpandableListAwardAdapter listAdapter;
	ExpandableListView expandable;
	static List<String> listDataHeader;
	static HashMap<String, List<String>> listDataChild;
	//static HashMap<ArrayList<String>, List<String>> listDataChild;
	static HashMap<String, List<String>> listDataChildsocial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.award_informtion);
		//image=(ImageView)findViewById(R.id.image);
		
		//award_category=(TextView)findViewById(R.id.category);
		//award_category.setText(getIntent().getExtras().getString("name"));	
	
		/*if(getIntent().getExtras().getInt("image_id")==0){
			image.setImageResource(R.drawable.digital);
		}
		if(getIntent().getExtras().getInt("image_id")==1){
			image.setImageResource(R.drawable.website);
		}
		if(getIntent().getExtras().getInt("image_id")==2){
			image.setImageResource(R.drawable.mobile);
		}
		if(getIntent().getExtras().getInt("image_id")==3){
			image.setImageResource(R.drawable.special);
		}*/
		
		
		 expandable=(ExpandableListView)findViewById(R.id.explist);
		 listDataChild = new HashMap<String, List<String>>();
		 //listDataChild = new HashMap<ArrayList<String>, List<String>>();
        listDataChildsocial = new HashMap<String, List<String>>();
        listDataHeader=new ArrayList<String>();
        listDataHeader.add("Organizational Awards");
        listDataHeader.add("Organization Award (International Category)");
        listDataHeader.add("Academic Institute Award");

        // Adding child data
        List<String> sub1 = new ArrayList<String>();
        sub1.add("Excellence in HR Analytics");
        sub1.add("Excellence in Diversity & Inclusion");
        sub1.add("Excellence in Social Media People Campaign");
        sub1.add("Excellence in Workplace Flexibility");
        sub1.add("Excellence in Community Impact");
        sub1.add("Excellence in Developing Leaders of Tomorrow");
        sub1.add("Employer with Best Employee Health and Wellness Initiatives");
        
        List<String> sub2 = new ArrayList<String>();
        sub2.add("Excellence in Human Resource - South Asia (excluding India) & Middle East");
        List<String> sub3 = new ArrayList<String>();
        sub3.add("Academic Institute of the Year (For Contribution in the field of HR)");
       
       //List<String> sociallistsocial = new ArrayList<String>();
      // sociallistsocial.add("social");
 
        listDataChild.put(listDataHeader.get(0), sub1); // Header, Child
        listDataChild.put(listDataHeader.get(1), sub2);
        listDataChild.put(listDataHeader.get(2), sub3);
        
        
        
        
        ArrayList<HashMap<String, ArrayList<String>>> map=new ArrayList<HashMap<String,ArrayList<String>>>();
        
        HashMap<String, ArrayList<String>> mp1=new HashMap<String,ArrayList<String>>();
        HashMap<String, ArrayList<String>> mp2=new HashMap<String,ArrayList<String>>();
        
        ArrayList<String> a0=new ArrayList<String>();//6
        ArrayList<String> a1=new ArrayList<String>();//1
        ArrayList<String> a2=new ArrayList<String>();//1
        a0.add("Excellence in HR Analytics");
        a0.add("Excellence in Diversity & Inclusion");
        a0.add("Excellence in Social Media People Campaign");
        a0.add("Excellence in Workplace Flexibility");
        a0.add("Excellence in Community Impact");
        a0.add("Excellence in Developing Leaders of Tomorrow");
        a0.add("Employer with Best Employee Health and Wellness Initiatives");
        a1.add("Excellence in Human Resource - South Asia (excluding India) & Middle East");
        a2.add("Academic Institute of the Year (For Contribution in the field of HR)");
        
        
        ArrayList<String> b0=new ArrayList<String>();//6
        ArrayList<String> b1=new ArrayList<String>();//1
        ArrayList<String> b2=new ArrayList<String>();//1
        
        mp1.put(listDataHeader.get(0), a0);
        mp1.put(listDataHeader.get(1), a1);
        mp1.put(listDataHeader.get(2), a2);
        
        mp2.put(listDataHeader.get(0), b0);
        mp2.put(listDataHeader.get(1), b1);
        mp2.put(listDataHeader.get(2), b2);
        
        b0.add("Definition:\n\nHuman Capital Analytics is the process by which the value of the organization's people is measured and improved for the purpose of enhancing overall organizational performance.The key elements that are important for Human Capital Analytics in the organization are those that add value to your organization which include the tools and technology being adopted, integrating analytics with organizational financials, educating stakeholders and the competency of an individual in this area.The HR Analytics Award recognizes the ability of the organization in incorporating all these key elements in their Human Capital Analytics approach.\n\nOverall Measures:\n\nRobust Human Capital Model which ensures that the HR function is involved in developing, implementing and measuring the organizational strategyHR Dashboard used to track progress on HR Strategy and the measurements used to detail performanceHR Technology and Tools used for capturing accurate data for business needs and utilization of this data for decision making.Process of measuring contribution at primarily 3 levels - organizational (in terms of human capital cost efficiency and ROI), functional / business unit level (contribution of human capital to the unit's productivity) and human capital management (in terms of effectiveness of the HR department, employee turnover and other related metrics).Competency of the HR Professional in Human Capital Analytics (e.g. use of Human Capital Analytics by the HR Professional in their daily working, HR people with HR Analytics skills) Trainings undertaken for business management in HR Analytics and its usage Initiatives undertaken to educate stakeholders in Human Capital AnalyticsUsage of HR Analytics by the top management Usage of HR Analytics by business managers");
        b0.add("Definition:\n\nDiversity & Inclusion is the collective mixture of differences and similarities that includes for example, individual and organizational characteristics, values, beliefs, experiences, backgrounds, preferences and behaviors.It has three primary dimensions -\nInternal dimensions of diversity, such as age, sexual orientation, race and gender. External dimensions of diversity, such as religion, marital status, income and educational background.Organizational dimensions of diversity, such as work location, function, seniority and management status.\nThis Award therefore recognizes companies that have adopted and implemented policies and practices to build in and promote diversity and inclusion. It seeks to reward the thought process that the \"culture of inclusion\" has become a business imperative.\n\nOverall Measures:\n\nEffective implementation of a proactive strategy in Diversity (for Gender, Generation, Culture, Physical Disabilities and so on) Financial and resource investments for diversity initiatives Process of measuring Diversity ROI, either through impact on the bottom line or the top line. Employee perception Audits and Surveys conducted Increase in organization's market share, profits and customer satisfaction in diverse market niches or geographical regions Diversity ratio and workforce composition at senior leadership levels and at the time of promotions Performance metrics defined for managers, for diverse teams Focus on resolving perception based differences in performance ratings for different employee groups (e.g. women and men) undergoing similar talent development trainings Availability of infrastructure that is suitable for all employee groups Regular employee survey to assess employee perception of inclusion in the organizationDiversity initiatives covering different areas of HR - Hiring, Training, Employee Communication, Engagement, Grievance Redressal, Flexible Policies Focus on increasing diversity in the Supplier base (e.g. empaneling high quality minority suppliers as a business practice).Demonstrate formal or informal leadership by embracing inclusion through positive interactions");
        b0.add("Definition:\n\nThe Social Media People Campaign by an organization refers to the effective use of social media channels, tools and applications for driving key people processes for high impact. The Award seeks to recognize companies that have a well-rounded approach towards integrating social media across talent sourcing, employee engagement, learning, employer branding and so on. It also recognizes initiatives that the organization is implementing through which Social Media is being used to foster a higher degree of collaboration between cross-functional teams, leveraging the content created by employees, developing Knowledge Management processes, connecting employees in remote locations to a common platform which allows for knowledge sharing for business value.\n\nMeasures:\n\nClear articulation of a proactive strategy for using Social media in HR processes for people and business impact (of different types - Facebook, Twitter, LinkedIn and so on) Financial and resource investments for HR-Social Media initiatives Integrated approach for implementing Social media usage in key initiatives such as Hiring, Training, Employee Communication, Engagement, Employer Branding and so on. Creative and appropriate use of different kinds of Web tools and technologies by the organization for employee related activities. Regular tracking of both lead and lag indicators to assess whether these initiatives have impacted the organization culture and business, in a positive manner. Adapting to the changing manner in which employees connect with each other (for example, having a social media platform internally) and implementing such new initiatives.");
        b0.add("Definition:\n\nFlexible work arrangements (FWAs)/Workplace flexibility means greater flexibility in the place of work, the scheduling of hours worked and the amount of hours worked. Such arrangements give employees greater control over where and when work gets done and over how much time they choose to work, leading to greater opportunities for employees to be able to enjoy an optimal balance between work and life responsibilities. This Award recognizes organizations that consistently leverage workplace flexibility to create a more effective and productive workplace, and help the employees to fulfill their potential.\n\nMeasures:\n\nInitiatives and investments made to implement workplace flexibility in your organization Process for tracking the use of this benefit and the positive impact it has on business as well as employee performance and retention. Initiatives undertaken to raise awareness and educate employees about workplace flexibility and its benefits Different types of work arrangements offered by the organization and employee usage of these arrangements. Supportive initiatives undertaken which supplement workplace flexibility, such as Healthcare / Wellness benefits, Child care, Employee Assistance programs and so on.");
        b0.add("Definition:\n\nThe Community Impact award showcases organizations which have created a significant impact on community through their sustainable social initiatives, particularly in the people realm. It seeks to recognize the commitment of these firms in creating a balanced approach between economic growth and profits on one hand, and community development related initiatives aimed at creating higher societal value on the other. The areas of impact can range at the micro level, from improving the quality of life for communities that their businesses are operating in, to driving financial empowerment and enterprise development at the macro level. The sectors can vary from Education, Healthcare, Agriculture, Energy, Environment, Livelihood-related training and so on. The essence of the Award is to recognize the spirit of being responsible as a corporate entity, towards contributing to solutions for identified social concerns by utilizing the organization's own financial and people resources.\n\nMeasures:\n\nMetrics used to measure the Community Impact (and Societal Value) of the initiatives, over a predefined timeframe. New initiatives added in the past 3-5 years, to which budgeted funds have been allocated Financial and people investments made in this area Process used to track accountability of funded programs through regular measurement and reporting of outcomes. Initiatives undertaken to increase Employee Involvement in the Community programs supported by the organization. Initiatives to develop community networks, facilitated/led by the organization that are self-sustaining.");
        b0.add("Definition:\n\nDeveloping the Leaders of Tomorrow requires a different kind of approach as compared to the leadership development frameworks that organizations typically have. The need of the market is to have a holistic LEADER development approach, which focuses on elements such as sustainability, a global mindset, value system based on ethics, accountability and so on. This award therefore seeks to recognize organizations which have been implementing best/ next practices/programs in this area in order to develop their potential leaders (3 to 5 years) or emerging leaders (roles which are a level below the CXO roles), for roles which are new and far more challenging than those in the past, due to the rapid market changes.\n\nMeasures:\n\nGrowth in the number/percentage of potential and emerging leaders identified in-house for future roles Quality and diversity of developmental projects/initiatives provided to potential leaders Parameters used to measure business and strategic impact of these programs Evaluation approach identified to assess the performance of the identified future leaders in roles which require them to demonstrate the requisite attributes. Financial and people related investments made in programs and practices in this area. Retention of high potential leadership talent that has been identified through this process.");
        b0.add("Definition:\n\nThis award seeks to recognize organizations that understand the linkage of employee health to business productivity. These have proactively identified or designed specific health and wellness programs which can support the needs of their employee segments.The initiatives that can be covered under these go beyond insurance plans. These can be onsite healthcare facilities or online health assessment tools, programs and information providers for healthy lifestyles, regular medical employee health screening camps, wellness and nutrition coaching/ counseling, tie-ups with any hospitals, club or gym memberships, women's health, stress management counseling. The programs for senior leadership can also be included in this category.Within the gamut of this award, will also be healthcare support provided to disabled employees or those who have been injured while at work, assistance provided to those who have a dependency on tobacco or alcohol, suffering from obesity. In inclusive and forward thinking organization, this could also involve counseling related to raising awareness in HIV/AIDS.\n\nMeasures:\n\nNumber and frequency of the health and wellness employee initiatives. Range (wide/diverse) of initiatives provided from the list shared above (and any others) and provisions made under each of these. Investment made (financial and people) in employee health and wellness related programs Parameters used to measure the satisfaction, usage and value of these programs to the employees and percentage of employees covered by these programs External partnerships and tie-ups for the programs Organization culture and Infrastructural changes made to incorporate Any process used to track the positive business impact of these programs");
        b1.add("Definition:\n\nExcellence in Human Resource requires organizations to think laterally and design and implement people management practices and/or systems that are exceptional as opposed to the traditional processes that organizations typically have. An ever-evolving external environment, where knowledge workers are the differentiators for successful organizations, it is essential for organizations to come up with practices that not only differentiate them but also support in achievement of their people and overall business strategy.\n\nThe key dimensions of such a people management practice or system are:\n\nUniqueness of the practice/system. This implies that while the practice/system would be grounded in the traditional Human Resource disciplines, it would still be innovative and unique to the organization.Integration of the practice/system with the overall human resource strategy of the organization. This also implies that the practice/system supports the organization in achieving its targets (bottom line and/or top line).This award, therefore, seeks to recognize organizations, which have successfully designed and implemented such “excellent” people management practice(s) or system(s). This could be a single/multiple innovative practice(s) or system(s) that the organization has implemented across any area in Human Resource and has been running successfully in the organization for some time.\n\nNote:\n\nEach organization may submit an Application Form which includes one or more (maximum 3) initiatives’ of people management/HR practice or systemThe said practice/system must have completed a minimum run of two years in the organization The entry can include details of a single practice or an entire system. For example: HR Practice: An organization may submit details of how they have introduced the concept of a ‘Career Manager’ where each individual in the company is assigned a career manager who focuses on the individual’s long-term growth through frequent mentoring and developmental inputs provided by the Career Manager ORHR System: An organization may submit details of their 'Career Management System', which has various sub-sets like individuals identifying their current competencies, career aspirations and competency gaps based on the needed competencies/skills for the next role in the organization. These individuals would then create a plan for covering the competency gap; be assisted in the same by their career counselors and work on the same. The entire system is online and automated with career opportunities in the organization also posted on the online portal in a regular manner. Within the gamut of this award, will be practices or systems under any HR discipline. The following are indicative disciplines that the practice/system could fall under:\n\nStrategic HRMTalent Acquisition and People FlowsPerformance ManagementLearning and Development (including Knowledge Management)Compensation, Reward and RecognitionTalent Development, Engagement and RetentionEmployee Advocacy and Relations (includes Employee Engagement)Industrial RelationsHR and Social MediaDiversityExecutive Coaching and LeadershipHuman Capital Standards and Analytics\n\nOverall Measures:\n\nInnovativeness of the practice/system. Its’ uniqueness to the organization and difference from practices followed in the industry or parallel industries.Adequate balance between human touch (exceptions, personal connect) of the practice/system and its routine and consistency (through established norms, detailed guidelines)Integration of the practice/system with the HR strategy and overall Business strategy Financial and people related investment made for the system/practice (can also include details of organizational culture and infrastructural changes made to incorporate the practice/system and leadership buy-in for the system/practice)Parameters used to measure business and strategic impact of the practice/system (Qualitative and quantitative data both will be accepted here. This specially tracks the business impact of the program)Parameters used to measure the satisfaction, usage and value of the practice/system to the target employees and percentage of employees (from the targeted population) covered by the program (this specifically tracks the people impact of the program)Variety of methods used in the practice/system (example: a leadership development program could use a variety of methods like coaching, classroom training and live projects)");
        b2.add("Definition:\n\nThis award is for the area of Academics, which has a huge impact on the kind of talent pool that organizations will receive. It recognizes academic institutions that have played a prominent role in contributing to the field of HR, through a market focused HR curriculum, path-breaking research, committed HR faculty and as well as initiatives that enable the students to become more employable for ready absorption into the corporate environment.While on one hand the institute should be focusing on regularly aligning its curriculum to ensure that the knowledge provided to the students ensures that global HR professionals are being developed, on the other hand, it should be working actively towards partnering with the HR community as well as corporates in order to focus on employable talent (e.g. through mentorship programs, actively seeking curriculum related inputs from corporates, industry interfaces and workshops, and so on).\n\nMeasures:\n\nResearch work in the field of HR which has been recognized (India and/or Asia level) Partnerships with corporates to enable the creation of employable talent (should also cover types of partnerships and Initiatives) Initiatives undertaken and Investments made towards ensuring a globally recognized HR curriculum. HR Faculty satisfaction report HR Student satisfaction scores Process/metrics (if any), used to track progress of initiatives through regular measurement and reporting of outcomes.");
        map.add(mp1);
        map.add(mp2);
        
        
        
        
        
     
  
        /*listAdapter = new ExpandableListMobileAdapter(AwardInfoActivity.this,
                        listDataHeader, listDataChild);*/
        listAdapter = new ExpandableListAwardAdapter(AwardInfoActivity.this,
                listDataHeader, map);

        // setting list adapter
        expandable.setAdapter(listAdapter);

        ImageView menu=(ImageView)findViewById(R.id.logo);
		menu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(AwardInfoActivity.this, HomeActivity.class);
				startActivity(intent);
				
			}
		});	
	}

}
