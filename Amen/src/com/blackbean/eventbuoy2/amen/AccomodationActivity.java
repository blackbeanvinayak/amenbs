
package com.blackbean.eventbuoy2.amen;

import com.blackbean.eventbuoy2.amen.R;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
/****
 * 
 * To show Accomodation for Delhi Event
 * 
 * @author blackbeanumesh
 *
 */
public class AccomodationActivity extends Activity{

	TextView Phone1,Phone2,Phone3,Phone4,Phone41,Phone5,Phone51;

	TextView Email1,Email2,Email3,Email4,Email5;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.accommodation);

		Phone1 = (TextView) findViewById(R.id.Phone1);
		Phone2 = (TextView) findViewById(R.id.Phone2);
		Phone3 = (TextView) findViewById(R.id.Phone3);
		Phone4 = (TextView) findViewById(R.id.Phone4);
		Phone41 = (TextView) findViewById(R.id.Phone41);
		Phone5 = (TextView) findViewById(R.id.Phone5);
		Phone51 = (TextView) findViewById(R.id.Phone51);

		Email1 = (TextView) findViewById(R.id.Email1);
		Email2 = (TextView) findViewById(R.id.Email2);
		Email3 = (TextView) findViewById(R.id.Email3);
		Email4 = (TextView) findViewById(R.id.Email4);
		Email5 = (TextView) findViewById(R.id.Email5);


		Phone1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				call("+911141501234");
			}
		});
		Phone2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				call("+911146215000");
			}
		});
		Phone3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				call("+911124363030");
			}
		});
		Phone4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				call("+919810798499");
			}
		});
		Phone41.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				call("+911144447054");
			}
		});
		Phone5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				call("+911166150000");
			}
		});
		Phone51.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				call("+919891586675");
			}
		});
		
		

		Email1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				EmailSend(new String[]{"bkaul@theimperialindia.com"});
			}
		});

		Email2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				EmailSend(new String[]{"shilpa.sharma@itchotels.in"});
			}
		});

		Email3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				EmailSend(new String[]{"Divya.Shekhar@oberoigroup.com"});
			}
		});

		Email4.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				EmailSend(new String[]{"lsharma@thelalit.com"});
			}
		});

		Email5.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				EmailSend(new String[]{"shoaib@hanshotels.com"});
			}
		});

		/***
		 * Menu click event
		 */
		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AccomodationActivity.this,MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();

			}
		});


	}
	public void call(String phno) {
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:" + phno));
		startActivity(callIntent);
	}


	public void EmailSend(String [] email) {
		Intent intentEmail = new Intent(Intent.ACTION_SEND);
		intentEmail.putExtra(Intent.EXTRA_EMAIL, email);
		//intentEmail.putExtra(Intent.EXTRA_SUBJECT, "your subject");
		//intentEmail.putExtra(Intent.EXTRA_TEXT, "message body");
		intentEmail.setType("message/rfc822");
		startActivity(Intent.createChooser(intentEmail, "Choose an email provider :"));
	}
}


