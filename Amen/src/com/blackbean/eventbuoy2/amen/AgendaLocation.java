package com.blackbean.eventbuoy2.amen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class AgendaLocation extends Activity{
	
	LinearLayout kochi,raipur,bangalore;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.agenda_loacation);
		
		kochi = (LinearLayout) findViewById(R.id.kochi);
		bangalore = (LinearLayout) findViewById(R.id.bangalore);
		raipur = (LinearLayout) findViewById(R.id.raippur);
		
		
		ImageView home = (ImageView) findViewById(R.id.home);
		home.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AgendaLocation.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();
			}
		});
		
		kochi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
			click("kochi")	;
			}
		});
		raipur.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				click("raipur");
			}
		});
		bangalore.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				click("bangalore");
			}
		});
	}
	
	public void click(String event) {
		Intent intent = new Intent(AgendaLocation.this, AgendaActivity.class);
		intent.putExtra("EVENT", event);
		startActivity(intent);
		//finish();
	}
}
