package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;

import com.blackbean.eventbuoy2.amen.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FAQsActivity extends Activity {
	ListView list;
	ArrayList<String> questions, answers;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_faq);
		list = (ListView) findViewById(R.id.list);
		questions = new ArrayList<String>();
		answers = new ArrayList<String>();

		questions.add("What are the various Award Categories?");
		answers.add("There are 3 broad categories:\nOrganization Awards (India specific)\nOrganization Award (International Category excluding India)\nAcademic Institute of the Year (For Contribution in the field of HR)\nYou can view the detailed categories on the below link along with their respective definitions and overall measures.\nLink: http://annualindia.shrmindia.org/awards.php#example-one2");

		questions.add("What is the last date for application?");
		answers.add("20th August, 2014");

		questions.add("How do we go about the process of applying?");
		answers.add("This is shared in the link below -\nhttp://annualindia.shrmindia.org/awards.php#example-one4\nYou will need to download the relevant -\nApplication forms- fill those in and send across (soft copy) with 2-3 supporting documents only (in pdf/document)\nDeclaration Form- fill it and send across a scanned copy along with the Application Forms at the Awards email address SHRMIAwards@shrm.org .\nPay one-time Application fee of INR 10,000 + Service tax at 12.36% i.e. INR 11,236/- . This will be a standard amount irrespective of the number of categories an organisation is applying for. Payment can be made online or can be sent via cheque/DD to either of the SHRM offices in India.");

		questions
				.add("Would there be any fees/cost involved to nominate the company for the award categories?");
		answers.add("All participating organizations would need to pay a one-time Application fee of INR 10,000 + Service tax at 12.36% i.e. INR 11,236/- .\n\nThis will be a standard amount irrespective of the number of categories an organization is applying for.\n\nPlease find more details on payment on this link-\nhttp://annualindia.shrmindia.org/awards.php#example-one4");

		questions
				.add("How many departments can nominate the company for the Award categories?");
		answers.add("We are not accepting department wise nominations. The organization has to be nominated. If it is a group of organizations, the group companies can apply separately but they have to be separate legal entities.");

		questions.add("Can IT Dept. also nominate for the Award category?");
		answers.add("No the department cannot be nominated. The organization can be nominated and practices from this department as applicable to the award category can be included.");

		questions.add("How are the companies selected?");
		answers.add("We have a 2-tier stringent Jury process which will be managed by our Process Advisors independently. The Jury will comprise of independent HR leaders from across consulting, industry and academics. There will be a screening jury which will shortlist finalists based on the Application Forms and the Finalists will be required to make a presentation in front of the Final Jury.");

		questions
				.add("How many award categories can a company nominate themselves for?");
		answers.add("A company can nominate itself in any/all the organization award categories as long as it has relevant people practices/ process to support the nomination.");

		questions
				.add("If we are nominating 2 initiatives in the same category, should we be sending 2 different entries/forms?");
		answers.add("No. All initiatives have to be included in one single Application Form. One organization can submit one application form only for one category.");

		questions.add("Can we have multiple entries per category?");
		answers.add("No");

		questions
				.add("Is there a restriction on the number of words to give answers to the questions?");
		answers.add("Please keep your responses under the Qualitative section under 400-500 words.");

		questions
				.add("Would it be possible to grant us an extension of few days to submit the application?");
		answers.add("No special / specific extension will be granted to an individual organization. Only if a general extension in the last date is announced with the approval of the Process advisors, will the same apply. Currently we have no plans to extend the last date for submission.");

		questions
				.add("Who are the jury members and how are they selected by SHRM?");
		answers.add("The Jury members will be shared shortly on the Conference website. It will be a mix of Business Heads/CHROs/Consultants/Academicians. They will be selected by SHRM with the inputs of the Process Advisors based on their expertise, stature in the HR community and experience, so that they are able to do a thorough evaluation of the applicants.");

		questions
				.add("Can I contact someone if I have any queries related to award categories?");
		answers.add("All award related queries should be mailed to SHRMIAwards@shrm.org and our Awards team will respond to those.");

		questions.add("How do I fill the declaration form?");
		answers.add("You have to tick the category/categories applied for and send us a signed, stamped , scanned copy of the same along with the Application Forms (* The Form needs to be signed by the authorized signatory form the participant organization (MD, CEO, COO, chairman or proprietor)");

		questions
				.add("Should the declaration form include the details of the current year or the previous year as well?");
		answers.add("Only current year.");

		questions
				.add("Should we send a scanned copy of the documents or the original documents?");
		answers.add("Only Scanned/soft copies need to be sent to the Awards mailbox -SHRMIAwards@shrm.org No hard copies to be sent by post.");

		questions.add("When will the shortlisted companies be announced?");
		answers.add("Post the 2 rounds of Jury evaluation and closer to the date of the conference. We will update the applicants shortly on the exact date.");

		questions.add("Who should be present at the award ceremony?");
		answers.add("Senior HR leader from the organization. Multiple representatives can also register for the conference and attend the award ceremony.");

		questions
				.add("Will SHRM take care of travel and accommodation for the winners to be present at the awards function?");
		answers.add("Participating organizations would need to register for the conference to be able to attend the Awards night on 25th September. Winners will have to take care of their own travel and accommodation.");

		questions.add("Does the company size matter?");
		answers.add("No it will not matter. While we have defined two sub categories based on company size, in the event that they need to be segregated into these and compared with similar size companies, we will ensure that there is sufficient participation in those sub categories. Else the comparison will be amongst all applicants in a single category irrespective of size and based on merit");

		questions
				.add("If a company is newly formed and does not have the complete information, can they still apply?");
		answers.add("Yes, as long as the information is not available, they can apply with the information they have as of now. However, if it is available and the company is not able to share it, we will accept the application, but the organization must be aware that they will be evaluated against those who have provided the complete information and have hence given a detailed insight. Hence, please understand that if you are not tracking some of the metrics mentioned then it is fine not to share.");

		questions
				.add("Is it mandatory to fill up the entire form? Can they skip few points which are not relevant to their company?");
		answers.add("Evaluation of the application is based on providing the key metrics to provide the Jury with an overall view of the organization's HR strategy. Therefore, if some information is present and available in these categories, but not shared for the purpose of the Award, then that could lead to incomplete information which may influence the Jury's assessment. All questions are mandatory fields for evaluation from a quantitative perspective/ the more information the company provides, the better picture it gives the Jury, of the initiatives as well as the organization, and makes the application more robust and comprehensive. Hence we would encourage you to fill in the entire form.");

		questions
				.add("What are the criteria's to qualify for an award category? What's the criteria for the award categories");
		answers.add("Please refer to the link below for the same - http://annualindia.shrmindia.org/awards.php#example-one3");

		questions.add("Can anyone else sign on behalf of CEO?");
		answers.add("The Form needs to be signed by the authorized signatory from the participating organization. So if it's not the CEO/ MD / Chairman/ Proprietor then it has to be the COO/CHRO/CFO.");

		questions.add("Is an MNC company eligible for the awards?");
		answers.add("Yes, however please refer to the link below for the same. http://annualindia.shrmindia.org/awards.php#example-one3");

		questions.add("Who will verify the details?");
		answers.add("Our Process Advisors will check the application forms, supporting documents, any background information available about the company and so on, to validate the authenticity of the submission. In case the Jury decides that a Field Visit is required that will also be planned out accordingly. In the event that incorrect data has been submitted, there will be immediate disqualification.");

		questions.add("How will they be notified if they are shortlisted?");
		answers.add("Through email from the SHRM Awards Team");

		questions
				.add("Is there an NDA (Non-Disclosure Agreement) that can be sent across to us prior to sending across our confidential company data?");
		answers.add("Yes, we can send a standard SHRM NDA. Please send the request for the same to SHRMIAwards@shrm.org. If you need us to sign your standard NDA, do send it at this email address, we will have our legal counsel validate it and then revert.\n\nAlso, as per the Award Rules & Regulations, all information provided by the Applicant will be confidential and will be used only for the limited purpose of evaluating the applicant's entry to these Awards.");

		questions
				.add("Do we need to furnish information at the Global level or only for India?");
		answers.add("India level only for Organizations awards that are India specific. For the International award category, please share information basis the country of operation (within Asia but excluding India)");

		questions.add("What format should the submissions be in?");
		answers.add("Word document or pdf");

		questions
				.add("I have few queries with reference to the format. Can you please share any contact number where I could call.");
		answers.add("There is a mailbox which is being checked by the Awards Team on a regular basis. Please send all queries to SHRMIAwards@shrm.org and we will get back to you.");

		questions.add("How do we plan to showcase the Winners?");
		answers.add("Post-event coverage of the HR Awards will include coverage through our media partners for the conference. In addition to that we will be profiling the winners on our website and similar to last year, we will be creating case studies showcasing the best practices of the winners which will be released as a publication.");

		questions.add("Section B of the form is for quantitative information-");
		answers.add("The information provided in this section should only stick to measurable metrics (numbers)");
		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(FAQsActivity.this,
						HomeActivity.class);
				startActivity(intent);

			}
		});
		
		list.setAdapter(new faq_adapter(this));
		
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				
				Intent intent=new Intent(FAQsActivity.this,FaqsDescription.class);
				intent.putExtra("question", questions.get(arg2));
				intent.putExtra("answer", answers.get(arg2));
				startActivity(intent);
			}
		});
		
	}

	class faq_adapter extends BaseAdapter {
		Context context;

		public faq_adapter(Context context) {
			// TODO Auto-generated constructor stub
			this.context = context;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return questions.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			
			LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view=inflater.inflate(R.layout.faq_adapter,arg2,false);
			
			TextView tv=(TextView)view.findViewById(R.id.txt);
			tv.setText(questions.get(arg0));
			return view;
		}
	}

}
