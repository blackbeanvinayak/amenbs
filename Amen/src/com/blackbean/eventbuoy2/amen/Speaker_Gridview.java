package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;
import com.blackbean.utility.DataBase;
import com.blackbean.utility.ImageLoader;
import com.google.analytics.tracking.android.EasyTracker;

public class Speaker_Gridview extends Activity {
	public static ArrayList<String> id;
	public static ArrayList<String> name;
	public static ArrayList<String> designation;
	public static ArrayList<String> description;
	public static ArrayList<String> speakerimage;
	GridView gridview;
	DataBase database;
	ImageView logoImageView;
	String event_category;
	int whichevent;
	private static final String Speaker_Image = "speaker_image";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.speakerlist);

		// right Image Setting code
		 whichevent=getIntent().getIntExtra("WHICH_EVENT", 0);
		if(whichevent==StaticData.Event_Nairobi)
		{
			event_category="kochi";
		}else {
			event_category="MDET";
		}
		/*String imageBlack=getIntent().getStringExtra("ImageName");
		ImageView icon=(ImageView) findViewById(R.id.icon);
		File pictureFile = new File(Environment
				.getExternalStorageDirectory().getPath()
				+ "/sankalp/menu/" + imageBlack);
		if (pictureFile != null) {
			Bitmap bitmap = BitmapFactory.decodeFile(pictureFile
					.getAbsolutePath());
			if(bitmap!=null)
				icon.setImageBitmap(bitmap);
		}*/
		//icon.setColorFilter(Color.argb(255, 0, 0, 0));
		
		
		
		
		

		logoImageView = (ImageView) findViewById(R.id.home);
		logoImageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Speaker_Gridview.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();
			}
		});

		id = new ArrayList<String>();
		name = new ArrayList<String>();
		designation = new ArrayList<String>();
		description = new ArrayList<String>();
		speakerimage = new ArrayList<String>();

		DataBase db = new DataBase(this);
		db.open();
		id.addAll(db.getSpeakerByfieldANDeventcategory("speaker_id",event_category));
		name.addAll(db.getSpeakerByfieldANDeventcategory("speaker_name",event_category));
		designation.addAll(db
				.getSpeakerByfieldANDeventcategory("speaker_designation",event_category));
		description.addAll(db
				.getSpeakerByfieldANDeventcategory("speaker_description",event_category));
		speakerimage.addAll(db.getSpeakerByfieldANDeventcategory("speaker_image",event_category));

		db.close();

		/*
		 * id.add("0"); id.add("1"); id.add("2"); id.add("3"); id.add("4");
		 * id.add("5"); id.add("6"); id.add("7"); id.add("8"); id.add("9");
		 * id.add("10");
		 * 
		 * 
		 * 
		 * name.add("Mr. G. Vishwanathan"); name.add("Prof.Jagdish Sheth");
		 * 
		 * name.add("Mr.Vijay Michihito Batra"); name.add("Prof.Dr.M.J.Xavier");
		 * name.add("Rajiv Dingra"); //name.add("Kulwinder Singh");
		 * 
		 * name.add("Mr.Rob Peck"); name.add("Shyam Sekar S.");
		 * name.add("Alet Viegas"); name.add("Ahmad Aftab Naqvi");
		 * name.add("Sorav Jain"); name.add("Sunny Nagpal");
		 * 
		 * designation.add("Chancellor VIT University");
		 * designation.add("Professor of Marketing Emory University"); ;
		 * designation.add("Motivational Speaker");
		 * designation.add("Former Director IIM (R)");
		 * designation.add("Founder & CEO WATConsult"); //designation.add(
		 * "Director-Global Marketing Communication Synechron Technologies");
		 * 
		 * designation.add(
		 * "Director Client Services,03M Directional Marketing Pvt Ltd");
		 * designation.add("S.Chief Mentor & Strategies,Startup Xperts");
		 * designation.add("Founder Black Bean Engagement");
		 * designation.add("CEO GOZOOP");
		 * designation.add("Thinker in Chief Echovme");
		 * designation.add("Managing Director & Co-Founder Httpool India");
		 * 
		 * description.add(""); description.add(
		 * "Vijay MichihitoBatra is driven by a deep-seated interest in creating awareness of the need to remain positively focused in life through good and positive reportage. This idea gave birth to Think Media Inc. – a creative showcasing of innovations in the business process and bringing Corporate News Updates and giving coverage to what is inspiring and motivating. Through his upcoming (project underway) personalized show Rendez-vous with Vijay Batra , he would like to explore the positive and inspirational thoughts and the"
		 * ); description.add(
		 * "Vijay MichihitoBatra is driven by a deep-seated interest in creating awareness of the need to remain positively focused in life through good and positive reportage. This idea gave birth to Think Media Inc. – a creative showcasing of innovations in the business process and bringing Corporate News Updates and giving coverage to what is inspiring and motivating. Through his upcoming (project underway) personalized show Rendez-vous with Vijay Batra , he would like to explore the positive and inspirational thoughts and the"
		 * ); description.add(
		 * "Vijay MichihitoBatra is driven by a deep-seated interest in creating awareness of the need to remain positively focused in life through good and positive reportage. This idea gave birth to Think Media Inc. – a creative showcasing of innovations in the business process and bringing Corporate News Updates and giving coverage to what is inspiring and motivating. Through his upcoming (project underway) personalized show Rendez-vous with Vijay Batra , he would like to explore the positive and inspirational thoughts and the"
		 * ); description.add(""); //description.add("");
		 * 
		 * description.add(""); description.add(""); description.add("");
		 * description.add(""); description.add(""); description.add("");
		 * 
		 * speakerimage.add(R.drawable.g_vishwanathan);
		 * speakerimage.add(R.drawable.jagadish_seth);
		 * 
		 * speakerimage.add(R.drawable.vijay);
		 * speakerimage.add(R.drawable.mj_xavier);
		 * //speakerimage.add(R.drawable.kulwinder_singh);
		 * speakerimage.add(R.drawable.rajivdingra);
		 * 
		 * speakerimage.add(R.drawable.rob);
		 * speakerimage.add(R.drawable.shyam_sekar_s);
		 * speakerimage.add(R.drawable.alet);
		 * speakerimage.add(R.drawable.ahemed);
		 * speakerimage.add(R.drawable.sorav_ain);
		 * speakerimage.add(R.drawable.sunny_nagpal);
		 */

		gridview = (GridView) findViewById(R.id.grid_view);
		GridAdapter adapter = new GridAdapter(Speaker_Gridview.this, name,
				designation, description, speakerimage);

		gridview.setAdapter(adapter);

		gridview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id1) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Speaker_Gridview.this,
						SpeakerDetailsActivity.class);
				intent.putExtra("_id", Integer.parseInt(id.get(position)));
				intent.putExtra("WHICH_EVENT", whichevent);

				startActivity(intent);
			}
		});

	}

	class GridAdapter extends BaseAdapter {
		Context context;
		ArrayList<String> speaker_id;
		ArrayList<String> name;
		ArrayList<String> designation;
		ArrayList<String> description;
		ArrayList<String> speaker_image;
		 public ImageLoader imageLoader; 

		public GridAdapter(Context context, ArrayList<String> name,
				ArrayList<String> designation, ArrayList<String> description,
				ArrayList<String> speaker_image) {
			// TODO Auto-generated constructor stub
			this.context = context;
			this.name = name;
			this.description = designation;
			this.designation = designation;
			//	this.speaker_id = speaker_id;
			this.speaker_image = speaker_image;
			imageLoader = new ImageLoader(context);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return name.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			View v = inflater.inflate(R.layout.speaker_single_item, parent,
					false);

			ImageView image = (ImageView) v.findViewById(R.id.speaker_image);

			TextView name = (TextView) v.findViewById(R.id.speaker_name);

			// Typeface mFont = Typeface.createFromAsset(getAssets(),
			// "nexa_light.otf");
			name.setText(this.name.get(position));
			// name.setTypeface(mFont);
			TextView detail1 = (TextView) v.findViewById(R.id.speaker_detail1);
			detail1.setText(designation.get(position));
			// detail1.setTypeface(mFont);
			/*File pictureFile = new File(Environment
					.getExternalStorageDirectory().getPath()
					+ "/sankalp/speakers/" + speaker_image.get(position));
			if (pictureFile != null) {
				Bitmap bitmap = BitmapFactory.decodeFile(pictureFile
						.getAbsolutePath());
				image.setImageBitmap(bitmap);
			}*/
			
			imageLoader.DisplayImage("http://amen.eventbuoy.com/admin/uploads/"
					+ speaker_image.get(position).replaceAll(" ", "%20"), image);

			/*ImageStore obj = new ImageStore();

			Bitmap bm = obj.getBitmapFromMemoryCache(speaker_image.get(position));
			
			
			if (bm == null) {
				LoadImage imgLoadImage = new LoadImage(context, image,speaker_image.get(position));
				imgLoadImage.execute("http://sankalp.eventbuoy.com/admin/uploads/"
						+ speaker_image.get(position).replaceAll(" ", "%20"));
				
			}
			else {
				image.setImageBitmap(bm);
				
				System.out.println("Bitmap from cache "+speaker_image.get(position)+"   "+bm);
			}*/

			// image.setImageResource(speaker_image.get(position));

			return v;
		}

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

}
