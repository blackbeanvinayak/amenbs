package com.blackbean.eventbuoy2.amen;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.google.analytics.tracking.android.EasyTracker;

public class ShareActivity extends Activity {
	ImageView image;

	@SuppressWarnings("deprecation")
	Facebook facebook=new Facebook("1376444199346062");
	Button sharebutton;
	ImageView logoImageView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.share);

		image = (ImageView) findViewById(R.id.share_image);
		logoImageView=(ImageView)findViewById(R.id.home);
		logoImageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(ShareActivity.this,HomeActivity.class);
				startActivity(intent);
				finish();
			}
		});
		image.setDrawingCacheEnabled(true);
		image.buildDrawingCache();

		sharebutton = (Button) findViewById(R.id.sharebutton);

		sharebutton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				/*Bitmap bmap = Bitmap.createBitmap(image.getDrawingCache());
				String path = writeToDirectory(bmap);
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("image/*");
				intent.putExtra(Intent.EXTRA_SUBJECT, "Sample Photo");
				intent.putExtra(android.content.Intent.EXTRA_TEXT, "hi");
				intent.putExtra(Intent.EXTRA_STREAM,
						Uri.fromFile(new File(path)));
				intent = Intent.createChooser(intent, "share with ");
				startActivity(intent);*/




				final Dialog dialog = new Dialog(ShareActivity.this);
				dialog.getWindow().setFlags(LayoutParams.FLAG_FULLSCREEN,
						LayoutParams.FLAG_FULLSCREEN);
				dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

				dialog.setContentView(R.layout.share_option);			
				Button share1 = (Button) dialog.findViewById(R.id.fb);
				Button other = (Button) dialog.findViewById(R.id.other);
				dialog.show();
				share1.setOnClickListener(new OnClickListener() {

					@SuppressWarnings("deprecation")
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						Bundle parameters = new Bundle();
						if (HomeActivity.whichEvent == StaticData.Event_Delhi) {
							parameters.putString("description","Bigger than ever! @SankalpForum’s global Summit brings #socent #investors #innovators #DFI #govt  to Delhi #SFGlobal is here, Join us!");
						}else {
							parameters.putString("description","Sankalp Africa Summit 2015 is here. Looking forward to the confluence of Global and African Leaders. An exciting power packed 2 day event awaits!");
						}
						//parameters.putString("picture","http://amen.eventbuoy.com/admin/uploads/sankalp_logo.jpg");
						parameters.putString("link","https://play.google.com/store/apps/details?id=com.blackbean.eventbuoy2.amen");
						parameters.putString("name", "Amen BS");
						parameters.putString("caption", "Amen BS");

						facebook.dialog(ShareActivity.this,
								"feed", parameters, new DialogListener() {

							@Override
							public void onFacebookError(FacebookError arg0) {
								// TODO Auto-generated method stub
								Toast.makeText(ShareActivity.this,
										"Error While Sharing ",
										Toast.LENGTH_LONG).show();
							}

							@Override
							public void onError(DialogError arg0) {
								// TODO Auto-generated method stub
								Toast.makeText(ShareActivity.this,
										"Error While Sharing ",
										Toast.LENGTH_LONG).show();
							}

							@Override
							public void onComplete(Bundle arg0) {
								// TODO Auto-generated method stub
								String string1 = null;
								for (String string : arg0.keySet()) {
									//System.out.println("val "+string);
									string1=string;
								}
								if(string1!=null)
									Toast.makeText(ShareActivity.this,
											"Share Successfully..",
											Toast.LENGTH_LONG).show();
							}

							@Override
							public void onCancel() {
								// TODO Auto-generated method stub
								Toast.makeText(ShareActivity.this,
										"Sharing Canceled By User ",
										Toast.LENGTH_LONG).show();

							}
						});
					}
				});

				other.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						Intent share = new Intent(Intent.ACTION_SEND);
						share.setType("text/plain");

						/*Uri imageUri = Uri.parse("android.resource://com.blackbean.eventbuoy2.shrm/drawable/icon_share");
						share.putExtra(Intent.EXTRA_STREAM, imageUri);*/

						share.putExtra(Intent.EXTRA_EMAIL, new String[]{"admin@amenbs.com"});
						share.putExtra(Intent.EXTRA_SUBJECT, "Amen BS");

						if (HomeActivity.whichEvent == StaticData.Event_Delhi) {
							String text="Hospital Planning and Infrastructure: Challenges and Solutions"+
						"\nhttps://play.google.com/store/apps/details?id=com.blackbean.eventbuoy2.amen";
						//#govt  to Delhi #SFGlobal is here, Join us!
						//"\nhttp://bit.do/sankalp2";
							share.putExtra(Intent.EXTRA_TEXT,text) ;
						}else {
							share.putExtra(Intent.EXTRA_TEXT, "Medical Devices, Equipment and Technology: Connect, Explore and Engage"+
									"\nhttps://play.google.com/store/apps/details?id=com.blackbean.eventbuoy2.amen");	
						}
						startActivity(Intent.createChooser(share, "Share"));
					}
				});

			}

		});

		ImageView menu=(ImageView)findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(ShareActivity.this, MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();
			}
		});
	}

	@SuppressLint("DefaultLocale")
	void share(String nameApp, String imagePath, String message) {
		try {
			List<Intent> targetedShareIntents = new ArrayList<Intent>();
			Intent share = new Intent(android.content.Intent.ACTION_SEND);
			share.setType("image/jpeg");
			List<ResolveInfo> resInfo = getPackageManager()
					.queryIntentActivities(share, 0);
			if (!resInfo.isEmpty()) {
				for (ResolveInfo info : resInfo) {
					Intent targetedShare = new Intent(
							android.content.Intent.ACTION_SEND);
					targetedShare.setType("image/png"); // put here your mime
					// type
					if (info.activityInfo.packageName.toLowerCase().contains(
							nameApp)
							|| info.activityInfo.name.toLowerCase().contains(
									nameApp)) {
						targetedShare.putExtra(Intent.EXTRA_SUBJECT,
								"Sample Photo");
						targetedShare.putExtra(
								android.content.Intent.EXTRA_TEXT, "hi");
						targetedShare.putExtra(Intent.EXTRA_STREAM,
								Uri.fromFile(new File(imagePath)));
						// targetedShare.setPackage(info.activityInfo.packageName);
						targetedShareIntents.add(targetedShare);
						// targetedShare.createChooser(target, title)
					}
				}
				Intent chooserIntent = Intent.createChooser(
						targetedShareIntents.remove(0), "Select app to share");
				chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,
						targetedShareIntents.toArray(new Parcelable[] {}));
				startActivity(chooserIntent);
			}
		}

		catch (Exception e) {
			Log.v("VM",
					"Exception while sending image on" + nameApp + " "
							+ e.getMessage());
		}
	}

	public String writeToDirectory(Bitmap bitmap) {
		String path = Environment.getExternalStorageDirectory()
				+ File.separator + "myApp";
		File f = new File(path);
		boolean dirExists = f.isDirectory();

		if (!f.isDirectory() && !f.exists()) {
			dirExists = f.mkdirs();
		}

		if (dirExists) {
			File file = new File(path, "share_image.png");
			try {
				FileOutputStream out = new FileOutputStream(file);
				bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
				out.close();
			} catch (IOException ex) {
				/*
				 * e(TAG, "Write failed!"); e(TAG, ex.getMessage());
				 */
				ex.printStackTrace();
			}
			return file.toString();
		} else {
			return "";
		}
	}
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

}
