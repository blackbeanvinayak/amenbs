package com.blackbean.eventbuoy2.amen;


import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.blackbean.eventbuoy2.amen.R;
import com.google.analytics.tracking.android.EasyTracker;

public class Twitter_tab1 extends  Activity {
	
	WebView webview;
	ProgressBar progressBar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//getting the progress of the window
				getWindow().requestFeature(Window.FEATURE_PROGRESS);
				//setting the layout for the activity
				 setContentView(R.layout.activity_twitter_tab1);
				
				 //using a webview to open a webpage
				 webview=(WebView)findViewById(R.id.web_allTweets);
				 progressBar=(ProgressBar)findViewById(R.id.progress_allTweets);
				 webview.getSettings().setJavaScriptEnabled(true);
				 webview.setWebViewClient(new myWebClient());		
				 webview.setWebChromeClient(new WebChromeClient() {
					 //manipulating the progress bar
		             public void onProgressChanged(WebView view, int progress) 
		                {
		                if(progress < 100 && progressBar.getVisibility() == ProgressBar.GONE){
		                	progressBar.setVisibility(ProgressBar.VISIBLE);
		                   
		                }
		                progressBar.setProgress(progress);
		                if(progress == 100) {
		                	progressBar.setVisibility(ProgressBar.GONE);
		                   
		                }
		             }
		         });
				 	
				 //loading the URL
			//	 webview.loadUrl("file:///android_asset/all_tweets.html");
				 webview.loadUrl("https://twitter.com/");

	}
	
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	//	FlurryAgent.onStartSession(this, "S8WXXMCCGWVYJ6M7XG8Z");
		//FlurryAgent.onStartSession(this, "C567X8X5BWZ8F3NFGFJ5");
		
		//FlurryAgent.onPageView();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//FlurryAgent.logEvent("Twitter_tab1 screen viewed");
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//FlurryAgent.onEndSession(this);
		EasyTracker.getInstance(this).activityStop(this);
	}


	@Override
	 public boolean onKeyDown(int keyCode, KeyEvent event)	
	 {
	  if ((keyCode == KeyEvent.KEYCODE_BACK) && webview.canGoBack()) {
	   webview.goBack();	
	   return true;
	  }	
	  return super.onKeyDown(keyCode, event);
	}
	
	 public class myWebClient extends WebViewClient
	  
     { 
      @Override 
      public void onPageStarted(WebView view, String url, Bitmap favicon) {
       super.onPageStarted(view, url, favicon); 
      }
  
      @Override
      public boolean shouldOverrideUrlLoading(WebView view, String url) {
       view.loadUrl(url); 
       return true;
      }
 
      @Override
      public void onPageFinished(WebView view, String url) {
      super.onPageFinished(view, url);
       progressBar.setVisibility(View.GONE);
 
      }
 
     }
	

}
