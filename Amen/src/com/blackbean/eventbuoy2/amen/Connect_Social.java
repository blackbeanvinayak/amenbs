package com.blackbean.eventbuoy2.amen;

import com.blackbean.eventbuoy2.amen.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

@SuppressLint("CutPasteId")
public class Connect_Social extends Activity {
	WebView web;
	ProgressBar progressBar;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.connect_social);

		/*TextView header_txt=(TextView) findViewById(R.id.header_txt);
		String text= getIntent().getExtras().getString("headerText");
		header_txt.setText(text);*/
		
		String Url = getIntent().getExtras().getString("url");
		String social = getIntent().getExtras().getString("social");

		TextView txt = (TextView) findViewById(R.id.header_txt);
		progressBar=(ProgressBar)findViewById(R.id.progress);
		txt.setText(social);
		web = (WebView) findViewById(R.id.web);
		web.setWebViewClient(new MyBrowser());
		web.getSettings().setLoadsImagesAutomatically(true);
		web.getSettings().setJavaScriptEnabled(true);
		web.setWebChromeClient(new WebChromeClient() {
			 //manipulating the progress bar
            public void onProgressChanged(WebView view, int progress) 
               {
               if(progress < 100 && progressBar.getVisibility() == ProgressBar.GONE){
               	progressBar.setVisibility(ProgressBar.VISIBLE);
                  
               }
               progressBar.setProgress(progress);
               if(progress == 100) {
               	progressBar.setVisibility(ProgressBar.GONE);
                  
               }
            }
        });
		web.loadUrl(Url);
		// web.l
		 ImageView menu = (ImageView) findViewById(R.id.home);
			menu.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(Connect_Social.this,
							MainScreen.class);
					startActivity(intent);
					HomeActivity.homeActivity.finish();
					finish();

				}
			});

	}

	private class MyBrowser extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}

}
