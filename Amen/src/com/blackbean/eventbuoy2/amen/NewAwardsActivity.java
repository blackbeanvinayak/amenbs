package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;

import com.blackbean.adapters.PartnerAdapter;
import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

public class NewAwardsActivity extends Activity{

	LinearLayout infoLayout,top10Layout;
	Button buttonInfo,buttonFinalist,buttonJury;
	ListView listTop10;
	ArrayList<String> arrayList;
	String val;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_awards);


		ImageView menu = (ImageView) findViewById(R.id.logo);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(NewAwardsActivity.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();
			}
		});

		infoLayout=(LinearLayout) findViewById(R.id.infoLayout);
		top10Layout = (LinearLayout) findViewById(R.id.top10Layout);
		buttonInfo = (Button) findViewById(R.id.buttonInfo);
		buttonFinalist = (Button) findViewById(R.id.buttonFinalist);
		buttonJury = (Button) findViewById(R.id.buttonJury);
		listTop10 = (ListView) findViewById(R.id.listTop10);
		if (HomeActivity.whichEvent == StaticData.Event_Delhi) {
			buttonFinalist.setText("Finalists");
		}


		val=getIntent().getStringExtra("val");
		if(val != null)
		{
			if (val.equals("info")) {
				infoLayout.setVisibility(View.VISIBLE);
				top10Layout.setVisibility(View.GONE);
				buttonFinalist.setBackgroundResource(android.R.color.transparent);
				buttonInfo.setBackgroundResource(R.drawable.color_cover_left);
			}else if (val.equals("finalist")) {
				infoLayout.setVisibility(View.GONE);
				top10Layout.setVisibility(View.VISIBLE);
				buttonFinalist.setBackgroundResource(R.drawable.color_cover_meddle);
				buttonInfo.setBackgroundResource(android.R.color.transparent);
			}
		}



		buttonInfo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				infoLayout.setVisibility(View.VISIBLE);
				top10Layout.setVisibility(View.GONE);
				buttonFinalist.setBackgroundResource(android.R.color.transparent);
				buttonInfo.setBackgroundResource(R.drawable.color_cover_left);
			}
		});
		buttonFinalist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				infoLayout.setVisibility(View.GONE);
				top10Layout.setVisibility(View.VISIBLE);
				buttonFinalist.setBackgroundResource(R.drawable.color_cover_meddle);
				buttonInfo.setBackgroundResource(android.R.color.transparent);
			}
		});
		buttonJury.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(NewAwardsActivity.this, Jury_Gridview.class);
				startActivity(intent);
				finish();
			}
		});

		arrayList = new ArrayList<String>();


		if(HomeActivity.whichEvent == StaticData.Event_Nairobi)
		{
			arrayList.add("Arbutus Medical");
			arrayList.add("EA Fruits Farm and Company");
			arrayList.add("Forestry and Agriculture Investment Management (FAIM)");
			arrayList.add("Ensibuuko");
			arrayList.add("Geel Medical Services");
			arrayList.add("Impact Africa Industries");
			arrayList.add("Jibu");
			arrayList.add("Kigali Farms");
			arrayList.add("PowerGen Renewable Energy");
			arrayList.add("Honey Products Industries Limited");
		}else if (HomeActivity.whichEvent == StaticData.Event_Delhi) {
			arrayList.add("Brun Healthcare");
			arrayList.add("Drinkwell Systems");
			arrayList.add("Eco Corner");
			arrayList.add("Ekam Eco Solutions");
			arrayList.add("Guru-G Learning Labs");
			arrayList.add("I Say Organic");
			arrayList.add("Janta Meals");
			arrayList.add("Krishi Star");
			arrayList.add("Medula Healthcare");
			arrayList.add("ReMaterials");
			arrayList.add("Sampurnearth");
			arrayList.add("Swami Samarth Electronics");
			arrayList.add("The Better India");
		}
		listTop10.setAdapter(new PartnerAdapter(NewAwardsActivity.this, arrayList));

		listTop10.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(NewAwardsActivity.this, Connect_Social.class);
				if(HomeActivity.whichEvent == StaticData.Event_Nairobi)
				{
					intent.putExtra("social","Top 10 Finalists");
					switch (arg2) {
					case 0: intent.putExtra("url", "file:///android_asset/f1.html");
					break;
					case 1: intent.putExtra("url", "file:///android_asset/f2.html");
					break;
					case 2: intent.putExtra("url", "file:///android_asset/f3.html");
					break;
					case 3: intent.putExtra("url", "file:///android_asset/f4.html");
					break;
					case 4: intent.putExtra("url", "file:///android_asset/f5.html");
					break;
					case 5: intent.putExtra("url", "file:///android_asset/f6.html");
					break;
					case 6: intent.putExtra("url", "file:///android_asset/f7.html");
					break;
					case 7: intent.putExtra("url", "file:///android_asset/f8.html");
					break;
					case 8: intent.putExtra("url", "file:///android_asset/f9.html");
					break;
					case 9: intent.putExtra("url", "file:///android_asset/f10.html");
					break;
					}
				}else if (HomeActivity.whichEvent == StaticData.Event_Delhi) {
					intent.putExtra("social","Finalists");
					switch (arg2) {
					case 0: intent.putExtra("url", "file:///android_asset/fa1.html");
					break;
					case 1: intent.putExtra("url", "file:///android_asset/fa2.html");
					break;
					case 2: intent.putExtra("url", "file:///android_asset/fa3.html");
					break;
					case 3: intent.putExtra("url", "file:///android_asset/fa4.html");
					break;
					case 4: intent.putExtra("url", "file:///android_asset/fa5.html");
					break;
					case 5: intent.putExtra("url", "file:///android_asset/fa6.html");
					break;
					case 6: intent.putExtra("url", "file:///android_asset/fa7.html");
					break;
					case 7: intent.putExtra("url", "file:///android_asset/fa8.html");
					break;
					case 8: intent.putExtra("url", "file:///android_asset/fa9.html");
					break;
					case 9: intent.putExtra("url", "file:///android_asset/fa10.html");
					break;
					case 10: intent.putExtra("url", "file:///android_asset/fa11.html");
					break;
					case 11: intent.putExtra("url", "file:///android_asset/fa12.html");
					break;
					case 12: intent.putExtra("url", "file:///android_asset/fa13.html");
					break;
					}
				}
				startActivity(intent);
			}

		});

	}
}
