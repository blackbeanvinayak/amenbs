package com.blackbean.adapters;
/**
 * @author blackbeanumesh
 *
 *This class holds the row of a listview which is display Notes
 */
public class NoteModel {

	String note_info;
	String note_content;
	String note_id;
	String note_palceId;
	String note_place;

	public NoteModel(String note_id,String note_info,String note_content,String note_palceId,String note_place) {
		// TODO Auto-generated constructor stub
		this.note_content=note_content;
		this.note_info=note_info;
		this.note_id=note_id;
		this.note_palceId=note_palceId;
		this.note_place=note_place;
	}


	// Get methods

	public String getNote_content() {
		return note_content;
	}
	public String getNote_info() {
		return note_info;
	}
	public String getNote_id() {
		return note_id;
	}
	public String getNote_palceId() {
		return note_palceId;
	}
	public String getNote_place() {
		return note_place;
	}


	///set Methods

	public void setNote_content(String note_content) {
		this.note_content = note_content;
	}

	public void setNote_info(String note_info) {
		this.note_info = note_info;
	}
	public void setNote_id(String note_id) {
		this.note_id = note_id;
	}
	public void setNote_palceId(String note_palceId) {
		this.note_palceId = note_palceId;
	}
	public void setNote_place(String note_place) {
		this.note_place = note_place;
	}

}
