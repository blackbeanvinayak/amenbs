
package com.blackbean.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;


public class ExpandableListSpecialAdapter extends BaseExpandableListAdapter  {

	private Context _context;
	private List<String> _listDataHeader; // header titles
	private HashMap<String, List<String>> _listDataChild;
	int id;
	 ArrayList<String> info;
     ArrayList<String> agenda_id;
     public ExpandableListSpecialAdapter(Context context, List<String> listDataHeader,
             HashMap<String, List<String>> listChildData) {
    	 
     this._context = context;
     this._listDataHeader = listDataHeader;
     this._listDataChild = listChildData;
   
   
}

	public Object getChild(int groupPosition, int childPosititon) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.get(childPosititon);
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	public View getChildView(int groupPosition,  final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
            
                        LayoutInflater infalInflater = (LayoutInflater) this._context
                                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        convertView = infalInflater.inflate(R.layout.list_item, null);
                
                TextView txtListChild = (TextView) convertView
                                .findViewById(R.id.lblListItem);
                
               txtListChild.setTextColor(Color.BLACK);
        
        /*	  txtListChild.setText(_listDataChild.get("Outstanding Digital Person of the year").get(childPosition));
        	  txtListChild.setText(_listDataChild.get("Outstanding Digital Start-up of the year").get(childPosition));
        	  txtListChild.setText(_listDataChild.get("Outstanding Digital Agency of the Year").get(childPosition));
        	  txtListChild.setText(_listDataChild.get("Outstanding Social Media Campaign of the Year").get(childPosition));

       */ 	  String headerTitle = (String) getGroup(groupPosition);
             
        	   if(headerTitle.equalsIgnoreCase("Outstanding Digital Person of the year"))
               {
       		   txtListChild.setText(_listDataChild.get("Outstanding Digital Person of the year").get(childPosition));
               }
       	   if(headerTitle.equalsIgnoreCase("Outstanding Digital Start-up of the year"))
              {
      		   txtListChild.setText(_listDataChild.get("Outstanding Digital Start-up of the year").get(childPosition));
              }
       	   if(headerTitle.equalsIgnoreCase("Outstanding Digital Agency of the Year"))
              {
      		   txtListChild.setText(_listDataChild.get("Outstanding Digital Agency of the Year").get(childPosition));
              }     
       	  if(headerTitle.equalsIgnoreCase("Outstanding Social Media Campaign of the Year"))
          {
  		   txtListChild.setText(_listDataChild.get("Outstanding Social Media Campaign of the Year").get(childPosition));
          }
                      
             
                return convertView;
		
		
	}

	public int getChildrenCount(int groupPosition) {
		return this._listDataChild.get(this._listDataHeader.get(groupPosition))
				.size();
	}

	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String headerTitle = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.list_group, null);
		}

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.lblListHeader);
		//lblListHeader.setTypeface(null, Typeface.ITALIC);
		lblListHeader.setText(headerTitle);
	
		return convertView;
	}

	public boolean hasStableIds() {
		return false;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}


}
