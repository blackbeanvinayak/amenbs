package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;

import com.blackbean.eventbuoy2.amen.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class WinnerActivity extends Activity
{
	ListView list;
	ArrayList<String> winArray,categoryArray;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.winner);
		winArray=new ArrayList<String>();
		categoryArray=new ArrayList<String>();
		winArray.add("Diversity and Inclusion");
		winArray.add("Social Media People Campaign");
		winArray.add("Workplace Flexibility");
		winArray.add("Community Impact");
		winArray.add("Developing Leaders ofTomorrow");
		winArray.add("Employee Health and Wellness Initiatives");
		winArray.add("CEO of the Year");
		winArray.add("Academic Institute of the Year");
		
		categoryArray.add("Aegis Global Runners up: Capgemini");
		categoryArray.add("Certificate of Special Recognition: Capgemini (Social media)");
		categoryArray.add("Arbitron Runners up: CA Technologies");
		categoryArray.add("Cognizant Runners up: Mahindra Finance");
		categoryArray.add("Pepsico Runners up: Genpact, Aegis Global");
		categoryArray.add("SCOPE International Runners up: SAP Labs, Reliance Industries Ltd");
		categoryArray.add("Shakti Sagar Runners up: Sears, Alok Kumar");
		categoryArray.add("School of Inspired Leadership");
		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(WinnerActivity.this,
						HomeActivity.class);
				startActivity(intent);

			}
		});
		
		
		list=(ListView)findViewById(R.id.winnerlist);
		list.setAdapter(new Winner_adapter(this, winArray,categoryArray));
		
	}
	
	
	class Winner_adapter extends BaseAdapter
	{
		Context context;
		ArrayList<String> category=new ArrayList<String>();
		ArrayList<String> win=new ArrayList<String>();

		public Winner_adapter(Context context,ArrayList<String> win,ArrayList<String> category) {
			// TODO Auto-generated constructor stub
			this.context=context;
			this.category=category;
			this.win=win;
		}
		
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return win.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView( int arg0, View arg1, ViewGroup arg2) {
			// TODO Auto-generated method stub
			LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view=inflater.inflate(R.layout.winner_adapter,null);
			
			TextView category_text=(TextView)view.findViewById(R.id.categorytext);
			category_text.setText("Category : \n"+this.category.get(arg0));
			
			TextView win_text=(TextView)view.findViewById(R.id.winnertext);
			win_text.setText("Winner : \n"+this.win.get(arg0));
			
			return view;
		}
		
	}

}
