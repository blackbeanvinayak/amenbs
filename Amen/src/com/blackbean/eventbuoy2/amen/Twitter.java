package com.blackbean.eventbuoy2.amen;



import android.app.LocalActivityManager;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;
import com.google.analytics.tracking.android.EasyTracker;

public class Twitter extends MainActivity{
	LocalActivityManager mlam ;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	//	getWindow().requestFeature(Window.FEATURE_PROGRESS);
		setContentView(R.layout.activity_twitter);
		
	
	//	_DrawerList.setAdapter(new ArrayAdapter<String>(this,
			//	R.layout.drawer_list_item, _Titles));
		
				
			//	 setContentView(R.layout.activity_twitter);
				 mlam = new LocalActivityManager(this, false);
				 TabHost host = (TabHost) findViewById(android.R.id.tabhost);
				 mlam.dispatchCreate(savedInstanceState);
				host.setup(mlam);
				 
				 TabHost.TabSpec spec1;
				 Intent AllTweets=new Intent(this,Twitter_tab1.class);	 	
				 spec1=host.newTabSpec("One")
				 .setIndicator("All Tweets")
				 .setContent(AllTweets);		 
				 host.addTab(spec1);
				 
				 
				 TabHost.TabSpec spec2;
				 Intent HostTweets =new Intent(this,Twitter_tab2.class)	;	 	
				 spec2=host.newTabSpec("Two")
				 .setIndicator("Host Tweets")
				 .setContent(HostTweets);		 
				 host.addTab(spec2);
		//----------------------------------------------------------
				 for(int i=0;i<host.getTabWidget().getChildCount();i++) 
			        { 
			            TextView tv = (TextView) host.getTabWidget().getChildAt(i).findViewById(android.R.id.title); //Unselected Tabs
			            tv.setTextColor(Color.BLACK);
			            //host.getTabWidget().getChildAt(i).setBackgroundResource(R.drawable.tab_background);
			            host.getTabWidget().getChildAt(i).setBackgroundColor(Color.parseColor("#3AB6E0"));
					    
			        } 
				 
				 ImageView menu = (ImageView) findViewById(R.id.home);
					menu.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							Intent intent = new Intent(Twitter.this,
									MainScreen.class);
							startActivity(intent);
							HomeActivity.homeActivity.finish();
							finish();
						}
					});

	}
	
	
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		//------------- Flurry Analytics-----------
		super.onStart();
	//	FlurryAgent.onStartSession(this, "S8WXXMCCGWVYJ6M7XG8Z");
		//FlurryAgent.onStartSession(this, "C567X8X5BWZ8F3NFGFJ5");
		
		//FlurryAgent.onPageView();
		//---------------------------------------------------------
		EasyTracker.getInstance(this).activityStart(this);
	}



	@Override
	protected void onStop() {
		super.onStop();
		//FlurryAgent.onEndSession(this);
		EasyTracker.getInstance(this).activityStop(this);
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//Intent i = new Intent(Twitter.this, MainActivity.class);
		//startActivity(i);
	}

	
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		
		super.onPause();
		 mlam.dispatchPause(this.isFinishing());
	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		 
		super.onResume();
		mlam.dispatchResume();
		//FlurryAgent.logEvent("Twitter screen viewed");
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
//		boolean drawerOpen = _DrawerLayout.isDrawerOpen(_DrawerList);
//		menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}
	



}
