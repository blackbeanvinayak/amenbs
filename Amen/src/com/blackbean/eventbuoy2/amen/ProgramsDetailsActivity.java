package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blackbean.adapters.ExpandableListAdapter;
import com.blackbean.adapters.NoteAdapter1;
import com.blackbean.adapters.NoteModel;
import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;
import com.blackbean.utility.DataBase;
import com.blackbean.utility.ImageLoader;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;
import com.google.analytics.tracking.android.EasyTracker;

public class ProgramsDetailsActivity extends FragmentActivity {
	ProgramsInfoAdapter programsInfoAdapter;
	static ViewPager mViewPager;
	int _id;
	static Context context;
	static String Vfb = null;
	static String Vtw;
	static String Vgoogle;
	static String Vlinked;

	private static final String Agenda_NAME = "agenda_name";
	static ExpandableListAdapter listAdapter;
	static ExpandableListView expListView;
	static List<String> listDataHeader;
	static HashMap<String, List<String>> listDataChild;
	static HashMap<String, List<String>> listDataChildsocial;
	ImageView previous;
	ImageView next;
	static boolean Vcheckspeaker_id = false;
	static int pagerCount, currentpage = 0;
	int last;
	public static ArrayList<String> program_id;
	public static ArrayList<String> namearray;
	public static ArrayList<String> venue;
	public static ArrayList<String> infoDes;
	public static ArrayList<String> link;
	public static ArrayList<String> date;
	public static ArrayList<String> programimage;

	public static ArrayList<String> sessionsArrayList;
	private static final String Speaker_Image = "speaker_image";

	public ImageLoader imageLoader;
	String event_category;
	
	@Override
	protected void onCreate(Bundle arg0) {
		// TODO Auto-generated method stub
		super.onCreate(arg0);
		setContentView(R.layout.programs_details_activity);
		RelativeLayout layout = (RelativeLayout) findViewById(R.id.ralativelayout);
		layout.setBackgroundColor(Color.WHITE);

		//DataBase database = new DataBase(this);
		//database.open();
		
		if(HomeActivity.whichEvent==StaticData.Event_Nairobi)
		{
			event_category="Nairobi";
		}else {
			event_category="Delhi";
		}
		
		
		program_id = new ArrayList<String>();

		namearray = new ArrayList<String>();
		venue = new ArrayList<String>();
		link = new ArrayList<String>();
		date = new ArrayList<String>();
		infoDes = new ArrayList<String>();
		programimage = new ArrayList<String>();

		DataBase db = new DataBase(this);
		db.open();
		program_id.addAll(db.getprogramsByfieldAndEventCategory("program_id",event_category));
		namearray.addAll(db.getprogramsByfieldAndEventCategory("program_name",event_category));
		venue.addAll(db.getprogramsByfieldAndEventCategory("program_venue",event_category));
		infoDes.addAll(db.getprogramsByfieldAndEventCategory("program_info",event_category));
		link.addAll(db.getprogramsByfieldAndEventCategory("program_link",event_category));//
		date.addAll(db.getprogramsByfieldAndEventCategory("program_date",event_category));//
		programimage.addAll(db.getprogramsByfieldAndEventCategory("program_image",event_category));

		db.close();

	
		_id = getIntent().getExtras().getInt("_id");
		context = ProgramsDetailsActivity.this;
		programsInfoAdapter = new ProgramsInfoAdapter(
				getSupportFragmentManager(), context);
		mViewPager = (ViewPager) findViewById(R.id.pager);
		pagerCount = programsInfoAdapter.getCount();
		mViewPager.setAdapter(programsInfoAdapter);
		pagerCount = getCount();
		previous = (ImageView) findViewById(R.id.previous);
		next = (ImageView) findViewById(R.id.next);
		if(program_id.contains(_id+""))
		{
			mViewPager.setCurrentItem(program_id.indexOf(_id+""));// ****************
		}
		if (mViewPager.getCurrentItem() == 0) {
			previous.setVisibility(View.INVISIBLE);
		}
		if (mViewPager.getCurrentItem() == (pagerCount - 1)) {
			next.setVisibility(View.INVISIBLE);
		}
		ImageView menu = (ImageView) findViewById(R.id.home);
		//menu.setColorFilter(Color.argb(255, 0, 0, 0));
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ProgramsDetailsActivity.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();

			}
		});
		final ImageView shareSHRM=(ImageView) findViewById(R.id.shareSHRM);
		/*shareSHRM.setOnClickListener(new OnClickListener() {

			@SuppressLint("InlinedApi")
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				LayoutInflater layoutInflater  = (LayoutInflater)getBaseContext()
						.getSystemService(LAYOUT_INFLATER_SERVICE); 
				View popupView = layoutInflater.inflate(R.layout.popup_speaker, null); 
				final PopupWindow popupWindow=new PopupWindow(popupView,LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);

				ImageView img_facebook=(ImageView) popupView.findViewById(R.id.img_facebook);
				ImageView img_twitter=(ImageView) popupView.findViewById(R.id.img_twitter);
				ImageView img_mail=(ImageView) popupView.findViewById(R.id.img_mail);

				img_facebook.setOnClickListener(new OnClickListener() {

					@SuppressWarnings("deprecation")
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String url;
						DataBase db=new DataBase(context);
						db.open();
						
						url = db.getSpeakeSocialUrl(mViewPager.getCurrentItem(), "speaker_fb");
						
						System.out.println("SPurl "+url);
						System.out.println("Spid "+mViewPager.getCurrentItem());
						db.close();
						
						Intent intent=new Intent(context, SocialActivity.class);
						intent.putExtra("URL", url);
						startActivity(intent);
						//finish();
						popupWindow.dismiss();	
					}
				});
				
				img_twitter.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String url;
						DataBase db=new DataBase(context);
						db.open();
						
						url = db.getSpeakeSocialUrl(_id, "speaker_twiter");
						System.out.println("SPurl "+url);
						System.out.println("Spid "+_id);
						db.close();
						
						Intent intent=new Intent(context, SocialActivity.class);
						intent.putExtra("URL", url);
						startActivity(intent);
						//finish();
						
						popupWindow.dismiss();	
						
					}
				});
				
				img_mail.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String url;
						DataBase db=new DataBase(context);
						db.open();
						
						url = db.getSpeakeSocialUrl(_id, "speaker_google");
						db.close();
						
						
						Intent intent=new Intent(context, SocialActivity.class);
						intent.putExtra("URL", url);
						startActivity(intent);
						//finish();
							
						popupWindow.dismiss();	
						
					}
				});
				popupWindow.setBackgroundDrawable(new BitmapDrawable());
				popupWindow.setOutsideTouchable(true);
				popupWindow.showAsDropDown(shareSHRM, 50, -20);
			}
		});*/
		
		
		previous.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1);

			}
		});

		next.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);

			}
		});

		last = mViewPager.getCurrentItem();

		mViewPager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				// TODO Auto-generated method stub

				if (arg0 == 0) {
					previous.setVisibility(View.INVISIBLE);
					next.setVisibility(View.VISIBLE);

				}
				if (arg0 > 0) {
					previous.setVisibility(View.VISIBLE);
				}

				if ((pagerCount - 1) == arg0) {
					previous.setVisibility(View.VISIBLE);
					next.setVisibility(View.INVISIBLE);
				}

				if (arg0 < pagerCount - 1) {
					next.setVisibility(View.VISIBLE);
				}

				currentpage = arg0;

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
				// TODO Auto-generated method stub

			}
		});

	}

	public static class ProgramsInfoAdapter extends FragmentStatePagerAdapter {

		Context context;

		public ProgramsInfoAdapter(FragmentManager fm, Context context) {
			super(fm);
			this.context = context;
			// TODO Auto-generated constructor stub
		}

		@Override
		public Fragment getItem(int i) {
			// TODO Auto-generated method stub
			Fragment fragment = new DemoObjectFragment();
			Bundle args = new Bundle();

			// args.putInt(DemoObjectFragment.ARG_OBJECT, i + 1); // Our object
			// is just an integer :-P

			args.putString("speaker_id", "speaker" + i);

			args.putInt("id", i);
			args.putInt("spkrid", Integer.parseInt(program_id.get(i)));

			fragment.setArguments(args);
			return fragment;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub

			return namearray.size();

		}

	}

	public int getCount() {
		return programsInfoAdapter.getCount();
	}

	public static class DemoObjectFragment extends Fragment {
		public int id;
		int spkr_id;
		String name;
		String detail1;
		String detail2;
		String image_url;

		ImageView spkr_image_speaker_info;
		public static String info;
		public static String sessions;
		public static String social;

		
		ListView listView;
		


		public static final String ARG_OBJECT = "object";
		Bitmap bmp;

		ImageView imgFacebook,imgTwitter,imgMessage;
		ImageLoader imageLoader;

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.adapter_program_details,
					container, false);
			Bundle get_info = getArguments();
			//NotesArrayList=new ArrayList<NoteModel>();
			imageLoader = new  ImageLoader(context);

			id = get_info.getInt("id");
			spkr_id=get_info.getInt("spkrid");
			final TextView program_info = (TextView) rootView.findViewById(R.id.txt_program_info);
			final TextView program_name = (TextView) rootView.findViewById(R.id.txt_program_name);
			final TextView program_venue = (TextView) rootView.findViewById(R.id.txt_program_venue);
			final TextView program_link = (TextView) rootView.findViewById(R.id.txt_program_link);
			final TextView program_date = (TextView) rootView.findViewById(R.id.txt_program_date);

			/*listView=(ListView) rootView
					.findViewById(R.id.list_note);*/

			imgFacebook=(ImageView) rootView.findViewById(R.id.imgFacebook);
			imgTwitter=(ImageView) rootView.findViewById(R.id.imgTwitter);
			imgMessage=(ImageView) rootView.findViewById(R.id.imgMessage);

			DataBase database=new DataBase(context);
			database.open();
			
			/*final Button btn_info = (Button) rootView
					.findViewById(R.id.btn_info);
			final Button btn_session = (Button) rootView
					.findViewById(R.id.btn_session); 
			final Button btn_notes = (Button) rootView
					.findViewById(R.id.btn_notes);
			Button btn_save = (Button) rootView
					.findViewById(R.id.save_btn);
			Button btn_Add = (Button) rootView
					.findViewById(R.id.add_btn);*/

			// RelativeLayout layout_info,layout_speaker,layout_notes;
			final RelativeLayout layout_info = (RelativeLayout) rootView
					.findViewById(R.id.layout_info);
			final RelativeLayout layout_speaker = (RelativeLayout) rootView
					.findViewById(R.id.layout_speaker);
			final RelativeLayout layout_notes = (RelativeLayout) rootView
					.findViewById(R.id.layout_notes);
			final RelativeLayout layout_notesList = (RelativeLayout) rootView
					.findViewById(R.id.layout_notesList);

			/*DataBase database = new DataBase(getActivity());
			database.open();*/
			ArrayList<String> sessions = new ArrayList<String>();
			ArrayList<String> sessionid=new ArrayList<String>();
			sessions.addAll(database.getSessionBySpeakerId(spkr_id + ""));
			sessionid.addAll(database.getSessionIdBySpeakerId(spkr_id+""));

			final EditText edittext = (EditText) rootView
					.findViewById(R.id.edit_note);

			edittext.setText(database.getNoteBySpeakerId(spkr_id));

			database.close();

			ListView agendalist = (ListView) rootView
					.findViewById(R.id.agenda_list);
			//agendalist.setAdapter(new ArrayAdapter<String>(context,
			//		android.R.layout.simple_expandable_list_item_1, sessions));
			//ShowAgenda ag=new ShowAgenda(context, sessions);


			spkr_image_speaker_info = (ImageView) rootView
					.findViewById(R.id.img_spkr);

			program_name.setText(namearray.get(id));
			// spkr_image_speaker_info.setImageResource(speakerimage.get(id));
			program_venue.setText(venue.get(id));
			program_info.setText(infoDes.get(id));
			program_link.setText(link.get(id));
			program_date.setText(date.get(id));
			
			
			program_link.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(context, ProgramLink.class);
					intent.putExtra("URL", program_link.getText().toString());
					startActivity(intent);
				}
			});

		/*	File pictureFile = new File(Environment
					.getExternalStorageDirectory().getPath()
					+ "/sankalp/speakers/" + speakerimage.get(id));
			if (pictureFile != null) {
				Bitmap bitmap = BitmapFactory.decodeFile(pictureFile
						.getAbsolutePath());
				spkr_image_speaker_info.setImageBitmap(bitmap);
			}*/
			
			imageLoader.DisplayImage("http://sankalp.eventbuoy.com/admin/uploads/"
					+ programimage.get(id).replaceAll(" ", "%20"), spkr_image_speaker_info);

			agendalist.setAdapter(new ShowAgenda(getActivity(), sessions,sessionid));


			agendalist.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) 
				{
					// TODO Auto-generated method stub
				}
			});


/*			btn_info.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					arg0.setBackgroundResource(R.drawable.color_cover_left);
					btn_notes
					.setBackgroundResource(android.R.color.transparent);
					btn_session
					.setBackgroundResource(android.R.color.transparent);
					layout_notes.setVisibility(View.INVISIBLE);
					layout_notesList.setVisibility(View.INVISIBLE);
					layout_speaker.setVisibility(View.INVISIBLE);
					layout_info.setVisibility(View.VISIBLE);

				}
			});

			btn_notes.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					arg0.setBackgroundResource(R.drawable.color_cover_right);
					btn_info.setBackgroundResource(android.R.color.transparent);
					btn_session
					.setBackgroundResource(android.R.color.transparent);
					layout_notes.setVisibility(View.INVISIBLE);
					layout_notesList.setVisibility(View.VISIBLE);
					layout_speaker.setVisibility(View.INVISIBLE);
					layout_info.setVisibility(View.INVISIBLE);

				}
			});
			btn_session.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					arg0.setBackgroundResource(R.drawable.color_cover_right);
					btn_notes
					.setBackgroundResource(android.R.color.transparent);
					btn_info.setBackgroundResource(android.R.color.transparent);
					layout_notes.setVisibility(View.INVISIBLE);
					layout_notesList.setVisibility(View.INVISIBLE);
					layout_speaker.setVisibility(View.VISIBLE);
					layout_info.setVisibility(View.INVISIBLE);

				}
			});



			btn_save.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub 

					DataBase database=new DataBase(getActivity());
					database.open();
					//database.addSpeakerNote(Integer.parseInt(speaker_id.get(mViewPager.getCurrentItem())), edittext.getText().toString().trim());

					database.addNote1(spkr_name.getText().toString(), "SPEAKER", Integer.parseInt(speaker_id.get(mViewPager.getCurrentItem())), edittext.getText().toString().trim());
					database.close();
					Toast.makeText(context, "Note Saved !", Toast.LENGTH_SHORT).show();
					 
					DataBase database=new DataBase(context);
					database.open();

					if(note_id!=-1)
					{
						database.UpdateNote1(note_id, edittext.getText().toString().trim(), "SPEAKER",
								Integer.valueOf(speaker_id.get(mViewPager.getCurrentItem())), 
								spkr_name.getText().toString());
						Toast.makeText(context, "Note Updated ", Toast.LENGTH_LONG).show();
						//Toast.makeText(context, "a " +a, Toast.LENGTH_LONG).show();
					}else {
						database.addNote1(edittext.getText().toString().trim(), "SPEAKER",
								Integer.valueOf(speaker_id.get(mViewPager.getCurrentItem())), 
								spkr_name.getText().toString());

						Toast.makeText(context, "Note Saved ", Toast.LENGTH_LONG).show();

						Toast.makeText(context, "a " +b, Toast.LENGTH_LONG).show();
						System.out.println("......"+edittext.getText().toString().trim());
						System.out.println("......"+Integer.valueOf(speaker_id.get(mViewPager.getCurrentItem())));
						System.out.println("......"+spkr_name.getText().toString());


					}
					database.close();


					NotesArrayList.clear();
					database.open();
					NotesArrayList=database.getNoteByPlaceId("SPEAKER", Integer.valueOf(speaker_id.get(mViewPager.getCurrentItem())));
					adapterNote=new NoteAdapter1(context, NotesArrayList);
					listView.setAdapter(adapterNote);
					for (int i = 0; i < NotesArrayList.size(); i++) {

						System.out.println("Id   "+NotesArrayList.get(i).getNote_id());
						System.out.println("Content   "+NotesArrayList.get(i).getNote_content());
						System.out.println("Info   "+NotesArrayList.get(i).getNote_info());
						System.out.println("Place   "+NotesArrayList.get(i).getNote_place());
						System.out.println("PlaceId   "+NotesArrayList.get(i).getNote_palceId());
					}			
					database.close();
					edittext.setText("");
					layout_notes.setVisibility(View.INVISIBLE);
					layout_info.setVisibility(View.INVISIBLE);
					layout_speaker.setVisibility(View.INVISIBLE);
					layout_notesList.setVisibility(View.VISIBLE);

				}
			});

			btn_Add.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					DataBase database=new DataBase(context);
					database.open();

					database.addNote1(edittext.getText().toString().trim(), "SPEAKER",
							Integer.valueOf(speaker_id.get(mViewPager.getCurrentItem())), 
							spkr_name.getText().toString());
					//note_id=-1;
					NotesArrayList.clear();
					NotesArrayList=database.getNoteByPlaceId("SPEAKER", Integer.valueOf(speaker_id.get(mViewPager.getCurrentItem())));
					adapterNote=new NoteAdapter1(context, NotesArrayList);
					listView.setAdapter(adapterNote);
						for (int i = 0; i < NotesArrayList.size(); i++) {

						System.out.println("Id   "+NotesArrayList.get(i).getNote_id());
						System.out.println("Content   "+NotesArrayList.get(i).getNote_content());
						System.out.println("Info   "+NotesArrayList.get(i).getNote_info());
						System.out.println("Place   "+NotesArrayList.get(i).getNote_place());
						System.out.println("PlaceId   "+NotesArrayList.get(i).getNote_palceId());
					}			

					database.close();
					layout_notes.setVisibility(View.VISIBLE);
					layout_info.setVisibility(View.INVISIBLE);
					layout_speaker.setVisibility(View.INVISIBLE);
					layout_notesList.setVisibility(View.INVISIBLE);
				}
			});*/
			/*listView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int arg2, long arg3) {
					// TODO Auto-generated method stub
					edittext.setText(NotesArrayList.get(arg2).getNote_content());
					note_id=Integer.parseInt(NotesArrayList.get(arg2).getNote_id());

					layout_notes.setVisibility(View.VISIBLE);
					layout_info.setVisibility(View.INVISIBLE);
					layout_speaker.setVisibility(View.INVISIBLE);
					layout_notesList.setVisibility(View.INVISIBLE);

				}
			});*/
			// TextView spkr_description = (TextView) rootView
			// .findViewById(R.id.txt_speaker_description);


			//Mail Sendimg Code
			imgMessage.setOnClickListener(new OnClickListener() {

				@SuppressLint("InlinedApi")
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					String to=null;
					Intent email = new Intent(Intent.ACTION_SEND);
					email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
					//email.putExtra(Intent.EXTRA_CC, new String[]{ to});
					//email.putExtra(Intent.EXTRA_BCC, new String[]{to});
					email.putExtra(Intent.EXTRA_SUBJECT , program_name.getText().toString());
					email.putExtra(Intent.EXTRA_TEXT , program_venue.getText().toString()
							+"\n"+"https://play.google.com/store/apps/details?id=com.blackbean.eventbuoy2.sankalp");

					//Convert drawable to Uri
					/*Resources resources = context.getResources();
					int resId=R.drawable.icon_share;*/

					email .putExtra(Intent.EXTRA_STREAM, Uri.parse(Environment
							.getExternalStorageDirectory().getPath()
							+ "/sankalp/speakers/" + programimage.get(id)));
					//need this to prompts email client only
					email.setType("message/rfc822");

					startActivity(Intent.createChooser(email, "Choose an Email client :"));
				}
			});


			//Facebook sharing code

			imgFacebook.setOnClickListener(new OnClickListener() {

				@SuppressWarnings("deprecation")
				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Bundle parameters = new Bundle();
					parameters.putString("description",program_venue.getText().toString());
					//parameters.putString("picture","link of picture your want add with share post.");
					parameters.putString("link","https://play.google.com/store/apps/details?id=com.blackbean.eventbuoy2.sankalp");
					parameters.putString("name", program_name.getText().toString());
					parameters.putString("caption", "Sankalp");

					Facebook mFacebook=new Facebook("1375486829431505");

					System.out.println("desc  "+parameters.get("description"));
					System.out.println("link  "+parameters.get("link"));
					System.out.println("name  "+parameters.get("name"));
					System.out.println("capt  "+parameters.get("caption"));


					mFacebook.dialog((Activity)context, "feed", parameters, new DialogListener() {

						@Override
						public void onFacebookError(FacebookError e) {
							// TODO Auto-generated method stub
							Toast.makeText(context, "Error While Sharing ", Toast.LENGTH_SHORT).show();
						}

						@Override
						public void onError(DialogError e) {
							// TODO Auto-generated method stub
							Toast.makeText(context, "Error While Sharing ", Toast.LENGTH_SHORT).show();
						}

						@Override
						public void onComplete(Bundle values) {
							// TODO Auto-generated method stub
							Toast.makeText(context, "Shared Sucessfully", Toast.LENGTH_SHORT).show();
						}

						@Override
						public void onCancel() {
							// TODO Auto-generated method stub
							Toast.makeText(context, "Sharing Canceled By User", Toast.LENGTH_SHORT).show();
						}
					});
				}
			});

			imgTwitter.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					Intent intent=new Intent(context, TwitterShare.class);
					String shareText=program_name.getText().toString()+"\n"+
							program_venue.getText().toString()+"\n" +
							"https://play.google.com/store/apps/details?id=com.blackbean.eventbuoy2.sankalp";
					intent.putExtra("ShareText", shareText);
					intent.putExtra("ImagePath", Environment.getExternalStorageDirectory().getPath()
							+ "/sankalp/speakers/" + programimage.get(id));

					startActivity(intent);
				}
			});



			return rootView;
		}


		class ShowAgenda extends BaseAdapter
		{
			Context context;
			ArrayList<String> agenda;
			ArrayList<String> agendaID;
			public ShowAgenda(Context context,ArrayList<String> agenda,ArrayList<String> agendaID) {
				// TODO Auto-generated constructor stub
				this.context=context;
				this.agenda=agenda;
				this.agendaID=agendaID;
			}

			@Override
			public int getCount() {
				// TODO Auto-generated method stub
				return agenda.size();
			}

			@Override
			public Object getItem(int arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public long getItemId(int arg0) {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public View getView(final int position, View arg1, ViewGroup arg2) {
				// TODO Auto-generated method stub
				LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View view=inflater.inflate(R.layout.single_text_adapter, null);

				TextView text=(TextView)view.findViewById(R.id.text1);
				text.setText(agenda.get(position));

				text.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View argp) {
						// TODO Auto-generated method stub

						Intent intent=new Intent(context, AgendaDetailsActivity.class);
						intent.putExtra("agenda_id",agendaID.get(position));
						startActivity(intent);

					}
				});

				return view;
			}

		}

	}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

}