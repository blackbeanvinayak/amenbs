package com.blackbean.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;

public class AboutAdapter extends ArrayAdapter<String> {
	Context context;
	ArrayList<String> arrayList;
	ArrayList<Integer> images;

	public AboutAdapter(Context context, int resource,
			ArrayList<String> arrayList) {
		super(context, resource, R.layout.adapter_home, arrayList);
		// TODO Auto-generated constructor stub
		this.context = context;
		this.arrayList = arrayList;
		images = new ArrayList<Integer>();
		images.add(R.drawable.sankalp1);

		images.add(R.drawable.bb);
		//images.add(R.drawable.help_blue);
		images.add(R.drawable.share);

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.aboutus_adapter, parent, false);
		ImageView imageView = (ImageView) view.findViewById(R.id.home_img);
		TextView txt = (TextView) view.findViewById(R.id.home_txt);
		imageView.setImageResource(images.get(position));
		txt.setText(this.arrayList.get(position));
		return view;
	}

}
