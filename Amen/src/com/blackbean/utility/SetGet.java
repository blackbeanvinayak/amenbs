package com.blackbean.utility;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;


public class SetGet {
	public static ArrayList<Bitmap> SponsersImageBitmap = new ArrayList<Bitmap>() ;
	public static ArrayList<String> SpeakerImageURL=new ArrayList<String>();
	public static ArrayList<String> SponsorImageURL=new ArrayList<String>();
	public static ArrayList<String> sponsorNameaArrayList=new ArrayList<String>();
	public static ArrayList<String> sponsorTypeArrayList=new ArrayList<String>();
	public static ArrayList<String> JuryImageUrl=new ArrayList<String>();
	public static ArrayList<String> MenuImageURL=new ArrayList<String>();
	
	public static ArrayList<Integer> agendaId_MySchedule=new ArrayList<Integer>();
	
	//public static ArrayList<String> MenuImageBlackURL=new ArrayList<String>();

	
	/*
	 * Variables for Automatic update only once
	 */
	
	public static int EVENT_COUNT;
	public static int SPEAKER_COUNT;
	public static int PARTNER_COUNT;
	public static int JURY_COUNT;
	public static int MENULIST_COUNT;
	
	
	static String mypref = "mypref";


	public static String confirmation;


	/*public static ArrayList<String> getMenuImageBlackURL() {
		return MenuImageBlackURL;
	}*/
	public static ArrayList<String> getSpeakerImageURL() {
		return SpeakerImageURL;
	}
	public static ArrayList<String> getJuryImageURL() {
		return JuryImageUrl;
	}
	public static void setJuryImageURL(ArrayList<String> juryimage) {
		JuryImageUrl=juryimage;
	}

	public static void setSpeakerImageURL(ArrayList<String> speakerImageURL) {
		SpeakerImageURL = speakerImageURL;
	}
	public static ArrayList<String> getSponsorImageURL() {
		return SponsorImageURL;
	}
	public static void setSponsorImageURL(ArrayList<String> sponsorImageURL) {
		SponsorImageURL = sponsorImageURL;
	}
	public static ArrayList<String> getSponsorNameaArrayList() {
		return sponsorNameaArrayList;
	}
	public static void setSponsorNameaArrayList(
			ArrayList<String> sponsorNameaArrayList) {
		SetGet.sponsorNameaArrayList = sponsorNameaArrayList;
	}
	public static ArrayList<String> getSponsorTypeArrayList() {
		return sponsorTypeArrayList;
	}
	public static void setSponsorTypeArrayList(
			ArrayList<String> sponsorTypeArrayList) {
		SetGet.sponsorTypeArrayList = sponsorTypeArrayList;
	}
	public static String getConfirmation() {
		return confirmation;
	}
	public static void setConfirmation(String confirmation) {
		SetGet.confirmation = confirmation;
	}
	/*public static void setMenuImageBlackURL(ArrayList<String> menuImageBlackURL) {
		MenuImageBlackURL = menuImageBlackURL;
	}*/
	public static void setMenuImageURL(ArrayList<String> menuImageURL) {
		MenuImageURL = menuImageURL;
	}
	public static ArrayList<String> getMenuImageURL() {
		return MenuImageURL;
	}


	public static void storeUsingSharedPreference(Context context, String key,
			String value) {
		SharedPreferences.Editor editor = context.getSharedPreferences(mypref,
				Context.MODE_PRIVATE).edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static String readUsingSharedPreference(Context context, String key) {
		SharedPreferences preference = context.getSharedPreferences(mypref,
				Context.MODE_PRIVATE);
		String value = preference.getString(key, "value");
		return value;
	}

	public static void cleareUsingSharedPreference(Context context) {
		SharedPreferences.Editor editor = context.getSharedPreferences(mypref,
				Context.MODE_PRIVATE).edit();

		editor.clear();
		editor.commit();

	}


}
