package com.blackbean.eventbuoy2.amen;



import java.util.ArrayList;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;

import com.blackbean.adapters.BMDialogAdapter;
import com.blackbean.adapters.BoardMemberAdapter;
import com.blackbean.eventbuoy2.amen.R;

public class BoardMemberActivity extends Activity{
ListView listView;
BoardMemberAdapter boardmemberadapter;
ArrayList<String> conference_patrons_name,conference_patrons_designation;
ArrayList<String> conference_chair_name,conference_chair_designation;
ArrayList<String> cAdvisory_board_name,cAdvisory_board_designation;
ArrayList<String> co_chair_name,co_chair_designation;
ArrayList<String> Organising_name,Organising_designation;
ArrayList<String> Student_name,Student_designation;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_boardmember);
		listView=(ListView)findViewById(R.id.listview_bordmember);
		ArrayList<String> memberlist=new ArrayList<String>();
		memberlist.add("Conference Patrons");
		memberlist.add("Conference Chair");
		memberlist.add("Advisory Board");
		memberlist.add("Co-chair and Organising...");
		memberlist.add("Organising Committee Members");
		memberlist.add("Student Committee");
		
		conference_patrons_name=new ArrayList<String>();
		conference_patrons_designation=new ArrayList<String>();
		conference_chair_name=new ArrayList<String>();
		conference_chair_designation=new ArrayList<String>();
		cAdvisory_board_name=new ArrayList<String>();
		cAdvisory_board_designation=new ArrayList<String>();
		co_chair_name=new ArrayList<String>();
		co_chair_designation=new ArrayList<String>();
		Organising_name=new ArrayList<String>();
		Organising_designation=new ArrayList<String>();
		Student_name=new ArrayList<String>();
		Student_designation=new ArrayList<String>();
		
		conference_patrons_name.add("Dr.G.Viswanathan");
		conference_patrons_designation.add("Chancellor,VIT University");
		conference_patrons_name.add("Mr.Sankar Viswanathan");
		conference_patrons_designation.add("Vice-President,VITChennai");
		conference_patrons_name.add("Mr. Sekar Viswanathan");
		conference_patrons_designation.add("Vice-President, University Affairs, VIT");
		conference_patrons_name.add("Mr.G.V.Selvam");
		conference_patrons_designation.add("Vice President,Administration,VIT University");
		conference_patrons_name.add("Ms. Kadhambari S. Viswanathan");
		conference_patrons_designation.add("Assistant Vice President (ChennaiCampus)");
		
		conference_chair_name.add("Prof.M.J.Xavier");
		conference_chair_designation.add("Executive Director,VIT University");
		conference_chair_name.add("Mr.Vijay Michihito Batra");
		conference_chair_designation.add("Founder Think MediaInc.");
		
		cAdvisory_board_name.add("Prof.V.Raju");
		cAdvisory_board_designation.add("Vice Chancellor,VITUniversity");
		cAdvisory_board_name.add("Prof. Anand Samuel");
		cAdvisory_board_designation.add("ProVice-Chancellor, ChennaiCampus");
		cAdvisory_board_name.add("Prof.S.Narayanan,");
		cAdvisory_board_designation.add("Pro-Vice Chancellor, VIT University, Vellore");
		
		
		
		co_chair_name.add("Prof.J.ReevesWesley,");
		co_chair_designation.add("Professor, VITBusinessSchool, VITChennai");
		
		Organising_name.add("Prof.Saju");
		Organising_name.add("Prof.Krishna Kumar");
		Organising_name.add("Prof.Joseph JeyaAnand");
		Organising_name.add("Sandeep Simon Behera");
		Organising_name.add("Ekhlaque Ansari");
		
		Organising_designation.add("");
		Organising_designation.add("");
		Organising_designation.add("");
		Organising_designation.add("");
		Organising_designation.add("");
		
		Student_name.add("Joans George - IIMBA");
		Student_name.add("Shaik Nagur Sharif - IIMBA");
		Student_name.add("Geetha - IIMBA");
		Student_name.add("Balaji - IIMBA");
		Student_name.add("Bharanidhar.S - IIMBA");
		Student_name.add("Chairman Raja - IIMBA");
		Student_name.add("Jennifer Chris Ramya - IIMBA");
		Student_name.add("Vasanth - IIMBA");
		Student_name.add("Mohammad Zahin - Int.MBA");
		Student_name.add("Soumya Elizabeth - Int.MBA");
		Student_name.add("Gayathri Nair - Int.MBA");
		
		Student_designation.add("");
		Student_designation.add("");
		Student_designation.add("");
		Student_designation.add("");
		Student_designation.add("");
		Student_designation.add("");
		Student_designation.add("");
		Student_designation.add("");
		Student_designation.add("");
		Student_designation.add("");
		Student_designation.add("");	
		
		boardmemberadapter=new BoardMemberAdapter(this, 0, memberlist);
		listView.setAdapter(boardmemberadapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				BMDialogAdapter bmdialogadapter;
				
				ArrayList<String> name=new ArrayList<String>();
				ArrayList<String> designation=new ArrayList<String>();
				
				if(position==0)	{ name=conference_patrons_name; designation=conference_patrons_designation;}
				if(position==1)	{name=conference_chair_name; designation=conference_chair_designation;}
				if(position==2)	{name=cAdvisory_board_name; designation=cAdvisory_board_designation;}
				if(position==3)	{name=co_chair_name; designation=co_chair_designation;}
				if(position==4)	{name=Organising_name; designation=Organising_designation;}
				if(position==5)	{name=Student_name; designation=Student_designation;}
				Dialog dialog=new Dialog(BoardMemberActivity.this);
				
				dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
				dialog.setContentView(R.layout.dialog_bm);
				ListView listView=(ListView)dialog.findViewById(R.id.listview_dialog_bm);
				bmdialogadapter=new BMDialogAdapter(BoardMemberActivity.this, 0, name, designation);
			listView.setAdapter(bmdialogadapter);
				
				dialog.show();
			}
		});
		ImageView menu=(ImageView)findViewById(R.id.logo);
		menu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(BoardMemberActivity.this, HomeActivity.class);
				startActivity(intent);
				
			}
		});
		
	}

}
