package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;

import com.blackbean.adapters.AboutAdapter;
import com.blackbean.adapters.HomeAdapter;
import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;
import com.google.analytics.tracking.android.EasyTracker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class SocialMenuActivity extends Activity{

	ListView listView;
	ArrayList<String> arrayList = new ArrayList<String>();

	String urlFb,urlTw,urlLink,urlGoogle,urlYoutube;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);

		ImageView icon=(ImageView) findViewById(R.id.icon);
		TextView text=(TextView) findViewById(R.id.header_txt);

		icon.setImageResource(R.drawable.social_san);
		text.setText("Social");

		urlFb= getIntent().getStringExtra("urlFb");
		urlTw= getIntent().getStringExtra("urlTw");
		urlLink= getIntent().getStringExtra("urlLink");
		urlGoogle = "https://plus.google.com/u/0/106816524399468381927/";
		urlYoutube = "https://www.youtube.com/user/sankalpforumlive";

		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(SocialMenuActivity.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();

			}
		});

		listView = (ListView) findViewById(R.id.aboutlistview);
		arrayList = new ArrayList<String>();
		arrayList.add("Facebook");
		arrayList.add("Twitter");
		arrayList.add("LinkedIn");
		if (HomeActivity.whichEvent == StaticData.Event_Delhi) {
			arrayList.add("Google+");
			arrayList.add("Youtube");
		}

		ArrayList<Integer>pageicon=new ArrayList<Integer>();
		pageicon.add(R.drawable.facebook);
		pageicon.add(R.drawable.twitter);
		pageicon.add(R.drawable.linkedin);
		if (HomeActivity.whichEvent == StaticData.Event_Delhi) {
			pageicon.add(R.drawable.google);
			pageicon.add(R.drawable.youtube);	
		}


		//listView.setAdapter(new AboutAdapter(this, 0, arrayList));

		listView.setAdapter(new HomeAdapter(this, 0, arrayList, pageicon));

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SocialMenuActivity.this,Connect_Social.class);
				if (arg2 == 0) {
					i.putExtra("url",urlFb);
					i.putExtra("social", "Facebook");	
				}
				if (arg2 == 1) {
					i.putExtra("url",urlTw);
					i.putExtra("social", "Twitter");	
				}
				if (arg2 == 2) {
					i.putExtra("url",urlLink);
					i.putExtra("social", "LinkedIn");	
				}
				if (arg2 == 3) {
					i.putExtra("url",urlGoogle);
					i.putExtra("social", "Google+");	
				}
				if (arg2 == 4) {
					i.putExtra("url",urlYoutube);
					i.putExtra("social", "Youtube");	
				}


				startActivity(i);
			}
		});

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}
}
