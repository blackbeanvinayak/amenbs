package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.blackbean.adapters.AboutAdapter;
import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;
import com.google.analytics.tracking.android.EasyTracker;

public class AboutActivity extends Activity {
	ListView listView;
	ArrayList<String> arrayList = new ArrayList<String>();
	int whichEvent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
		listView = (ListView) findViewById(R.id.aboutlistview);
		arrayList = new ArrayList<String>();

		whichEvent=getIntent().getIntExtra("WHICH_EVENT", 1);

		if(whichEvent == StaticData.Event_Nairobi)
		{
			arrayList.add("About Africa Summit 2015");
		}else if (whichEvent == StaticData.Event_Delhi) {
			arrayList.add("About Global Sankalp Summit 2015");
		}


		arrayList.add("About Blackbean");
		//arrayList.add("Help");


		//arrayList.add("SHARE");
		arrayList.add("Share");

		listView.setAdapter(new AboutAdapter(this, 0, arrayList));

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				if (arg2 == 0) {
					/*
					Intent intent = new Intent(AboutActivity.this,
							AboutTHIActivity.class);
					startActivity(intent);*/

					String url = "";
					if(whichEvent == StaticData.Event_Nairobi)
					{
						url="http://www.sankalpforum.com/summit/sankalp-africa-summit-2015/";
					}else if (whichEvent == StaticData.Event_Delhi) {
						url=" http://www.sankalpforum.com/summit/global-summit-2015/";
					}
					Intent i = new Intent(AboutActivity.this,Connect_Social.class);
					i.putExtra("url",url );
					i.putExtra("social", "About Sankalp");
					startActivity(i);


				}
				if (arg2 == 1)

				{
					String url = "http://www.myblackbean.com/";
					Intent i = new Intent(AboutActivity.this,Connect_Social.class);
					i.putExtra("url",url );
					i.putExtra("social", "Blackbean");
					startActivity(i);
				}

				if (arg2 == 2) {
					Intent intent = new Intent(AboutActivity.this,
							ShareActivity.class);
					startActivity(intent);

				}

			}
		});

		/**
		 * Menu click event
		 */
		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AboutActivity.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();

			}
		});
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

}
