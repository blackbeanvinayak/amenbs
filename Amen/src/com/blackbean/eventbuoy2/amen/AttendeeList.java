package com.blackbean.eventbuoy2.amen;

import com.blackbean.eventbuoy2.amen.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

public class AttendeeList extends Activity{
	
	WebView web;
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.connect_social);
		
		TextView text = (TextView) findViewById(R.id.header_txt);
		text.setText("Attendee List");
		
		web=(WebView) findViewById(R.id.web);
		
		web=(WebView)findViewById(R.id.web);
		web.getSettings().setJavaScriptEnabled(true);
		web.loadUrl("https://docs.google.com/a/myblackbean.com/spreadsheets/d/1xWg6afIl1SxHOJpIu4Ma3695E5Y7HP05j88W_TDI7lA/edit#gid=1525865416");
		
		ImageView menu = (ImageView) findViewById(R.id.home);
		menu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(AttendeeList.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();
			}
		});
	}
}
