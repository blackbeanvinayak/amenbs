package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.setget.StaticData;
import com.blackbean.utility.DataBase;
import com.blackbean.utility.ImageLoader;
import com.google.analytics.tracking.android.EasyTracker;

public class Jury_Gridview extends Activity {
	ArrayList<String> Screen_id;
	ArrayList<String> Screen_name;
	ArrayList<String> Screen_designation;
	ArrayList<String> Screen_description;
	ArrayList<String> Screen_juryimage;
	// ArrayList<String> Screen_jurytype;

	ArrayList<String> final_id;
	ArrayList<String> final_name;
	ArrayList<String> final_designation;
	ArrayList<String> final_description;
	ArrayList<String> final_juryimage;
	// ArrayList<String> final_jurytype;

	GridView gridview;
	DataBase database;
	Button jury_final, jury_screen;
	ImageView logoImageView, juriicon;
	TextView header_txt;

	Button buttonInfo,buttonFinalist,buttonJury;

	private static final String Speaker_Image = "speaker_image";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.jury_gridview);

		juriicon = (ImageView) findViewById(R.id.agenda_speaker_icon);
		// juriicon.setImageResource(R.drawable.jury_icon);

		jury_final = (Button) findViewById(R.id.btn_final_jury);
		jury_screen = (Button) findViewById(R.id.btn_screening_jury);

		juriicon.setVisibility(View.INVISIBLE);

		header_txt = (TextView) findViewById(R.id.header_txt);
		header_txt.setText("JURY MEMBERS");

		logoImageView = (ImageView) findViewById(R.id.home);
		logoImageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Jury_Gridview.this,
						MainScreen.class);
				startActivity(intent);
				HomeActivity.homeActivity.finish();
				finish();
			}
		});

		Screen_id = new ArrayList<String>();
		Screen_name = new ArrayList<String>();
		Screen_designation = new ArrayList<String>();
		Screen_description = new ArrayList<String>();
		Screen_juryimage = new ArrayList<String>();
		// Screen_jurytype=new ArrayList<String>();

		final_id = new ArrayList<String>();
		final_name = new ArrayList<String>();
		final_description = new ArrayList<String>();
		final_designation = new ArrayList<String>();
		final_juryimage = new ArrayList<String>();
		// final_jurytype=new ArrayList<String>();

		/*
		 * DataBase db = new DataBase(this); db.open();
		 * id.addAll(db.getJuryByfield("jury_id"));
		 * name.addAll(db.getJuryByfield("jury_name"));
		 * designation.addAll(db.getJuryByfield("jury_info"));
		 * description.addAll(db.getJuryByfield("jury_designation"));
		 * juryimage.addAll(db.getJuryByfield("jury_image"));
		 * jurytype.addAll(db.getJuryByfield("jury_type"));
		 * 
		 * db.close();
		 */

		String event_category;
		if (HomeActivity.whichEvent == StaticData.Event_Nairobi) {
			event_category="Nairobi";
		}else {
			event_category="Delhi";
		}

		DataBase db = new DataBase(this);
		db.open();
		Screen_id.addAll(db.getJuryByfieldAndType("jury_id", "Screening",event_category));
		Screen_name.addAll(db.getJuryByfieldAndType("jury_name", "Screening",event_category));
		Screen_designation.addAll(db.getJuryByfieldAndType("jury_info","Screening",event_category));
		Screen_description.addAll(db.getJuryByfieldAndType("jury_designation","Screening",event_category));
		Screen_juryimage.addAll(db.getJuryByfieldAndType("jury_image","Screening",event_category));
		// Screen_jurytype.addAll(db.getJuryByfield("jury_type","Screening"));

		final_id.addAll(db.getJuryByfieldAndType("jury_id", "Final",event_category));
		final_name.addAll(db.getJuryByfieldAndType("jury_name", "Final",event_category));
		final_designation.addAll(db.getJuryByfieldAndType("jury_info", "Final",event_category));
		final_description.addAll(db.getJuryByfieldAndType("jury_designation","Final",event_category));
		final_juryimage.addAll(db.getJuryByfieldAndType("jury_image", "Final",event_category));
		// final_jurytype.addAll(db.getJuryByfield("jury_type","Final"));

		db.close();

		buttonInfo=(Button) findViewById(R.id.buttonInfo);
		buttonFinalist=(Button) findViewById(R.id.buttonFinalist);

		buttonInfo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Jury_Gridview.this, NewAwardsActivity.class);
				intent.putExtra("val", "info");
				startActivity(intent);
				finish();

			}
		});

		if (HomeActivity.whichEvent == StaticData.Event_Delhi) {
			buttonFinalist.setText("Finalists");
		}

		buttonFinalist.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(Jury_Gridview.this, NewAwardsActivity.class);
				intent.putExtra("val", "finalist");
				startActivity(intent);
				finish();
			}
		});

		/*
		 * id.add("0"); id.add("1"); id.add("2"); id.add("3"); id.add("4");
		 * id.add("5");
		 * 
		 * name.add("Rajat Bhatra"); name.add("Ranjith Ramanujam");
		 * name.add("Samir Patra"); name.add("Rama Brahmam");
		 * name.add("K.V Narendra Prasad"); name.add("Gopinath Ramkrishnan");
		 * 
		 * designation.add("CEO Stenum Asia");
		 * designation.add("Director & Lead Architech");
		 * designation.add("Director Banding & Strategy BrandEx, India");
		 * designation .add(
		 * "Co-Founder Director User Experience Design Think Design Collaborative Pvt. Ltd"
		 * );
		 * designation.add("Chief Operating officer HITEX Exhibition Centre");
		 * designation.add("Business Consultant");
		 * 
		 * description.add(""); description.add(""); description.add("");
		 * description.add(""); description.add(""); description.add("");
		 * 
		 * speakerimage.add(R.drawable.rajat_batra);
		 * speakerimage.add(R.drawable.ranajith_ramanujam);
		 * speakerimage.add(R.drawable.samir_patra);
		 * speakerimage.add(R.drawable.rama_brahmam);
		 * speakerimage.add(R.drawable.k_v_nagendra_prasad);
		 * speakerimage.add(R.drawable.gopinath_ramakrishnan);
		 */

		gridview = (GridView) findViewById(R.id.grid_view);
		final GridJuryAdapter screenadapter = new GridJuryAdapter(
				Jury_Gridview.this, Screen_name, Screen_designation,
				Screen_description, Screen_juryimage);
		final GridJuryAdapter finaladapter = new GridJuryAdapter(
				Jury_Gridview.this, final_name, final_designation,
				final_description, final_juryimage);

		gridview.setAdapter(finaladapter);

		gridview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id1) {
				// TODO Auto-generated method stub
				if (gridview.getAdapter().equals(screenadapter)) {
					Intent intent = new Intent(Jury_Gridview.this,
							JuryDetailsActivity.class);
					intent.putExtra("_id",
							Integer.parseInt(Screen_id.get(position)));

					startActivity(intent);
				} else {
					Intent intent = new Intent(Jury_Gridview.this,
							JuryDetailsActivity.class);
					intent.putExtra("_id",
							Integer.parseInt(final_id.get(position)));

					startActivity(intent);
				}

			}
		});

		jury_final.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				arg0.setBackgroundResource(R.drawable.color_cover_left);
				jury_screen.setBackgroundResource(android.R.color.transparent);
				gridview.setAdapter(finaladapter);

			}
		});

		jury_screen.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				arg0.setBackgroundResource(R.drawable.color_cover_right);
				jury_final.setBackgroundResource(android.R.color.transparent);
				gridview.setAdapter(screenadapter);

			}
		});


	}

	class GridJuryAdapter extends BaseAdapter {
		Context context;
		ArrayList<String> speaker_id;
		ArrayList<String> name;
		ArrayList<String> designation;
		ArrayList<String> description;
		ArrayList<String> speaker_image;
		ImageLoader imageLoader;

		public GridJuryAdapter(Context context, ArrayList<String> name,
				ArrayList<String> designation, ArrayList<String> description,
				ArrayList<String> speaker_image) {
			// TODO Auto-generated constructor stub
			this.context = context;
			this.name = name;
			this.description = designation;
			this.designation = designation;
			this.speaker_id = speaker_id;
			this.speaker_image = speaker_image;
			imageLoader = new ImageLoader(context);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return name.size();
		}

		@Override
		public Object getItem(int position) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
			View v = inflater.inflate(R.layout.speaker_single_item, parent,
					false);

			ImageView image = (ImageView) v.findViewById(R.id.speaker_image);

			TextView name = (TextView) v.findViewById(R.id.speaker_name);

			// Typeface mFont = Typeface.createFromAsset(getAssets(),
			// "nexa_light.otf");
			name.setText(this.name.get(position));
			// name.setTypeface(mFont);
			TextView detail1 = (TextView) v.findViewById(R.id.speaker_detail1);
			detail1.setText(designation.get(position));
			// detail1.setTypeface(mFont);

			// image.setImageResource(speaker_image.get(position));
			/*File pictureFile = new File(Environment
					.getExternalStorageDirectory().getPath()
					+ "/sankalp/jury/"
					+ speaker_image.get(position));
			if (pictureFile != null) {
				Bitmap bitmap = BitmapFactory.decodeFile(pictureFile
						.getAbsolutePath());
				image.setImageBitmap(bitmap);
			}*/

			imageLoader.DisplayImage("http://sankalp.eventbuoy.com/admin/uploads/jury/"
					+ speaker_image.get(position).replaceAll(" ", "%20"), image);

			return v;
		}

	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this);
	}

}
