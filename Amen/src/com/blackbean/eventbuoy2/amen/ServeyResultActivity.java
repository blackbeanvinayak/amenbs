package com.blackbean.eventbuoy2.amen;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.utility.JSONParser;
import com.blackbean.utility.NetworkCheck;

public class ServeyResultActivity extends Activity {
	// RadioGroup linear;
	Button submit;
	RadioGroup group;
	TextView question;
	ListView listview;

	// int id=-1;
	public static List<String> survey_vote_count, survey_option_title;
	ArrayList<Integer> buttonID;
	int selected=-1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.survey_result);
		int _id = getIntent().getExtras().getInt("_id");
		int _ans = getIntent().getExtras().getInt("ans");


		listview=(ListView)findViewById(R.id.result);
		question=(TextView)findViewById(R.id.question);

		question.setText(getIntent().getExtras().getString("question"));
		question.setTextSize(25);



		// linear = (LinearLayout) findViewById(R.id.list_option);
		if (NetworkCheck.getConnectivityStatusString(ServeyResultActivity.this)) {
			GetOptions option = new GetOptions(this, _id,_ans);
			option.execute();
		}else {
			Toast.makeText(ServeyResultActivity.this, "Internet is needed", Toast.LENGTH_LONG).show();
		}

	}

	class GetOptions extends AsyncTask<Void, Void, Void> {
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = null;
		Context context;
		JSONArray response = new JSONArray();
		int id,ans;
		Dialog dialog;

		public GetOptions(Context context, int id,int ans) {
			// TODO Auto-generated constructor stub
			this.context = context;
			this.id = id;
			this.ans=ans;
			survey_option_title = new ArrayList<String>();
			survey_vote_count = new ArrayList<String>();
			buttonID=new ArrayList<Integer>();

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			dialog = new Dialog(context);
			ProgressBar bar = new ProgressBar(context);
			dialog.setContentView(bar);
			dialog.setTitle("Please Wait...");
			dialog.show();

		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			//http://listoftablets.com/SHRM/admin/GetSurveyVoteByQ_IDAndOpt_ID.php?survey_question_id=1&option_id=2

			try {

				jsonObject = jsonParser
						.getJSONFromUrl("http://listoftablets.com/SHRM/admin/GetSurveyVoteByQ_IDAndOpt_ID.php?survey_question_id="+id+"&option_id="
								+ans);

				// String
				// story_id,story_type,story_person_name,story_person_location,story_person_image,story_description,story_video;

				if (jsonObject != null)

				{
					response = jsonObject.getJSONArray("survey_votes_details");
					for (int i = 0; i < response.length(); i++) {
						JSONObject agenda_temp = new JSONObject();
						try {
							agenda_temp = response.getJSONObject(i);

							survey_vote_count.add(agenda_temp
									.getString("survey_vote_count"));
							survey_option_title.add(agenda_temp
									.getString("survey_option_title"));

						} catch (JSONException e) {
							System.out.println("Json Exception :" + e);
						}
					}
				} else {
					System.out.println("sorry story");
				}

			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e);
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			dialog.dismiss();

			listview.setAdapter(new adapter(ServeyResultActivity.this,survey_vote_count ));

		}

	}


	class adapter extends ArrayAdapter<String>
	{
		Context context;

		public adapter(Context context,
				List<String> objects) {
			super(context,0, objects);
			// TODO Auto-generated constructor stub
			this.context=context;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			LayoutInflater inflator=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View view=inflator.inflate(R.layout.single_text,parent, false);

			TextView tv=(TextView)view.findViewById(R.id.resultText);
			tv.setText(Html.fromHtml("<b>"+Math.round(Float.parseFloat(survey_vote_count.get(position)))+"% </b>  -"+survey_option_title.get(position)));
			tv.setTextColor(Color.WHITE);


			return view;
		}
	}

}
