package com.blackbean.eventbuoy2.amen;
/*package com.blackbean.eventbuoy2.shrm;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.blackbean.adapters.ExpandableListSpecialAdapter;


public class AwardInformationSpecialActivity extends Activity
{
	TextView award_category;
	ImageView image;
	ExpandableListSpecialAdapter listAdapter;
	ExpandableListView expandable;
	static List<String> listDataHeader;
	static HashMap<String, List<String>> listDataChild;
	static HashMap<String, List<String>> listDataChildsocial;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.award_informtion);
		image=(ImageView)findViewById(R.id.image);
		
		award_category=(TextView)findViewById(R.id.category);
		award_category.setText(getIntent().getExtras().getString("name"));	
	
		if(getIntent().getExtras().getInt("image_id")==0){
			image.setImageResource(R.drawable.digital);
		}
		if(getIntent().getExtras().getInt("image_id")==1){
			image.setImageResource(R.drawable.website);
		}
		if(getIntent().getExtras().getInt("image_id")==2){
			image.setImageResource(R.drawable.mobile);
		}
		if(getIntent().getExtras().getInt("image_id")==3){
			image.setImageResource(R.drawable.special);
		}
		
		
		 expandable=(ExpandableListView)findViewById(R.id.explist);
		 listDataChild = new HashMap<String, List<String>>();
        listDataChildsocial = new HashMap<String, List<String>>();
        listDataHeader=new ArrayList<String>();
        listDataHeader.add("Outstanding Digital Person of the year");
        listDataHeader.add("Outstanding Digital Start-up of the year");
 
        listDataHeader.add("Outstanding Digital Agency of the Year");
        listDataHeader.add("Outstanding Social Media Campaign of the Year");

        // Adding child data
        List<String> mam = new ArrayList<String>();
        mam.add("Apex individual title, looks forward to awarding a person who has made an outstanding contribution for the promotion of Internet and/or mobile Services in India through endeavours or works of his Company, Government Department or NGO in the form of any executed project or application that rendered significant developments in the digital domain in terms of reach, accessibility, convenience and acceptance with proven results.");
        List<String> cms = new ArrayList<String>();
        cms.add("To award a company pertaining to the arena of Internet or Mobile that was started or incorporated on or after 1st January, 2013 and has demonstrated commendable growth in its o operations at a brisk pace and promises to make it big in the industry in near future.");
        List<String> lma = new ArrayList<String>();
       lma.add("To award an Agency which scores more than the rest of its competition based on the average score (in percentage), quality of entries and number of entries (Significant work done) across all categories mentioned above. ");
       
       List<String> lma1 = new ArrayList<String>();
       lma1.add("Creative and innovative activity performed using online social media viz. websites,applications, APIs, plug-ins, etc. in a marketing campaign.");
     
       
       List<String> sociallistsocial = new ArrayList<String>();
       sociallistsocial.add("social");
 
        listDataChild.put(listDataHeader.get(0), mam); // Header, Child
        listDataChild.put(listDataHeader.get(1), cms);
        listDataChild.put(listDataHeader.get(2), lma);
        listDataChild.put(listDataHeader.get(3), lma1);
  
        listAdapter = new ExpandableListSpecialAdapter(AwardInformationSpecialActivity.this,
                        listDataHeader, listDataChild);

        // setting list adapter
        expandable.setAdapter(listAdapter);

        ImageView menu=(ImageView)findViewById(R.id.logo);
		menu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(AwardInformationSpecialActivity.this, HomeActivity.class);
				startActivity(intent);
				
			}
		});	
	}

}
*/