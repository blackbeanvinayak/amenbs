package com.blackbean.eventbuoy2.amen;

import java.io.File;
import java.util.ArrayList;

import com.blackbean.adapters.NoteAdapter;
import com.blackbean.adapters.NoteModel;
import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.utility.DataBase;
import com.blackbean.utility.SetGet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class NoteActivity extends Activity{

	ListView listView;
	TextView header_txt;
	DataBase db ;

	/*ArrayList<String>list_content;
	ArrayList<String>list_info;*/

	ArrayList<NoteModel>rowItems;
	NoteAdapter adapter;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note);

		listView=(ListView) findViewById(R.id.listView_note);
		header_txt=(TextView) findViewById(R.id.header_txt);
		/*list_content=new ArrayList<String>();
		list_info=new ArrayList<String>();*/
		rowItems=new ArrayList<NoteModel>();

		// Creating database
		db = new DataBase(this);
		db.open();



		// right Image Setting code
		//int position=getIntent().getIntExtra("POSITION", 0);
		ImageView icon=(ImageView) findViewById(R.id.icon);
		String imageBlack=getIntent().getStringExtra("ImageName");
		File pictureFile = new File(Environment
				.getExternalStorageDirectory().getPath()
				+ "/shrm/menu/" + imageBlack);
		if (pictureFile != null) {
			Bitmap bitmap = BitmapFactory.decodeFile(pictureFile
					.getAbsolutePath());
			if(bitmap!=null)
				icon.setImageBitmap(bitmap);
		}
		//icon.setColorFilter(Color.argb(255, 0, 0, 0));
		
		//Home Button Functionality
		ImageView menu = (ImageView) findViewById(R.id.home);

		menu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(NoteActivity.this,
						HomeActivity.class);
				startActivity(intent);
				finish();
			}
		});


		//Get value from Intent and assign it to Header
		String header=getIntent().getStringExtra("header");
		header_txt.setText(header);


		// Test code
		/*for (int i = 0; i < 4; i++) {
			long a=db.addNote1("Vinayak "+i, "AGENDA", i, "NNNN "+ i);	
			System.out.println("Value inserted "+i+" = "+a);
		}*/


		/*
		 * Getting Notes Content From database
		 */
		Cursor c=db.getAllNotes();



		if(c!=null)
		{
			/*	while (c.moveToNext()) {
				list_content.add(c.getString(c.getColumnIndex("note_content1")));
				list_info.add(c.getString(c.getColumnIndex("note_info1")));
			}*/
			if (c.moveToFirst()) {
				do {
					/*list_content.add(c.getString(c.getColumnIndex("note_content1")));
					list_info.add(c.getString(c.getColumnIndex("note_info1")));*/
					rowItems.add(new NoteModel(String.valueOf(c.getInt(0))
							,c.getString(1), c.getString(2), 
							String.valueOf(c.getInt(4)), c.getString(3)));		
					System.out.println("Content  "+c.getString(c.getColumnIndex("note_content1")));
					System.out.println("Info  "+c.getString(c.getColumnIndex("note_info1")));
				} while (c.moveToNext());
			}

		}
		adapter=new NoteAdapter(NoteActivity.this, rowItems);
		listView.setAdapter(adapter);

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				Intent intent=new Intent(NoteActivity.this, MyNotes.class);
				intent.putExtra("NoteID", rowItems.get(arg2).getNote_id());
				intent.putExtra("NotePlaceID", rowItems.get(arg2).getNote_palceId());
				intent.putExtra("NotePlace", rowItems.get(arg2).getNote_place());
				intent.putExtra("NoteInfo", rowItems.get(arg2).getNote_info());
				intent.putExtra("NoteContent", rowItems.get(arg2).getNote_content());
				startActivity(intent);
			}
		});

		listView.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					final int arg2, long arg3) {
				// TODO Auto-generated method stub
				final String noteId=rowItems.get(arg2).getNote_id();
				final AlertDialog.Builder dialogAlert=new AlertDialog.Builder(NoteActivity.this);
				//dialog.setTitle("Confirmation");
				dialogAlert.setMessage("Are you want to delete this note");
				dialogAlert.setPositiveButton("Yes", 
						new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						DataBase db=new DataBase(NoteActivity.this);
						db.open();
						db.deleteNoteByNoteId(noteId);
						Toast.makeText(NoteActivity.this, "Note Deleted", Toast.LENGTH_SHORT).show();
						rowItems.remove(arg2);
						adapter.notifyDataSetChanged();
						arg0.dismiss();
					}
				});

				dialogAlert.setNegativeButton("No", 
						new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						arg0.cancel();
					}
				});
				dialogAlert.show();

				return true;
			}
		});

	}
}
