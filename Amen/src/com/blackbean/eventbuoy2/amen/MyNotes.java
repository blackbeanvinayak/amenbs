package com.blackbean.eventbuoy2.amen;

import com.blackbean.eventbuoy2.amen.R;
import com.blackbean.utility.DataBase;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MyNotes extends Activity{
	
	ImageView menu;
	Button btnEditSave,btnMail;
	EditText editTextNote;
	TextView textViewNoteInfo,textViewNoteContent,textViewNoteTaken;
	
	String note_id,note_info,note_content,note_place,note_placeID;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mynotes);
		
		menu=(ImageView) findViewById(R.id.home);
		btnEditSave=(Button) findViewById(R.id.btnEditSave);
		btnMail=(Button) findViewById(R.id.btnMail);
		
		editTextNote=(EditText) findViewById(R.id.editTextNote);
		textViewNoteContent=(TextView) findViewById(R.id.textViewNoteContent);
		textViewNoteInfo=(TextView) findViewById(R.id.textViewNoteInfo);
		textViewNoteTaken=(TextView) findViewById(R.id.textViewNoteTaken);
		
		Intent intent=getIntent();
		note_id=intent.getStringExtra("NoteID");
		note_info=intent.getStringExtra("NoteInfo");
		note_content=intent.getStringExtra("NoteContent");
		note_place=intent.getStringExtra("NotePlace");
		note_placeID=intent.getStringExtra("NotePlaceID");
		
		textViewNoteContent.setText(note_content);
		textViewNoteInfo.setText(note_info);
		
		
		menu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MyNotes.this,
						HomeActivity.class);
				startActivity(intent);
				finish();	
			}
		});
		
		btnEditSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(btnEditSave.getText().toString().equalsIgnoreCase("Edit"))
				{
					textViewNoteContent.setVisibility(View.GONE);
					editTextNote.setVisibility(View.VISIBLE);
					editTextNote.setText(textViewNoteContent.getText().toString());
					btnEditSave.setText("Save");
				}else {
					textViewNoteContent.setVisibility(View.VISIBLE);
					editTextNote.setVisibility(View.INVISIBLE);
					btnEditSave.setText("Edit");
					
					textViewNoteContent.setText(editTextNote.getText().toString().trim());
					
					//Toast.makeText(MyNotes.this, ""+Integer.valueOf(note_id), Toast.LENGTH_LONG).show();
					DataBase database=new DataBase(MyNotes.this);
					database.open();
					
					database.UpdateNote1(Integer.valueOf(note_id), editTextNote.getText().toString().trim(),
							note_place, Integer.valueOf(note_placeID), note_info);
					database.close();
				}
			}
		});
		btnMail.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				/*final Dialog dialog=new Dialog(MyNotes.this);
				dialog.setContentView(R.layout.send_mail);
				dialog.setTitle("Send Email");
				final EditText editTextTo=(EditText) dialog.findViewById(R.id.editTextTo);
				final EditText editTextSubject=(EditText) dialog.findViewById(R.id.editTextSubject);
				final EditText editTextMessage=(EditText) dialog.findViewById(R.id.editTextMessage);
				
				Button buttonSend=(Button) dialog.findViewById(R.id.buttonSend);
				buttonSend.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						 String to = editTextTo.getText().toString();
						  String subject = editTextSubject.getText().toString();
						  String message = editTextMessage.getText().toString();
			 
						  Intent email = new Intent(Intent.ACTION_SEND);
						  email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
						  //email.putExtra(Intent.EXTRA_CC, new String[]{ to});
						  //email.putExtra(Intent.EXTRA_BCC, new String[]{to});
						  email.putExtra(Intent.EXTRA_SUBJECT, subject);
						  email.putExtra(Intent.EXTRA_TEXT, message);
			 
						  //need this to prompts email client only
						  email.setType("message/rfc822");
			 
						  startActivity(Intent.createChooser(email, "Choose an Email client :"));
			 	
					}
				});
				dialog.show();*/
				
				String to=null;
				Intent email = new Intent(Intent.ACTION_SEND);
				  email.putExtra(Intent.EXTRA_EMAIL, new String[]{ to});
				  //email.putExtra(Intent.EXTRA_CC, new String[]{ to});
				  //email.putExtra(Intent.EXTRA_BCC, new String[]{to});
				  email.putExtra(Intent.EXTRA_SUBJECT, textViewNoteTaken.getText().toString()+" - "+
						  textViewNoteInfo.getText().toString());
				  email.putExtra(Intent.EXTRA_TEXT, textViewNoteContent.getText().toString());
	 
				  //need this to prompts email client only
				  email.setType("message/rfc822");
	 
				  startActivity(Intent.createChooser(email, "Choose an Email client :"));
			}
		});
	}
}
