package com.blackbean.eventbuoy2.amen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.blackbean.eventbuoy2.amen.R;
import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.facebook.widget.LoginButton.UserInfoChangedCallback;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

public class LoginActivity extends Activity implements ConnectionCallbacks,OnConnectionFailedListener{
	
	//Facebook Login Variable
	private LoginButton fb_btnLogin;
	private UiLifecycleHelper uiHelper;
	
	//Google+ Login Variable
	private SignInButton g_btnLogin;
	GoogleApiClient mGoogleApiClient;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		
		//Facebook 
		uiHelper=new UiLifecycleHelper(LoginActivity.this, statusCallback);
		uiHelper.onCreate(savedInstanceState);
		
		setContentView(R.layout.login_activity);
		fb_btnLogin=(LoginButton) findViewById(R.id.fb_login_button);
		
		fb_btnLogin.setUserInfoChangedCallback(new UserInfoChangedCallback() {
			
			@Override
			public void onUserInfoFetched(GraphUser user) {
				// TODO Auto-generated method stub
				if (user != null) {
					startActivity(new Intent(LoginActivity.this, HomeActivity.class));
					finish();
				}else {
					//Toast.makeText(LoginActivity.this, "Yor are not logged", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		
		g_btnLogin=(SignInButton) findViewById(R.id.btn_sign_in);
		
		// Initializing google plus api client
		 mGoogleApiClient = new GoogleApiClient.Builder(this)
	        .addConnectionCallbacks(this)
	        .addOnConnectionFailedListener(this)
	        .addApi(Plus.API)
	        .addScope(Plus.SCOPE_PLUS_LOGIN)
	        .build();
	        
		
		g_btnLogin.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				 if (!mGoogleApiClient.isConnecting()) {
				       mGoogleApiClient.connect();
				    }
			}
		});
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		mGoogleApiClient.connect();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		uiHelper.onResume();
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		uiHelper.onPause();
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		uiHelper.onDestroy();
		mGoogleApiClient.disconnect();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}
	
	private Session.StatusCallback statusCallback = new StatusCallback() {
		
		@Override
		public void call(Session session, SessionState state, Exception exception) {
			// TODO Auto-generated method stub
			if (state.isOpened()) {
				//buttonsEnabled(true);
				System.out.println("FacebookSampleActivity " + " Facebook session opened");
			} else if (state.isClosed()) {
				//buttonsEnabled(false);
				System.out.println("FacebookSampleActivity " + " Facebook session closed");
			}
		}
	};

	@Override
	public void onConnected(Bundle arg0) {
		// TODO Auto-generated method stub
		if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
		    Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
		    String personName = currentPerson.getDisplayName();
		    String personPhoto = currentPerson.getImage()+"";
		    String personGooglePlusProfile = currentPerson.getUrl();
		    System.out.println("Google + Info  "+personName +"\n"+personPhoto +"\n"+personGooglePlusProfile);
		  }
	}

	@Override
	public void onConnectionSuspended(int arg0) {
		// TODO Auto-generated method stub
		 mGoogleApiClient.connect();
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		if (!arg0.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(arg0.getErrorCode(), this, 0).show();
			return;
		}
	}
}
